<?php

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Support\Facades\Route;

Route::get('/', 'AuthController@showFormLogin')->name('login');
Route::get('login', 'AuthController@showFormLogin')->name('login');
Route::post('login', 'AuthController@login');

Route::get('cronmutasiinternalunit', 'CronMutasiInternalUnit@calculate');
Route::get('cronpencairangadai', 'CronPencairanGadai@calculate');
Route::get('cronpencairangadaijf', 'CronPencairanGadaiJf@calculate');

Route::group(['middleware' => 'auth'], function () {

    Route::get('home', 'HomeController@index')->name('home');
    Route::get('logout', 'AuthController@logout')->name('logout');

    // // ==== Start Laporan Pendana Register ====
    // Route::get('/laporanlender/pendanaregister',[
    //     'uses' => 'login\PendanaRegister@index',
    //     'as' => 'login.pendanaregister'
    // ]);
    // Route::post('/laporanlender/pendanaregisterpopulate',[
    //     'uses' => 'login\PendanaRegister@populate',
    //     'as' => 'login.pendanaregisterpopulate'
    // ]);
    // Route::post('/laporanlender/pendanaregisterpopulatemodal',[
    //     'uses' => 'login\PendanaRegister@modal',
    //     'as' => 'login.pendanaregistermodal'
    // ]);
    // Route::get('pendanaregisterexcel',[
    //     'uses' => 'login\PendanaRegister@excel',
    //     'as' => 'pendanaregisterexcel'
    // ]);

    // // ==== Start Laporan Pendana Aktif ====
    // Route::get('/laporanlender/pendanaaktif',[
    //     'uses' => 'login\PendanaAktif@index',
    //     'as' => 'login.pendanaaktif'
    // ]);
    // Route::post('/laporanlender/pendanaaktifpopulate',[
    //     'uses' => 'login\PendanaAktif@populate',
    //     'as' => 'login.pendanaaktifpopulate'
    // ]);
    // Route::get('pendanaaktifexcel',[
    //     'uses' => 'login\PendanaAktif@excel',
    //     'as' => 'pendanaaktifexcel'
    // ]);

    // // ==== Start Laporan Pendana Autobid ====
    // Route::get('/laporanlender/pendanaautobid',[
    //     'uses' => 'login\PendanaAutobid@index',
    //     'as' => 'login.pendanaautobid'
    // ]);
    // Route::post('/laporanlender/pendanaautobidpopulate',[
    //     'uses' => 'login\PendanaAutobid@populate',
    //     'as' => 'login.pendanaautobidpopulate'
    // ]);
    // Route::get('pendanaautobidexcel',[
    //     'uses' => 'login\PendanaAutobid@excel',
    //     'as' => 'pendanaautobidexcel'
    // ]);

    // // ==== Start Laporan Agent Referal ====
    // Route::get('/laporanlender/agentreferal',[
    //     'uses' => 'login\AgentReferal@index',
    //     'as' => 'login.agentreferal'
    // ]);
    // Route::post('/laporanlender/agentreferalpopulate',[
    //     'uses' => 'login\AgentReferal@populate',
    //     'as' => 'login.agentreferalpopulate'
    // ]);
    // Route::post('/laporanlender/agentreferalpopulatemodal',[
    //     'uses' => 'login\AgentReferal@modal',
    //     'as' => 'login.agentreferalmodal'
    // ]);
    // Route::get('agentreferalexcel',[
    //     'uses' => 'login\AgentReferal@excel',
    //     'as' => 'agentreferalexcel'
    // ]);

    // // ==== Start Laporan Outstanding Saldo ====
    // Route::get('/laporanlender/outstandingsaldo',[
    //     'uses' => 'login\OutstandingSaldo@index',
    //     'as' => 'login.outstandingsaldo'
    // ]);
    // Route::post('/laporanlender/outstandingsaldopopulate',[
    //     'uses' => 'login\OutstandingSaldo@populate',
    //     'as' => 'login.outstandingsaldopopulate'
    // ]);
    // Route::post('/laporanlender/outstandingsaldopopulatemodaldetail',[
    //     'uses' => 'login\OutstandingSaldo@populatedetail',
    //     'as' => 'login.outstandingsaldopopulatedetail'
    // ]);
    // Route::post('/laporanlender/outstandingsaldopopulatemodal',[
    //     'uses' => 'login\OutstandingSaldo@modal',
    //     'as' => 'login.outstandingsaldomodal'
    // ]);
    // Route::get('outstandingsaldoexcel',[
    //     'uses' => 'login\OutstandingSaldo@excel',
    //     'as' => 'outstandingsaldoexcel'
    // ]);

    // // ==== Start Laporan Deposit VA ====
    // Route::get('/laporanlender/depositva',[
    //     'uses' => 'login\DepositVa@index',
    //     'as' => 'login.depositva'
    // ]);
    // Route::post('/laporanlender/depositvapopulate',[
    //     'uses' => 'login\DepositVa@populate',
    //     'as' => 'login.depositvapopulate'
    // ]);
    // Route::post('/laporanlender/depositvapopulatemodal',[
    //     'uses' => 'login\DepositVa@modal',
    //     'as' => 'login.depositvamodal'
    // ]);
    // Route::get('depositvaexcel',[
    //     'uses' => 'login\DepositVa@excel',
    //     'as' => 'depositvaexcel'
    // ]);

    // // ==== Start Laporan Voucher VA ====
    // Route::get('/laporanlender/voucherva',[
    //     'uses' => 'login\VoucherVa@index',
    //     'as' => 'login.voucherva'
    // ]);
    // Route::post('/laporanlender/vouchervapopulate',[
    //     'uses' => 'login\VoucherVa@populate',
    //     'as' => 'login.vouchervapopulate'
    // ]);
    // Route::post('/laporanlender/vouchervapopulatemodaldetail',[
    //     'uses' => 'login\VoucherVa@populatedetail',
    //     'as' => 'login.vouchervapopulatedetail'
    // ]);
    // Route::post('/laporanlender/vouchervapopulatemodal',[
    //     'uses' => 'login\VoucherVa@modal',
    //     'as' => 'login.vouchervamodal'
    // ]);
    // Route::get('vouchervaexcel',[
    //     'uses' => 'login\VoucherVa@excel',
    //     'as' => 'vouchervaexcel'
    // ]);

    // // ==== Start Laporan Cashback VA ====
    // Route::get('/laporanlender/cashbackva',[
    //     'uses' => 'login\CashbackVa@index',
    //     'as' => 'login.cashbackva'
    // ]);
    // Route::post('/laporanlender/cashbackvapopulate',[
    //     'uses' => 'login\CashbackVa@populate',
    //     'as' => 'login.cashbackvapopulate'
    // ]);
    // Route::post('/laporanlender/cashbackvapopulatemodaldetail',[
    //     'uses' => 'login\CashbackVa@populatedetail',
    //     'as' => 'login.cashbackvapopulatedetail'
    // ]);
    // Route::post('/laporanlender/cashbackvapopulatemodal',[
    //     'uses' => 'login\CashbackVa@modal',
    //     'as' => 'login.cashbackvamodal'
    // ]);
    // Route::get('cashbackvaexcel',[
    //     'uses' => 'login\CashbackVa@excel',
    //     'as' => 'cashbackvaexcel'
    // ]);

    // // ==== Start Laporan Transaksi ====
    // Route::get('/laporanlender/transaksilender',[
    //     'uses' => 'login\TransaksiLender@index',
    //     'as' => 'login.transaksilender'
    // ]);
    // Route::post('/laporanlender/transaksilenderpopulate',[
    //     'uses' => 'login\TransaksiLender@populate',
    //     'as' => 'login.transaksilenderpopulate'
    // ]);
    // Route::post('/laporanlender/transaksilenderpopulatemodal',[
    //     'uses' => 'login\TransaksiLender@modal',
    //     'as' => 'login.transaksilendermodal'
    // ]);
    // Route::get('transaksilenderexcel',[
    //     'uses' => 'login\TransaksiLender@excel',
    //     'as' => 'transaksilenderexcel'
    // ]);

    // // ==== Start Laporan Borrower Register ====
    // Route::get('/laporanborrower/borrowerregister',[
    //     'uses' => 'login\BorrowerRegister@index',
    //     'as' => 'login.borrowerregister'
    // ]);
    // Route::post('/laporanborrower/borrowerregisterpopulate',[
    //     'uses' => 'login\BorrowerRegister@populate',
    //     'as' => 'login.borrowerregisterpopulate'
    // ]);
    // Route::post('/laporanborrower/borrowerregisterpopulatemodal',[
    //     'uses' => 'login\BorrowerRegister@modal',
    //     'as' => 'login.borrowerregistermodal'
    // ]);
    // Route::get('borrowerregisterexcel',[
    //     'uses' => 'login\BorrowerRegister@excel',
    //     'as' => 'borrowerregisterexcel'
    // ]);

    // // ==== Start Laporan Borrower Referal ====
    // Route::get('/laporanborrower/borrowerreferal',[
    //     'uses' => 'login\BorrowerReferal@index',
    //     'as' => 'login.borrowerreferal'
    // ]);
    // Route::post('/laporanborrower/borrowerreferalpopulate',[
    //     'uses' => 'login\BorrowerReferal@populate',
    //     'as' => 'login.borrowerreferalpopulate'
    // ]);
    // Route::post('/laporanborrower/borrowerreferalpopulatemodal',[
    //     'uses' => 'login\BorrowerReferal@modal',
    //     'as' => 'login.borrowerreferalmodal'
    // ]);
    // Route::get('borrowerreferalexcel',[
    //     'uses' => 'login\BorrowerReferal@excel',
    //     'as' => 'borrowerreferalexcel'
    // ]);

    // // ==== Start Laporan Pending Mitra ====
    // Route::get('/laporanborrower/pendingmitra',[
    //     'uses' => 'login\PendingMitra@index',
    //     'as' => 'login.pendingmitra'
    // ]);
    // Route::post('/laporanborrower/pendingmitrapopulate',[
    //     'uses' => 'login\PendingMitra@populate',
    //     'as' => 'login.pendingmitrapopulate'
    // ]);
    // Route::post('/laporanborrower/pendingmitrapopulatemodal',[
    //     'uses' => 'login\PendingMitra@modal',
    //     'as' => 'login.pendingmitramodal'
    // ]);

    // // ==== Start Laporan Pencairan Pinjaman ====
    // Route::get('/laporanborrower/pencairanpinjaman',[
    //     'uses' => 'login\PencairanPinjaman@index',
    //     'as' => 'login.pencairanpinjaman'
    // ]);
    // Route::post('/laporanborrower/pencairanpinjamanpopulate',[
    //     'uses' => 'login\PencairanPinjaman@populate',
    //     'as' => 'login.pencairanpinjamanpopulate'
    // ]);
    // Route::post('/laporanborrower/pencairanpinjamanpopulatemodal',[
    //     'uses' => 'login\PencairanPinjaman@modal',
    //     'as' => 'login.pencairanpinjamanmodal'
    // ]);
    // Route::post('/laporanborrower/pencairanpinjamanpopulateajax',[
    //     'uses' => 'login\PencairanPinjaman@ajax',
    //     'as' => 'login.pencairanpinjamanajax'
    // ]);
    // Route::get('pencairanpinjamanexcel',[
    //     'uses' => 'login\PencairanPinjaman@excel',
    //     'as' => 'pencairanpinjamanexcel'
    // ]);

    // // ==== Start Laporan Pelunasan Pinjaman ====
    // Route::get('/laporanborrower/pelunasanpinjaman',[
    //     'uses' => 'login\PelunasanPinjaman@index',
    //     'as' => 'login.pelunasanpinjaman'
    // ]);
    // Route::post('/laporanborrower/pelunasanpinjamanpopulate',[
    //     'uses' => 'login\PelunasanPinjaman@populate',
    //     'as' => 'login.pelunasanpinjamanpopulate'
    // ]);
    // Route::post('/laporanborrower/pelunasanpinjamanpopulatemodal',[
    //     'uses' => 'login\PelunasanPinjaman@modal',
    //     'as' => 'login.pelunasanpinjamanmodal'
    // ]);
    // Route::get('pelunasanpinjamanexcel',[
    //     'uses' => 'login\PelunasanPinjaman@excel',
    //     'as' => 'pelunasanpinjamanexcel'
    // ]);

    // // ==== Start Laporan Pinjaman Aktif ====
    // Route::get('/laporanborrower/outstandingpinjaman',[
    //     'uses' => 'login\OutstandingPinjaman@index',
    //     'as' => 'login.outstandingpinjaman'
    // ]);
    // Route::post('/laporanborrower/outstandingpinjamanpopulate',[
    //     'uses' => 'login\OutstandingPinjaman@populate',
    //     'as' => 'login.outstandingpinjamanpopulate'
    // ]);
    // Route::post('/laporanborrower/outstandingpinjamanpopulatemodal',[
    //     'uses' => 'login\OutstandingPinjaman@modal',
    //     'as' => 'login.outstandingpinjamanmodal'
    // ]);
    // Route::post('/laporanborrower/outstandingpinjamanpopulateajax',[
    //     'uses' => 'login\OutstandingPinjaman@ajax',
    //     'as' => 'login.outstandingpinjamanajax'
    // ]);
    // Route::get('outstandingpinjamanexcel',[
    //     'uses' => 'login\OutstandingPinjaman@excel',
    //     'as' => 'outstandingpinjamanexcel'
    // ]);

    // // ==== Start List Pinjaman OVD ====
    // Route::get('/laporanborrower/listpinjamanovd',[
    //     'uses' => 'login\ListPinjamanOvd@index',
    //     'as' => 'login.listpinjamanovd'
    // ]);
    // Route::post('/laporanborrower/listpinjamanovdpopulate',[
    //     'uses' => 'login\ListPinjamanOvd@populate',
    //     'as' => 'login.listpinjamanovdpopulate'
    // ]);
    // Route::post('/laporanborrower/listpinjamanovdpopulatemodal',[
    //     'uses' => 'login\ListPinjamanOvd@modal',
    //     'as' => 'login.listpinjamanovdmodal'
    // ]);
    // Route::post('/laporanborrower/listpinjamanovdpopulateajax',[
    //     'uses' => 'login\ListPinjamanOvd@ajax',
    //     'as' => 'login.listpinjamanovdajax'
    // ]);
    // Route::post('/laporanborrower/listpinjamanovdpopulatepost',[
    //     'uses' => 'login\ListPinjamanOvd@proses',
    //     'as' => 'login.listpinjamanovdpost'
    // ]);

    // // ==== Start List Penjualan ====
    // Route::get('/laporanborrower/listpenjualan',[
    //     'uses' => 'login\ListPenjualan@index',
    //     'as' => 'login.listpenjualan'
    // ]);
    // Route::post('/laporanborrower/listpenjualanpopulate',[
    //     'uses' => 'login\ListPenjualan@populate',
    //     'as' => 'login.listpenjualanpopulate'
    // ]);
    // Route::post('/laporanborrower/listpenjualanpopulatemodal',[
    //     'uses' => 'login\ListPenjualan@modal',
    //     'as' => 'login.listpenjualanmodal'
    // ]);
    // Route::post('/laporanborrower/listpenjualanpopulateajax',[
    //     'uses' => 'login\ListPenjualan@ajax',
    //     'as' => 'login.listpenjualanajax'
    // ]);
    // Route::post('/laporanborrower/listpenjualanpopulatepost',[
    //     'uses' => 'login\ListPenjualan@proses',
    //     'as' => 'login.listpenjualanpost'
    // ]);

    // // ==== Start Summary Penjualan ====
    // Route::get('/laporanborrower/summarypinjaman',[
    //     'uses' => 'login\SummaryPinjaman@index',
    //     'as' => 'login.summarypinjaman'
    // ]);
    // Route::post('/laporanborrower/summarypinjamanpopulate',[
    //     'uses' => 'login\SummaryPinjaman@populate',
    //     'as' => 'login.summarypinjamanpopulate'
    // ]);
    // Route::post('/laporanborrower/summarypinjamanpopulatemodal',[
    //     'uses' => 'login\SummaryPinjaman@modal',
    //     'as' => 'login.summarypinjamanmodal'
    // ]);
    // Route::post('/laporanborrower/summarypinjamanpopulateajax',[
    //     'uses' => 'login\SummaryPinjaman@ajax',
    //     'as' => 'login.summarypinjamanajax'
    // ]);
    // Route::post('/laporanborrower/summarypinjamanpopulatepost',[
    //     'uses' => 'login\SummaryPinjaman@proses',
    //     'as' => 'login.summarypinjamanpost'
    // ]);
    // Route::get('summarypinjamanexcel',[
    //     'uses' => 'login\SummaryPinjaman@excel',
    //     'as' => 'summarypinjamanexcel'
    // ]);

    // // ==== Start Laporan Borrower Black List ====
    // Route::get('/laporanborrower/borrowerblacklist',[
    //     'uses' => 'login\BorrowerBlacklist@index',
    //     'as' => 'login.borrowerblacklist'
    // ]);
    // Route::post('/laporanborrower/borrowerblacklistpopulate',[
    //     'uses' => 'login\BorrowerBlacklist@populate',
    //     'as' => 'login.borrowerblacklistpopulate'
    // ]);
    // Route::post('/laporanborrower/borrowerblacklistpopulatemodal',[
    //     'uses' => 'login\BorrowerBlacklist@modal',
    //     'as' => 'login.borrowerblacklistmodal'
    // ]);

    // // ==== Start Laporan Barang Jaminan ====
    // Route::get('/laporanborrower/barangjaminan',[
    //     'uses' => 'login\BarangJaminan@index',
    //     'as' => 'login.barangjaminan'
    // ]);
    // Route::post('/laporanborrower/barangjaminanpopulate',[
    //     'uses' => 'login\BarangJaminan@populate',
    //     'as' => 'login.barangjaminanpopulate'
    // ]);
    // Route::post('/laporanborrower/barangjaminanpopulatemodal',[
    //     'uses' => 'login\BarangJaminan@modal',
    //     'as' => 'login.barangjaminanmodal'
    // ]);
    // Route::post('/laporanborrower/barangjaminanpopulateajax',[
    //     'uses' => 'login\BarangJaminan@ajax',
    //     'as' => 'login.barangjaminanajax'
    // ]);
    // Route::get('barangjaminanexcel',[
    //     'uses' => 'login\BarangJaminan@excel',
    //     'as' => 'barangjaminanexcel'
    // ]);

    // ==== Start Menu ====
    Route::get('/master-akses/menu',[
        'uses' => 'login\Menu@index',
        'as' => 'login.menu'
    ]);
    Route::post('/master-akses/menupopulate',[
        'uses' => 'login\Menu@populate',
        'as' => 'login.menupopulate'
    ]);
    Route::post('/master-akses/menupopulatemodal',[
        'uses' => 'login\Menu@modal',
        'as' => 'login.menumodal'
    ]);
    Route::post('/master-akses/menupopulatepost',[
        'uses' => 'login\Menu@proses',
        'as' => 'login.menupost'
    ]);

    // ==== Start Modul ====
    Route::get('/master-akses/modul',[
        'uses' => 'login\Modul@index',
        'as' => 'login.modul'
    ]);
    Route::post('/master-akses/modulpopulate',[
        'uses' => 'login\Modul@populate',
        'as' => 'login.modulpopulate'
    ]);
    Route::post('/master-akses/modulpopulatemodal',[
        'uses' => 'login\Modul@modal',
        'as' => 'login.modulmodal'
    ]);
    Route::post('/master-akses/modulpopulatepost',[
        'uses' => 'login\Modul@proses',
        'as' => 'login.modulpost'
    ]);
    Route::post('/master-akses/modulpopulatepostakses',[
        'uses' => 'login\Modul@prosesakses',
        'as' => 'login.modulpostakses'
    ]);

    // ==== Start Mitra User ====
    Route::get('/master-akses/mitrauser',[
        'uses' => 'login\MitraUser@index',
        'as' => 'login.mitrauser'
    ]);
    Route::post('/master-akses/mitrauserpopulate',[
        'uses' => 'login\MitraUser@populate',
        'as' => 'login.mitrauserpopulate'
    ]);
    Route::post('/master-akses/mitrauserpopulatemodal',[
        'uses' => 'login\MitraUser@modal',
        'as' => 'login.mitrausermodal'
    ]);
    Route::post('/master-akses/mitrauserpopulateajax',[
        'uses' => 'login\MitraUser@ajax',
        'as' => 'login.mitrauserajax'
    ]);
    Route::post('/master-akses/mitrauserpopulatepost',[
        'uses' => 'login\MitraUser@proses',
        'as' => 'login.mitrauserpost'
    ]);

    // ==== Start Akunting - Menu Coa ====
    Route::get('/akunting/menucoa',[
        'uses' => 'login\MenuCoa@index',
        'as' => 'login.menucoa'
    ]);
    Route::post('/akunting/menucoapopulate',[
        'uses' => 'login\MenuCoa@populate',
        'as' => 'login.menucoapopulate'
    ]);
    Route::post('/akunting/menucoapopulatemodal',[
        'uses' => 'login\MenuCoa@modal',
        'as' => 'login.menucoamodal'
    ]);
    Route::post('/akunting/menucoapopulatepost',[
        'uses' => 'login\MenuCoa@proses',
        'as' => 'login.menucoapost'
    ]);

    // ==== Start Akunting - GL ====
    Route::get('/akunting/gl',[
        'uses' => 'login\GL@index',
        'as' => 'login.gl'
    ]);
    Route::post('/akunting/glpopulate',[
        'uses' => 'login\GL@populate',
        'as' => 'login.glpopulate'
    ]);
    Route::post('/akunting/glpopulateajax',[
        'uses' => 'login\GL@ajax',
        'as' => 'login.glajax'
    ]);
    Route::get('glpopulateexport',[
        'uses' => 'login\GL@export',
        'as' => 'glpopulateexport'
    ]);

    // ==== Core MAS - Saldo Bank ====
    Route::get('/coremas/saldobank',[
        'uses' => 'login\SaldoBank@index',
        'as' => 'login.saldobank'
    ]);
    Route::post('/coremas/saldobankpopulate',[
        'uses' => 'login\SaldoBank@populate',
        'as' => 'login.saldobankpopulate'
    ]);
    Route::post('/coremas/saldobankpopulatemutasi',[
        'uses' => 'login\SaldoBank@populatemutasi',
        'as' => 'login.saldobankpopulatemutasi'
    ]);
    Route::post('/coremas/saldobankpopulatemodal',[
        'uses' => 'login\SaldoBank@modal',
        'as' => 'login.saldobankmodal'
    ]);
    Route::post('/coremas/saldobankpopulatepost',[
        'uses' => 'login\SaldoBank@proses',
        'as' => 'login.saldobankpost'
    ]);
    Route::get('/laporan/saldobankpopulateexcel',[
        'uses' => 'login\SaldoBank@excel',
        'as' => 'login.saldobankexcel'
    ]);

    // ==== Core MAS - Mutasi Internal Unit ====
    Route::get('/coremas/mutasiinternalunit',[
        'uses' => 'login\MutasiInternalUnit@index',
        'as' => 'login.mutasiinternalunit'
    ]);
    Route::post('/coremas/mutasiinternalunitpopulate',[
        'uses' => 'login\MutasiInternalUnit@populate',
        'as' => 'login.mutasiinternalunitpopulate'
    ]);
    Route::post('/coremas/mutasiinternalunitpopulatemodal',[
        'uses' => 'login\MutasiInternalUnit@modal',
        'as' => 'login.mutasiinternalunitmodal'
    ]);
    Route::post('/coremas/mutasiinternalunitpopulatepost',[
        'uses' => 'login\MutasiInternalUnit@proses',
        'as' => 'login.mutasiinternalunitpost'
    ]);
    Route::post('/coremas/mutasiinternalunitpopulateajax',[
        'uses' => 'login\MutasiInternalUnit@ajax',
        'as' => 'login.mutasiinternalunitajax'
    ]);

    // ==== Core MAS - Pencairan Gadai ====
    Route::get('/coremas/pencairangadai',[
        'uses' => 'login\PencairanGadai@index',
        'as' => 'login.pencairangadai'
    ]);
    Route::post('/coremas/pencairangadaipopulate',[
        'uses' => 'login\PencairanGadai@populate',
        'as' => 'login.pencairangadaipopulate'
    ]);
    Route::post('/coremas/pencairangadaipopulatemodal',[
        'uses' => 'login\PencairanGadai@modal',
        'as' => 'login.pencairangadaimodal'
    ]);
    Route::post('/coremas/pencairangadaipopulatepost',[
        'uses' => 'login\PencairanGadai@proses',
        'as' => 'login.pencairangadaipost'
    ]);
    Route::post('/coremas/pencairangadaipopulateajax',[
        'uses' => 'login\PencairanGadai@ajax',
        'as' => 'login.pencairangadaiajax'
    ]);

    // ==== Core MAS - Pencairan Gadai JF ====
    Route::get('/coremas/pencairangadaijf',[
        'uses' => 'login\PencairanGadaiJf@index',
        'as' => 'login.pencairangadaijf'
    ]);
    Route::post('/coremas/pencairangadaijfpopulate',[
        'uses' => 'login\PencairanGadaiJf@populate',
        'as' => 'login.pencairangadaijfpopulate'
    ]);
    Route::post('/coremas/pencairangadaijfpopulatemodal',[
        'uses' => 'login\PencairanGadaiJf@modal',
        'as' => 'login.pencairangadaijfmodal'
    ]);
    Route::post('/coremas/pencairangadaijfpopulatepost',[
        'uses' => 'login\PencairanGadaiJf@proses',
        'as' => 'login.pencairangadaijfpost'
    ]);
    Route::post('/coremas/pencairangadaijfpopulateajax',[
        'uses' => 'login\PencairanGadaiJf@ajax',
        'as' => 'login.pencairangadaijfajax'
    ]);

    // ==== Core MAS - Dropping ====
    Route::get('/coremas/dropping',[
        'uses' => 'login\Dropping@index',
        'as' => 'login.dropping'
    ]);
    Route::post('/coremas/droppingpopulate',[
        'uses' => 'login\Dropping@populate',
        'as' => 'login.droppingpopulate'
    ]);
    Route::post('/coremas/droppingpopulatemodal',[
        'uses' => 'login\Dropping@modal',
        'as' => 'login.droppingmodal'
    ]);
    Route::post('/coremas/droppingpopulatepost',[
        'uses' => 'login\Dropping@proses',
        'as' => 'login.droppingpost'
    ]);
    Route::post('/coremas/droppingpopulateajax',[
        'uses' => 'login\Dropping@ajax',
        'as' => 'login.droppingajax'
    ]);

    // ==== Core MAS - Mutasi Bank ====
    Route::get('/coremas/mutasibank',[
        'uses' => 'login\MutasiBank@index',
        'as' => 'login.mutasibank'
    ]);
    Route::post('/coremas/mutasibankpopulate',[
        'uses' => 'login\MutasiBank@populate',
        'as' => 'login.mutasibankpopulate'
    ]);
    Route::post('/coremas/mutasibankpopulatemodal',[
        'uses' => 'login\MutasiBank@modal',
        'as' => 'login.mutasibankmodal'
    ]);
    Route::post('/coremas/mutasibankpopulatepost',[
        'uses' => 'login\MutasiBank@proses',
        'as' => 'login.mutasibankpost'
    ]);
    Route::post('/coremas/mutasibankpopulateajax',[
        'uses' => 'login\MutasiBank@ajax',
        'as' => 'login.mutasibankajax'
    ]);

    // ==== Core MAS - Realisasi Petty Cash ====
    Route::get('/coremas/realisasipc',[
        'uses' => 'login\RealisasiPC@index',
        'as' => 'login.realisasipc'
    ]);
    Route::post('/coremas/realisasipcpopulate',[
        'uses' => 'login\RealisasiPC@populate',
        'as' => 'login.realisasipcpopulate'
    ]);
    Route::post('/coremas/realisasipcpopulatemodal',[
        'uses' => 'login\RealisasiPC@modal',
        'as' => 'login.realisasipcmodal'
    ]);
    Route::post('/coremas/realisasipcpopulatepost',[
        'uses' => 'login\RealisasiPC@proses',
        'as' => 'login.realisasipcpost'
    ]);
    Route::post('/coremas/realisasipcpopulateajax',[
        'uses' => 'login\RealisasiPC@ajax',
        'as' => 'login.realisasipcajax'
    ]);

    // ==== Core MAS - AR Tunggu ====
    Route::get('/coremas/artunggu',[
        'uses' => 'login\ARTunggu@index',
        'as' => 'login.artunggu'
    ]);
    Route::post('/coremas/artunggupopulate',[
        'uses' => 'login\ARTunggu@populate',
        'as' => 'login.artunggupopulate'
    ]);
    Route::post('/coremas/artunggupopulatemodal',[
        'uses' => 'login\ARTunggu@modal',
        'as' => 'login.artunggumodal'
    ]);
    Route::post('/coremas/artunggupopulatepost',[
        'uses' => 'login\ARTunggu@proses',
        'as' => 'login.artunggupost'
    ]);
    Route::post('/coremas/artunggupopulateajax',[
        'uses' => 'login\ARTunggu@ajax',
        'as' => 'login.artungguajax'
    ]);

    // ==== Core MAS - Titipan ====
    Route::get('/coremas/titipan',[
        'uses' => 'login\Titipan@index',
        'as' => 'login.titipan'
    ]);
    Route::post('/coremas/titipanpopulate',[
        'uses' => 'login\Titipan@populate',
        'as' => 'login.titipanpopulate'
    ]);
    Route::post('/coremas/titipanpopulatemodal',[
        'uses' => 'login\Titipan@modal',
        'as' => 'login.titipanmodal'
    ]);
    Route::post('/coremas/titipanpopulatepost',[
        'uses' => 'login\Titipan@proses',
        'as' => 'login.titipanpost'
    ]);
    Route::post('/coremas/titipanpopulateajax',[
        'uses' => 'login\Titipan@ajax',
        'as' => 'login.titipanajax'
    ]);

    // ==== Core MAS - Pelunasan Gadai ====
    Route::get('/coremas/pelunasangadai',[
        'uses' => 'login\PelunasanGadai@index',
        'as' => 'login.pelunasangadai'
    ]);
    Route::post('/coremas/pelunasangadaipopulate',[
        'uses' => 'login\PelunasanGadai@populate',
        'as' => 'login.pelunasangadaipopulate'
    ]);
    Route::post('/coremas/pelunasangadaipopulatemodal',[
        'uses' => 'login\PelunasanGadai@modal',
        'as' => 'login.pelunasangadaimodal'
    ]);
    Route::post('/coremas/pelunasangadaipopulatepost',[
        'uses' => 'login\PelunasanGadai@proses',
        'as' => 'login.pelunasangadaipost'
    ]);
    Route::post('/coremas/pelunasangadaipopulateajax',[
        'uses' => 'login\PelunasanGadai@ajax',
        'as' => 'login.pelunasangadaiajax'
    ]);

    // ==== Core MAS - Pelunasan Gadai JF====
    Route::get('/coremas/pelunasangadaijf',[
        'uses' => 'login\PelunasanGadaiJF@index',
        'as' => 'login.pelunasangadaijf'
    ]);
    Route::post('/coremas/pelunasangadaijfpopulate',[
        'uses' => 'login\PelunasanGadaiJF@populate',
        'as' => 'login.pelunasangadaijfpopulate'
    ]);
    Route::post('/coremas/pelunasangadaijfpopulatemodal',[
        'uses' => 'login\PelunasanGadaiJF@modal',
        'as' => 'login.pelunasangadaijfmodal'
    ]);
    Route::post('/coremas/pelunasangadaijfpopulatepost',[
        'uses' => 'login\PelunasanGadaiJF@proses',
        'as' => 'login.pelunasangadaijfpost'
    ]);
    Route::post('/coremas/pelunasangadaijfpopulateajax',[
        'uses' => 'login\PelunasanGadaiJF@ajax',
        'as' => 'login.pelunasangadaijfajax'
    ]);

    // ==== Core MAS - Suspend ====
    Route::get('/coremas/suspend',[
        'uses' => 'login\Suspend@index',
        'as' => 'login.suspend'
    ]);
    Route::post('/coremas/suspendpopulate',[
        'uses' => 'login\Suspend@populate',
        'as' => 'login.suspendpopulate'
    ]);
    Route::post('/coremas/suspendpopulatemodal',[
        'uses' => 'login\Suspend@modal',
        'as' => 'login.suspendmodal'
    ]);
    Route::post('/coremas/suspendpopulatepost',[
        'uses' => 'login\Suspend@proses',
        'as' => 'login.suspendpost'
    ]);
    Route::post('/coremas/suspendpopulateajax',[
        'uses' => 'login\Suspend@ajax',
        'as' => 'login.suspendajax'
    ]);

    // ==== Core MAS - Rekon Bank ====
    Route::get('/coremas/rekonbank',[
        'uses' => 'login\RekonBank@index',
        'as' => 'login.rekonbank'
    ]);
    Route::post('/coremas/rekonbankpopulate',[
        'uses' => 'login\RekonBank@populate',
        'as' => 'login.rekonbankpopulate'
    ]);
    Route::post('/coremas/rekonbankpopulatemodal',[
        'uses' => 'login\RekonBank@modal',
        'as' => 'login.rekonbankmodal'
    ]);
    Route::post('/coremas/rekonbankpopulatepost',[
        'uses' => 'login\RekonBank@proses',
        'as' => 'login.rekonbankpost'
    ]);
    Route::post('/coremas/rekonbankpopulateajax',[
        'uses' => 'login\RekonBank@ajax',
        'as' => 'login.rekonbankajax'
    ]);

    // ==== Core MAS - Acrue ====
    Route::get('/coremas/acrue',[
        'uses' => 'login\Acrue@index',
        'as' => 'login.acrue'
    ]);
    Route::post('/coremas/acruepopulate',[
        'uses' => 'login\Acrue@populate',
        'as' => 'login.acruepopulate'
    ]);
    Route::post('/coremas/acruepopulatemodal',[
        'uses' => 'login\Acrue@modal',
        'as' => 'login.acruemodal'
    ]);
    Route::post('/coremas/acruepopulatepost',[
        'uses' => 'login\Acrue@proses',
        'as' => 'login.acruepost'
    ]);
    Route::post('/coremas/acruepopulateajax',[
        'uses' => 'login\Acrue@ajax',
        'as' => 'login.acrueajax'
    ]);

    // ==== Core MAS - Penjualan ====
    Route::get('/coremas/penjualan',[
        'uses' => 'login\Penjualan@index',
        'as' => 'login.penjualan'
    ]);
    Route::post('/coremas/penjualanpopulate',[
        'uses' => 'login\Penjualan@populate',
        'as' => 'login.penjualanpopulate'
    ]);
    Route::post('/coremas/penjualanpopulatemodal',[
        'uses' => 'login\Penjualan@modal',
        'as' => 'login.penjualanmodal'
    ]);
    Route::post('/coremas/penjualanpopulatepost',[
        'uses' => 'login\Penjualan@proses',
        'as' => 'login.penjualanpost'
    ]);
    Route::post('/coremas/penjualanpopulateajax',[
        'uses' => 'login\Penjualan@ajax',
        'as' => 'login.penjualanajax'
    ]);

    // ==== Core MAS - Cicilan ====
    Route::get('/coremas/cicilan',[
        'uses' => 'login\Cicilan@index',
        'as' => 'login.cicilan'
    ]);
    Route::post('/coremas/cicilanpopulate',[
        'uses' => 'login\Cicilan@populate',
        'as' => 'login.cicilanpopulate'
    ]);
    Route::post('/coremas/cicilanpopulatemodal',[
        'uses' => 'login\Cicilan@modal',
        'as' => 'login.cicilanmodal'
    ]);
    Route::post('/coremas/cicilanpopulatepost',[
        'uses' => 'login\Cicilan@proses',
        'as' => 'login.cicilanpost'
    ]);
    Route::post('/coremas/cicilanpopulateajax',[
        'uses' => 'login\Cicilan@ajax',
        'as' => 'login.cicilanajax'
    ]);

    // ==== Start Plafon Buffer ====
    Route::get('/setting-finance/plafonbuffer',[
        'uses' => 'login\PlafonBuffer@index',
        'as' => 'login.plafonbuffer'
    ]);
    Route::post('/setting-finance/plafonbufferpopulate',[
        'uses' => 'login\PlafonBuffer@populate',
        'as' => 'login.plafonbufferpopulate'
    ]);
    Route::post('/setting-finance/plafonbufferpopulatemodal',[
        'uses' => 'login\PlafonBuffer@modal',
        'as' => 'login.plafonbuffermodal'
    ]);
    Route::post('/setting-finance/plafonbufferpopulatepost',[
        'uses' => 'login\PlafonBuffer@proses',
        'as' => 'login.plafonbufferpost'
    ]);

    // ==== Start Plafon Petty Cash ====
    Route::get('/setting-finance/plafonpettycash',[
        'uses' => 'login\PlafonPettyCash@index',
        'as' => 'login.plafonpettycash'
    ]);
    Route::post('/setting-finance/plafonpettycashpopulate',[
        'uses' => 'login\PlafonPettyCash@populate',
        'as' => 'login.plafonpettycashpopulate'
    ]);
    Route::post('/setting-finance/plafonpettycashpopulatemodal',[
        'uses' => 'login\PlafonPettyCash@modal',
        'as' => 'login.plafonpettycashmodal'
    ]);
    Route::post('/setting-finance/plafonpettycashpopulatepost',[
        'uses' => 'login\PlafonPettyCash@proses',
        'as' => 'login.plafonpettycashpost'
    ]);

    // ==== Start Plafon Kas Marketing ====
    Route::get('/setting-finance/plafonkasmarketing',[
        'uses' => 'login\PlafonKasMarketing@index',
        'as' => 'login.plafonkasmarketing'
    ]);
    Route::post('/setting-finance/plafonkasmarketingpopulate',[
        'uses' => 'login\PlafonKasMarketing@populate',
        'as' => 'login.plafonkasmarketingpopulate'
    ]);
    Route::post('/setting-finance/plafonkasmarketingpopulatemodal',[
        'uses' => 'login\PlafonKasMarketing@modal',
        'as' => 'login.plafonkasmarketingmodal'
    ]);
    Route::post('/setting-finance/plafonkasmarketingpopulatepost',[
        'uses' => 'login\PlafonKasMarketing@proses',
        'as' => 'login.plafonkasmarketingpost'
    ]);

    // ==== Start Plafon Buffer Syariah ====
    Route::get('/setting-finance/plafonbuffersyariah',[
        'uses' => 'login\PlafonBufferSyariah@index',
        'as' => 'login.plafonbuffersyariah'
    ]);
    Route::post('/setting-finance/plafonbuffersyariahpopulate',[
        'uses' => 'login\PlafonBufferSyariah@populate',
        'as' => 'login.plafonbuffersyariahpopulate'
    ]);
    Route::post('/setting-finance/plafonbuffersyariahpopulatemodal',[
        'uses' => 'login\PlafonBufferSyariah@modal',
        'as' => 'login.plafonbuffersyariahmodal'
    ]);
    Route::post('/setting-finance/plafonbuffersyariahpopulatepost',[
        'uses' => 'login\PlafonBufferSyariah@proses',
        'as' => 'login.plafonbuffersyariahpost'
    ]);

    // ==== Start Plafon Petty Cash Syariah ====
    Route::get('/setting-finance/plafonpettycashsyariah',[
        'uses' => 'login\PlafonPettyCashSyariah@index',
        'as' => 'login.plafonpettycashsyariah'
    ]);
    Route::post('/setting-finance/plafonpettycashsyariahpopulate',[
        'uses' => 'login\PlafonPettyCashSyariah@populate',
        'as' => 'login.plafonpettycashsyariahpopulate'
    ]);
    Route::post('/setting-finance/plafonpettycashsyariahpopulatemodal',[
        'uses' => 'login\PlafonPettyCashSyariah@modal',
        'as' => 'login.plafonpettycashsyariahmodal'
    ]);
    Route::post('/setting-finance/plafonpettycashsyariahpopulatepost',[
        'uses' => 'login\PlafonPettyCashSyariah@proses',
        'as' => 'login.plafonpettycashsyariahpost'
    ]);

    // ==== Start Plafon Kas Marketing Syariah ====
    Route::get('/setting-finance/plafonkasmarketingsyariah',[
        'uses' => 'login\PlafonKasMarketingSyariah@index',
        'as' => 'login.plafonkasmarketingsyariah'
    ]);
    Route::post('/setting-finance/plafonkasmarketingsyariahpopulate',[
        'uses' => 'login\PlafonKasMarketingSyariah@populate',
        'as' => 'login.plafonkasmarketingsyariahpopulate'
    ]);
    Route::post('/setting-finance/plafonkasmarketingsyariahpopulatemodal',[
        'uses' => 'login\PlafonKasMarketingSyariah@modal',
        'as' => 'login.plafonkasmarketingsyariahmodal'
    ]);
    Route::post('/setting-finance/plafonkasmarketingsyariahpopulatepost',[
        'uses' => 'login\PlafonKasMarketingSyariah@proses',
        'as' => 'login.plafonkasmarketingsyariahpost'
    ]);

    // ==== Start Mutasi Antar Unit ====
    Route::get('/report-akunting/mutasiantarunit',[
        'uses' => 'login\MutasiAntarUnit@index',
        'as' => 'login.mutasiantarunit'
    ]);
    Route::post('/report-akunting/mutasiantarunitpopulate',[
        'uses' => 'login\MutasiAntarUnit@populate',
        'as' => 'login.mutasiantarunitpopulate'
    ]);
    Route::post('/report-akunting/mutasiantarunitpopulatemodal',[
        'uses' => 'login\MutasiAntarUnit@modal',
        'as' => 'login.mutasiantarunitmodal'
    ]);
    Route::post('/report-akunting/mutasiantarunitpopulateajax',[
        'uses' => 'login\MutasiAntarUnit@ajax',
        'as' => 'login.mutasiantarunitajax'
    ]);
    Route::get('mutasiantarunitexcel',[
        'uses' => 'login\MutasiAntarUnit@excel',
        'as' => 'mutasiantarunitexcel'
    ]);

    // ==== Start Waktu Transaksi Konvensional ====
    Route::get('/report-transaksi/waktutransaksikonvensional',[
        'uses' => 'login\WaktuTransaksiKonvensional@index',
        'as' => 'login.waktutransaksikonvensional'
    ]);
    Route::post('/report-transaksi/waktutransaksikonvensionalpopulate',[
        'uses' => 'login\WaktuTransaksiKonvensional@populate',
        'as' => 'login.waktutransaksikonvensionalpopulate'
    ]);
    Route::post('/report-transaksi/waktutransaksikonvensionalpopulatemodal',[
        'uses' => 'login\WaktuTransaksiKonvensional@modal',
        'as' => 'login.waktutransaksikonvensionalmodal'
    ]);
    Route::post('/report-transaksi/waktutransaksikonvensionalpopulateajax',[
        'uses' => 'login\WaktuTransaksiKonvensional@ajax',
        'as' => 'login.waktutransaksikonvensionalajax'
    ]);
    Route::get('waktutransaksikonvensionalexcel',[
        'uses' => 'login\WaktuTransaksiKonvensional@excel',
        'as' => 'waktutransaksikonvensionalexcel'
    ]);
    Route::get('waktutransaksikonvensionalexport',[
        'uses' => 'login\WaktuTransaksiKonvensional@export',
        'as' => 'waktutransaksikonvensionalexport'
    ]);

    // ==== Start Waktu Transaksi Syariah ====
    Route::get('/report-transaksi/waktutransaksisyariah',[
        'uses' => 'login\WaktuTransaksiSyariah@index',
        'as' => 'login.waktutransaksisyariah'
    ]);
    Route::post('/report-transaksi/waktutransaksisyariahpopulate',[
        'uses' => 'login\WaktuTransaksiSyariah@populate',
        'as' => 'login.waktutransaksisyariahpopulate'
    ]);
    Route::post('/report-transaksi/waktutransaksisyariahpopulatemodal',[
        'uses' => 'login\waktutransaksiSyariah@modal',
        'as' => 'login.waktutransaksisyariahmodal'
    ]);
    Route::post('/report-transaksi/waktutransaksisyariahpopulateajax',[
        'uses' => 'login\WaktuTransaksiSyariah@ajax',
        'as' => 'login.waktutransaksisyariahajax'
    ]);
    Route::get('waktutransaksisyariahexcel',[
        'uses' => 'login\WaktuTransaksiSyariah@excel',
        'as' => 'waktutransaksisyariahexcel'
    ]);

    // ==== Start Update Informasi ====
    Route::get('/master-informasi/updateinformasi',[
        'uses' => 'login\UpdateInformasi@index',
        'as' => 'login.updateinformasi'
    ]);
    Route::post('/master-informasi/updateinformasipopulate',[
        'uses' => 'login\UpdateInformasi@populate',
        'as' => 'login.updateinformasipopulate'
    ]);
    Route::post('/master-informasi/updateinformasipopulatereport',[
        'uses' => 'login\UpdateInformasi@populatereport',
        'as' => 'login.updateinformasipopulatereport'
    ]);
    Route::post('/master-informasi/updateinformasipopulatemodal',[
        'uses' => 'login\UpdateInformasi@modal',
        'as' => 'login.updateinformasimodal'
    ]);
    Route::post('/master-informasi/updateinformasipopulatepost',[
        'uses' => 'login\UpdateInformasi@proses',
        'as' => 'login.updateinformasipost'
    ]);
    Route::post('/master-informasi/updateinformasipopulateajax',[
        'uses' => 'login\UpdateInformasi@ajax',
        'as' => 'login.updateinformasiajax'
    ]);
    Route::get('updateinformasiexcel',[
        'uses' => 'login\UpdateInformasi@excel',
        'as' => 'login.updateinformasiexcel'
    ]);

    // ==== Start AZ Konvensional ====
    Route::get('/report-promas/azkonvensional',[
        'uses' => 'login\AzKonvensional@index',
        'as' => 'login.azkonvensional'
    ]);
    Route::post('/report-promas/azkonvensionalpopulate',[
        'uses' => 'login\AzKonvensional@populate',
        'as' => 'login.azkonvensionalpopulate'
    ]);
    Route::post('/report-promas/azkonvensionalpopulatemodal',[
        'uses' => 'login\AzKonvensional@modal',
        'as' => 'login.azkonvensionalmodal'
    ]);
    Route::post('/report-promas/azkonvensionalpopulateajax',[
        'uses' => 'login\AzKonvensional@ajax',
        'as' => 'login.azkonvensionalajax'
    ]);
    Route::get('azkonvensionalexcel',[
        'uses' => 'login\AzKonvensional@excel',
        'as' => 'azkonvensionalexcel'
    ]);
    Route::get('azkonvensionaltxt',[
        'uses' => 'login\AzKonvensional@download',
        'as' => 'azkonvensionaltxt'
    ]);
    // Route::get('/downloadaz/{url}', 'AzKonvensional@download')->name('downloadaz.file');
    // Route::get('/download-file-az', 'AzKonvensional@downloadFile')->name('download.file');
    // Route::get('/download/{url}', 'login\AzKonvensional@download')->name('download');



    // ==== Start Jaminan Konvensional ====
    Route::get('/report-promas/jaminankonvensional',[
        'uses' => 'login\JaminanKonvensional@index',
        'as' => 'login.jaminankonvensional'
    ]);
    Route::post('/report-promas/jaminankonvensionalpopulate',[
        'uses' => 'login\JaminanKonvensional@populate',
        'as' => 'login.jaminankonvensionalpopulate'
    ]);
    Route::post('/report-promas/jaminankonvensionalpopulatemodal',[
        'uses' => 'login\JaminanKonvensional@modal',
        'as' => 'login.jaminankonvensionalmodal'
    ]);
    Route::post('/report-promas/jaminankonvensionalpopulateajax',[
        'uses' => 'login\JaminanKonvensional@ajax',
        'as' => 'login.jaminankonvensionalajax'
    ]);
    Route::get('jaminankonvensionalexcel',[
        'uses' => 'login\JaminanKonvensional@excel',
        'as' => 'jaminankonvensionalexcel'
    ]);
    Route::get('jaminankonvensionaltxt',[
        'uses' => 'login\JaminanKonvensional@download',
        'as' => 'jaminankonvensionaltxt'
    ]);

    // ==== Start Booking Konvensional ====
    Route::get('/report-promas/bookingkonvensional',[
        'uses' => 'login\BookingKonvensional@index',
        'as' => 'login.bookingkonvensional'
    ]);
    Route::post('/report-promas/bookingkonvensionalpopulate',[
        'uses' => 'login\BookingKonvensional@populate',
        'as' => 'login.bookingkonvensionalpopulate'
    ]);
    Route::post('/report-promas/bookingkonvensionalpopulatemodal',[
        'uses' => 'login\BookingKonvensional@modal',
        'as' => 'login.bookingkonvensionalmodal'
    ]);
    Route::post('/report-promas/bookingkonvensionalpopulateajax',[
        'uses' => 'login\BookingKonvensional@ajax',
        'as' => 'login.bookingkonvensionalajax'
    ]);
    Route::get('bookingkonvensionalexcel',[
        'uses' => 'login\BookingKonvensional@excel',
        'as' => 'bookingkonvensionalexcel'
    ]);
    Route::get('bookingkonvensionaltxt',[
        'uses' => 'login\BookingKonvensional@download',
        'as' => 'bookingkonvensionaltxt'
    ]);

    // ==== Start Pelunasan Konvensional ====
    Route::get('/report-promas/pelunasankonvensional',[
        'uses' => 'login\PelunasanKonvensional@index',
        'as' => 'login.pelunasankonvensional'
    ]);
    Route::post('/report-promas/pelunasankonvensionalpopulate',[
        'uses' => 'login\PelunasanKonvensional@populate',
        'as' => 'login.pelunasankonvensionalpopulate'
    ]);
    Route::post('/report-promas/pelunasankonvensionalpopulatemodal',[
        'uses' => 'login\PelunasanKonvensional@modal',
        'as' => 'login.pelunasankonvensionalmodal'
    ]);
    Route::post('/report-promas/pelunasankonvensionalpopulateajax',[
        'uses' => 'login\PelunasanKonvensional@ajax',
        'as' => 'login.pelunasankonvensionalajax'
    ]);
    Route::get('pelunasankonvensionalexcel',[
        'uses' => 'login\PelunasanKonvensional@excel',
        'as' => 'pelunasankonvensionalexcel'
    ]);
    Route::get('pelunasankonvensionaltxt',[
        'uses' => 'login\PelunasanKonvensional@download',
        'as' => 'pelunasankonvensionaltxt'
    ]);

    // ==== Start Penjualan Konvensional ====
    Route::get('/report-promas/penjualankonvensional',[
        'uses' => 'login\PenjualanKonvensional@index',
        'as' => 'login.penjualankonvensional'
    ]);
    Route::post('/report-promas/penjualankonvensionalpopulate',[
        'uses' => 'login\PenjualanKonvensional@populate',
        'as' => 'login.penjualankonvensionalpopulate'
    ]);
    Route::post('/report-promas/penjualankonvensionalpopulatemodal',[
        'uses' => 'login\PenjualanKonvensional@modal',
        'as' => 'login.penjualankonvensionalmodal'
    ]);
    Route::post('/report-promas/penjualankonvensionalpopulateajax',[
        'uses' => 'login\PenjualanKonvensional@ajax',
        'as' => 'login.penjualankonvensionalajax'
    ]);
    Route::get('penjualankonvensionalexcel',[
        'uses' => 'login\PenjualanKonvensional@excel',
        'as' => 'penjualankonvensionalexcel'
    ]);
    Route::get('penjualankonvensionaltxt',[
        'uses' => 'login\JaminanKonvensional@download',
        'as' => 'penjualankonvensionaltxt'
    ]);

    // ==== Start Master Time Full Fill Harian ====
    Route::get('/parameter-finance/mastertimefullfill',[
        'uses' => 'login\MasterTimeFullFill@index',
        'as' => 'login.mastertimefullfill'
    ]);
    Route::post('/parameter-finance/mastertimefullfillpopulate',[
        'uses' => 'login\MasterTimeFullFill@populate',
        'as' => 'login.mastertimefullfillpopulate'
    ]);
    Route::post('/parameter-finance/mastertimefullfillpopulatemodal',[
        'uses' => 'login\MasterTimeFullFill@modal',
        'as' => 'login.mastertimefullfillmodal'
    ]);
    Route::post('/parameter-finance/mastertimefullfillpopulatepost',[
        'uses' => 'login\MasterTimeFullFill@proses',
        'as' => 'login.mastertimefullfillpost'
    ]);

    // ==== Start Master Coa ====
    Route::get('/parameter-finance/mastercoa',[
        'uses' => 'login\MasterCoa@index',
        'as' => 'login.mastercoa'
    ]);
    Route::post('/parameter-finance/mastercoapopulate',[
        'uses' => 'login\MasterCoa@populate',
        'as' => 'login.mastercoapopulate'
    ]);
    Route::post('/parameter-finance/mastercoapopulatemodal',[
        'uses' => 'login\MasterCoa@modal',
        'as' => 'login.mastercoamodal'
    ]);
    Route::post('/parameter-finance/mastercoapopulatepost',[
        'uses' => 'login\MasterCoa@proses',
        'as' => 'login.mastercoapost'
    ]);

    // ==== Start Master Bank Unit ====
    Route::get('/parameter-finance/masterbankunit',[
        'uses' => 'login\MasterBankUnit@index',
        'as' => 'login.masterbankunit'
    ]);
    Route::post('/parameter-finance/masterbankunitpopulate',[
        'uses' => 'login\MasterBankUnit@populate',
        'as' => 'login.masterbankunitpopulate'
    ]);
    Route::post('/parameter-finance/masterbankunitpopulatemodal',[
        'uses' => 'login\MasterBankUnit@modal',
        'as' => 'login.masterbankunitmodal'
    ]);
    Route::post('/parameter-finance/masterbankunitpopulatepost',[
        'uses' => 'login\MasterBankUnit@proses',
        'as' => 'login.masterbankunitpost'
    ]);


    // // ==== Start Data Borrower ====
    // Route::get('/pusdafil/databorrower',[
    //     'uses' => 'login\DataBorrower@index',
    //     'as' => 'login.databorrower'
    // ]);
    // Route::post('/pusdafil/databorrowerpopulate',[
    //     'uses' => 'login\DataBorrower@populate',
    //     'as' => 'login.databorrowerpopulate'
    // ]);
    // Route::post('/pusdafil/databorrowerpopulatemodal',[
    //     'uses' => 'login\DataBorrower@modal',
    //     'as' => 'login.databorrowermodal'
    // ]);

    // // ==== Start Data Pinjam Meminjam ====
    // Route::get('/pusdafil/datapinjammeminjam',[
    //     'uses' => 'login\DataPinjamMeminjam@index',
    //     'as' => 'login.datapinjammeminjam'
    // ]);
    // Route::post('/pusdafil/datapinjammeminjampopulate',[
    //     'uses' => 'login\DataPinjamMeminjam@populate',
    //     'as' => 'login.datapinjammeminjampopulate'
    // ]);
    // Route::post('/pusdafil/datapinjammeminjampopulatemodal',[
    //     'uses' => 'login\DataPinjamMeminjam@modal',
    //     'as' => 'login.datapinjammeminjammodal'
    // ]);

    // // ==== Start Data Pelunasan ====
    // Route::get('/pusdafil/datapelunasan',[
    //     'uses' => 'login\DataPelunasan@index',
    //     'as' => 'login.datapelunasan'
    // ]);
    // Route::post('/pusdafil/datapelunasanpopulate',[
    //     'uses' => 'login\DataPelunasan@populate',
    //     'as' => 'login.datapelunasanpopulate'
    // ]);
    // Route::post('/pusdafil/datapelunasanpopulatemodal',[
    //     'uses' => 'login\DataPelunasan@modal',
    //     'as' => 'login.datapelunasanmodal'
    // ]);

    // // ==== Start Data Logpusdafil ====
    // Route::get('/pusdafil/logpusdafil',[
    //     'uses' => 'login\Logpusdafil@index',
    //     'as' => 'login.logpusdafil'
    // ]);
    // Route::post('/pusdafil/logpusdafilpopulate',[
    //     'uses' => 'login\Logpusdafil@populate',
    //     'as' => 'login.logpusdafilpopulate'
    // ]);
    // Route::post('/pusdafil/logpusdafilpopulatemodal',[
    //     'uses' => 'login\Logpusdafil@modal',
    //     'as' => 'login.logpusdafilmodal'
    // ]);

    // // ==== Start Data Vendor ====
    // Route::get('/mastervendor/datavendor',[
    //     'uses' => 'login\DataVendor@index',
    //     'as' => 'login.datavendor'
    // ]);
    // Route::post('/mastervendor/datavendorpopulate',[
    //     'uses' => 'login\DataVendor@populate',
    //     'as' => 'login.datavendorpopulate'
    // ]);
    // Route::post('/mastervendor/datavendorpopulatemodal',[
    //     'uses' => 'login\DataVendor@modal',
    //     'as' => 'login.datavendormodal'
    // ]);
    // Route::post('/mastervendor/datavendorpopulatepost',[
    //     'uses' => 'login\DataVendor@proses',
    //     'as' => 'login.datavendorpost'
    // ]);

    // // ==== Start User Vendor ====
    // Route::get('/mastervendor/uservendor',[
    //     'uses' => 'login\UserVendor@index',
    //     'as' => 'login.uservendor'
    // ]);
    // Route::post('/mastervendor/uservendorpopulate',[
    //     'uses' => 'login\UserVendor@populate',
    //     'as' => 'login.uservendorpopulate'
    // ]);
    // Route::post('/mastervendor/uservendorpopulatemodal',[
    //     'uses' => 'login\UserVendor@modal',
    //     'as' => 'login.uservendormodal'
    // ]);
    // Route::post('/mastervendor/uservendorpopulatepost',[
    //     'uses' => 'login\UserVendor@proses',
    //     'as' => 'login.uservendorpost'
    // ]);

    // // ==== Start Master Menu Vendor ====
    // Route::get('/mastervendor/mastermenuvendor',[
    //     'uses' => 'login\MasterMenuVendor@index',
    //     'as' => 'login.mastermenuvendor'
    // ]);
    // Route::post('/mastervendor/mastermenuvendorpopulate',[
    //     'uses' => 'login\MasterMenuVendor@populate',
    //     'as' => 'login.mastermenuvendorpopulate'
    // ]);
    // Route::post('/mastervendor/mastermenuvendorpopulatemodal',[
    //     'uses' => 'login\MasterMenuVendor@modal',
    //     'as' => 'login.mastermenuvendormodal'
    // ]);
    // Route::post('/mastervendor/mastermenuvendorpopulatepost',[
    //     'uses' => 'login\MasterMenuVendor@proses',
    //     'as' => 'login.mastermenuvendorpost'
    // ]);

    // // ==== Start Kelompok Akses Vendor ====
    // Route::get('/mastervendor/kelompokaksesvendor',[
    //     'uses' => 'login\KelompokAksesVendor@index',
    //     'as' => 'login.kelompokaksesvendor'
    // ]);
    // Route::post('/mastervendor/kelompokaksesvendorpopulate',[
    //     'uses' => 'login\KelompokAksesVendor@populate',
    //     'as' => 'login.kelompokaksesvendorpopulate'
    // ]);
    // Route::post('/mastervendor/kelompokaksesvendorpopulatemodal',[
    //     'uses' => 'login\KelompokAksesVendor@modal',
    //     'as' => 'login.kelompokaksesvendormodal'
    // ]);
    // Route::post('/mastervendor/kelompokaksesvendorpopulatepost',[
    //     'uses' => 'login\KelompokAksesVendor@proses',
    //     'as' => 'login.kelompokaksesvendorpost'
    // ]);
    // Route::post('/mastervendor/kelompokaksesvendorpopulatepostakses',[
    //     'uses' => 'login\KelompokAksesVendor@prosesakses',
    //     'as' => 'login.kelompokaksesvendorpostakses'
    // ]);

    // // ==== Start Master Jenis Emas ====
    // Route::get('/mastervendor/masterjenisemas',[
    //     'uses' => 'login\MasterJenisEmas@index',
    //     'as' => 'login.masterjenisemas'
    // ]);
    // Route::post('/mastervendor/masterjenisemaspopulate',[
    //     'uses' => 'login\MasterJenisEmas@populate',
    //     'as' => 'login.masterjenisemaspopulate'
    // ]);
    // Route::post('/mastervendor/masterjenisemaspopulatemodal',[
    //     'uses' => 'login\MasterJenisEmas@modal',
    //     'as' => 'login.masterjenisemasmodal'
    // ]);
    // Route::post('/mastervendor/masterjenisemaspopulatepost',[
    //     'uses' => 'login\MasterJenisEmas@proses',
    //     'as' => 'login.masterjenisemaspost'
    // ]);

    // // ==== Start List Produk ====
    // Route::get('/mastervendor/listproduk',[
    //     'uses' => 'login\ListProduk@index',
    //     'as' => 'login.listproduk'
    // ]);
    // Route::post('/mastervendor/listprodukpopulate',[
    //     'uses' => 'login\ListProduk@populate',
    //     'as' => 'login.listprodukpopulate'
    // ]);
    // Route::post('/mastervendor/listprodukpopulatemodal',[
    //     'uses' => 'login\ListProduk@modal',
    //     'as' => 'login.listprodukmodal'
    // ]);
    // Route::post('/mastervendor/listprodukpopulatepost',[
    //     'uses' => 'login\ListProduk@proses',
    //     'as' => 'login.listprodukpost'
    // ]);

    // // ==== Start Bank Code ====
    // Route::get('/master-bni/bankcode',[
    //     'uses' => 'login\MasterBni@index',
    //     'as' => 'login.bankcode'
    // ]);
    // Route::post('/master-bni/bankcodepopulate',[
    //     'uses' => 'login\MasterBni@populate',
    //     'as' => 'login.bankcodepopulate'
    // ]);
    // Route::post('/master-bni/bankcodepopulatemodal',[
    //     'uses' => 'login\MasterBni@modal',
    //     'as' => 'login.bankcodemodal'
    // ]);
    // Route::post('/master-bni/bankcodepopulatepost',[
    //     'uses' => 'login\MasterBni@proses',
    //     'as' => 'login.bankcodepost'
    // ]);

    // // ==== Start Customer Title ====
    // Route::get('/master-bni/customertitle',[
    //     'uses' => 'login\BniCusttitle@index',
    //     'as' => 'login.custtitle'
    // ]);
    // Route::post('/master-bni/customertitlepopulate',[
    //     'uses' => 'login\BniCusttitle@populate',
    //     'as' => 'login.custtitlepopulate'
    // ]);
    // Route::post('/master-bni/customertitlepopulatemodal',[
    //     'uses' => 'login\BniCusttitle@modal',
    //     'as' => 'login.custtitlemodal'
    // ]);
    // Route::post('/master-bni/customertitlepopulatepost',[
    //     'uses' => 'login\BniCusttitle@proses',
    //     'as' => 'login.custtitlepost'
    // ]);

    // ///////BNI Branch Code///////
    // Route::get('/master-bni/branchcode',[
    //     'uses' => 'login\BniBranch@index',
    //     'as' => 'login.branchcode'
    // ]);
    // Route::get('/master-bni/branchcode/edit/{id}',[
    //     'uses' => 'login\BniBranch@getedit',
    //     'as' => 'login.branchcode.getedit'
    // ]);
    // Route::post('/master-bni/branchcode/postedit',[
    //     'uses' => 'login\BniBranch@postedit',
    //     'as' => 'login.branchcode.postedit'
    // ]);
    // Route::get('/master-bni/branchcode/new',[
    //     'uses' => 'login\BniBranch@getnew',
    //     'as' => 'login.branchcode.getnew'
    // ]);
    // Route::post('/master-bni/branchcode/new',[
    //     'uses' => 'login\BniBranch@postnew',
    //     'as' => 'login.branchcode.postnew'
    // ]);

    // ///////BNI Credential///////
    // Route::get('/master-bni/credential',[
    //     'uses' => 'login\BniCredential@index',
    //     'as' => 'login.credential'
    // ]);
    // Route::get('/master-bni/credential/edit/{id}',[
    //     'uses' => 'login\BniCredential@getedit',
    //     'as' => 'login.credential.getedit'
    // ]);
    // Route::post('/master-bni/credential/postedit',[
    //     'uses' => 'login\BniCredential@postedit',
    //     'as' => 'login.credential.postedit'
    // ]);
    // Route::get('/master-bni/credential/new',[
    //     'uses' => 'login\BniCredential@getnew',
    //     'as' => 'login.credential.getnew'
    // ]);
    // Route::post('/master-bni/credential/new',[
    //     'uses' => 'login\BniCredential@postnew',
    //     'as' => 'login.credential.postnew'
    // ]);

    // // ==== Start Customer Education ====
    // Route::get('/master-bni/customereducation',[
    //     'uses' => 'login\BniCustedu@index',
    //     'as' => 'login.custedu'
    // ]);
    // Route::post('/master-bni/customereducationpopulate',[
    //     'uses' => 'login\BniCustedu@populate',
    //     'as' => 'login.custedupopulate'
    // ]);
    // Route::post('/master-bni/customereducationpopulatemodal',[
    //     'uses' => 'login\BniCustedu@modal',
    //     'as' => 'login.custedumodal'
    // ]);
    // Route::post('/master-bni/customereducationpopulatepost',[
    //     'uses' => 'login\BniCustedu@proses',
    //     'as' => 'login.custedupost'
    // ]);

    // // ==== Start Customer Income ====
    // Route::get('/master-bni/customerincome',[
    //     'uses' => 'login\BniCustincome@index',
    //     'as' => 'login.custincome'
    // ]);
    // Route::post('/master-bni/customerincomepopulate',[
    //     'uses' => 'login\BniCustincome@populate',
    //     'as' => 'login.custincomepopulate'
    // ]);
    // Route::post('/master-bni/customerincomepopulatemodal',[
    //     'uses' => 'login\BniCustincome@modal',
    //     'as' => 'login.custincomemodal'
    // ]);
    // Route::post('/master-bni/customerincomepopulatepost',[
    //     'uses' => 'login\BniCustincome@proses',
    //     'as' => 'login.custincomepost'
    // ]);

    // // ==== Start Customer Job ====
    // Route::get('/master-bni/customerjob',[
    //     'uses' => 'login\BniCustjob@index',
    //     'as' => 'login.custjob'
    // ]);
    // Route::post('/master-bni/customerjobpopulate',[
    //     'uses' => 'login\BniCustjob@populate',
    //     'as' => 'login.custjobpopulate'
    // ]);
    // Route::post('/master-bni/customerjobpopulatemodal',[
    //     'uses' => 'login\BniCustjob@modal',
    //     'as' => 'login.custjobmodal'
    // ]);
    // Route::post('/master-bni/customerjobpopulatepost',[
    //     'uses' => 'login\BniCustjob@proses',
    //     'as' => 'login.custjobpost'
    // ]);

    // // ==== Start Customer Marital Status ====

    // Route::get('/master-bni/customermarital',[
    //     'uses' => 'login\BniCustmarital@index',
    //     'as' => 'login.custmarital'
    // ]);
    // Route::post('/master-bni/customermaritalpopulate',[
    //     'uses' => 'login\BniCustmarital@populate',
    //     'as' => 'login.custmaritalpopulate'
    // ]);
    // Route::post('/master-bni/customermaritalpopulatemodal',[
    //     'uses' => 'login\BniCustmarital@modal',
    //     'as' => 'login.custmaritalmodal'
    // ]);
    // Route::post('/master-bni/customermaritalpopulatepost',[
    //     'uses' => 'login\BniCustmarital@proses',
    //     'as' => 'login.custmaritalpost'
    // ]);

    // ///////BNI Customer Religion///////

    // Route::get('/master-bni/customerreligion',[
    //     'uses' => 'login\BniCustreligion@index',
    //     'as' => 'login.custreligion'
    // ]);
    // Route::get('/master-bni/customerreligion/edit/{id}',[
    //     'uses' => 'login\BniCustreligion@getedit',
    //     'as' => 'login.custreligion.getedit'
    // ]);
    // Route::post('/master-bni/customerreligion/postedit',[
    //     'uses' => 'login\BniCustreligion@postedit',
    //     'as' => 'login.custreligion.postedit'
    // ]);
    // Route::get('/master-bni/customerreligion/new',[
    //     'uses' => 'login\BniCustreligion@getnew',
    //     'as' => 'login.custreligion.getnew'
    // ]);
    // Route::post('/master-bni/customerreligion/new',[
    //     'uses' => 'login\BniCustreligion@postnew',
    //     'as' => 'login.custreligion.postnew'
    // ]);

    // /////////////////////////////////////////Master Agama/////////////////////////////////////////////////////////

    // Route::get('/master-parameter/masteragama',[
    //     'uses' => 'login\MasterAgama@index',
    //     'as' => 'login.masteragama'
    // ]);
    // Route::get('/master-parameter/masteragama/edit/{id}',[
    //     'uses' => 'login\MasterAgama@getedit',
    //     'as' => 'login.masteragama.getedit'
    // ]);
    // Route::post('/master-parameter/masteragama/postedit',[
    //     'uses' => 'login\MasterAgama@postedit',
    //     'as' => 'login.masteragama.postedit'
    // ]);
    // Route::get('/master-parameter/masteragama/new',[
    //     'uses' => 'login\MasterAgama@getnew',
    //     'as' => 'login.masteragama.getnew'
    // ]);
    // Route::post('/master-parameter/masteragama/new',[
    //     'uses' => 'login\MasterAgama@postnew',
    //     'as' => 'login.masteragama.postnew'
    // ]);

    // /////////////////////////////////////////////Master Jabatan////////////////////////////////////////////////

    // Route::get('/master-parameter/masterjabatan',[
    //     'uses' => 'login\MasterJabatan@index',
    //     'as' => 'login.masterjabatan'
    // ]);
    // Route::get('/master-parameter/masterjabatan/edit/{id}',[
    //     'uses' => 'login\MasterJabatan@getedit',
    //     'as' => 'login.masterjabatan.getedit'
    // ]);
    // Route::post('/master-parameter/masterjabatan/postedit',[
    //     'uses' => 'login\MasterJabatan@postedit',
    //     'as' => 'login.masterjabatan.postedit'
    // ]);
    // Route::get('/master-parameter/masterjabatan/new',[
    //     'uses' => 'login\MasterJabatan@getnew',
    //     'as' => 'login.masterjabatan.getnew'
    // ]);
    // Route::post('/master-parameter/masterjabatan/new',[
    //     'uses' => 'login\MasterJabatan@postnew',
    //     'as' => 'login.masterjabatan.postnew'
    // ]);

    // /////////////////////////////////////////////Master Jenis Kelamin////////////////////////////////////

    // Route::get('/master-parameter/masterjeniskelamin',[
    //     'uses' => 'login\MasterJeniskelamin@index',
    //     'as' => 'login.masterjeniskelamin'
    // ]);
    // Route::get('/master-parameter/masterjeniskelamin/edit/{id}',[
    //     'uses' => 'login\MasterJeniskelamin@getedit',
    //     'as' => 'login.masterjeniskelamin.getedit'
    // ]);
    // Route::post('/master-parameter/masterjeniskelamin/postedit',[
    //     'uses' => 'login\MasterJeniskelamin@postedit',
    //     'as' => 'login.masterjeniskelamin.postedit'
    // ]);
    // Route::get('/master-parameter/masterjeniskelamin/new',[
    //     'uses' => 'login\MasterJeniskelamin@getnew',
    //     'as' => 'login.masterjeniskelamin.getnew'
    // ]);
    // Route::post('/master-parameter/masterjeniskelamin/new',[
    //     'uses' => 'login\MasterJeniskelamin@postnew',
    //     'as' => 'login.masterjeniskelamin.postnew'
    // ]);

    // //////////////////////////////////////////////Master Provinsi///////////////////////////////////////////////

    // Route::get('/master-parameter/masterprovinsi',[
    //     'uses' => 'login\MasterProvinsi@index',
    //     'as' => 'login.masterprovinsi'
    // ]);
    // Route::get('/master-parameter/masterprovinsi/edit/{id}',[
    //     'uses' => 'login\MasterProvinsi@getedit',
    //     'as' => 'login.masterprovinsi.getedit'
    // ]);
    // Route::post('/master-parameter/masterprovinsi/postedit',[
    //     'uses' => 'login\MasterProvinsi@postedit',
    //     'as' => 'login.masterprovinsi.postedit'
    // ]);
    // Route::get('/master-parameter/masterprovinsi/new',[
    //     'uses' => 'login\MasterProvinsi@getnew',
    //     'as' => 'login.masterprovinsi.getnew'
    // ]);
    // Route::post('/master-parameter/masterprovinsi/new',[
    //     'uses' => 'login\MasterProvinsi@postnew',
    //     'as' => 'login.masterprovinsi.postnew'
    // ]);

    // /////////////////////////////////////Master Kabupaten/////////////////////////////////////////////////

    // Route::get('/master-parameter/masterkabupaten',[
    //     'uses' => 'login\MasterKabupaten@index',
    //     'as' => 'login.masterkabupaten'
    // ]);
    // Route::get('/master-parameter/masterkabupaten/edit/{id}',[
    //     'uses' => 'login\MasterKabupaten@getedit',
    //     'as' => 'login.masterkabupaten.getedit'
    // ]);
    // Route::post('/master-parameter/masterkabupaten/postedit',[
    //     'uses' => 'login\MasterKabupaten@postedit',
    //     'as' => 'login.masterkabupaten.postedit'
    // ]);
    // Route::get('/master-parameter/masterkabupaten/new',[
    //     'uses' => 'login\MasterKabupaten@getnew',
    //     'as' => 'login.masterkabupaten.getnew'
    // ]);
    // Route::post('/master-parameter/masterkabupaten/new',[
    //     'uses' => 'login\MasterKabupaten@postnew',
    //     'as' => 'login.masterkabupaten.postnew'
    // ]);

    // ////////////////////////////////////////////Master Pekerjaan/////////////////////////////////////////////////

    // Route::get('/master-parameter/masterpekerjaan',[
    //     'uses' => 'login\MasterPekerjaan@index',
    //     'as' => 'login.masterpekerjaan'
    // ]);
    // Route::get('/master-parameter/masterpekerjaan/edit/{id}',[
    //     'uses' => 'login\MasterPekerjaan@getedit',
    //     'as' => 'login.masterpekerjaan.getedit'
    // ]);
    // Route::post('/master-parameter/masterpekerjaan/postedit',[
    //     'uses' => 'login\MasterPekerjaan@postedit',
    //     'as' => 'login.masterpekerjaan.postedit'
    // ]);
    // Route::get('/master-parameter/masterpekerjaan/new',[
    //     'uses' => 'login\MasterPekerjaan@getnew',
    //     'as' => 'login.masterpekerjaan.getnew'
    // ]);
    // Route::post('/master-parameter/masterpekerjaan/new',[
    //     'uses' => 'login\MasterPekerjaan@postnew',
    //     'as' => 'login.masterpekerjaan.postnew'
    // ]);

    // //////////////////////////////////////////////Master Pendidikan////////////////////////////////////////////////

    // Route::get('/master-parameter/masterpendidikan',[
    //     'uses' => 'login\MasterPendidikan@index',
    //     'as' => 'login.masterpendidikan'
    // ]);
    // Route::get('/master-parameter/masterpendidikan/edit/{id}',[
    //     'uses' => 'login\MasterPendidikan@getedit',
    //     'as' => 'login.masterpendidikan.getedit'
    // ]);
    // Route::post('/master-parameter/masterpendidikan/postedit',[
    //     'uses' => 'login\MasterPendidikan@postedit',
    //     'as' => 'login.masterpendidikan.postedit'
    // ]);
    // Route::get('/master-parameter/masterpendidikan/new',[
    //     'uses' => 'login\MasterPendidikan@getnew',
    //     'as' => 'login.masterpendidikan.getnew'
    // ]);
    // Route::post('/master-parameter/masterpendidikan/new',[
    //     'uses' => 'login\MasterPendidikan@postnew',
    //     'as' => 'login.masterpendidikan.postnew'
    // ]);

    // //////////////////////////////////////////////Master Sektor Kelompok///////////////////////////////////////////////

    // Route::get('/master-parameter/mastersektorkelompok',[
    //     'uses' => 'login\MasterSektorkelompok@index',
    //     'as' => 'login.mastersektorkelompok'
    // ]);
    // Route::get('/master-parameter/mastersektorkelompok/edit/{id}',[
    //     'uses' => 'login\MasterSektorkelompok@getedit',
    //     'as' => 'login.mastersektorkelompok.getedit'
    // ]);
    // Route::post('/master-parameter/mastersektorkelompok/postedit',[
    //     'uses' => 'login\MasterSektorkelompok@postedit',
    //     'as' => 'login.mastersektorkelompok.postedit'
    // ]);
    // Route::get('/master-parameter/mastersektorkelompok/new',[
    //     'uses' => 'login\MasterSektorkelompok@getnew',
    //     'as' => 'login.mastersektorkelompok.getnew'
    // ]);
    // Route::post('/master-parameter/mastersektorkelompok/new',[
    //     'uses' => 'login\MasterSektorkelompok@postnew',
    //     'as' => 'login.mastersektorkelompok.postnew'
    // ]);

    // //////////////////////////////////////////////Master Sektor Ekonomi///////////////////////////////////////////////

    // Route::get('/master-parameter/mastersektorekonomi',[
    //     'uses' => 'login\MasterSektorekonomi@index',
    //     'as' => 'login.mastersektorekonomi'
    // ]);
    // Route::get('/master-parameter/mastersektorekonomi/edit/{id}',[
    //     'uses' => 'login\MasterSektorekonomi@getedit',
    //     'as' => 'login.mastersektorekonomi.getedit'
    // ]);
    // Route::post('/master-parameter/mastersektorekonomi/postedit',[
    //     'uses' => 'login\MasterSektorekonomi@postedit',
    //     'as' => 'login.mastersektorekonomi.postedit'
    // ]);
    // Route::get('/master-parameter/mastersektorekonomi/new',[
    //     'uses' => 'login\MasterSektorekonomi@getnew',
    //     'as' => 'login.mastersektorekonomi.getnew'
    // ]);
    // Route::post('/master-parameter/mastersektorekonomi/new',[
    //     'uses' => 'login\MasterSektorekonomi@postnew',
    //     'as' => 'login.mastersektorekonomi.postnew'
    // ]);

    // ////////////////////////////////////////////Master Status Perkawinan///////////////////////////////////////////////

    // Route::get('/master-parameter/masterstatusperkawinan',[
    //     'uses' => 'login\MasterStatusperkawinan@index',
    //     'as' => 'login.masterstatusperkawinan'
    // ]);
    // Route::get('/master-parameter/masterstatusperkawinan/edit/{id}',[
    //     'uses' => 'login\MasterStatusperkawinan@getedit',
    //     'as' => 'login.masterstatusperkawinan.getedit'
    // ]);
    // Route::post('/master-parameter/masterstatusperkawinan/postedit',[
    //     'uses' => 'login\MasterStatusperkawinan@postedit',
    //     'as' => 'login.masterstatusperkawinan.postedit'
    // ]);
    // Route::get('/master-parameter/masterstatusperkawinan/new',[
    //     'uses' => 'login\MasterStatusperkawinan@getnew',
    //     'as' => 'login.masterstatusperkawinan.getnew'
    // ]);
    // Route::post('/master-parameter/masterstatusperkawinan/new',[
    //     'uses' => 'login\MasterStatusperkawinan@postnew',
    //     'as' => 'login.masterstatusperkawinan.postnew'
    // ]);

    // //////////////////////////////////////////////Master Status Rumah///////////////////////////////////////////////

    // Route::get('/master-parameter/masterstatusrumah',[
    //     'uses' => 'login\MasterStatusrumah@index',
    //     'as' => 'login.masterstatusrumah'
    // ]);
    // Route::get('/master-parameter/masterstatusrumah/edit/{id}',[
    //     'uses' => 'login\MasterStatusrumah@getedit',
    //     'as' => 'login.masterstatusrumah.getedit'
    // ]);
    // Route::post('/master-parameter/masterstatusrumah/postedit',[
    //     'uses' => 'login\MasterStatusrumah@postedit',
    //     'as' => 'login.masterstatusrumah.postedit'
    // ]);
    // Route::get('/master-parameter/masterstatusrumah/new',[
    //     'uses' => 'login\MasterStatusrumah@getnew',
    //     'as' => 'login.masterstatusrumah.getnew'
    // ]);
    // Route::post('/master-parameter/masterstatusrumah/new',[
    //     'uses' => 'login\MasterStatusrumah@postnew',
    //     'as' => 'login.masterstatusrumah.postnew'
    // ]);

    // //////////////////////////////////////////////Master Sumber Dana///////////////////////////////////////////////

    // Route::get('/master-parameter/mastersumberdana',[
    //     'uses' => 'login\MasterSumberdana@index',
    //     'as' => 'login.mastersumberdana'
    // ]);
    // Route::get('/master-parameter/mastersumberdana/edit/{id}',[
    //     'uses' => 'login\MasterSumberdana@getedit',
    //     'as' => 'login.mastersumberdana.getedit'
    // ]);
    // Route::post('/master-parameter/mastersumberdana/postedit',[
    //     'uses' => 'login\MasterSumberdana@postedit',
    //     'as' => 'login.mastersumberdana.postedit'
    // ]);
    // Route::get('/master-parameter/mastersumberdana/new',[
    //     'uses' => 'login\MasterSumberdana@getnew',
    //     'as' => 'login.mastersumberdana.getnew'
    // ]);
    // Route::post('/master-parameter/mastersumberdana/new',[
    //     'uses' => 'login\MasterSumberdana@postnew',
    //     'as' => 'login.mastersumberdana.postnew'
    // ]);

    // ////////////////////////////////////////////Master Jenis Barang///////////////////////////////////////

    // Route::get('/master-parameter/masterjenisbarang',[
    //     'uses' => 'login\MasterJenisbarang@index',
    //     'as' => 'login.masterjenisbarang'
    // ]);
    // Route::get('/master-parameter/masterjenisbarang/edit/{id}',[
    //     'uses' => 'login\MasterJenisbarang@getedit',
    //     'as' => 'login.masterjenisbarang.getedit'
    // ]);
    // Route::post('/master-parameter/masterjenisbarang/postedit',[
    //     'uses' => 'login\MasterJenisbarang@postedit',
    //     'as' => 'login.masterjenisbarang.postedit'
    // ]);
    // Route::get('/master-parameter/masterjenisbarang/new',[
    //     'uses' => 'login\MasterJenisbarang@getnew',
    //     'as' => 'login.masterjenisbarang.getnew'
    // ]);
    // Route::post('/master-parameter/masterjenisbarang/new',[
    //     'uses' => 'login\MasterJenisbarang@postnew',
    //     'as' => 'login.masterjenisbarang.postnew'
    // ]);

    // //////////////////////////////////////////////Master OVD/////////////////////////////////////////////////

    // Route::get('/master-parameter/masterovd',[
    //     'uses' => 'login\MasterOvd@index',
    //     'as' => 'login.masterovd'
    // ]);
    // Route::get('/master-parameter/masterovd/edit/{id}',[
    //     'uses' => 'login\MasterOvd@getedit',
    //     'as' => 'login.masterovd.getedit'
    // ]);
    // Route::post('/master-parameter/masterovd/postedit',[
    //     'uses' => 'login\MasterOvd@postedit',
    //     'as' => 'login.masterovd.postedit'
    // ]);
    // Route::get('/master-parameter/masterovd/new',[
    //     'uses' => 'login\MasterOvd@getnew',
    //     'as' => 'login.masterovd.getnew'
    // ]);
    // Route::post('/master-parameter/masterovd/new',[
    //     'uses' => 'login\MasterOvd@postnew',
    //     'as' => 'login.masterovd.postnew'
    // ]);

    // //mitra stle
    // Route::get('/master-parameter/mitrastle',[
    //     'uses' => 'login\MitraStle@index',
    //     'as' => 'login.mitrastle'
    // ]);
    // Route::get('/master-parameter/mitrastle/edit/{id}',[
    //     'uses' => 'login\MitraStle@getedit',
    //     'as' => 'login.mitrastle.getedit'
    // ]);
    // Route::post('/master-parameter/mitrastle/postedit',[
    //     'uses' => 'login\MitraStle@postedit',
    //     'as' => 'login.mitrastle.postedit'
    // ]);
    // Route::get('/master-parameter/mitrastle/new',[
    //     'uses' => 'login\MitraStle@getnew',
    //     'as' => 'login.mitrastle.getnew'
    // ]);
    // Route::post('/master-parameter/mitrastle/new',[
    //     'uses' => 'login\MitraStle@postnew',
    //     'as' => 'login.mitrastle.postnew'
    // ]);

    // // ==== Harga Pasar ====
    // Route::get('/master-parameter/hargapasar',[
    //     'uses' => 'login\HargaPasar@index',
    //     'as' => 'login.hargapasar'
    // ]);
    // Route::post('/master-parameter/hargapasarpopulate',[
    //     'uses' => 'login\HargaPasar@populate',
    //     'as' => 'login.hargapasarpopulate'
    // ]);
    // Route::post('/master-parameter/hargapasarpopulatemodal',[
    //     'uses' => 'login\HargaPasar@modal',
    //     'as' => 'login.hargapasarmodal'
    // ]);
    // Route::post('/master-parameter/hargapasarpopulatepost',[
    //     'uses' => 'login\HargaPasar@proses',
    //     'as' => 'login.hargapasarpost'
    // ]);



    // // ==== Start Mitra ====
    // Route::get('/master-mitra/membermitra',[
    //     'uses' => 'login\MemberMitra@index',
    //     'as' => 'login.membermitra'
    // ]);
    // Route::post('/master-mitra/membermitrapopulate',[
    //     'uses' => 'login\MemberMitra@populate',
    //     'as' => 'login.membermitrapopulate'
    // ]);
    // Route::post('/master-mitra/membermitrapopulatemodal',[
    //     'uses' => 'login\MemberMitra@modal',
    //     'as' => 'login.membermitramodal'
    // ]);
    // Route::post('/master-mitra/membermitrapopulateajax',[
    //     'uses' => 'login\MemberMitra@ajax',
    //     'as' => 'login.membermitraajax'
    // ]);
    // Route::post('/master-mitra/membermitrapopulatepost',[
    //     'uses' => 'login\MemberMitra@proses',
    //     'as' => 'login.membermitrapost'
    // ]);

    // // ==== Start Mitra Company ====
    // Route::get('/master-mitra/membercompany',[
    //     'uses' => 'login\MemberCompany@index',
    //     'as' => 'login.membercompany'
    // ]);
    // Route::post('/master-mitra/membercompanypopulate',[
    //     'uses' => 'login\MemberCompany@populate',
    //     'as' => 'login.membercompanypopulate'
    // ]);
    // Route::post('/master-mitra/membercompanypopulatemodal',[
    //     'uses' => 'login\MemberCompany@modal',
    //     'as' => 'login.membercompanymodal'
    // ]);
    // Route::post('/master-mitra/membercompanypopulateajax',[
    //     'uses' => 'login\MemberCompany@ajax',
    //     'as' => 'login.membercompanyajax'
    // ]);
    // Route::post('/master-mitra/membercompanypopulatepost',[
    //     'uses' => 'login\MemberCompany@proses',
    //     'as' => 'login.membercompanypost'
    // ]);

    // // ==== Start Mitra Branch ====
    // Route::get('/master-mitra/memberbranch',[
    //     'uses' => 'login\MemberBranch@index',
    //     'as' => 'login.memberbranch'
    // ]);
    // Route::post('/master-mitra/memberbranchpopulate',[
    //     'uses' => 'login\MemberBranch@populate',
    //     'as' => 'login.memberbranchpopulate'
    // ]);
    // Route::post('/master-mitra/memberbranchpopulatemodal',[
    //     'uses' => 'login\MemberBranch@modal',
    //     'as' => 'login.memberbranchmodal'
    // ]);
    // Route::post('/master-mitra/memberbranchpopulateajax',[
    //     'uses' => 'login\MemberBranch@ajax',
    //     'as' => 'login.memberbranchajax'
    // ]);
    // Route::post('/master-mitra/memberbranchpopulatepost',[
    //     'uses' => 'login\MemberBranch@proses',
    //     'as' => 'login.memberbranchpost'
    // ]);

    // // ==== Start Mitra Limit ====
    // Route::get('/master-mitra/mitralimit',[
    //     'uses' => 'login\MitraLimit@index',
    //     'as' => 'login.mitralimit'
    // ]);
    // Route::post('/master-mitra/mitralimitpopulate',[
    //     'uses' => 'login\MitraLimit@populate',
    //     'as' => 'login.mitralimitpopulate'
    // ]);
    // Route::post('/master-mitra/mitralimitpopulatemodal',[
    //     'uses' => 'login\MitraLimit@modal',
    //     'as' => 'login.mitralimitmodal'
    // ]);
    // Route::post('/master-mitra/mitralimitpopulateajax',[
    //     'uses' => 'login\MitraLimit@ajax',
    //     'as' => 'login.mitralimitajax'
    // ]);
    // Route::post('/master-mitra/mitralimitpopulatepost',[
    //     'uses' => 'login\MitraLimit@proses',
    //     'as' => 'login.mitralimitpost'
    // ]);

    // // ==== Start Middle Man ====
    // Route::get('/master-mitra/middleman',[
    //     'uses' => 'login\MiddleMan@index',
    //     'as' => 'login.middleman'
    // ]);
    // Route::post('/master-mitra/middlemanpopulate',[
    //     'uses' => 'login\MiddleMan@populate',
    //     'as' => 'login.middlemanpopulate'
    // ]);
    // Route::post('/master-mitra/middlemanpopulatemodal',[
    //     'uses' => 'login\MiddleMan@modal',
    //     'as' => 'login.middlemanmodal'
    // ]);
    // Route::post('/master-mitra/middlemanpopulateajax',[
    //     'uses' => 'login\MiddleMan@ajax',
    //     'as' => 'login.middlemanajax'
    // ]);
    // Route::post('/master-mitra/middlemanpopulatepost',[
    //     'uses' => 'login\MiddleMan@proses',
    //     'as' => 'login.middlemanpost'
    // ]);

    // // ==== Start Spesial Borrower ====
    // Route::get('/master-mitra/spesialborrower',[
    //     'uses' => 'login\SpesialBorrower@index',
    //     'as' => 'login.spesialborrower'
    // ]);
    // Route::post('/master-mitra/spesialborrowerpopulate',[
    //     'uses' => 'login\SpesialBorrower@populate',
    //     'as' => 'login.spesialborrowerpopulate'
    // ]);
    // Route::post('/master-mitra/spesialborrowerpopulatemodal',[
    //     'uses' => 'login\SpesialBorrower@modal',
    //     'as' => 'login.spesialborrowermodal'
    // ]);
    // Route::post('/master-mitra/spesialborrowerpopulateajax',[
    //     'uses' => 'login\SpesialBorrower@ajax',
    //     'as' => 'login.spesialborrowerajax'
    // ]);
    // Route::post('/master-mitra/spesialborrowerpopulatepost',[
    //     'uses' => 'login\SpesialBorrower@proses',
    //     'as' => 'login.spesialborrowerpost'
    // ]);
    // Route::get('spesialborrowerexcel',[
    //     'uses' => 'login\SpesialBorrower@excel',
    //     'as' => 'spesialborrowerexcel'
    // ]);

    // // ==== Start Spesial Rate ====
    // Route::get('/master-mitra/spesialrate',[
    //     'uses' => 'login\SpesialRate@index',
    //     'as' => 'login.spesialrate'
    // ]);
    // Route::post('/master-mitra/spesialratepopulate',[
    //     'uses' => 'login\SpesialRate@populate',
    //     'as' => 'login.spesialratepopulate'
    // ]);
    // Route::post('/master-mitra/spesialratepopulatemodal',[
    //     'uses' => 'login\SpesialRate@modal',
    //     'as' => 'login.spesialratemodal'
    // ]);
    // Route::post('/master-mitra/spesialratepopulateajax',[
    //     'uses' => 'login\SpesialRate@ajax',
    //     'as' => 'login.spesialrateajax'
    // ]);
    // Route::post('/master-mitra/spesialratepopulatepost',[
    //     'uses' => 'login\SpesialRate@proses',
    //     'as' => 'login.spesialratepost'
    // ]);
    // Route::get('spesialrateexcel',[
    //     'uses' => 'login\SpesialRate@excel',
    //     'as' => 'spesialrateexcel'
    // ]);

    // // ==== Start Toko Emas ====
    // Route::get('/master-mitra/tokoemas',[
    //     'uses' => 'login\TokoEmas@index',
    //     'as' => 'login.tokoemas'
    // ]);
    // Route::post('/master-mitra/tokoemaspopulate',[
    //     'uses' => 'login\TokoEmas@populate',
    //     'as' => 'login.tokoemaspopulate'
    // ]);
    // Route::post('/master-mitra/tokoemaspopulatemodal',[
    //     'uses' => 'login\TokoEmas@modal',
    //     'as' => 'login.tokoemasmodal'
    // ]);
    // Route::post('/master-mitra/tokoemaspopulatepost',[
    //     'uses' => 'login\TokoEmas@proses',
    //     'as' => 'login.tokoemaspost'
    // ]);

    // // ==== Start Data Produk ====
    // Route::get('/master-produk/produk',[
    //     'uses' => 'login\Produk@index',
    //     'as' => 'login.produk'
    // ]);
    // Route::post('/master-produk/produkpopulate',[
    //     'uses' => 'login\Produk@populate',
    //     'as' => 'login.produkpopulate'
    // ]);
    // Route::post('/master-produk/produkpopulatemodal',[
    //     'uses' => 'login\Produk@modal',
    //     'as' => 'login.produkmodal'
    // ]);
    // Route::post('/master-produk/produkpopulatepost',[
    //     'uses' => 'login\Produk@proses',
    //     'as' => 'login.produkpost'
    // ]);

    // // ==== Start Admin Produk ====
    // Route::get('/master-produk/adminproduk',[
    //     'uses' => 'login\ProdukAdmin@index',
    //     'as' => 'login.adminproduk'
    // ]);
    // Route::post('/master-produk/adminprodukpopulate',[
    //     'uses' => 'login\ProdukAdmin@populate',
    //     'as' => 'login.adminprodukpopulate'
    // ]);
    // Route::post('/master-produk/adminprodukpopulatemodal',[
    //     'uses' => 'login\ProdukAdmin@modal',
    //     'as' => 'login.adminprodukmodal'
    // ]);
    // Route::post('/master-produk/adminprodukpopulatepost',[
    //     'uses' => 'login\ProdukAdmin@proses',
    //     'as' => 'login.adminprodukpost'
    // ]);

    // // ==== Start Master Rate ====
    // Route::get('/master-produk/masterrate',[
    //     'uses' => 'login\MasterRate@index',
    //     'as' => 'login.masterrate'
    // ]);
    // Route::post('/master-produk/masterratepopulate',[
    //     'uses' => 'login\MasterRate@populate',
    //     'as' => 'login.masterratepopulate'
    // ]);
    // Route::post('/master-produk/masterratepopulatemodal',[
    //     'uses' => 'login\MasterRate@modal',
    //     'as' => 'login.masterratemodal'
    // ]);
    // Route::post('/master-produk/masterratepopulatepost',[
    //     'uses' => 'login\MasterRate@proses',
    //     'as' => 'login.masterratepost'
    // ]);

    // // ==== Start Rate Produk ====
    // Route::get('/master-produk/rateproduk',[
    //     'uses' => 'login\RateProduk@index',
    //     'as' => 'login.rateproduk'
    // ]);
    // Route::post('/master-produk/rateprodukpopulate',[
    //     'uses' => 'login\RateProduk@populate',
    //     'as' => 'login.rateprodukpopulate'
    // ]);
    // Route::post('/master-produk/rateprodukpopulatemodal',[
    //     'uses' => 'login\RateProduk@modal',
    //     'as' => 'login.rateprodukmodal'
    // ]);
    // Route::post('/master-produk/rateprodukpopulatepost',[
    //     'uses' => 'login\RateProduk@proses',
    //     'as' => 'login.rateprodukpost'
    // ]);

    // // ==== Start Rate Produk Simulasi ====
    // Route::get('/master-produk/rateproduksimulasi',[
    //     'uses' => 'login\RateProdukSimulasi@index',
    //     'as' => 'login.rateproduksimulasi'
    // ]);
    // Route::post('/master-produk/rateproduksimulasipopulate',[
    //     'uses' => 'login\RateProdukSimulasi@populate',
    //     'as' => 'login.rateproduksimulasipopulate'
    // ]);
    // Route::post('/master-produk/rateproduksimulasipopulatemodal',[
    //     'uses' => 'login\RateProdukSimulasi@modal',
    //     'as' => 'login.rateproduksimulasimodal'
    // ]);
    // Route::post('/master-produk/rateproduksimulasipopulatepost',[
    //     'uses' => 'login\RateProdukSimulasi@proses',
    //     'as' => 'login.rateproduksimulasipost'
    // ]);

    // // ==== Start Testimoni ====
    // Route::get('/master-website/testimoni',[
    //     'uses' => 'login\Testimoni@index',
    //     'as' => 'login.testimoni'
    // ]);
    // Route::post('/master-website/testimonipopulate',[
    //     'uses' => 'login\Testimoni@populate',
    //     'as' => 'login.testimonipopulate'
    // ]);
    // Route::post('/master-website/testimonipopulatemodal',[
    //     'uses' => 'login\Testimoni@modal',
    //     'as' => 'login.testimonimodal'
    // ]);
    // Route::post('/master-website/testimonipopulatepost',[
    //     'uses' => 'login\Testimoni@proses',
    //     'as' => 'login.testimonipost'
    // ]);

    // // ==== Start Liputan ====
    // Route::get('/master-website/liputan',[
    //     'uses' => 'login\Liputan@index',
    //     'as' => 'login.liputan'
    // ]);
    // Route::post('/master-website/liputanpopulate',[
    //     'uses' => 'login\Liputan@populate',
    //     'as' => 'login.liputanpopulate'
    // ]);
    // Route::post('/master-website/liputanpopulatemodal',[
    //     'uses' => 'login\Liputan@modal',
    //     'as' => 'login.liputanmodal'
    // ]);
    // Route::post('/master-website/liputanpopulatepost',[
    //     'uses' => 'login\Liputan@proses',
    //     'as' => 'login.liputanpost'
    // ]);

    // // ==== Start Keuntungan ====
    // Route::get('/master-website/keuntungan',[
    //     'uses' => 'login\Keuntungan@index',
    //     'as' => 'login.keuntungan'
    // ]);
    // Route::post('/master-website/keuntunganpopulate',[
    //     'uses' => 'login\Keuntungan@populate',
    //     'as' => 'login.keuntunganpopulate'
    // ]);
    // Route::post('/master-website/keuntunganpopulatemodal',[
    //     'uses' => 'login\Keuntungan@modal',
    //     'as' => 'login.keuntunganmodal'
    // ]);
    // Route::post('/master-website/keuntunganpopulatepost',[
    //     'uses' => 'login\Keuntungan@proses',
    //     'as' => 'login.keuntunganpost'
    // ]);

    // // ==== Start Profil ====
    // Route::get('/master-website/profil',[
    //     'uses' => 'login\Profil@index',
    //     'as' => 'login.profil'
    // ]);
    // Route::post('/master-website/profilpopulate',[
    //     'uses' => 'login\Profil@populate',
    //     'as' => 'login.profilpopulate'
    // ]);
    // Route::post('/master-website/profilpopulatemodal',[
    //     'uses' => 'login\Profil@modal',
    //     'as' => 'login.profilmodal'
    // ]);
    // Route::post('/master-website/profilpopulatepost',[
    //     'uses' => 'login\Profil@proses',
    //     'as' => 'login.profilpost'
    // ]);

    // // ==== Start FAQ Header ====
    // Route::get('/master-website/faqheader',[
    //     'uses' => 'login\FaqHeader@index',
    //     'as' => 'login.faqheader'
    // ]);
    // Route::post('/master-website/faqheaderpopulate',[
    //     'uses' => 'login\FaqHeader@populate',
    //     'as' => 'login.faqheaderpopulate'
    // ]);
    // Route::post('/master-website/faqheaderpopulatemodal',[
    //     'uses' => 'login\FaqHeader@modal',
    //     'as' => 'login.faqheadermodal'
    // ]);
    // Route::post('/master-website/faqheaderpopulatepost',[
    //     'uses' => 'login\FaqHeader@proses',
    //     'as' => 'login.faqheaderpost'
    // ]);

    // // ==== Start FAQ Content ====
    // Route::get('/master-website/faqcontent',[
    //     'uses' => 'login\FaqContent@index',
    //     'as' => 'login.faqcontent'
    // ]);
    // Route::post('/master-website/faqcontentpopulate',[
    //     'uses' => 'login\FaqContent@populate',
    //     'as' => 'login.faqcontentpopulate'
    // ]);
    // Route::post('/master-website/faqcontentpopulatemodal',[
    //     'uses' => 'login\FaqContent@modal',
    //     'as' => 'login.faqcontentmodal'
    // ]);
    // Route::post('/master-website/faqcontentpopulatepost',[
    //     'uses' => 'login\FaqContent@proses',
    //     'as' => 'login.faqcontentpost'
    // ]);
    // Route::post('/master-website/faqcontentpopulateposttop',[
    //     'uses' => 'login\FaqContent@prosestop',
    //     'as' => 'login.faqcontentposttop'
    // ]);

    // // ==== Start Daftar Super Lender ====
    // Route::get('/superlender/daftarsuper',[
    //     'uses' => 'login\DaftarSuper@index',
    //     'as' => 'login.daftarsuper'
    // ]);
    // Route::post('/superlender/daftarsuperpopulate',[
    //     'uses' => 'login\DaftarSuper@populate',
    //     'as' => 'login.daftarsuperpopulate'
    // ]);
    // Route::post('/superlender/daftarsuperpopulatemodal',[
    //     'uses' => 'login\DaftarSuper@modal',
    //     'as' => 'login.daftarsupermodal'
    // ]);
    // Route::post('/superlender/daftarsuperpopulatepost',[
    //     'uses' => 'login\DaftarSuper@proses',
    //     'as' => 'login.daftarsuperpost'
    // ]);

    // // ==== Start Deposit Super Lender ====
    // Route::get('/superlender/depositsuperlender',[
    //     'uses' => 'login\DepositSuperLender@index',
    //     'as' => 'login.depositsuperlender'
    // ]);
    // Route::post('/superlender/depositsuperlenderpopulate',[
    //     'uses' => 'login\DepositSuperLender@populate',
    //     'as' => 'login.depositsuperlenderpopulate'
    // ]);
    // Route::post('/superlender/depositsuperlenderpopulatemodal',[
    //     'uses' => 'login\DepositSuperLender@modal',
    //     'as' => 'login.depositsuperlendermodal'
    // ]);
    // Route::post('/superlender/depositsuperlenderpopulatepost',[
    //     'uses' => 'login\DepositSuperLender@proses',
    //     'as' => 'login.depositsuperlenderpost'
    // ]);

    // // ==== Start Saldo Super Lender ====
    // Route::get('/superlender/saldosuper',[
    //     'uses' => 'login\SaldoSuper@index',
    //     'as' => 'login.saldosuper'
    // ]);
    // Route::post('/superlender/saldosuperpopulate',[
    //     'uses' => 'login\SaldoSuper@populate',
    //     'as' => 'login.saldosuperpopulate'
    // ]);
    // Route::post('/superlender/saldosuperpopulatemodal',[
    //     'uses' => 'login\SaldoSuper@modal',
    //     'as' => 'login.saldosupermodal'
    // ]);
    // Route::post('/superlender/saldosuperpopulatepost',[
    //     'uses' => 'login\SaldoSuper@proses',
    //     'as' => 'login.saldosuperpost'
    // ]);

    // Route::get('/testemail',[
    //     'uses' => 'login\TestEmail@index',
    //     'as' => 'login.testemail'
    // ]);

    // // ==== Start Perubahan Data Lender ====
    // Route::get('/it-operasional/perubahandatalender',[
    //     'uses' => 'login\PerubahanDataLender@index',
    //     'as' => 'login.perubahandatalender'
    // ]);
    // Route::post('/it-operasional/perubahandatalenderpopulate',[
    //     'uses' => 'login\PerubahanDataLender@populate',
    //     'as' => 'login.perubahandatalenderpopulate'
    // ]);
    // Route::post('/it-operasional/perubahandatalenderpopulatemodal',[
    //     'uses' => 'login\PerubahanDataLender@modal',
    //     'as' => 'login.perubahandatalendermodal'
    // ]);
    // Route::post('/it-operasional/perubahandatalenderpopulatepost',[
    //     'uses' => 'login\PerubahanDataLender@proses',
    //     'as' => 'login.perubahandatalenderpost'
    // ]);

    // // ==== Start Perubahan Data WD ====
    // Route::get('/it-operasional/perubahandatawd',[
    //     'uses' => 'login\PerubahanDataWD@index',
    //     'as' => 'login.perubahandatawd'
    // ]);
    // Route::post('/it-operasional/perubahandatawdpopulate',[
    //     'uses' => 'login\PerubahanDataWD@populate',
    //     'as' => 'login.perubahandatawdpopulate'
    // ]);
    // Route::post('/it-operasional/perubahandatawdpopulatemodal',[
    //     'uses' => 'login\PerubahanDataWD@modal',
    //     'as' => 'login.perubahandatawdmodal'
    // ]);
    // Route::post('/it-operasional/perubahandatawdpopulatepost',[
    //     'uses' => 'login\PerubahanDataWD@proses',
    //     'as' => 'login.perubahandatawdpost'
    // ]);

    // // ==== Start Delete Barcode ====
    // Route::get('/it-operasional/deletebarcode',[
    //     'uses' => 'login\DeleteBarcode@index',
    //     'as' => 'login.deletebarcode'
    // ]);
    // Route::post('/it-operasional/deletebarcodepopulate',[
    //     'uses' => 'login\DeleteBarcode@populate',
    //     'as' => 'login.deletebarcodepopulate'
    // ]);
    // Route::post('/it-operasional/deletebarcodepopulatemodal',[
    //     'uses' => 'login\DeleteBarcode@modal',
    //     'as' => 'login.deletebarcodemodal'
    // ]);
    // Route::post('/it-operasional/deletebarcodepopulatepost',[
    //     'uses' => 'login\DeleteBarcode@proses',
    //     'as' => 'login.deletebarcodepost'
    // ]);

    // // ==== Start Proses Mitra ====
    // Route::get('/it-operasional/prosesmitra',[
    //     'uses' => 'login\ProsesMitra@index',
    //     'as' => 'login.prosesmitra'
    // ]);
    // Route::post('/it-operasional/prosesmitrapopulate',[
    //     'uses' => 'login\ProsesMitra@populate',
    //     'as' => 'login.prosesmitrapopulate'
    // ]);
    // Route::post('/it-operasional/prosesmitrapopulatemodal',[
    //     'uses' => 'login\ProsesMitra@modal',
    //     'as' => 'login.prosesmitramodal'
    // ]);
    // Route::post('/it-operasional/prosesmitrapopulatepost',[
    //     'uses' => 'login\ProsesMitra@proses',
    //     'as' => 'login.prosesmitrapost'
    // ]);

    // // ==== Rekon Saldo Lender ====
    // Route::get('/it-operasional/rekonsaldolender',[
    //     'uses' => 'login\RekonSaldoLender@index',
    //     'as' => 'login.rekonsaldolender'
    // ]);
    // Route::post('/it-operasional/rekonsaldolenderpopulate',[
    //     'uses' => 'login\RekonSaldoLender@populate',
    //     'as' => 'login.rekonsaldolenderpopulate'
    // ]);
    // Route::post('/it-operasional/rekonsaldolenderpopulatemodal',[
    //     'uses' => 'login\RekonSaldoLender@modal',
    //     'as' => 'login.rekonsaldolendermodal'
    // ]);
    // Route::post('/it-operasional/rekonsaldolenderpopulatepost',[
    //     'uses' => 'login\RekonSaldoLender@proses',
    //     'as' => 'login.rekonsaldolenderpost'
    // ]);

    // // ==== Update Foto Jaminan ====
    // Route::get('/it-operasional/updatefotojaminan',[
    //     'uses' => 'login\UpdateFotoJaminan@index',
    //     'as' => 'login.updatefotojaminan'
    // ]);
    // Route::post('/it-operasional/updatefotojaminanpopulate',[
    //     'uses' => 'login\UpdateFotoJaminan@populate',
    //     'as' => 'login.updatefotojaminanpopulate'
    // ]);
    // Route::post('/it-operasional/updatefotojaminanpopulatemodal',[
    //     'uses' => 'login\UpdateFotoJaminan@modal',
    //     'as' => 'login.updatefotojaminanmodal'
    // ]);
    // Route::post('/it-operasional/updatefotojaminanpopulatepost',[
    //     'uses' => 'login\UpdateFotoJaminan@proses',
    //     'as' => 'login.updatefotojaminanpost'
    // ]);

    // // ==== Start Perubahan Data Borrower ====
    // Route::get('/customerservice/perubahandataborrower',[
    //     'uses' => 'login\PerubahanDataBorrower@index',
    //     'as' => 'login.perubahandataborrower'
    // ]);
    // Route::post('/customerservice/perubahandataborrowerpopulate',[
    //     'uses' => 'login\PerubahanDataBorrower@populate',
    //     'as' => 'login.perubahandataborrowerpopulate'
    // ]);
    // Route::post('/customerservice/perubahandataborrowerpopulatemodal',[
    //     'uses' => 'login\PerubahanDataBorrower@modal',
    //     'as' => 'login.perubahandataborrowermodal'
    // ]);
    // Route::post('/customerservice/perubahandataborrowerpopulatepost',[
    //     'uses' => 'login\PerubahanDataBorrower@proses',
    //     'as' => 'login.perubahandataborrowerpost'
    // ]);

    // // ==== Start Penggunaan Voucher ====
    // Route::get('/customerservice/penggunaanvoucher',[
    //     'uses' => 'login\PenggunaanVoucher@index',
    //     'as' => 'login.penggunaanvoucher'
    // ]);
    // Route::post('/customerservice/penggunaanvoucherpopulate',[
    //     'uses' => 'login\PenggunaanVoucher@populate',
    //     'as' => 'login.penggunaanvoucherpopulate'
    // ]);
    // Route::post('/customerservice/penggunaanvoucherpopulatemodal',[
    //     'uses' => 'login\PenggunaanVoucher@modal',
    //     'as' => 'login.penggunaanvouchermodal'
    // ]);
    // Route::post('/customerservice/penggunaanvoucherpopulatepost',[
    //     'uses' => 'login\PenggunaanVoucher@proses',
    //     'as' => 'login.penggunaanvoucherpost'
    // ]);
    // Route::post('/customerservice/penggunaanvoucherpopulateajax',[
    //     'uses' => 'login\PenggunaanVoucher@ajax',
    //     'as' => 'login.penggunaanvoucherajax'
    // ]);
    // Route::get('penggunaanvoucherexcel',[
    //     'uses' => 'login\PenggunaanVoucher@excel',
    //     'as' => 'penggunaanvoucherexcel'
    // ]);

    // // ==== Start Lock Device ====
    // Route::get('/customerservice/lockdevice',[
    //     'uses' => 'login\LockDevice@index',
    //     'as' => 'login.lockdevice'
    // ]);
    // Route::post('/customerservice/lockdevicepopulate',[
    //     'uses' => 'login\LockDevice@populate',
    //     'as' => 'login.lockdevicepopulate'
    // ]);
    // Route::post('/customerservice/lockdevicepopulatemodal',[
    //     'uses' => 'login\LockDevice@modal',
    //     'as' => 'login.lockdevicemodal'
    // ]);
    // Route::post('/customerservice/lockdevicepopulatepost',[
    //     'uses' => 'login\LockDevice@proses',
    //     'as' => 'login.lockdevicepost'
    // ]);

    // // ==== Finance - Start Laporan Pencairan Waiting ====
    // Route::get('/finance/pencairanwaiting',[
    //     'uses' => 'login\PencairanWaiting@index',
    //     'as' => 'login.pencairanwaiting'
    // ]);
    // Route::post('/finance/pencairanwaitingpopulate',[
    //     'uses' => 'login\PencairanWaiting@populate',
    //     'as' => 'login.pencairanwaitingpopulate'
    // ]);
    // Route::post('/finance/pencairanwaitingpopulatemodal',[
    //     'uses' => 'login\PencairanWaiting@modal',
    //     'as' => 'login.pencairanwaitingmodal'
    // ]);
    // Route::post('/finance/pencairanwaitingpopulateajax',[
    //     'uses' => 'login\PencairanWaiting@ajax',
    //     'as' => 'login.pencairanwaitingjax'
    // ]);
    // Route::post('/finance/pencairanwaitingpopulatepost',[
    //     'uses' => 'login\PencairanWaiting@proses',
    //     'as' => 'login.pencairanwaitingpost'
    // ]);

    // // ==== Finance - Start Laporan Pencairan Finance ====
    // Route::get('/finance/financepencairan',[
    //     'uses' => 'login\FinancePencairan@index',
    //     'as' => 'login.financepencairan'
    // ]);
    // Route::post('/finance/financepencairanpopulate',[
    //     'uses' => 'login\FinancePencairan@populate',
    //     'as' => 'login.financepencairanpopulate'
    // ]);
    // Route::post('/finance/financepencairanpopulatemodal',[
    //     'uses' => 'login\FinancePencairan@modal',
    //     'as' => 'login.financepencairanmodal'
    // ]);
    // Route::get('financepencairanexcel',[
    //     'uses' => 'login\FinancePencairan@excel',
    //     'as' => 'financepencairanexcel'
    // ]);

    // // ==== Finance - Start Laporan Pelunasan Pinjaman Finance ====
    // Route::get('/finance/financepelunasan',[
    //     'uses' => 'login\FinancePelunasan@index',
    //     'as' => 'login.financepelunasan'
    // ]);
    // Route::post('/finance/financepelunasanpopulate',[
    //     'uses' => 'login\FinancePelunasan@populate',
    //     'as' => 'login.financepelunasanpopulate'
    // ]);
    // Route::post('/finance/financepelunasanpopulatemodal',[
    //     'uses' => 'login\FinancePelunasan@modal',
    //     'as' => 'login.financepelunasanmodal'
    // ]);
    // Route::post('/finance/financepelunasanpopulateajax',[
    //     'uses' => 'login\FinancePelunasan@ajax',
    //     'as' => 'login.financepelunasanajax'
    // ]);
    // Route::get('financepelunasanexcel',[
    //     'uses' => 'login\FinancePelunasan@excel',
    //     'as' => 'financepelunasanexcel'
    // ]);

    // // ==== Finance - Start Laporan Pinjaman Outstanding Finance ====
    // Route::get('/finance/financeoutstanding',[
    //     'uses' => 'login\FinanceOutstanding@index',
    //     'as' => 'login.financeoutstanding'
    // ]);
    // Route::post('/finance/financeoutstandingpopulate',[
    //     'uses' => 'login\FinanceOutstanding@populate',
    //     'as' => 'login.financeoutstandingpopulate'
    // ]);
    // Route::post('/finance/financeoutstandingpopulatemodal',[
    //     'uses' => 'login\FinanceOutstanding@modal',
    //     'as' => 'login.financeoutstandingmodal'
    // ]);
    // Route::post('/finance/financeoutstandingpopulateajax',[
    //     'uses' => 'login\FinanceOutstanding@ajax',
    //     'as' => 'login.financeoutstandingajax'
    // ]);
    // Route::get('financeoutstandingexcel',[
    //     'uses' => 'login\FinanceOutstanding@excel',
    //     'as' => 'financeoutstandingexcel'
    // ]);




    // Route::get('/download/{id}',[
    //     'uses' => 'login\ExportPDF@download',
    //     'as' => 'login.exportpdf.download'
    // ]);
    // Route::get('/suratperingatan',[
    //     'uses' => 'login\ExportPDF@index',
    //     'as' => 'login.exportpdf.index'
    // ]);
    // Route::post('/suratperingatan/new',[
    //     'uses' => 'login\ExportPDF@new',
    //     'as' => 'login.exportpdf.new'
    // ]);

    // // ==== Start Scoring Pinjaman ====
    // Route::get('/collection/scoringpinjaman',[
    //     'uses' => 'login\ScoringPinjaman@index',
    //     'as' => 'login.scoringpinjaman'
    // ]);
    // Route::post('/collection/scoringpinjamanpopulate',[
    //     'uses' => 'login\ScoringPinjaman@populate',
    //     'as' => 'login.scoringpinjamanpopulate'
    // ]);
    // Route::post('/collection/scoringpinjamanpopulatemodal',[
    //     'uses' => 'login\ScoringPinjaman@modal',
    //     'as' => 'login.scoringpinjamanmodal'
    // ]);

    // // ==== Start Penagihan ====
    // Route::get('/collection/mendekatioverdue',[
    //     'uses' => 'login\MendekatiOverdue@index',
    //     'as' => 'login.mendekatioverdue'
    // ]);
    // Route::post('/collection/mendekatioverduepopulate',[
    //     'uses' => 'login\MendekatiOverdue@populate',
    //     'as' => 'login.mendekatioverduepopulate'
    // ]);
    // Route::post('/collection/mendekatioverduepopulatemodal',[
    //     'uses' => 'login\MendekatiOverdue@modal',
    //     'as' => 'login.mendekatioverduemodal'
    // ]);
    // Route::post('/collection/mendekatioverduepopulateajax',[
    //     'uses' => 'login\MendekatiOverdue@ajax',
    //     'as' => 'login.mendekatioverdueajax'
    // ]);
    // Route::post('/collection/mendekatioverduepopulatepost',[
    //     'uses' => 'login\MendekatiOverdue@proses',
    //     'as' => 'login.mendekatioverduepost'
    // ]);

    // // ==== Start Penagihan ====
    // Route::get('/collection/penagihan',[
    //     'uses' => 'login\Penagihan@index',
    //     'as' => 'login.penagihan'
    // ]);
    // Route::post('/collection/penagihanpopulate',[
    //     'uses' => 'login\Penagihan@populate',
    //     'as' => 'login.penagihanpopulate'
    // ]);
    // Route::post('/collection/penagihanpopulatemodal',[
    //     'uses' => 'login\Penagihan@modal',
    //     'as' => 'login.penagihanmodal'
    // ]);
    // Route::post('/collection/penagihanpopulateajax',[
    //     'uses' => 'login\Penagihan@ajax',
    //     'as' => 'login.penagihanajax'
    // ]);
    // Route::post('/collection/penagihanpopulatepost',[
    //     'uses' => 'login\Penagihan@proses',
    //     'as' => 'login.penagihanpost'
    // ]);

    // // ==== Start Pinjaman OVD ====
    // Route::get('/collection/payctrl',[
    //     'uses' => 'login\Payctrl@index',
    //     'as' => 'login.payctrl'
    // ]);
    // Route::post('/collection/payctrlpopulate',[
    //     'uses' => 'login\Payctrl@populate',
    //     'as' => 'login.payctrlpopulate'
    // ]);
    // Route::post('/collection/payctrlpopulatemodal',[
    //     'uses' => 'login\Payctrl@modal',
    //     'as' => 'login.payctrlmodal'
    // ]);
    // Route::post('/collection/payctrlpopulateajax',[
    //     'uses' => 'login\Payctrl@ajax',
    //     'as' => 'login.payctrlajax'
    // ]);

    // // ==== Start Surat Kuasa Jual ====
    // Route::get('/penjualanjaminan/suratkuasajual',[
    //     'uses' => 'login\SuratKuasaJual@index',
    //     'as' => 'login.suratkuasajual'
    // ]);
    // Route::post('/penjualanjaminan/suratkuasajualpopulate',[
    //     'uses' => 'login\SuratKuasaJual@populate',
    //     'as' => 'login.suratkuasajualpopulate'
    // ]);
    // Route::post('/penjualanjaminan/suratkuasajualpopulatemodal',[
    //     'uses' => 'login\SuratKuasaJual@modal',
    //     'as' => 'login.suratkuasajualmodal'
    // ]);
    // Route::post('/penjualanjaminan/suratkuasajualpopulateajax',[
    //     'uses' => 'login\SuratKuasaJual@ajax',
    //     'as' => 'login.suratkuasajualajax'
    // ]);
    // Route::post('/penjualanjaminan/suratkuasajualpopulatepost',[
    //     'uses' => 'login\SuratKuasaJual@proses',
    //     'as' => 'login.suratkuasajualpost'
    // ]);

    // // ==== Start Simulasi Harga Jual ====
    // Route::get('/penjualanjaminan/simulasihargajual',[
    //     'uses' => 'login\SimulasiHargaJual@index',
    //     'as' => 'login.simulasihargajual'
    // ]);
    // Route::post('/penjualanjaminan/simulasihargajualpopulate',[
    //     'uses' => 'login\SimulasiHargaJual@populate',
    //     'as' => 'login.simulasihargajualpopulate'
    // ]);

    // // ==== Start Approval Penjualan ====
    // Route::get('/penjualanjaminan/approvalpenjualan',[
    //     'uses' => 'login\ApprovalPenjualan@index',
    //     'as' => 'login.approvalpenjualan'
    // ]);
    // Route::post('/penjualanjaminan/approvalpenjualanpopulate',[
    //     'uses' => 'login\ApprovalPenjualan@populate',
    //     'as' => 'login.approvalpenjualanpopulate'
    // ]);
    // Route::post('/penjualanjaminan/approvalpenjualanpopulatemodal',[
    //     'uses' => 'login\ApprovalPenjualan@modal',
    //     'as' => 'login.approvalpenjualanmodal'
    // ]);
    // Route::post('/penjualanjaminan/approvalpenjualanpopulateajax',[
    //     'uses' => 'login\ApprovalPenjualan@ajax',
    //     'as' => 'login.approvalpenjualanajax'
    // ]);
    // Route::post('/penjualanjaminan/approvalpenjualanpopulatepost',[
    //     'uses' => 'login\ApprovalPenjualan@proses',
    //     'as' => 'login.approvalpenjualanpost'
    // ]);
    // Route::post('/penjualanjaminan/approvalpenjualanpopulatepoststatus',[
    //     'uses' => 'login\ApprovalPenjualan@prosesstatus',
    //     'as' => 'login.approvalpenjualanpoststatus'
    // ]);

    // // ==== Start Agunan Terjual ====
    // Route::get('/penjualanjaminan/agunanterjual',[
    //     'uses' => 'login\AgunanTerjual@index',
    //     'as' => 'login.agunanterjual'
    // ]);
    // Route::post('/penjualanjaminan/agunanterjualpopulate',[
    //     'uses' => 'login\AgunanTerjual@populate',
    //     'as' => 'login.agunanterjualpopulate'
    // ]);
    // Route::post('/penjualanjaminan/agunanterjualpopulatemodal',[
    //     'uses' => 'login\AgunanTerjual@modal',
    //     'as' => 'login.agunanterjualmodal'
    // ]);
    // Route::post('/penjualanjaminan/agunanterjualpopulateajax',[
    //     'uses' => 'login\AgunanTerjual@ajax',
    //     'as' => 'login.agunanterjualajax'
    // ]);
    // Route::post('/penjualanjaminan/agunanterjualpopulatepost',[
    //     'uses' => 'login\AgunanTerjual@proses',
    //     'as' => 'login.agunanterjualpost'
    // ]);
});
