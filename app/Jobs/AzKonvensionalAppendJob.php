<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\AzKonvensionalExcel;

class AzKonvensionalAppendJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $tanggal = date('Y-m-d');
        // $fileName = "AzKonvensional";
        // $extension = "xlsx";

        // $fileNameWithExtension = "excel/$fileName-$tanggal.$extension";
        // Excel::store(new AzKonvensionalExcel($tanggal, '', '', ''), $fileNameWithExtension, 'local');
        
            // Load the existing Excel file and append new data
        // $existingFile = storage_path('excel/tester.xlsx'); // Replace with your existing file path

        // Run the export and append the data
        // return Excel::append(new AzKonvensionalExcel($tanggal, 25, '', ''), $existingFile);
            // Excel::load(storage_path('excel/tester.xlsx'), function($reader)      
            // {
            //     $reader->sheet(function($sheet) 
            //     {
            //         $sheet->appendRow([
            //             'test1', 'test2',
            //         ]);
            //     });
            // })->export('xlsx');
            Excel::load('excel/tester.xlsx', function($reader) {

                $reader->sheet('ReportsMaster',function($sheet) {
                  $sheet->appendRow([
                       'test1', 'test2',
                   ]);
                });
                })->export('xlsx');
    }
}
