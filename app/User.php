<?php

namespace App;


use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $table = "tbluser";
    // protected $primaryKey = 'idUser';
    // protected $fillable = [
    //     'idUser', 'email', 'userName', 'password', 'idModul', 'isActive','userEdit','hp'
    // ];
    protected $table = "tbluser";
    protected $primaryKey = 'idUser';
    protected $fillable = [
        'idUser', 'idKaryawan', 'idLevel', 'username', 'hp', 'isActive'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function karyawan() {
        return $this->belongsTo('App\model\TblKaryawanInternal', 'idKaryawan');
    }

    public function level() {
        return $this->belongsTo('App\model\ads_user_level', 'idUser');
    }

}
