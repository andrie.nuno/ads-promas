<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegisterSuperLender extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function build()
    {
        $address = 'notifikasi@danain.co.id';
        $subject = 'Informasi UserId dan Password Super Lender';
        $name = 'Danain';

        return $this->view('login.superlender.daftarsuper.email')
                    ->from($address, $name)
                    // ->cc($address, $name)
                    // ->bcc('emaildanain@gmail.com', $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([
                        'namaPIC'           => $this->data['namaPIC'], 
                        'namaPerusahaan'    => $this->data['namaPerusahaan'], 
                        'userId'            => $this->data['userId'], 
                        'password'          => $this->data['password'], 
                    ]);
    }
}
