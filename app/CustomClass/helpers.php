<?php
namespace App\CustomClass;
use Ramsey\Uuid\Uuid;
use App\model\menuads;
use App\model\agreement_peminjam;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_user;
use App\model\payctrl;
use App\model\kasbank;
use App\model\master\master_ovd;
use App\model\payctrl_log;
use App\model\tblmenu;
use App\model\tblmodul_list;
use App\model\tblvendor;
use Session;
use DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class helpers
{
    static function leftMenu($id) {
    // static function leftMenu() {
        // $data = menuads::headerAktif(Auth::user()->idLevel);
        $data = menuads::headerAktif($id);
        return $data;
    }

    static function getModulList(){
        $data = tblmodul_list::getModul_listById(Auth::user()->idModul);
        return empty($data) ? null : $data;
    }
    static function getNameMenu($id){
        $data = tblmenu::getNameByIdMenu($id);
        return empty($data) ? null : $data;
    }
    static function getModulList2($idMenu){
        $data = tblmodul_list::getModul_listById2(Auth::user()->idModul,$idMenu);
        return empty($data) ? null : $data;
    }

    // PAGINATION START
    static function pagination($totalData, $limit, $page) {
        return view('layouts.paging',[
            "totalData"     => $totalData, 
            "limit"         => $limit,
            "page"          => $page, 
        ]);
    }

    static function paginationModal($totalData, $limit, $page) {
        return view('layouts.pagingmodal',[
            "totalData"     => $totalData, 
            "limit"         => $limit,
            "page"          => $page, 
        ]);
    }

    static function get_no($limit, $page){
        return (($limit * $page) - $limit) + 1;
    }
    // PAGINATION END

    static function saveimgresizeS3($img, $path, $s3){
        try {
            $uuid4          = Uuid::uuid4();
            $uuidfresh      = date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            // list($width, $height) = getimagesize($img);
            // if ($width >= $height) {
            //     $newwidth = 900;
            //     $newheight = 600;
            // } else {
            //     $newwidth = 600;
            //     $newheight = 900;
            // }
            $storagePath    = Storage::disk('publicupload')->putFileAs($path, $img, $uuidfresh.'.jpg');
            $pathfile       = base_path('public'.$path.$uuidfresh.'.jpg');
            // $image          = Image::make($pathfile)->resize($newwidth, $newheight);
            // $result         = $image->save($pathfile);
            // upload ktp to S3
            // jalankan dulu : composer require league/flysystem-aws-s3-v3
            // atau ini : composer require league/flysystem-aws-s3-v3 "~1.0"
            $file           = base_path('public'.$path.$uuidfresh.'.jpg');
            $s3Path         = $s3.$uuidfresh.'.jpg';
            $storageS3      = Storage::disk('s3upload')->put($s3Path, file_get_contents($file), 'public');
            $s3Link         = "http://nos.jkt-1.neo.id/appsdanain-borrower".$s3Path;
            // delete local file
            if(Storage::disk('publicupload')->exists($path.$uuidfresh.'.jpg')){
                Storage::disk('publicupload')->delete($path.$uuidfresh.'.jpg');
            }
            return $s3Link;
            // return $s3Link;
        }
        catch (\Exception $e){
            return $e;
        }
    }

    static function saveimgS3($img, $path, $s3, $extension){
        try {
            $uuid4          = Uuid::uuid4();
            $uuidfresh      = date('Y-m-d-H-i-s').'-'.str_replace("-", "", $uuid4->toString());
            $storagePath    = Storage::disk('publicupload')->putFileAs($path, $img, $uuidfresh.'.'.$extension);
            $pathfile       = base_path('public'.$path.$uuidfresh.'.'.$extension);
            $file           = base_path('public'.$path.$uuidfresh.'.'.$extension);
            $s3Path         = $s3.$uuidfresh.'.'.$extension;
            $storageS3      = Storage::disk('s3upload')->put($s3Path, file_get_contents($file), 'public');
            $s3Link         = "https://nos.jkt-1.neo.id/appsdanain-borrower".$s3Path;
            // delete local file
            if(Storage::disk('publicupload')->exists($path.$uuidfresh.'.'.$extension)){
                Storage::disk('publicupload')->delete($path.$uuidfresh.'.'.$extension);
            }
            return $s3Link;
        }
        catch (\Exception $e){
            return $e;
        }
    }

    public static function namaHari($hari)
    {
        switch ($hari) {
            case 'Sunday':
              return 'Minggu';
            case 'Monday':
              return 'Senin';
            case 'Tuesday':
              return 'Selasa';
            case 'Wednesday':
              return 'Rabu';
            case 'Thursday':
              return 'Kamis';
            case 'Friday':
              return 'Jumat';
            case 'Saturday':
              return 'Sabtu';
            default:
              return 'hari tidak valid';
        }
    }

    public static function namaBulan($bulan)
    {
        switch ($bulan) {
            case 1:
              return 'Januari';
            case 2:
              return 'Februari';
            case 3:
              return 'Maret';
            case 4:
              return 'April';
            case 5:
              return 'Mei';
            case 6:
              return 'Juni';
            case 7:
              return 'Juli';
            case 8:
              return 'Agustus';
            case 9:
              return 'September';
            case 10:
              return 'Oktober';
            case 11:
              return 'November';
            case 12:
              return 'Desember';
            default:
              return 'bulan tidak valid';
        }
    }

    public static function tglLongIndo($tanggal)
    {
        $tgl = explode("-",$tanggal);
        return $tgl[2]." ".helpers::namaBulan($tgl[1])." ".$tgl[0];
    }

    public static function sembilanAngka($angka){
      if ($angka < 10) {
          return '00000000'.$angka;
      } else if ($angka < 100) {
          return '0000000'.$angka;
      } else if ($angka < 1000) {
          return '000000'.$angka;
      } else if ($angka < 10000) {
          return '00000'.$angka;
      } else if ($angka < 100000) {
          return '0000'.$angka;
      } else if ($angka < 1000000) {
          return '000'.$angka;
      } else if ($angka < 10000000) {
          return '00'.$angka;
      } else if ($angka < 100000000) {
          return '0'.$angka;
      } else {
          return $angka;
      }
  }
}
