<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use App\model\TblCabang;
use App\model\FaSaldoBank;
use App\model\TblBank;
use App\model\ReportBankTransaksi;
use app\CustomClass\helpers;

class SaldoBankExcel implements FromView, ShouldAutoSize
{
    protected $idCabang;
    protected $idBank;
    protected $tanggalAwal;
    protected $tanggalAkhir;

    function __construct($idCabang, $idBank, $tanggalAwal, $tanggalAkhir) {
        //dd($idCabang);
        $this->idCabang = $idCabang;
        $this->idBank = $idBank;
        $this->tanggalAwal = $tanggalAwal;
        $this->tanggalAkhir = $tanggalAkhir;
    }
    
    public function view(): View
    {
        $dataCabang = TblCabang::find($this->idCabang);
        $dataBank = TblBank::find($this->idBank);
        $data = ReportBankTransaksi::where('idCabang', '=', $this->idCabang)
            ->where('idBank', '=', $this->idBank)
            ->where('tanggalSistem', '>=', $this->tanggalAwal)
            ->where('tanggalSistem', '<=', $this->tanggalAkhir)
            ->orderBy('tanggalSistem', 'ASC')
            ->orderBy('idBankTransaksi', 'ASC')
            ->get();
        return view('login.saldobank.excel',[
            "tanggalAwal"   => $this->tanggalAwal, 
            "tanggalAkhir"  => $this->tanggalAkhir, 
            "idCabang"      => $this->idCabang, 
            "idBank"        => $this->idBank, 
            "dataCabang"    => $dataCabang, 
            "dataBank"      => $dataBank, 
            "data"          => $data, 
        ]);
    }
}
