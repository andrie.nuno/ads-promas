<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\payctrl;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class OutstandingPinjamanExcel implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class OutstandingPinjamanExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_company;
    protected $nama_branch;
    protected $rate_pendana;
    protected $lender;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_company, $nama_branch, $rate_pendana, $lender) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_company = $nama_company;
        $this->nama_branch = $nama_branch;
        $this->rate_pendana = $rate_pendana;
        $this->lender = $lender;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $query = payctrl::query()
            ->join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
            ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
            ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
            ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
            ->leftJoin('db_mid_peminjam.master_jeniskelamin AS gender', 'gender.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
            ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
            ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
            ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
            ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
            ->join('tblsuperlender', 'tblsuperlender.idUserClient', '=', 'payctrl.idUserClient')
            ->orderBy('agreement_data.tanggal', 'DESC')
            ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, payctrl.status, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_jaminan.ltvHargaBeliPasar AS nilaiLtv, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv AS nilaiLtvProduk, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol, gender.namaJenisKelamin, tblsuperlender.namaPerson')
            ->selectRaw('LPAD(agreement_data.idBorrower, 9, "0") AS idBorrower')
            ->selectRaw('TIMESTAMPDIFF(YEAR, register_peminjam.tanggalLahir, agreement_data.tanggal) AS usia')
            ->selectRaw('(DATEDIFF("'.$tanggal.'", agreement_data.tanggal) + 1) AS jumlahHari')
            ->selectRaw('DATEDIFF("'.$tanggal.'", agreement_data.tglJt) AS jumlahOvd')
            ->selectRaw('CEIL((DATEDIFF("'.$tanggal.'", agreement_data.tanggal) + 1) * payctrl.bungaHarian) AS bungaAcrue')
            ->selectRaw('CEIL((DATEDIFF("'.$tanggal.'", agreement_data.tanggal) + 1) * payctrl.jasaMitraHarian) AS mitraAcrue')
            ->selectRaw('CEIL(((DATEDIFF("'.$tanggal.'", agreement_data.tanggal) + 1) - agreement_peminjam.tenor) * payctrl.dendaHarian) AS dendaAcrue')
            ->where('agreement_data.tanggal', '<=', $tanggal)
            ->where(function ($query2) use ($tanggal) {
                $query2->where('agreement_data.tglBayar', '>', $tanggal)
                    ->orWhereNull('agreement_data.tglBayar');
            });
        if ($this->nama_company) {
            $query = $query->where('member_branch.idCompany', $this->nama_company);
        }
        if ($this->nama_branch) {
            $query = $query->where('member_branch.idBranch', $this->nama_branch);
        }
        if ($this->rate_pendana) {
            $query = $query->where('agreement_peminjam.ratePendana', $this->rate_pendana);
        }
        if ($this->lender) {
            $query = $query->where('payctrl.idUserClient', $this->lender);
        }
        $query = $query->groupBy('agreement_data.idAgreement');
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal Outstanding Pinjaman : '.$this->tanggal
            ],
            [
                'No',
                'No PP',
                'Email',
                'CIF',
                'No KTP',
                'Nama Peminjam',
                'Jenis Kelamin',
                'Tanggal Lahir',
                'Usia',
                'Pekerjaan',
                'Penghasilan/Bulan',
                'Biaya Hidup/Bulan',
                'Tujuan Pinjaman',
                'Pendana',
                'Tgl. Pencairan',
                'Tgl. JT',
                'Wilayah',
                'Kode Outlet',
                'Outlet Penaksir',
                'Produk Pinjaman',
                'Nama Jaminan',
                'Jumlah Jaminan',
                'Karat',
                'Rata-Rata Karatase',
                'Gram',
                'Jumlah Gram',
                'STLE',
                'LTV',
                'Rate Jasa Taksir',
                'Rate Pendana',
                'Rate Danain',
                'Rate Admin',
                'Nilai Taksiran',
                'Max Pinjaman',
                'Nilai Pinjaman',
                'Jasa Taksir Mitra',
                'Bunga Pinjaman',
                'Nilai Admin',
                'Nilai Pencairan',
                'Jumlah Hari',
                'OVD',
                'Status Pinjaman', 
                'Bunga Harian',
                'Bunga Terhutang',
                'Jasa Mitra Harian',
                'Jasa Mitra Terhutang',
                'Admin Pelunasan',
                'Denda Harian',
                'Denda Terhutang',
                'Total Hutang',
                'Status Aplikasi Danain',
                'Status Aplikasi Gadai MAS',
                'Status Aplikasi Konsol',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            "'".$data->noPerjanjian, 
            $data->email, 
            $data->idBorrower, 
            "'".$data->ktp, 
            $data->namaBorrower, 
            $data->namaJenisKelamin, 
            $data->tanggalLahir, 
            $data->usia, 
            $data->namaPekerjaan, 
            $data->penghasilanPerBulan, 
            $data->biayaHidupPerBulan, 
            $data->tujuanPinjaman, 
            $data->namaPerson, 
            $data->tanggal, 
            $data->tglJt, 
            $data->namaCompany, 
            $data->kodeBranch, 
            $data->namaBranch, 
            $data->namaProduk, 
            $data->namaJaminan, 
            $data->jumlahJaminan, 
            $data->karat, 
            $data->avgKarat, 
            $data->gram, 
            $data->sumGram, 
            $data->nilaiStle, 
            $data->nilaiLtv."%",
            $data->rateJasaTaksir."%",
            $data->ratePendana."%",
            $data->rateFeeDanain."%",
            $data->ratePencairan."%",
            $data->nilaiTaksiran, 
            $data->nilaiMax, 
            $data->nilaiPinjaman, 
            $data->jasaTaksir, 
            $data->bungaPinjaman, 
            $data->adminPencairan, 
            $data->nilaiPencairan, 
            // $data->jumlahHari, 
            // $data->jumlahOvd, 
            // $data->status == 'Proses Jual' ? $data->jmlHari : $data->jumlahHari, 
            // $data->status == 'Proses Jual' ? $data->ovd : $data->jumlahOvd, 
            $data->jumlahHari <= $data->jmlHari ? $data->jumlahHari : $data->jmlHari, 
            $data->jumlahOvd <= $data->ovd ? $data->jumlahOvd : $data->ovd, 
            $data->status, 
            $data->bungaHarian, 
            $data->tglJt < date('Y-m-d') ? $data->bungaTerhutang : $data->bungaAcrue, 
            $data->jasaMitraHarian, 
            // $data->status == 'Gagal Bayar' ? $data->jasaMitraTerhutang : $data->mitraAcrue, 
            $data->status == 'Proses Jual' ? $data->jasaMitraTerhutang : $data->mitraAcrue, 
            $data->adminPelunasan, 
            $data->dendaHarian, 
            // $data->jumlahHari > $data->tenor ? $data->dendaAcrue : 0, 
            $data->status == 'Proses Jual' ? $data->dendaTerhutang : ($data->jumlahHari > $data->tenor ? $data->dendaAcrue : 0), 
            '=AI'.$this->current_row_al++.' + AR'.$this->current_row_ap++.' + AT'.$this->current_row_ar++.' + AW'.$this->current_row_au++, 
            $data->statusAplikasi, 
            $data->statusAplikasiMeta, 
            $data->statusAplikasiKonsol, 
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class OutstandingPinjamanExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_company;
//     protected $nama_branch;
//     protected $rate_pendana;

//     function __construct($tanggal, $nama_company, $nama_branch, $rate_pendana) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_company = $nama_company;
//         $this->nama_branch = $nama_branch;
//         $this->rate_pendana = $rate_pendana;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_company) {
//             $query = $query->where('member_branch.idCompany', $this->nama_company);
//         }
//         if ($this->nama_branch) {
//             $query = $query->where('member_branch.idBranch', $this->nama_branch);
//         }
//         if ($this->rate_pendana) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->rate_pendana);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_company"  => $this->nama_company, 
//             "nama_branch"   => $this->nama_branch, 
//             "tanggal"       => $this->tanggal, 
//             "rate_pendana"  => $this->rate_pendana, 
//             "data"          => $data, 
//         ]);
//     }
// }
