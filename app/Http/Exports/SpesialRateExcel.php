<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\tblspesialrate;

class SpesialRateExcel implements FromView, ShouldAutoSize
{
    protected $email_borrower;
    protected $produk;

    function __construct($email_borrower, $produk) {
        $this->email_borrower = $email_borrower;
        $this->produk = $produk;
    }
    
    public function view(): View
    {
        $query = tblspesialrate::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'tblspesialrate.idBorrower')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'tblspesialrate.idProduk')
            ->selectRaw('tblspesialrate.idSpesialRate, tblspesialrate.idBorrower, tblspesialrate.rateFeeDanain, tblspesialrate.ltv, tblspesialrate.isActive, register_peminjam.email, register_peminjam.namaBorrower, tblproduk.keterangan')
            ->orderBy('register_peminjam.namaBorrower', 'ASC')
            ->orderBy('tblproduk.idProduk', 'ASC');
        if ($this->email_borrower) {
            $data = $query->where('register_peminjam.email', 'like', '%'.$this->email_borrower.'%');
        }
        if ($this->produk) {
            $data = $query->where('tblspesialrate.idProduk', '=', $this->produk);
        }
        $data = $query->get();
        return view('login.mastermitra.spesialrate.excel',[
            "email_borrower"=> $this->email_borrower, 
            "produk"        => $this->produk,
            "data"          => $data, 
        ]);
    }
}
