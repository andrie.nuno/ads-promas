<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Auth;
use app\CustomClass\helpers;
use App\model\va_deposit;

class DepositVaExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $awal;
    protected $sampai;

    function __construct($jenis, $awal, $sampai) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->awal = $awal;
        $this->sampai = $sampai;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Detail") {
            $data = va_deposit::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'va_deposit.idUserClient')
                ->orderBy('va_deposit.tanggal')
                ->selectRaw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d") as tanggalDeposit, va_deposit.customerAccount, va_deposit.paymentReffId, va_deposit.amount, va_deposit.paymentMessage, tbluserclient.userId, tbluserclient.userName')
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '>=', $this->awal)
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '<=', $this->sampai)
                ->get();
        } else if ($this->jenis == "Tanggal") {
            $data = va_deposit::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'va_deposit.idUserClient')
                ->orderBy(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'))
                ->selectRaw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d") AS tanggalDeposit, SUM(va_deposit.amount) AS jumlah')
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '>=', $this->awal)
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '<=', $this->sampai)
                ->groupBy(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'))
                ->get();
        } else {
            $data = va_deposit::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'va_deposit.idUserClient')
                ->selectRaw('YEAR(va_deposit.tanggal) tahun, MONTH(va_deposit.tanggal) bulan, SUM(va_deposit.amount) AS jumlah')
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '>=', $this->awal)
                ->where(DB::raw('DATE_FORMAT(va_deposit.tanggal, "%Y-%m-%d")'), '<=', $this->sampai)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->orderBy('tahun')
                ->orderBy('bulan')
                ->get();
        }
        return view('login.laporanlender.depositva.excel',[
            "jenisLaporan"  => $this->jenis, 
            "tglAwal"       => $this->awal,
            "tglSampai"     => $this->sampai,
            "data"          => $data, 
        ]);
    }
}
