<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_gadai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class PelunasanKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class PelunasanKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $tanggalAwal = substr($tanggal,0,7)."-01";
        $query = trans_gadai::
            join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
            ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
            ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
            ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
            ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
            ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
            ->leftJoin("trans_pelunasangadai", function($join){
                $join->on('trans_pelunasangadai.idGadai', '=', 'trans_gadai.idGadai');
                $join->where(DB::raw("trans_pelunasangadai.isStatus"), "=", 1);
            })
            ->leftJoin("trans_pelunasanjual", function($join){
                $join->on('trans_pelunasanjual.idGadai', '=', 'trans_gadai.idGadai');
                $join->where(DB::raw("trans_pelunasanjual.isStatus"), "=", 1);
            })
            ->leftJoin("trans_pembayarancicilan", function($join){
                $join->on('trans_pembayarancicilan.idGadai', '=', 'trans_gadai.idGadai');
                $join->where(DB::raw("trans_pembayarancicilan.isStatus"), "=", 1);
            })
            ->leftJoin("trans_payment_schedule", function($join){
                $join->on('trans_payment_schedule.idGadai', '=', 'trans_payment_schedule.idGadai');
                $join->on('trans_payment_schedule.angsuranKe', '=', 'trans_pembayarancicilan.angsuranKe');
            })
            ->leftJoin("trans_gadaijf", function($join){
                $join->on('trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai');
                $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            })
            ->leftjoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
            ->leftJoin(\DB::raw("(SELECT outs.idCustomer FROM trans_payctrl AS outs WHERE outs.tanggalPencairan <= '$tanggal' AND (outs.tanggalPelunasan IS NULL OR outs.tanggalPelunasan > '$tanggal') GROUP BY outs.idCustomer) AS pinjamanAktif"), 'pinjamanAktif.idCustomer', '=', 'trans_gadai.idCustomer')
            ->selectRaw("tblproduk.kodeProduk AS produk, trans_gadai.noSbg, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, 
            CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, tblcustomer.namaCustomer, 
            tblcustomer.cif, tblcustomer.noKtp, tblcustomer.hp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, trans_gadai.lamaPinjaman AS tenor, 
            IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.ovd, DATEDIFF(trans_pembayarancicilan.tglPembayaran, trans_payment_schedule.tglJatuhTempo)) AS ovd, DATE(trans_gadai.tglCair) AS tglCair, 
            IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.tanggalPelunasan, trans_pembayarancicilan.tglPembayaran) AS tanggalPelunasan, 
            IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.tglJatuhTempo, trans_payment_schedule.tglJatuhTempo) AS tglJatuhTempo, 
            IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.nilaiPinjaman, IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', trans_payment_schedule.pokokAngsuran, trans_pembayarancicilan.nilaiPokok)) AS pokokAwal, 
            IF(trans_pembayarancicilan.idGadai IS NULL, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan), (trans_payment_schedule.pokokAngsuran + trans_payment_schedule.bungaAngsuran)) AS pinjamAwal, 
            ROUND(trans_gadai.rateFlat * 12, 2) AS rate, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.totalPelunasan, trans_pelunasanjual.totalKewajiban), (trans_pembayarancicilan.totalPembayaran)) AS nilaiLunas, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(tblsettingjf.kodeJf IS NULL, trans_payctrl.acrueBunga, (trans_payctrl.acrueBungaMurni + trans_payctrl.acrueBungaSelisih)), (trans_payment_schedule.acrualAkhirBulan + trans_payment_schedule.acrualJatuhTempo)) AS acrueBunga, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiBunga, trans_pelunasanjual.nilaiBunga), trans_pembayarancicilan.nilaiBunga) AS pendapatanBunga, 
            IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiDenda, 0) AS biayaPemeliharaan, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, trans_pelunasanjual.nilaiAdminJual), 0) AS adminPenjualan, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_gadai.biayaPenyimpanan - trans_pelunasangadai.nilaiBunga), (trans_gadai.biayaPenyimpanan - trans_pelunasanjual.nilaiBunga)), 0) AS upsite, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_pelunasangadai.totalKewajiban - trans_pelunasangadai.totalPelunasan), 0), 0) AS diskonPelunasan, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, (trans_pelunasanjual.totalKewajiban - trans_pelunasanjual.totalPelunasan)), 0) AS diskonPenjualan, 
            tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.namaJf AS bankPendana, tblsettingjf.rate AS RateJf, tblsettingjf.porsi AS porsiBank, 
            IF(trans_pelunasangadai.pelunasanBankJfReal != 0, trans_pelunasangadai.pelunasanBankJfReal, trans_pelunasangadai.pelunasanBankJf) AS pelunasanJf, 
            IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.statusLunas, trans_payctrl.statusPinjaman), IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', 'Pembayaran Cicilan', 'Pelunasan Cicilan Dipercepat')) AS statusPelunasan, 
            IF(pinjamanAktif.idCustomer IS NULL, 'EXIT', '') AS statusExit")
            ->whereRaw("trans_payctrl.tanggalPelunasan >= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggalAwal') 
            AND trans_payctrl.tanggalPelunasan <= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggal') OR 
            (trans_pembayarancicilan.tglPembayaran >= '$tanggalAwal' AND trans_pembayarancicilan.tglPembayaran <= '$tanggal')")
            // ->where(DB::raw("(trans_payctrl.tanggalpelunasan > $tanggal or trans_payctrl.tanggalpelunasan is null)"))
            ;
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query = $query->where('tblcabang.idCabang', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query = $query->where('tblcabang.headCabang', $this->nama_unit);
        }
        // $query = $query->groupBy('trans_payctrl.idGadai');
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tanggal
            ],
            [
                'No',
                'Kode Produk',
                'No SBG',
                'Kode Outlet',
                'Nama Outlet',
                'Nama Cabang',
                'Nama Wilayah',
                'No CIF',
                'No KTP',
                'No Hp',
                'Tempat Lahir',
                'Tanggal Lahir',
                'Tenor',
                'ovd',
                'Tanggal Cair',
                'Tanggal Pelunasan',
                'Tanggal Jatuh Tempo',
                'Pokok Awal',
                'Pinjam Awal',
                'Rate',
                'Nilai Lunas',
                'Acrue Bunga',
                'Pendapatan Bunga',
                'Biaya Pemeliharaan',
                'Admin Penjualan',
                'Upsite',
                'Diskon Pelunasan',
                'Diskon Penjualan',
                'Kode Mitra',
                'Bank Pendana',
                'Rate Jf',
                'Porsi Bank',
                'Pelunasan Jf',
                'Status Pelunasan',
                'Status Exit',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->produk, 
            $data->noSbg, 
            $data->kodeOutlet, 
            $data->namaOutlet, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->namaCustomer, 
            $data->cif,
            "'".$data->noKtp, 
            $data->hp, 
            $data->tempatLahir, 
            $data->tanggalLahir, 
            $data->ovd, 
            $data->tglCair, 
            $data->tanggalPelunasan, 
            $data->tglJatuhTempo, 
            $data->pokokAwal, 
            $data->pinjamAwal, 
            $data->rate, 
            $data->nilaiLunas, 
            $data->acrueBunga, 
            $data->pendapatanBunga, 
            $data->biayaPemeliharaan, 
            $data->adminPenjualan, 
            $data->upsite, 
            $data->diskonPelunasan, 
            $data->diskonPenjualan, 
            $data->kodeMitra, 
            $data->bankPendana, 
            $data->RateJf, 
            $data->porsiBank, 
            $data->pelunasanJf, 
            $data->statusPelunasan,
            $data->statusExit,
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class PelunasanKonvensionalExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah, 
//             "nama_cabang"   => $this->nama_cabang, 
//             "tanggal"       => $this->tanggal, 
//             "nama_unit"  => $this->nama_unit, 
//             "data"          => $data, 
//         ]);
//     }
// }
