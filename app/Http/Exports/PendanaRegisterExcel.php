<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\tbluserclient;

class PendanaRegisterExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $awal;
    protected $sampai;

    function __construct($jenis, $awal, $sampai) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->awal = $awal;
        $this->sampai = $sampai;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Detail") {
            $data = tbluserclient::join('tblpendana', 'tblpendana.email', '=', 'tbluserclient.email')
                ->leftJoin('tblpropinsi', 'tblpropinsi.idPropinsi', '=', 'tbluserclient.idPropinsi')
                ->leftJoin('tblkota', 'tblkota.idKota', '=', 'tbluserclient.idKota')
                ->leftJoin('tblpendidikan', 'tblpendidikan.idPendidikan', '=', 'tbluserclient.idPendidikan')
                ->leftJoin('tblpekerjaan', 'tblpekerjaan.idPekerjaan', '=', 'tbluserclient.idPekerjaan')
                ->leftJoin('tblbank', 'tblbank.idBank', '=', 'tbluserclient.idBank')
                ->leftJoin('bca_va', 'bca_va.idUserClient', '=', 'tbluserclient.idUserClient')
                ->leftJoin('bni_va', 'bni_va.idUserClient', '=', 'tbluserclient.idUserClient')
                ->orderBy('tbluserclient.tglJoin')
                ->selectRaw('tbluserclient.*, tblpropinsi.keterangan AS provinsi, tblkota.keterangan AS kota, tblpendidikan.keterangan AS pendidikan, tblpekerjaan.keterangan AS pekerjaan, tblbank.keterangan AS bank, bca_va.customerAccount AS va_bca, bni_va.customerAccount AS va_bni')
                ->where('tbluserclient.tglJoin', '>=', $this->awal)
                ->where('tbluserclient.tglJoin', '<=', $this->sampai)
                ->get();
        } else if ($this->jenis == "Tanggal") {
            $data = tbluserclient::orderBy('tglJoin')
                ->selectRaw('tglJoin AS tanggal, count(idUserClient) AS jumlah')
                ->where('tglJoin', '>=', $this->awal)
                ->where('tglJoin', '<=', $this->sampai)
                ->groupBy('tglJoin')
                ->get();
        } else {
            $data = tbluserclient::orderBy('tglJoin')
                ->selectRaw('YEAR(tglJoin) tahun, MONTH(tglJoin) bulan, count(idUserClient) AS jumlah')
                ->where('tglJoin', '>=', $this->awal)
                ->where('tglJoin', '<=', $this->sampai)
                ->groupBy('tahun')
                ->groupBy('bulan')
                ->get();
        }
        return view('login.laporanlender.pendanaregister.excel',[
            "jenisLaporan"  => $this->jenis, 
            "tglAwal"       => $this->awal,
            "tglSampai"     => $this->sampai,
            "data"          => $data, 
        ]);
    }
}
