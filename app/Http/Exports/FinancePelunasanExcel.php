<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\payctrl;

class FinancePelunasanExcel implements FromView, ShouldAutoSize
{
    protected $company;
    protected $branch;
    protected $tglAwal;
    protected $tglSampai;

    function __construct($company, $branch, $tglAwal, $tglSampai) {
        //dd($tanggal);
        $this->company = $company;
        $this->branch = $branch;
        $this->tglAwal = $tglAwal;
        $this->tglSampai = $tglSampai;
    }
    
    public function view(): View
    {
        $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
            ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'payctrl.idAgreement')
            ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
            ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
            ->join('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
            ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
            ->orderBy('agreement_data.tanggal', 'DESC')
            ->selectRaw('agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.tglBayar, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, tblproduk.keterangan AS namaProduk, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.tlpMobile, payctrl.jmlHari, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaTerhutang, payctrl.ovd, payctrl.adminPelunasan, payctrl.totalHutang, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany')
            // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
            ->where('agreement_data.tglBayar', '>=', $this->tglAwal)
            ->where('agreement_data.tglBayar', '<=', $this->tglSampai);
        if ($this->company) {
            $data = $query->where('member_branch.idCompany', $this->company);
        }
        if ($this->branch) {
            $data = $query->where('register_jaminan.idBranch', $this->branch);
        }
        $data = $query->get();
        return view('login.finance.financepelunasan.excel',[
            "tglAwal"       => $this->tglAwal, 
            "tglSampai"     => $this->tglSampai, 
            "data"          => $data, 
        ]);
    }
}
