<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_gadai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class WaktuTransaksiKonvensionalExcel implements FromQuery, ShouldQueue, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class WaktuTransaksiKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    
    protected $nama_wilayah;
    protected $tgl_awal;
    protected $tgl_sampai;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($nama_wilayah, $tgl_awal, $tgl_sampai) {
        //dd($tanggal);
        $this->nama_wilayah = $nama_wilayah;
        $this->tgl_awal = $tgl_awal;
        $this->tgl_sampai = $tgl_sampai;
    }
    
    public function query()
    {
        // $tanggal = $this->tanggal;
        $query = trans_gadai::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang', '=', 'tblcabang.headCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
        ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai.idCustomer')
        ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'trans_gadai.idGadai')
        ->join('tran_fapg', 'tran_fapg.idFAPG', '=', 'trans_gadai.idFAPG')
        ->join('tran_taksiran AS taksirawal', function($join){
            $join->on('taksirawal.idFAPG', '=', 'trans_gadai.idFAPG');
            $join->where(DB::raw('taksirawal.isFinal'), '=', 0);
        })
        ->join('tran_taksiran as taksirfinal', function($join){
            $join->on('taksirfinal.idFAPG', '=', 'trans_gadai.idFAPG');
            $join->where(DB::raw('taksirfinal.isFinal'), '=', 1);
        })
        ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, CONCAT(cabang.kodeCabang, '-', cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, tran_fapg.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, trans_gadai.created_at AS timeInputGadai, trans_gadai.tglApprovalKaunit, trans_gadai.tglApprovalKacab, trans_gadai.tglApprovalKawil, trans_gadai.tglApprovalDirektur, trans_gadai.tglApprovalDirut, trans_gadai.tglCetakKwitansi AS timeCetakKwitansi, trans_gadai.nilaiPinjaman, ROUND((trans_gadai.nilaiPinjaman / taksirfinal.totalTaksiran) * 100, 2) AS ltv, trans_gadai.rateFlat, trans_gadai.totalObligor, trans_gadai.statusAplikasi, trans_gadai.approvalFinal, trans_gadai.approvalLtv, trans_gadai.approvalUangPinjaman, trans_gadai.approvalRateFlat, trans_gadai.approvalOneObligor, trans_gadai.timeFotoNasabah, trans_gadai.tglApprovalKadeptCpa, trans_gadai.tglApprovalKadivCpa")
        ->orderBy('trans_payctrl.tanggalPencairan');
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->tgl_awal) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '>=', $this->tgl_awal);
        }
        if ($this->tgl_sampai) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '<=', $this->tgl_sampai);
        }
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tgl_awal .' S/d '. $this->tgl_sampai
            ],
            [
                'No',
                'Kode Outlet',
                'Nama Outlet',
                'Nama Cabang',
                'Nama Wilayah',
                'Tgl Pencairan',
                'No SBG',
                'Jenis Pembayaran Final',
                'CIF',
                'Nama',
                'No Ktp',
                'Nilai Pinjaman',
                'Rate',
                'LTV',
                'One Obligor',
                'Status Aplikasi',
                'Time Input FAPG',
                'Time Taksiran I',
                'Time Taksiran II',
                'Time Input Gadai',
                'Time KaUnit',
                'Time Kacab',
                'Time Kawil',
                'Time KaDept CPA',
                'Time KaDiv CPA',
                'Time Direktur',
                'Time Dirut',
                'Time Kwitansi',
                'Time Foto Nasabah',
                'Final Approval',
                'Approval UP',
                'Approval Rate',
                'Approval LTV',
                'Approval Obligor',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->kodeOutlet, 
            $data->namaOutlet, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->tanggalPencairan, 
            "'".$data->noSbg, 
            $data->jenisPembayaranFinal,
            $data->cif, 
            $data->namaCustomer, 
            "'".$data->noKtp, 
            $data->nilaiPinjaman, 
            $data->rateFlat, 
            $data->ltv, 
            $data->totalObligor, 
            $data->statusAplikasi, 
            $data->timeInputFAPG, 
            $data->timeTaksirPenaksir, 
            $data->timeTaksirKaUnit, 
            $data->timeInputGadai, 
            $data->tglApprovalKaunit, 
            $data->tglApprovalKacab, 
            $data->tglApprovalKawil, 
            $data->tglApprovalKadeptCpa, 
            $data->tglApprovalKadivCpa, 
            $data->tglApprovalDirektur, 
            $data->tglApprovalDirut, 
            $data->timeCetakKwitansi,
            $data->timeFotoNasabah,
            $data->approvalFinal,
            $data->approvalUangPinjaman,
            $data->approvalRateFlat,
            $data->approvalLtv,
            $data->approvalOneObligor,
        ];
    }
}
