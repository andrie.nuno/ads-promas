<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_gadai_syariah;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class WaktuTransaksiSyariahExcel implements FromQuery, ShouldQueue, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class WaktuTransaksiSyariahExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    
    protected $nama_wilayah;
    protected $tgl_awal;
    protected $tgl_sampai;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($nama_wilayah, $tgl_awal, $tgl_sampai) {
        //dd($tanggal);
        $this->nama_wilayah = $nama_wilayah;
        $this->tgl_awal = $tgl_awal;
        $this->tgl_sampai = $tgl_sampai;
    }
    
    public function query()
    {
        // $tanggal = $this->tanggal;
        $query = trans_gadai_syariah::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai_syariah.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang', '=', 'tblcabang.headCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
        ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai_syariah.idCustomer')
        ->join('trans_payctrl_syariah', 'trans_payctrl_syariah.idGadai', '=', 'trans_gadai_syariah.idGadai')
        ->join('tran_fapg_syariah', 'tran_fapg_syariah.idFAPG', '=', 'trans_gadai_syariah.idFAPG')
        ->join('tran_taksiran as taksirawal', function($join){
            $join->on('taksirawal.idFAPG', '=', 'trans_gadai_syariah.idFAPG');
            $join->where(DB::raw('taksirawal.isFinal'), '=', 0);
        })
        ->join('tran_taksiran as taksirfinal', function($join){
            $join->on('taksirfinal.idFAPG', '=', 'trans_gadai_syariah.idFAPG');
            $join->where(DB::raw("taksirfinal.isFinal"), '=', 1);
        })
        ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl_syariah.tanggalPencairan, trans_gadai_syariah.noSbg, trans_gadai_syariah.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, tran_fapg_syariah.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, trans_gadai_syariah.created_at AS timeInputGadai, trans_gadai_syariah.tglApprovalKaunit, trans_gadai_syariah.tglApprovalKacab, trans_gadai_syariah.tglApprovalKawil, trans_gadai_syariah.tglApprovalDirektur, trans_gadai_syariah.tglApprovalDirut, trans_gadai_syariah.tglCetakKwitansi AS timeCetakKwitansi, trans_gadai_syariah.nilaiPinjaman, ROUND((trans_gadai_syariah.nilaiPinjaman / taksirfinal.totalTaksiran) * 100, 2) AS ltv, trans_gadai_syariah.rateFlat, trans_gadai_syariah.totalObligor, trans_gadai_syariah.statusAplikasi, trans_gadai_syariah.approvalFinal, trans_gadai_syariah.approvalLtv, trans_gadai_syariah.approvalUangPinjaman, trans_gadai_syariah.approvalRateFlat, trans_gadai_syariah.approvalOneObligor, trans_gadai_syariah.timeFotoNasabah, trans_gadai_syariah.tglApprovalKadeptCpa, trans_gadai_syariah.tglApprovalKadivCpa")
        ->orderBy('trans_payctrl_syariah.tanggalPencairan');
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->tgl_awal) {
            $query = $query->where('trans_payctrl_syariah.tanggalPencairan', '>=', $this->tgl_awal);
        }
        if ($this->tgl_sampai) {
            $query = $query->where('trans_payctrl_syariah.tanggalPencairan', '<=', $this->tgl_sampai);
        }
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tgl_awal .' S/d '. $this->tgl_sampai
            ],
            [
                'No',
                'Kode Outlet',
                'Nama Outlet',
                'Nama Cabang',
                'Nama Wilayah',
                'Tgl Pencairan',
                'No SBG',
                'Jenis Pembayaran Final',
                'CIF',
                'Nama',
                'No Ktp',
                'Nilai Pinjaman',
                'Rate',
                'LTV',
                'One Obligor',
                'Status Aplikasi',
                'Time Input FAPG',
                'Time Taksiran I',
                'Time Taksiran II',
                'Time Input Gadai',
                'Time KaUnit',
                'Time Kacab',
                'Time Kawil',
                'Time KaDept CPA',
                'Time KaDiv CPA',
                'Time Direktur',
                'Time Dirut',
                'Time Kwitansi',
                'Time Foto Nasabah',
                'Final Approval',
                'Approval UP',
                'Approval Rate',
                'Approval LTV',
                'Approval Obligor',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->kodeOutlet, 
            $data->namaOutlet, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->tanggalPencairan, 
            "'".$data->noSbg, 
            $data->jenisPembayaranFinal,
            $data->cif, 
            $data->namaCustomer, 
            "'".$data->noKtp, 
            $data->nilaiPinjaman, 
            $data->rateFlat, 
            $data->ltv, 
            $data->totalObligor, 
            $data->statusAplikasi, 
            $data->timeInputFAPG, 
            $data->timeTaksirPenaksir, 
            $data->timeTaksirKaUnit, 
            $data->timeInputGadai, 
            $data->tglApprovalKaunit, 
            $data->tglApprovalKacab, 
            $data->tglApprovalKawil, 
            $data->tglApprovalKadeptCpa, 
            $data->tglApprovalKadivCpa, 
            $data->tglApprovalDirektur, 
            $data->tglApprovalDirut, 
            $data->timeCetakKwitansi,
            $data->timeFotoNasabah,
            $data->approvalFinal,
            $data->approvalUangPinjaman,
            $data->approvalRateFlat,
            $data->approvalLtv,
            $data->approvalOneObligor,
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class WaktuTransaksiSyariahExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah, 
//             "nama_cabang"   => $this->nama_cabang, 
//             "tanggal"       => $this->tanggal, 
//             "nama_unit"  => $this->nama_unit, 
//             "data"          => $data, 
//         ]);
//     }
// }
