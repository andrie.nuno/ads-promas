<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Auth;
use app\CustomClass\helpers;
use App\model\kasbank;
use App\model\tbluserclient;
use App\model\invsaldo;
use App\model\invsaldoevent;

class OutstandingSaldoExcel implements FromView, ShouldAutoSize
{
    protected $id;
    protected $start;
    protected $end;

    function __construct($id, $start, $end) {
        //dd($idUserClient);
        $this->idUserClient = $id;
        $this->start = $start;
        $this->end = $end;
    }
    
    public function view(): View
    {
        if ($this->idUserClient != "") {
            $user = tbluserclient::where('idUserClient', '=', $this->idUserClient)->first();
            $data = kasbank::leftJoin('deb_pinjaman', 'deb_pinjaman.refNo', '=', 'kasbank.refNo')
                ->leftJoin('mitra_gadai', 'mitra_gadai.noSbg', '=', 'deb_pinjaman.noSbg')
                ->leftJoin('tblmitra', 'tblmitra.idMitra', '=', 'deb_pinjaman.idMitra')
                ->selectRaw('kasbank.tanggal, kasbank.refNo, kasbank.kdTrans, kasbank.keterangan, kasbank.amount, deb_pinjaman.noSbg, mitra_gadai.nama, mitra_gadai.namaBarang, mitra_gadai.karat, mitra_gadai.gram, mitra_gadai.TujuanPinjaman, tblmitra.namaMitra')
                ->where('kasbank.idUserClient', '=', $this->idUserClient)
                ->orderBy('kasbank.tglRekap', 'DESC')
                ->offset($this->start)
                ->limit($this->end)
                ->get();
            return view('login.laporanlender.outstandingsaldo.excel',[
                "user"          => $user,
                "data"          => $data, 
                "start"         => $this->start, 
            ]);
        } else {
            // $data = invsaldo::latest('inv_saldo.idUserClient')
            //     ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'inv_saldo.idUserClient')
            //     ->leftJoin('inv_saldo_event', 'inv_saldo_event.idUserClient', '=', 'inv_saldo.idUserClient')
            //     ->leftJoin('sell_trans AS bung', 'bung.idUserClient', '=', 'inv_saldo.idUserClient')
            //     ->whereIn('tbluserclient.isSystem', array(1, 2))
            //     ->selectRaw('inv_saldo.idUserClient, tbluserclient.userId, tbluserclient.userName, inv_saldo.saldo, SUM(inv_saldo_event.saldo) AS saldoEvent, SUM(bung.bunga) AS bunga')
            //     ->groupBy('inv_saldo.idUserClient')
            //     ->orderBy('inv_saldo.idUserClient')
            //     ->get();

            $data = invsaldo::latest('inv_saldo.idUserClient')
                ->join('tbluserclient', 'tbluserclient.idUserClient', '=', 'inv_saldo.idUserClient')
                ->leftJoin('inv_saldo_event', 'inv_saldo_event.idUserClient', '=', 'inv_saldo.idUserClient')
                ->whereIn('tbluserclient.isSystem', array(1, 2))
                ->where('inv_saldo.saldo', '!=', 0)
                ->selectRaw('inv_saldo.idUserClient, tbluserclient.userId, tbluserclient.userName, inv_saldo.saldo, SUM(inv_saldo_event.saldo) AS saldoEvent')
                ->groupBy('inv_saldo.idUserClient')
                ->orderBy('inv_saldo.idUserClient')
                ->get();

            // $data = Inspector::latest('id')
            //     ->select('id', 'firstname', 'status', 'state', 'phone')
            //     ->where('firstname', 'LIKE', '%' . $searchtext . '%')
            //     ->chunk(50, function($inspectors) {
            //         foreach ($inspectors as $inspector) {
            //             // apply some action to the chunked results here
            //         }
            //     });

            $dataInv = invsaldo::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'inv_saldo.idUserClient')
                ->whereIn('tbluserclient.isSystem', array(1, 2))
                ->selectRaw('SUM(inv_saldo.saldo) AS saldoInv')
                ->first();

            $dataEvn = invsaldoevent::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'inv_saldo_event.idUserClient')
                ->whereIn('tbluserclient.isSystem', array(1, 2))
                ->selectRaw('SUM(inv_saldo_event.saldo) AS saldoEvent')
                ->first();

            return view('login.laporanlender.outstandingsaldo.excelall',[
                "data"          => $data, 
                "totalInv"      => $dataInv->saldoInv, 
                "totalEvn"      => $dataEvn->saldoEvent, 
            ]);
        }
        
    }
}
