<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_payctrl;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class AzKonvensionalExcel implements FromQuery, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class AzKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    protected $lender;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $query = trans_payctrl::
            join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_payctrl.idCustomer')
            ->leftjoin('tblkelurahan', 'tblkelurahan.idKelurahan', '=', 'tblcustomer.idKelurahanKtp')
            ->leftjoin('tblkelurahan as domisili', 'domisili.idKelurahan', '=', 'tblcustomer.idKelurahanDomisili')
            ->leftjoin('tblpekerjaan', 'tblpekerjaan.idPekerjaan', '=', 'tblcustomer.idPekerjaan')
            ->leftjoin('tblsektorekonomi', 'tblsektorekonomi.idSektorEkonomi', '=', 'tblcustomer.idSektorEkonomi')
            ->join('tran_taksiran', 'tran_taksiran.idTaksiran', '=', 'trans_gadai.idTaksiran')
            ->join('trans_payment_schedule', function ($join) use ($tanggal) {
                $join->on('trans_payment_schedule.idGadai', '=', 'trans_payctrl.idGadai')
                ->where(function ($query) use ($tanggal) {
                    $query->whereNull('trans_payment_schedule.tglLunas')
                    // $dd2 = dd($tanggal);
                        ->orWhere('trans_payment_schedule.tglLunas', '>', $tanggal);
                });
                })
            ->leftjoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai')
            ->leftJoin("tblsettingjf", function($join){
                $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
                $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
                
            })
            ->leftjoin('tbltujuantransaksi', 'tbltujuantransaksi.idTujuanTransaksi', '=', 'trans_gadai.idTujuanTransaksi')
            ->leftJoin("trans_fundingdetail", function($join){
                $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
                $join->whereNull('trans_fundingdetail.tglUnpledging');
                
            })
            ->leftjoin('trans_funding', 'trans_funding.idFunding', '=', 'trans_fundingdetail.idFunding')
            ->leftjoin('tblpartner', 'tblpartner.idPartner', '=', 'trans_funding.idPartner')
            ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.kd_wilayah AS kodeWilayah, tblwilayah.namaWilayah, 
            trans_gadai.noSbg, tblcustomer.cif AS noCif, tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
            tblcustomer.jenisKelamin, tblcustomer.statusPerkawinan AS statusPernikahan, tblpekerjaan.namaPekerjaan AS pekerjaan, 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
            tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
            tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
            IF(tblcustomer.alamatDomisili != '', 
                REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
                REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
            ) AS alamatDomisili, 
            IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
            IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
            IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
            IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
            IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
            IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
            IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
            tblcustomer.hp AS noHp, tblsektorekonomi.namaSektorEkonomi, trans_payctrl.tanggalPencairan AS tanggalCair, trans_gadai.tglJatuhTempo, 
            IF(tblproduk.idJenisProduk = 2, trans_payctrl.tanggalJtCicilan, trans_gadai.tglJatuhTempo) AS tglJatuhTempoCicilan, 
            IF(tblproduk.idJenisProduk = 2, trans_payctrl.angsuranKe, 1) AS angsuranKe, 
            IF(tblproduk.idJenisProduk = 2, DATEDIFF($tanggal, trans_payctrl.tanggalJtCicilan), DATEDIFF($tanggal, trans_gadai.tglJatuhTempo)) AS ovd,
            trans_gadai.lamaPinjaman AS tenor, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, 
            trans_gadai.nilaiPinjaman AS pokokAwal, 
            IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.pokokAngsuran), trans_gadai.nilaiPinjaman) AS saldoPokok, 
            trans_payctrl.bungaFull AS saldoSimpan, 
            IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.bungaAngsuran), trans_payctrl.bungaFull) AS saldoSimpanOutstanding, 
            trans_gadai.biayaAdmin, tblpartner.namaPartner AS bankFunding, trans_gadaijf.idJf, 
            tblsettingjf.namaPendana, trans_gadaijf.rateJf, trans_gadaijf.porsiJf, trans_payctrl.statusPinjaman, tran_taksiran.totalTaksiran, 
            tran_taksiran.sumBeratBersih, tran_taksiran.avgKarat, tbltujuantransaksi.namaTujuanTransaksi, trans_gadai.statusAplikasi, 
            trans_gadai.idAsalJaminan")
            ->where('trans_payctrl.statusPinjaman', '!=', 'WO')
            ->where('trans_payctrl.tanggalPencairan', '<=', $tanggal)
            ->where('trans_payctrl.tanggalpelunasan', '>', $tanggal)
            ->orWhereNull('trans_payctrl.tanggalpelunasan');
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query = $query->where('tblcabang.idCabang', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query = $query->where('tblcabang.headCabang', $this->nama_unit);
        }
        $query = $query->groupBy('trans_payctrl.idGadai');
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tanggal
            ],
            [
                'No',
                'Kode Produk',
                'Nama Cabang',
                'Nama Wilayah',
                'No SBG',
                'No CIF',
                'Nama Customer',
                'No Ktp',
                'Tempat Lahir',
                'Tanggal Lahir',
                'Jenis Kelamin',
                'Status Pernikahan',
                'Pekerjaan',
                'Rt Ktp',
                'Rw Ktp',
                'Kelurahan',
                'Kecamatan',
                'Kabupaten',
                'Provinsi',
                'Kodepos',
                'Alamat Domisili',
                'rt Domisili',
                'rw Domisili',
                'Kelurahan Domisili',
                'Kecamatan Domisili',
                'Kabupaten Domisili',
                'Provinsi Domisili',
                'Kodepos Domisili',
                'No Hp',
                'Nama Sektor Ekonomi',
                'Tgl. Cair',
                'Tgl. Jatuh Tempo',
                'Tgl. Jatuh Tempo Cicilan',
                'Angsuran Ke',
                'ovd',
                'Tenor',
                'Rate Flat',
                'LTV',
                'Pokok Awal',
                'Saldo Pokok',
                'Saldo Simpan',
                'Saldo Simpan Outstanding',
                'Biaya Admin',
                'Bank Funding',
                'IDJF',
                'Nama Pendana',
                'Rate Jf',
                'Porsi Jf',
                'Status Pinjaman',
                'Total Taksiran',
                'Sum Berat Bersih',
                'AVG Karat',
                'Nama Tujuan Transaksi',
                'Status Aplikasi',
                'ID Asal Jaminan',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->produk, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->noSbg, 
            $data->noCif, 
            $data->namaCustomer, 
            "'".$data->noKtp, 
            $data->tempatLahir, 
            $data->tanggalLahir, 
            $data->jenisKelamin, 
            $data->statusPernikahan, 
            $data->pekerjaan, 
            $data->rtKtp, 
            $data->rwKtp, 
            $data->namakelurahan, 
            $data->namaKecamatan, 
            $data->namaKabupaten, 
            $data->namaProvinsi, 
            $data->kodepos, 
            $data->alamatDomisili, 
            $data->rtDomisili, 
            $data->rwDomisili, 
            $data->kelurahanDomisili, 
            $data->kecamatanDomisili, 
            $data->kabupatenDomisili, 
            $data->provinsiDomisili, 
            $data->kodePosDomisili, 
            $data->noHp, 
            $data->namaSektorEkonomi, 
            $data->tanggalCair,
            $data->tglJatuhTempo,
            $data->tglJatuhTempoCicilan,
            $data->angsuranKe,
            $data->ovd,
            $data->tenor, 
            $data->rateFlat, 
            $data->ltv, 
            $data->pokokAwal, 
            $data->saldoPokok, 
            $data->saldoSimpan, 
            $data->saldoSimpanOutstanding, 
            $data->biayaAdmin, 
            $data->bankFunding, 
            // $data->status == 'Proses Jual' ? $data->jmlHari : $data->jumlahHari, 
            // $data->status == 'Proses Jual' ? $data->ovd : $data->jumlahOvd, 
            // $data->jumlahHari <= $data->jmlHari ? $data->jumlahHari : $data->jmlHari, 
            // $data->jumlahOvd <= $data->ovd ? $data->jumlahOvd : $data->ovd, 
            $data->idJf, 
            $data->namaPendana, 
            // $data->tglJt < date('Y-m-d') ? $data->bungaTerhutang : $data->bungaAcrue, 
            $data->rateJf, 
            // $data->status == 'Gagal Bayar' ? $data->jasaMitraTerhutang : $data->mitraAcrue, 
            // $data->status == 'Proses Jual' ? $data->jasaMitraTerhutang : $data->mitraAcrue, 
            $data->porsiJf, 
            $data->statusPinjaman, 
            // $data->jumlahHari > $data->tenor ? $data->dendaAcrue : 0, 
            // $data->status == 'Proses Jual' ? $data->dendaTerhutang : ($data->jumlahHari > $data->tenor ? $data->dendaAcrue : 0), 
            // '=AI'.$this->current_row_al++.' + AR'.$this->current_row_ap++.' + AT'.$this->current_row_ar++.' + AW'.$this->current_row_au++, 
            $data->totalTaksiran, 
            $data->sumBeratBersih, 
            $data->avgKarat, 
            $data->namaTujuanTransaksi, 
            $data->statusAplikasi, 
            $data->idAsalJaminan, 
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class AzKonvensionalExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah, 
//             "nama_cabang"   => $this->nama_cabang, 
//             "tanggal"       => $this->tanggal, 
//             "nama_unit"  => $this->nama_unit, 
//             "data"          => $data, 
//         ]);
//     }
// }
