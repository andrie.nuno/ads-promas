<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\tbluserclient;

class PendanaAktifExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $awal;
    protected $sampai;

    function __construct($jenis, $awal, $sampai) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->awal = $awal;
        $this->sampai = $sampai;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Detail") {
            $data = tbluserclient::join('tblpendana', 'tblpendana.email', '=', 'tbluserclient.email')
                ->join('inv_saldo', 'inv_saldo.idUserClient', '=', 'tbluserclient.idUserClient')
                ->orderBy('tblpendana.tgl_proses')
                ->selectRaw('tbluserclient.idUserClient, tbluserclient.userId, tbluserclient.userName, tbluserclient.email, tbluserclient.isActive, tblpendana.tgl_proses, inv_saldo.saldo')
                ->where('inv_saldo.saldo', '!=', 0)
                ->where('tblpendana.tgl_proses', '>=', $this->awal.' 00:00:00')
                ->where('tblpendana.tgl_proses', '<=', $this->sampai.' 23:59:59')
                ->get();
        } else if ($this->jenis == "Tanggal") {
            $data = tbluserclient::join('tblpendana', 'tblpendana.email', '=', 'tbluserclient.email')
                ->join('inv_saldo', 'inv_saldo.idUserClient', '=', 'tbluserclient.idUserClient')
                ->selectRaw('SUBSTRING(tblpendana.tgl_proses,1,10) AS tanggal, count(tbluserclient.idUserClient) AS jumlah, sum(inv_saldo.saldo) AS totalSaldo')
                ->where('inv_saldo.saldo', '!=', 0)
                ->where('tblpendana.tgl_proses', '>=', $this->awal.' 00:00:00')
                ->where('tblpendana.tgl_proses', '<=', $this->sampai.' 23:59:59')
                ->groupByRaw('SUBSTRING(tblpendana.tgl_proses,1,10)')
                ->orderByRaw('SUBSTRING(tblpendana.tgl_proses,1,10)')
                ->get();
        } else {
            $data = tbluserclient::join('tblpendana', 'tblpendana.email', '=', 'tbluserclient.email')
                ->join('inv_saldo', 'inv_saldo.idUserClient', '=', 'tbluserclient.idUserClient')
                ->selectRaw('SUBSTRING(tblpendana.tgl_proses,1,4) AS tahun, SUBSTRING(tblpendana.tgl_proses,6,2) AS bulan, count(tbluserclient.idUserClient) AS jumlah, sum(inv_saldo.saldo) AS totalSaldo')
                ->where('inv_saldo.saldo', '!=', 0)
                ->where('tblpendana.tgl_proses', '>=', $this->awal.' 00:00:00')
                ->where('tblpendana.tgl_proses', '<=', $this->sampai.' 23:59:59')
                ->groupByRaw('SUBSTRING(tblpendana.tgl_proses,1,7)')
                ->orderByRaw('SUBSTRING(tblpendana.tgl_proses,1,7)')
                ->get();
        }
        return view('login.laporanlender.pendanaaktif.excel',[
            "jenisLaporan"  => $this->jenis, 
            "tglAwal"       => $this->awal,
            "tglSampai"     => $this->sampai,
            "data"          => $data, 
        ]);
    }
}
