<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\trans_gadai;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class BookingKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class BookingKonvensionalExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $query = trans_gadai::
        join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        ->leftJoin('tblkelurahan','tblkelurahan.idKelurahan','=','tblcustomer.idKelurahanKtp')
        ->leftJoin('tblkelurahan AS domisili','domisili.idKelurahan','=','tblcustomer.idKelurahanDomisili')
        ->leftJoin('tblpekerjaan','tblpekerjaan.idPekerjaan','=','tblcustomer.idPekerjaan')
        ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        ->join('tran_taksiran','tran_taksiran.idTaksiran','=','trans_gadai.idTaksiran')
        ->join('tran_fapg','tran_fapg.idFAPG','=','trans_gadai.idFAPG')
        ->leftJoin("tran_taksiran as taksir", function($join){
            $join->on('taksir.idFAPG','=','trans_gadai.idFAPG');
            $join->where(DB::raw("taksir.isFinal"), "=", 0);
            
        })
        ->leftJoin('tbltujuantransaksi','tbltujuantransaksi.idTujuanTransaksi','=','trans_gadai.idTujuanTransaksi')
        ->leftJoin('tblsektorekonomi','tblsektorekonomi.idSektorEkonomi','=','trans_gadai.idSektorEkonomi')
        ->leftJoin('tbljenisreferensi','tbljenisreferensi.idJenisReferensi','=','trans_gadai.idJenisReferensi')
        ->leftJoin('tblcustomer AS cgc','cgc.cif','=','trans_gadai.referensiCif')
        ->leftJoin('tblasaljaminan','tblasaljaminan.idAsalJaminan','=','trans_gadai.idAsalJaminan')
        ->leftJoin('tbljenisreferensi AS refCust','refCust.idJenisReferensi','=','tblcustomer.idJenisReferensi')
        ->leftJoin('tblcustomer AS cgcNasabah','cgcNasabah.cif','=','tblcustomer.referensiCif')
        ->leftJoin('trans_gadai AS sbgLama','sbgLama.idGadai','=','tran_fapg.idGadai')
        ->leftJoin('trans_payctrl AS payctrlLama','payctrlLama.idGadai','=','tran_fapg.idGadai')
        ->leftJoin('trans_gadaijf','trans_gadaijf.idGadai','=','trans_gadai.idGadai')
        ->leftJoin('tbluser','tbluser.idUser','=','tran_taksiran.idUser')
        ->leftJoin('tblkaryawaninternal','tblkaryawaninternal.idKaryawan','=','tbluser.idKaryawan')
        ->leftJoin("tblkaryawaninternal as refInternal", function($join){
            $join->on('refInternal.npk', '=', 'trans_gadai.referensiNpk');
            $join->where(DB::raw("refInternal.isActive"), "=", 1);
            
        })
        ->leftJoin("tblkaryawaneksternal as refEksternal", function($join){
            $join->on('refEksternal.kodeAgent', '=', 'trans_gadai.referensiNpk');
            $join->where(DB::raw("refEksternal.isActive"), "=", 1);
            
        })
        ->leftJoin('tbljabatankaryawan as jabInternal','jabInternal.idJabatanKaryawan','=','refInternal.idJabatanKaryawan')
        ->leftJoin('tbljabatankaryawan as jabeksternal','jabeksternal.idJabatanKaryawan','=','refEksternal.idJabatanKaryawan')
        ->leftJoin('tblprogram','tblprogram.idProgram','=','trans_gadai.idProgram')
        ->leftJoin("trans_fundingdetail", function($join){
            $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
            $join->whereNull('trans_fundingdetail.tglUnpledging');
            
        })
        ->leftJoin("tblsettingjf", function($join){
            $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
            $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            
        })
        ->leftJoin('trans_funding','trans_funding.idFunding','=','trans_fundingdetail.idFunding')
        ->leftJoin('tblpartner','tblpartner.idPartner','=','trans_funding.idPartner')
        ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet,
        CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, tblcustomer.cif, 
        tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
        tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
        tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
        IF(tblcustomer.alamatDomisili != '', 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
        ) AS alamatDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
        tblcustomer.hp AS noHp, tblpekerjaan.namaPekerjaan AS pekerjaan, 
        tblcustomer.tglRegister, DATE(trans_gadai.tglCair) AS tglCair, trans_payctrl.tanggalPelunasan, trans_gadai.lamaPinjaman AS tenor, trans_payctrl.ovd, 
        trans_gadai.transKe AS ke, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, 
        trans_gadai.nilaiPinjaman AS pokokAwal, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS pinjamAwal, 
        trans_gadai.nilaiPinjaman AS saldoPokok, trans_gadai.biayaPenyimpanan AS saldoBunga, 
        (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS saldoPinjam, 
        trans_gadai.biayaAdmin, trans_gadai.biayaAdminAwal, trans_gadai.diskonAdmin, taksir.totalTaksiran AS taksir1, 
        taksir.avgKarat AS karat1, taksir.sumBeratBersih AS berat1, tran_taksiran.totalTaksiran AS taksir, tran_taksiran.avgKarat AS karat, 
        tran_taksiran.sumBeratBersih AS berat, tran_fapg.stleAntam AS stle, tbltujuantransaksi.namaTujuanTransaksi, 
        tblsektorekonomi.namaSektorEkonomi, tbljenisreferensi.namaJenisReferensi AS asalAplikasi, trans_gadai.referensiNpk AS kodeReferensiAplikasi, 
        trans_gadai.referensiNama AS namaReferensiAplikasi, 
        IF(refInternal.idKaryawan != '', jabInternal.namaJabatanKaryawan, jabeksternal.namaJabatanKaryawan) AS jabatan, 
        CONCAT(cgc.cif, '-', cgc.namaCustomer) AS namaCgcAplikasi, tblprogram.namaProgram, trans_gadai.statusAplikasi, tblasaljaminan.namaAsalJaminan, 
        refCust.namaJenisReferensi AS referensiNasabah, tblcustomer.referensiNpk AS kodeReferensiNasabah, tblcustomer.referensiNama AS namaReferensiNasabah, 
        tblcustomer.referensiCif AS cifReferensiNasabah, cgcNasabah.namaCustomer AS namaCgcReferensiNasabah, sbgLama.noSbg AS noSbgLama, 
        payctrlLama.ovd AS ovdLama, (trans_gadai.transKe - 1) AS perpanjanganKe, trans_gadai.jenisPembayaran, tblcustomer.statusOcrKtp, 
        tblpartner.namaPartner AS bankFunding, tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.rate AS RatePendana, 
        tblsettingjf.namaJf AS bankPendana, tblsettingjf.porsi AS porsiBank, tblkaryawaninternal.namaKaryawan AS namaPenaksir, 
        trans_payctrl.created_at AS tanggalJam, tran_fapg.imgJaminan AS foto, trans_gadai.approvalFinal, 
        IF(trans_gadai.statusAplikasi = 'NEW ORDER', tblcustomer.kodeVoucher, '') AS kodeVoucher")
            ->where('trans_payctrl.tanggalPencairan', '>=', substr($tanggal,0,7)."-01")
            ->where('trans_payctrl.tanggalPencairan', '<', $tanggal)
            // ->where(DB::raw("(trans_payctrl.tanggalpelunasan > $tanggal or trans_payctrl.tanggalpelunasan is null)"))
            ;
        if ($this->nama_wilayah) {
            $query = $query->where('tblcabang.idWilayah', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query = $query->where('tblcabang.idCabang', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query = $query->where('tblcabang.headCabang', $this->nama_unit);
        }
        // $query = $query->groupBy('trans_payctrl.idGadai');
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tanggal
            ],
            [
                'No',
                'Kode Produk',
                'Kode Outlet',
                'Nama Outlet',
                'Nama Cabang',
                'Nama Wilayah',
                'No SBG',
                'No CIF',
                'Nama Customer',
                'No KTP',
                'Tempat Lahir',
                'Tanggal Lahir',
                'Alamat KTP',
                'RT Ktp',
                'RW Ktp',
                'Kelurahan',
                'Kecamatan',
                'Kabupaten',
                'Provinsi',
                'Kodepos',
                'Alamat Domisili',
                'RT Domisili',
                'RW Domisili',
                'Kelurahan Domisili',
                'Kecamatan Domisili',
                'Kabupaten Domisili',
                'Provinsi Domisili',
                'Kodepos Domisili',
                'No HP',
                'Pekerjaan',
                'Tanggal Register',
                'Tanggal Cair',
                'Tanggal Pelunasan',
                'Tenor',
                'ovd',
                'ke',
                'Rate Flat',
                'ltv',
                'Pokok Awal',
                'Pinjam Awal',
                'Saldo Pokok',
                'Saldo Bunga',
                'Saldo Pinjam',
                'Biaya Admin',
                'Biaya Admin Awal',
                'Diskon Admin',
                'taksir1',
                'karat1',
                'berat1',
                'taksir',
                'karat',
                'berat',
                'stle',
                'Nama Tujuan Transaksi',
                'Nama Sektor Ekonomi',
                'Asal Aplikasi',
                'Kode Referensi Aplikasi',
                'Nama Referensi Aplikasi',
                'Jabatan',
                'Nama Cgc Aplikasi',
                'Nama Program',
                'Nama Asal Jaminan',
                'Referensi Nasabah',
                'Kode Referensi Nasabah',
                'Nama Referensi Nasabah',
                'cif Referensi Nasabah',
                'Nama Cgc Referensi Nasabah',
                'no Sbg Lama',
                'ovd Lama',
                'perpanjangan Ke',
                'Jenis Pembayaran',
                'Status Ocr Ktp',
                'Bank Funding',
                'Kode Mitra',
                'Rate Pendana',
                'Bank Pendana',
                'Porsi Bank',
                'Nama Penaksir',
                'Tanggal Jam',
                'foto',
                'Approval Final',
                'kode Voucher',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->produk, 
            $data->kodeOutlet, 
            $data->namaOutlet, 
            $data->namaCabang, 
            $data->namaWilayah, 
            $data->noSbg, 
            $data->cif,
            $data->namaCustomer,
            "'".$data->noKtp, 
            $data->tempatLahir, 
            $data->tanggalLahir, 
            $data->alamatKtp, 
            $data->rtKtp, 
            $data->rwKtp, 
            $data->namakelurahan, 
            $data->namaKecamatan, 
            $data->namaKabupaten, 
            $data->namaProvinsi, 
            $data->kodepos, 
            $data->alamatDomisili, 
            $data->rtDomisili, 
            $data->rwDomisili, 
            $data->kelurahanDomisili, 
            $data->kecamatanDomisili, 
            $data->kabupatenDomisili, 
            $data->provinsiDomisili, 
            $data->kodePosDomisili, 
            $data->noHp, 
            $data->pekerjaan, 
            $data->tglRegister, 
            $data->tglCair, 
            $data->tanggalPelunasan,
            $data->tenor,
            $data->ovd,
            $data->ke,
            $data->rateFlat,
            $data->ltv,
            $data->pokokAwal,
            $data->pinjamAwal,
            $data->saldoPokok,
            $data->saldoBunga,
            $data->saldoPinjam,
            $data->biayaAdmin,
            $data->biayaAdminAwal,
            $data->diskonAdmin,
            $data->taksir1,
            $data->karat1,
            $data->berat1,
            $data->taksir,
            $data->karat,
            $data->berat,
            $data->stle,
            $data->namaTujuanTransaksi,
            $data->namaSektorEkonomi,
            $data->asalAplikasi,
            $data->kodeReferensiAplikasi,
            $data->namaReferensiAplikasi,
            $data->jabatan,
            $data->namaCgcAplikasi,
            $data->namaProgram,
            $data->statusAplikasi,
            $data->namaAsalJaminan,
            $data->kodeReferensiNasabah,
            $data->namaReferensiNasabah,
            $data->cifReferensiNasabah,
            $data->namaCgcReferensiNasabah,
            $data->noSbgLama,
            $data->ovdLama,
            $data->perpanjanganKe,
            $data->jenisPembayaran,
            $data->statusOcrKtp,
            $data->bankFunding,
            $data->kodeMitra,
            $data->RatePendana,
            $data->bankPendana,
            $data->porsiBank,
            $data->namaPenaksir,
            $data->tanggalJam,
            $data->foto,
            $data->approvalFinal,
            $data->kodeVoucher,
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class BookingKonvensionalExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }
    
//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah, 
//             "nama_cabang"   => $this->nama_cabang, 
//             "tanggal"       => $this->tanggal, 
//             "nama_unit"  => $this->nama_unit, 
//             "data"          => $data, 
//         ]);
//     }
// }
