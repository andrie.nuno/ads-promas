<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Auth;
use app\CustomClass\helpers;
use App\model\kasbank;

class CashbackVaExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $email;

    function __construct($jenis, $email) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->email = $email;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Total") {
            $query = kasbank::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->selectRaw('kasbank.idUserClient, SUM(kasbank.amount) AS cashback, tbluserclient.userId, tbluserclient.userName')
                ->where('kasbank.kdTrans', '=', 'CASH')
                ->groupBy('kasbank.idUserClient')
                ->orderBy('cashback', 'DESC');
            if ($this->email) {
                $query = $query->where('tbluserclient.userId', $this->email);
            }
            $data = $query->get();
        } else {
            $query = kasbank::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->selectRaw('kasbank.idUserClient, kasbank.tanggal, kasbank.keterangan, kasbank.amount AS cashback, tbluserclient.userId, tbluserclient.userName')
                ->where('kasbank.kdTrans', '=', 'CASH')
                ->orderBy('kasbank.tanggal', 'ASC');
                if ($this->email) {
                    $query = $query->where('tbluserclient.userId', $this->email);
                }
            $data = $query->get();
        }
        return view('login.laporanlender.cashbackva.excel',[
            "jenisLaporan"  => $this->jenis, 
            "email"         => $this->email,
            "data"          => $data, 
        ]);
    }
}
