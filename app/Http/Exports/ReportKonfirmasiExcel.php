<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\tbluserkonfirmasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class ReportKonfirmasiExcel implements FromQuery, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class ReportKonfirmasiExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $idUpdate;
    protected $nama_wilayah;
    protected $nama_cabang;
    protected $nama_unit;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($idUpdate, $nama_wilayah, $nama_cabang, $nama_unit) {
        // dd($idUpdate);
        $this->idUpdate = $idUpdate;
        $this->nama_wilayah = $nama_wilayah;
        $this->nama_cabang = $nama_cabang;
        $this->nama_unit = $nama_unit;
    }
    
    public function query()
    {
        $query = tbluserkonfirmasi::join('update_informasi', 'update_informasi.idUpdate', '=', 'tbluserkonfirmasi.idUpdate')
            ->join('tbluser', 'tbluser.idUser', '=', 'tbluserkonfirmasi.idUser')
            ->join('tblkaryawaninternal', 'tblkaryawaninternal.idKaryawan', '=', 'tbluser.idKaryawan')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'tbluser.idCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->selectRaw('tbluser.username, tblkaryawaninternal.namaKaryawan, tbluserkonfirmasi.timeKonfirmasi, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.namaWilayah');
        if ($this->idUpdate) {
            $query->where('update_informasi.idUpdate', $this->idUpdate);
        }
        if ($this->nama_wilayah) {
            $query->where('tblwilayah.idWilayah', $this->nama_wilayah);
        }
        if ($this->nama_cabang) {
            $query->where('tblcabang.headCabang', $this->nama_cabang);
        }
        if ($this->nama_unit) {
            $query->where('tblcabang.idCabang', $this->nama_unit);
        }
        $data = $query->orderBy('tbluserkonfirmasi.timeKonfirmasi');
        // dd($query);
        return $data;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Report Informasi'
            ],
            [
                'No',
                'NIK',
                'Nama',
                'Time Konfirmasi',
                'Kode Unit',
                'Nama Unit',
                'Nama Wilayah',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->username, 
            $data->namaKaryawan, 
            $data->timeKonfirmasi, 
            $data->kodeCabang, 
            $data->namaCabang, 
            $data->namaWilayah,
        ];
    }
}