<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use DB;
use Auth;
use app\CustomClass\helpers;
use App\model\kasbank;

class VoucherVaExcel implements FromView, ShouldAutoSize
{
    protected $jenis;
    protected $email;

    function __construct($jenis, $email) {
        //dd($jenis);
        $this->jenis = $jenis;
        $this->email = $email;
    }
    
    public function view(): View
    {
        if ($this->jenis == "Total") {
            $query = kasbank::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->selectRaw('kasbank.idUserClient, SUM(kasbank.amount) AS voucher, tbluserclient.userId, tbluserclient.userName')
                ->where('kasbank.kdTrans', '=', 'VCH')
                ->groupBy('kasbank.idUserClient')
                ->orderBy('voucher', 'DESC');
            if ($this->email) {
                $query = $query->where('tbluserclient.userId', $this->email);
            }
            $data = $query->get();
        } else {
            $query = kasbank::join('tbluserclient', 'tbluserclient.idUserClient', '=', 'kasbank.idUserClient')
                ->selectRaw('kasbank.idUserClient, kasbank.tanggal, kasbank.refNo, kasbank.amount AS voucher, tbluserclient.userId, tbluserclient.userName')
                ->where('kasbank.kdTrans', '=', 'VCH')
                ->orderBy('kasbank.tanggal', 'ASC');
                if ($this->email) {
                    $query = $query->where('tbluserclient.userId', $this->email);
                }
            $data = $query->get();
        }
        return view('login.laporanlender.voucherva.excel',[
            "jenisLaporan"  => $this->jenis, 
            "email"         => $this->email,
            "data"          => $data, 
        ]);
    }
}
