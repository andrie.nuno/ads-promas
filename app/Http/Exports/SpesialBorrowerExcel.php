<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\RegisterPeminjam;

class SpesialBorrowerExcel implements FromView, ShouldAutoSize
{
    protected $email_borrower;
    protected $middleman;

    function __construct($email_borrower, $middleman) {
        $this->email_borrower = $email_borrower;
        $this->middleman = $middleman;
    }
    
    public function view(): View
    {
        $query = RegisterPeminjam::join('tblmiddleman', 'tblmiddleman.idMiddleMan', '=', 'register_peminjam.idMiddleMan')
            ->selectRaw('register_peminjam.idBorrower, register_peminjam.email, register_peminjam.namaBorrower, tblmiddleman.namaMiddleMan, tblmiddleman.ratePelunasan, tblmiddleman.isActive');
        if ($this->email_borrower) {
            $data = $query->where('register_peminjam.email', 'like', '%'.$this->email_borrower.'%');
        }
        if ($this->middleman) {
            $data = $query->where('register_peminjam.idMiddleMan', '=', $this->middleman);
        }
        $data = $query->get();
        return view('login.mastermitra.spesialborrower.excel',[
            "email_borrower"=> $this->email_borrower, 
            "middleman"     => $this->middleman,
            "data"          => $data, 
        ]);
    }
}
