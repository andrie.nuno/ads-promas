<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use app\CustomClass\helpers;
use App\model\tbluserclient;

class PendanaAutobidExcel implements FromView, ShouldAutoSize
{
    protected $tanggal;

    function __construct($tanggal) {
        //dd($jenis);
        $this->tanggal = $tanggal;
    }
    
    public function view(): View
    {
        $data = tbluserclient::join('autobid', 'autobid.idUserClient', '=', 'tbluserclient.idUserClient')
            ->leftJoin('inv_saldo', 'inv_saldo.idUserClient', '=', 'tbluserclient.idUserClient')
            ->orderBy('inv_saldo.saldo', 'DESC')
            ->selectRaw('tbluserclient.idUserClient, tbluserclient.userId, tbluserclient.userName, tbluserclient.email, autobid.nominal, autobid.isActive, autobid.tgl_proses, inv_saldo.saldo')
            ->get();
        return view('login.laporanlender.pendanaautobid.excel',[
            "tanggal"       => $this->tanggal, 
            "data"          => $data, 
        ]);
    }
}
