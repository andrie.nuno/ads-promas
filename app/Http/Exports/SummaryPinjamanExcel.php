<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\payctrl;
use App\model\payctrl_view;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class SummaryPinjamanExcel implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
{
    use Exportable;

    protected $email_borrower;
    protected $status_borrower;
    private $rownum = 1;

    function __construct($email_borrower, $status_borrower, $jumlahAktif, $totalAktif, $jumlahExit) {
        //dd($email_borrower);
        $this->email_borrower = $email_borrower;
        $this->status_borrower = $status_borrower;
        $this->jumlahAktif = $jumlahAktif;
        $this->totalAktif = $totalAktif;
        $this->jumlahExit = $jumlahExit;
    }
    
    public function query()
    {
        $query = payctrl_view::query()
            ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'payctrl_view.idBorrower')
            ->orderBy('payctrl_view.totalOs', 'DESC')
            ->selectRaw('payctrl_view.idBorrower, payctrl_view.namaBorrower, payctrl_view.tglFirstPinjaman, payctrl_view.tglLastPinjaman, payctrl_view.jumlahPinjaman, payctrl_view.totalPinjaman, payctrl_view.jumlahPelunasan, payctrl_view.totalPelunasan, payctrl_view.jumlahOs, payctrl_view.totalOs');
        if ($this->email_borrower) {
            $query = $query->where('register_peminjam.email', $this->email_borrower);
        }
        if ($this->status_borrower == "Aktif") {
            $query = $query->whereNotNull('payctrl_view.jumlahOs');
        }
        if ($this->status_borrower == "Exit") {
            $query = $query->whereNull('payctrl_view.jumlahOs');
        }
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Jumlah Konsumen Aktif : '.$this->jumlahAktif
            ],
            [
                'Total Outstanding : '.$this->totalAktif
            ],
            [
                'Jumlah Konsumen Exit : '.$this->jumlahExit
            ],
            [
                'No',
                'Nama Konsumen',
                'Jml Pinjaman',
                'Total Pinjaman',
                'Jml Pelunasan',
                'Total Pelunasan',
                'Jml Outstanding',
                'Total Outstanding',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            $data->namaBorrower, 
            $data->jumlahPinjaman, 
            $data->totalPinjaman, 
            $data->jumlahPelunasan, 
            $data->totalPelunasan, 
            $data->jumlahOs, 
            $data->totalOs, 
        ];
    }
}
