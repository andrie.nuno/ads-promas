<?php

namespace App\Http\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
//use Maatwebsite\Excel\ExcelServiceProvider;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class PelunasanPinjamanExcel implements FromView, ShouldAutoSize
{
    protected $jenisLaporan;
    protected $nama_company;
    protected $nama_branch;
    protected $tglAwal;
    protected $tglSampai;
    protected $borrower;
    protected $lender;

    function __construct($jenisLaporan, $nama_company, $nama_branch, $tglAwal, $tglSampai, $borrower, $lender) {
        //dd($jenis);
        $this->jenisLaporan = $jenisLaporan;
        $this->nama_company = $nama_company;
        $this->nama_branch = $nama_branch;
        $this->tglAwal = $tglAwal;
        $this->tglSampai = $tglSampai;
        $this->borrower = $borrower;
        $this->lender = $lender;
    }
    
    public function view(): View
    {
        if ($this->jenisLaporan == "Detail") {
            $query = DB::Connection('mysql3')->table('view_pencairan AS view')
                ->join('tblsuperlender AS superlender', 'superlender.idUserClient', '=', 'view.idUserClient')
                ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
                ->join('db_mid_peminjam.register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'jaminan.idJaminan')
                ->join('db_mid_peminjam.register_peminjam AS peminjam', 'peminjam.idBorrower', '=', 'jaminan.idBorrower')
                ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
                ->leftJoin('db_mid_peminjam.member_company AS company', 'company.idCompany', '=', 'branch.idCompany')
                ->join('db_mid_peminjam.agreement_data AS agreement', 'agreement.idJaminan', '=', 'jaminan.idJaminan')
                ->join('db_mid_peminjam.payctrl AS payctrl', 'payctrl.idAgreement', '=', 'agreement.idAgreement')
                ->leftJoin('db_mid_peminjam.penjualan_jaminan AS penjualan', 'penjualan.idJaminan', '=', 'view.idJaminan')
                ->selectRaw('view.idAgreement, view.idJaminan, view.idBorrower, view.nama, view.ktpBorrower, view.ktp_peminjam, view.email, view.tlpMobile, view.hp_peminjam, view.namaPekerjaan, view.penghasilanPerBulan, view.namaSumberDana, view.sectorNama, view.userId, view.userName, superlender.namaPerson, view.ktp, view.hp, view.noSbg, view.idProduk, view.namaProduk, view.tenor, view.tujuanPinjaman, view.tujuanMitra, view.namaBranch, view.tanggalPengajuan, view.tanggalPencairan, view.tglJt, view.tangalJamPengajuan, view.created_at, view.tgl_prepayment, view.namaBarang, view.jumlah, view.gram, view.karat, view.nilaiStle, view.nilaiLtv, view.rateJasaTaksir, view.ratePendana, view.rateFeeDanain, view.ratePencairan, jaminan.hargaBeliPasar, view.nilaiTaksiran, view.nilaiMax, view.pinjaman, view.jasaTaksir, view.bungaPinjaman, view.feeDanain, view.bungaPendana, view.adminPencairan, view.nilaiPencairan, view.jmlHari, view.ovd, view.bungaDanainTerhutang, view.bungaLenderTerhutang, view.bungaTerhutang, view.jasaMitraTerhutang, view.dendaTerhutang, view.totalHutang, branch.kodeBranch, branch.namaBranch, company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, peminjam.jenisKelamin, peminjam.tanggalLahir, payctrl.pphNilai, penjualan.idJaminan AS lelang, penjualan.saldoPokokSelisih, penjualan.bungaLenderSelisih, penjualan.bungaDanainSelisih, penjualan.dendaLenderSelisih, penjualan.dendaDanainSelisih, penjualan.jasaMitraSelisih, penjualan.feePenjualanMitra, penjualan.feePenjualanDanain')
                ->where('view.tgl_prepayment', '>=', $this->tglAwal)
                ->where('view.tgl_prepayment', '<=', $this->tglSampai)
                ->orderBy('view.tgl_prepayment', 'ASC')
                ->groupBy('view.idJaminan');
            if ($this->nama_company) {
                $query = $query->where('branch.idCompany', $this->nama_company);
            }
            if ($this->nama_branch) {
                $query = $query->where('branch.idBranch', $this->nama_branch);
            }
            if ($this->borrower) {
                $query = $query->where('view.email', $this->borrower);
            }
            // if ($this->lender) {
            //     $query = $query->where('view.userId', $this->lender);
            // }
            if ($this->lender) {
                $query = $query->where('view.idUserClient', $this->lender);
            }
            $data = $query->get();
        } else if ($this->jenisLaporan == "Tanggal") {
            $query = DB::Connection('mysql3')->table('view_pencairan AS view')
                ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
                ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
                ->selectRaw('tgl_prepayment, COUNT(view.idDebPinjaman) AS jumlah, SUM(view.pinjaman) AS total')
                ->where('view.tgl_prepayment', '>=', $this->tglAwal)
                ->where('view.tgl_prepayment', '<=', $this->tglSampai)
                ->orderBy('view.tgl_prepayment', 'ASC')
                ->groupBy('view.tgl_prepayment');
            if ($this->nama_company) {
                $query = $query->where('branch.idCompany', $this->nama_company);
            }
            if ($this->nama_branch) {
                $query = $query->where('branch.idBranch', $this->nama_branch);
            }
            if ($this->borrower) {
                $query = $query->where('view.email', $this->borrower);
            }
            // if ($this->lender) {
            //     $query = $query->where('view.userId', $this->lender);
            // }
            if ($this->lender) {
                $query = $query->where('view.idUserClient', $this->lender);
            }
            $data = $query->get();
        } else {
            $query = DB::Connection('mysql3')->table('view_pencairan AS view')
                ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
                ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
                ->selectRaw('YEAR(view.tgl_prepayment) tahun, MONTH(view.tgl_prepayment) bulan, COUNT(view.idDebPinjaman) AS jumlah, SUM(view.pinjaman) AS total')
                ->where('view.tgl_prepayment', '>=', $this->tglAwal)
                ->where('view.tgl_prepayment', '<=', $this->tglSampai)
                ->orderBy('tahun', 'ASC')
                ->orderBy('bulan', 'ASC')
                ->groupBy('tahun')
                ->groupBy('bulan');
            if ($this->nama_company) {
                $query = $query->where('branch.idCompany', $this->nama_company);
            }
            if ($this->nama_branch) {
                $query = $query->where('branch.idBranch', $this->nama_branch);
            }
            if ($this->borrower) {
                $query = $query->where('view.email', $this->borrower);
            }
            // if ($this->lender) {
            //     $query = $query->where('view.userId', $this->lender);
            // }
            if ($this->lender) {
                $query = $query->where('view.idUserClient', $this->lender);
            }
            $data = $query->get();
        }
        return view('login.laporanborrower.pelunasanpinjaman.excel',[
            "jenisLaporan"  => $this->jenisLaporan, 
            "nama_company"  => $this->nama_company, 
            "nama_branch"   => $this->nama_branch, 
            "tglAwal"       => $this->tglAwal,
            "tglSampai"     => $this->tglSampai,
            "borrower"      => $this->borrower,
            "lender"        => $this->lender,
            "data"          => $data, 
        ]);
    }
}
