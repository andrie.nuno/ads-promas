<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\fa_transaksiinternal;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class MutasiAntarUnitExcel implements FromQuery, ShouldQueue, WithChunkReading, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class MutasiAntarUnitExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;


    protected $nama_wilayah;
    protected $tgl_awal;
    protected $tgl_sampai;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tgl_awal, $tgl_sampai) {
        //dd($tanggal);
        $this->tgl_awal = $tgl_awal;
        $this->tgl_sampai = $tgl_sampai;
    }

    public function query()
    {
        // $tanggal = $this->tanggal;
        $query = fa_transaksiinternal::join('tblcabang AS pengirim', 'pengirim.idCabang', '=', 'fa_transaksiinternal.idCabangKeluar')
        ->join('tblcabang AS penerima', 'penerima.idCabang', '=', 'fa_transaksiinternal.idCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'pengirim.idWilayah')
        ->join('tblbank AS bankpengirim', 'bankpengirim.idbank', '=', 'fa_transaksiinternal.idBankKeluar')
        ->join('tblbank AS bankpenerima', 'bankpenerima.idbank', '=', 'fa_transaksiinternal.idBankMasuk')
        ->join('tbluser AS userpengirim', 'userpengirim.idUser', '=', 'fa_transaksiinternal.idCreated')
        ->join('tblkaryawaninternal AS karpengirim', 'karpengirim.idKaryawan', '=', 'userpengirim.idKaryawan')
        ->leftJoin('tbluser AS userpenerima', 'userpenerima.idUser', '=', 'fa_transaksiinternal.idApproval')
        ->leftJoin('tblkaryawaninternal AS karpenerima', 'karpenerima.idKaryawan', '=', 'userpenerima.idKaryawan')
        ->selectRaw("fa_transaksiinternal.tanggal, 'Mutasi Antar Unit' AS jenisTransaksi, tblwilayah.namaWilayah,
        pengirim.kodeCabang AS kodeCabangPengirim, pengirim.namaCabang AS namaCabangPengirim, bankpengirim.namaBank AS bankKeluar,
        penerima.kodeCabang AS kodeCabangPenerima, penerima.namaCabang AS namaCabangPenerima, bankpenerima.namaBank AS bankMasuk,
        fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(fa_transaksiinternal.keteranganTransaksi, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS keteranganTransaksi,
        karpengirim.npk AS npkPengirim, karpengirim.namaKaryawan AS namaPengirim,
        IF(fa_transaksiinternal.statusApproval = 1, 'Sudah di Approve', 'Belum di Approve / Reject') AS statusApproval, karpenerima.npk AS npkPenerima,
        karpenerima.namaKaryawan AS namaPenerima, fa_transaksiinternal.dateApproval,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(fa_transaksiinternal.keteranganApproval, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS keteranganApproval")
        ->where('fa_transaksiinternal.idJenisTransaksi', 30)
        ->where('fa_transaksiinternal.tanggal', '>=', $this->tgl_awal)
        ->where('fa_transaksiinternal.tanggal', '<=', $this->tgl_sampai)
        ->orderBy('fa_transaksiinternal.tanggal');
        return $query;
    }

    public function chunkSize(): int
    {
        return 5000; // Adjust the chunk size as needed
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal : '.$this->tgl_awal .' S/d '. $this->tgl_sampai
            ],
            [
                'No',
                'Tanggal',
                'Jenis Transaksi',
                'Nama Wilayah',
                'Kode Cabang Pengirim',
                'Nama Cabang Pengirim',
                'Bank Keluar',
                'Kode Cabang Penerima',
                'Nama Cabang Penerima',
                'Bank Masuk',
                'No Voucher',
                'Nominal Transaksi',
                'Keterangan Transaksi',
                'NPK Pengirim',
                'Nama Pengirim',
                'Status Approval',
                'NPK Penerima',
                'Nama Penerima',
                'Date Approval',
                'Keterangan Approval',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++,
            $data->tanggal,
            $data->jenisTransaksi,
            $data->namaWilayah,
            $data->kodeCabangPengirim,
            $data->namaCabangPengirim,
            $data->bankKeluar,
            $data->kodeCabangPenerima,
            $data->namaCabangPenerima,
            $data->bankMasuk,
            $data->noVoucher,
            $data->nominalTransaksi,
            $data->keteranganTransaksi,
            $data->npkPengirim,
            $data->namaPengirim,
            $data->statusApproval,
            $data->npkPenerima,
            $data->namaPenerima,
            $data->dateApproval,
            $data->keteranganApproval,
        ];
    }
}

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// use Auth;
// use app\CustomClass\helpers;
// use App\model\payctrl;

// class MutasiAntarUnitExcel implements FromView, ShouldAutoSize
// {
//     protected $tanggal;
//     protected $nama_wilayah;
//     protected $nama_cabang;
//     protected $nama_unit;

//     function __construct($tanggal, $nama_wilayah, $nama_cabang, $nama_unit) {
//         //dd($tanggal);
//         $this->tanggal = $tanggal;
//         $this->nama_wilayah = $nama_wilayah;
//         $this->nama_cabang = $nama_cabang;
//         $this->nama_unit = $nama_unit;
//     }

//     public function view(): View
//     {
//         $tanggal = $this->tanggal;
//         $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
//             ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
//             ->join('register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'register_jaminan.idJaminan')
//             ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
//             ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
//             ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
//             ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
//             ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
//             ->leftJoin('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
//             ->orderBy('agreement_data.tanggal', 'DESC')
//             ->selectRaw('payctrl.idAgreement, payctrl.jmlHari, payctrl.ovd, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaHarian, payctrl.dendaTerhutang, payctrl.adminPelunasan, payctrl.totalHutang, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, agreement_data.pokokHutang AS nilaiPinjaman, register_jaminan.tujuanPinjaman, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.jenisKelamin, register_peminjam.tanggalLahir, register_peminjam.ktp, register_peminjam.tlpMobile, register_peminjam.penghasilanPerBulan, register_peminjam.biayaHidupPerBulan, master_pekerjaan.namaPekerjaan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, agreement_peminjam.tenor, tblproduk.keterangan AS namaProduk, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, register_jaminan.statusAplikasi, register_jaminan.statusAplikasiMeta, register_jaminan.statusAplikasiKonsol')
//             // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
//             ->where('agreement_data.tanggal', '<=', $tanggal)
//             ->where(function ($query2) use ($tanggal) {
//                 $query2->where('agreement_data.tglBayar', '>', $tanggal)
//                     ->orWhereNull('agreement_data.tglBayar');
//             });
//         if ($this->nama_wilayah) {
//             $query = $query->where('member_branch.idCompany', $this->nama_wilayah);
//         }
//         if ($this->nama_cabang) {
//             $query = $query->where('member_branch.idBranch', $this->nama_cabang);
//         }
//         if ($this->nama_unit) {
//             $query = $query->where('agreement_peminjam.ratePendana', $this->nama_unit);
//         }
//         $data = $query->groupBy('agreement_data.idAgreement')
//             ->get();
//         return view('login.laporanborrower.outstandingpinjaman.excel',[
//             "nama_wilayah"  => $this->nama_wilayah,
//             "nama_cabang"   => $this->nama_cabang,
//             "tanggal"       => $this->tanggal,
//             "nama_unit"  => $this->nama_unit,
//             "data"          => $data,
//         ]);
//     }
// }
