<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use App\model\payctrl;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;
use app\CustomClass\helpers;

class BarangJaminanExcel implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class OutstandingPinjamanExcel implements FromQuery, ShouldQueue, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $tanggal;
    protected $nama_company;
    protected $nama_branch;
    protected $rate_pendana;
    protected $lender;
    private $rownum = 1;
    private $current_row_al = 3;
    private $current_row_ap = 3;
    private $current_row_ar = 3;
    private $current_row_au = 3;

    function __construct($tanggal, $nama_company, $nama_branch, $rate_pendana, $lender) {
        //dd($tanggal);
        $this->tanggal = $tanggal;
        $this->nama_company = $nama_company;
        $this->nama_branch = $nama_branch;
        $this->rate_pendana = $rate_pendana;
        $this->lender = $lender;
    }
    
    public function query()
    {
        $tanggal = $this->tanggal;
        $query = payctrl::join('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
            ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'payctrl.idAgreement')
            ->join('register_jaminan', 'register_jaminan.idJaminan', '=', 'agreement_data.idJaminan')
            ->join('register_jaminan_detail', 'register_jaminan_detail.idJaminan', '=', 'register_jaminan.idJaminan')
            ->join('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_data.idBorrower')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
            ->join('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
            ->join('member_company', 'member_company.idCompany', '=', 'member_branch.idCompany')
            ->orderBy('agreement_data.tanggal', 'DESC')
            ->selectRaw('agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.idBorrower, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, tblproduk.keterangan AS namaProduk, register_peminjam.ktp, register_peminjam.email, register_peminjam.namaBorrower, register_peminjam.tlpMobile, payctrl.ovd, register_jaminan.statusAplikasi, register_jaminan_detail.jumlahJaminan, register_jaminan_detail.namaJaminan, register_jaminan_detail.gram, register_jaminan_detail.karat, register_jaminan_detail.keterangan, register_jaminan_detail.nilaiStle, register_jaminan_detail.nilaiLtv, register_jaminan_detail.nilaiTaksiran, register_jaminan_detail.nilaiMax, register_jaminan_detail.hargaPasar, register_jaminan_detail.hargaBeliPasar, register_jaminan_detail.ltvHargaBeliPasar, register_jaminan_detail.fotoJaminan, member_branch.kodeBranch, member_branch.namaBranch, member_company.namaCompany')
            // ->whereIn('payctrl.status', array("Aktif","Telat Bayar","Gagal Bayar"))
            ->where('agreement_data.tanggal', '<=', $tanggal)
            ->where(function ($query2) use ($tanggal) {
                $query2->where('agreement_data.tglBayar', '>', $tanggal)
                    ->orWhereNull('agreement_data.tglBayar');
            });
        if ($this->nama_company) {
            $query = $query->where('member_branch.idCompany', $this->nama_company);
        }
        if ($this->nama_branch) {
            $query = $query->where('member_branch.idBranch', $this->nama_branch);
        }
        if ($this->rate_pendana) {
            $query = $query->where('agreement_peminjam.ratePendana', $this->rate_pendana);
        }
        if ($this->lender) {
            $query = $query->where('payctrl.idUserClient', $this->lender);
        }
        $query = $query->orderBy('agreement_data.tanggal')
            ->orderBy('register_jaminan.idJaminan');
        return $query;
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Tanggal Outstanding Barang Jaminan : '.$this->tanggal
            ],
            [
                'No',
                'No PP',
                'Email',
                'CIF',
                'No KTP',
                'Nama Peminjam',
                'Tgl. Pencairan',
                'Tgl. JT',
                'Wilayah',
                'Kode Outlet',
                'Outlet Penaksir',
                'Produk Pinjaman',
                'Nama Jaminan',
                'Keterangan Jaminan',
                'Jumlah Jaminan',
                'Karat',
                'Gram',
                'STLE',
                'LTV',
                'Nilai Taksiran',
                'Max Pinjaman',
                'Nilai Pinjaman',
                'Harga Pasar', 
                'Harga Beli Pasar',
                'Ltv Harga Beli Pasar',
                'Foto Jaminan',
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            "'".$data->noPerjanjian, 
            $data->email, 
            $data->idBorrower, 
            "'".$data->ktp, 
            $data->namaBorrower, 
            $data->tanggal, 
            $data->tglJt, 
            $data->namaCompany, 
            $data->kodeBranch, 
            $data->namaBranch, 
            $data->namaProduk, 
            $data->namaJaminan, 
            $data->keterangan, 
            $data->jumlahJaminan, 
            $data->karat, 
            $data->gram, 
            $data->nilaiStle, 
            $data->nilaiLtv."%",
            $data->nilaiTaksiran, 
            $data->nilaiMax, 
            $data->nilaiPencairan, 
            $data->hargaPasar, 
            $data->hargaBeliPasar, 
            $data->ltvHargaBeliPasar."%", 
            $data->fotoJaminan, 
        ];
    }
}
