<?php

namespace App\Http\Exports;

use App\model\view_pencairan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithCustomQuerySize;
use Auth;
use DB;
use Exception;
use Session;

class PencairanPinjamanExcel implements FromQuery, WithHeadings, ShouldAutoSize, WithMapping, WithCustomQuerySize
// class PencairanPinjamanExcel implements WithCustomQuerySize, WithHeadings, ShouldAutoSize, WithMapping
{
    use Exportable;

    protected $jenisLaporan;
    protected $nama_company;
    protected $nama_branch;
    protected $tglAwal;
    protected $tglSampai;
    protected $borrower;
    protected $lender;
    private $rownum = 1;

    function __construct($jenisLaporan, $nama_company, $nama_branch, $tglAwal, $tglSampai, $borrower, $lender) {
        //dd($jenis);
        $this->jenisLaporan = $jenisLaporan;
        $this->nama_company = $nama_company;
        $this->nama_branch = $nama_branch;
        $this->tglAwal = $tglAwal;
        $this->tglSampai = $tglSampai;
        $this->borrower = $borrower;
        $this->lender = $lender;
    }

    public function query()
    // public function querySize(): int
    {
        if ($this->jenisLaporan == "Detail") {
            $query = view_pencairan::query()
                ->join('tblsuperlender AS superlender', 'superlender.idUserClient', '=', 'view_pencairan.idUserClient')
                ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view_pencairan.idJaminan')
                ->join('db_mid_peminjam.register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'jaminan.idJaminan')
                ->join('db_mid_peminjam.register_peminjam AS peminjam', 'peminjam.idBorrower', '=', 'jaminan.idBorrower')
                ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
                ->leftJoin('db_mid_peminjam.member_company AS company', 'company.idCompany', '=', 'branch.idCompany')
                ->leftJoin('db_mid_peminjam.master_jeniskelamin AS gender', 'gender.idJenisKelamin', '=', 'peminjam.jenisKelamin')
                ->selectRaw('view_pencairan.nama, view_pencairan.email, view_pencairan.namaPekerjaan, view_pencairan.penghasilanPerBulan, view_pencairan.namaSumberDana, view_pencairan.sectorNama, view_pencairan.userId, view_pencairan.userName, superlender.namaPerson, view_pencairan.ktp, view_pencairan.hp, view_pencairan.noSbg, view_pencairan.namaProduk, view_pencairan.tenor, view_pencairan.created_at, view_pencairan.tglJt, view_pencairan.namaBarang, view_pencairan.jumlah, view_pencairan.gram, view_pencairan.karat, view_pencairan.nilaiStle, view_pencairan.nilaiLtv, view_pencairan.rateJasaTaksir, view_pencairan.ratePendana, view_pencairan.rateFeeDanain, view_pencairan.ratePencairan, view_pencairan.nilaiTaksiran, view_pencairan.nilaiMax, view_pencairan.pinjaman, view_pencairan.jasaTaksir, view_pencairan.bungaPinjaman, view_pencairan.adminPencairan, view_pencairan.ktp_peminjam, view_pencairan.hp_peminjam, view_pencairan.tujuanPinjaman, view_pencairan.created_at AS tangalPengajuan, view_pencairan.nilaiPencairan, peminjam.tanggalLahir, peminjam.biayaHidupPerBulan, branch.kodeBranch, branch.namaBranch, company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, jaminan.referalSales, jaminan.namaSales, jaminan.referalSo, jaminan.namaSo, jaminan.statusAplikasi, jaminan.statusAplikasiMeta, jaminan.statusAplikasiKonsol, gender.namaJenisKelamin, (view_pencairan.pinjaman + view_pencairan.jasaTaksir + view_pencairan.bungaPinjaman) AS estimasiPelunasan, ROUND(((view_pencairan.jasaTaksir + view_pencairan.bungaPinjaman) / view_pencairan.tenor / view_pencairan.pinjaman * 360) * 100, 2) AS ekuivalensi')
                ->selectRaw('LPAD(view_pencairan.idBorrower, 9, "0") AS idBorrower')
                ->selectRaw('TIMESTAMPDIFF(YEAR, peminjam.tanggalLahir, view_pencairan.tanggalPencairan) AS usia')
                ->where('view_pencairan.tanggalPencairan', '>=', $this->tglAwal)
                ->where('view_pencairan.tanggalPencairan', '<=', $this->tglSampai)
                ->orderBy('view_pencairan.created_at', 'ASC')
                ->groupBy('view_pencairan.idJaminan');
            if ($this->nama_company) {
                $query = $query->where('branch.idCompany', $this->nama_company);
            }
            if ($this->nama_branch) {
                $query = $query->where('branch.idBranch', $this->nama_branch);
            }
            if ($this->borrower) {
                $query = $query->where('view_pencairan.email', $this->borrower);
            }
            // if ($this->lender) {
            //     $query = $query->where('view_pencairan.userId', $this->lender);
            // }
            if ($this->lender) {
                $query = $query->where('view_pencairan.idUserClient', $this->lender);
            }
            return $query;
            // $size = $this->query()->count();
            // return $size;
        }
    }

    public function querySize(): int
    {
        $size = $this->query()->count();
        return $size;
    }

    public function headings(): array
    {
        return [
            [
                'Laporan Pencairan'
            ],
            [
                'Mulai : '.$this->tglAwal
            ],
            [
                'Sampai : '.$this->tglSampai
            ],
            [
                'Borrower : '.$this->borrower
            ],
            [
                'Lender : '.$this->lender
            ],
            [
                'No',
                'No PP',
                'CIF',
                'Nama Peminjam',
                'Jenis Kelamin',
                'Tanggal Lahir',
                'Usia',
                'KTP Peminjam',
                'Email Peminjam',
                'HP Peminjam',
                'Pekerjaan Peminjam',
                'Penghasilan/Bulan Peminjam',
                'Biaya Hidup/Bulan Peminjam',
                'Sumber Penghasilan Peminjam',
                'Sektor Ekonomi Peminjam',
                'Nama Pendana',
                'Email Pendana',
                'KTP Pendana',
                'HP Pendana',
                'Produk Pinjaman',
                'Tujuan Pinjaman',
                'Wilayah',
                'Kode Outlet',
                'Outlet Penaksir',
                'Tgl. Pengajuan',
                'Tgl. Pencairan',
                'Tgl. JT',
                'Nama Jaminan',
                'Jumlah Jaminan',
                'Karat',
                'Rata-Rata Karatase',
                'Gram',
                'Jumlah Gram',
                'STLE',
                'LTV',
                'Rate Jasa Taksir',
                'Rate Pendana',
                'Rate Danain',
                'Rate Admin',
                'Nilai Taksiran',
                'Max Pinjaman',
                'Nilai Pinjaman',
                'Jasa Taksir Mitra',
                'Bunga Pinjaman',
                'Nilai Admin',
                'Nilai Pencairan',
                'Estimasi Pelunasan',
                'Ekuivalensi / Tahun',
                'Status Aplikasi Danain',
                'Status Aplikasi Gadai MAS',
                'Status Aplikasi Konsol',
                'Kode SA',
                'Nama SA',
                'Kode SO',
                'Nama SO'
            ]
        ];
    }

    public function map($data): array
    {
        return [
            $this->rownum++, 
            "'".$data->noSbg, 
            $data->idBorrower, 
            $data->nama, 
            $data->namaJenisKelamin, 
            $data->tanggalLahir, 
            $data->usia, 
            "'".$data->ktp_peminjam, 
            $data->email, 
            $data->hp_peminjam, 
            $data->namaPekerjaan, 
            $data->penghasilanPerBulan, 
            $data->biayaHidupPerBulan, 
            $data->namaSumberDana, 
            $data->sectorNama, 
            // $data->userName, 
            $data->namaPerson, 
            $data->userId, 
            "'".$data->ktp, 
            $data->hp, 
            $data->namaProduk, 
            $data->tujuanPinjaman, 
            $data->namaCompany, 
            $data->kodeBranch, 
            $data->namaBranch, 
            $data->tangalPengajuan, 
            $data->created_at, 
            $data->tglJt, 
            $data->namaBarang, 
            $data->jumlah, 
            $data->karat, 
            $data->avgKarat, 
            $data->gram, 
            $data->sumGram, 
            $data->nilaiStle, 
            $data->nilaiLtv."%",
            $data->rateJasaTaksir."%",
            $data->ratePendana."%",
            $data->rateFeeDanain."%",
            $data->ratePencairan >= 100 ? number_format(($data->ratePencairan / $data->pinjaman) * 100, 1)."%" : $data->ratePencairan."%",
            $data->nilaiTaksiran, 
            $data->nilaiMax, 
            $data->pinjaman, 
            $data->jasaTaksir, 
            $data->bungaPinjaman, 
            $data->adminPencairan, 
            $data->nilaiPencairan, 
            $data->estimasiPelunasan, 
            $data->ekuivalensi, 
            $data->statusAplikasi, 
            $data->statusAplikasiMeta, 
            $data->statusAplikasiKonsol, 
            $data->referalSo != '' ? $data->referalSales : '', 
            $data->referalSo != '' ? $data->namaSales : '', 
            $data->referalSo != '' ? $data->referalSo : $data->referalSales, 
            $data->referalSo != '' ? $data->namaSo : $data->namaSales, 
        ];
    }
}

// <tr>
//                     <td>{{ $nomor++ }}</td>
//                     <td>'{{ $dita->noSbg }}</td>
//                     <td>{{ \helpers::sembilanAngka($dita->idBorrower) }}</td>
//                     <td>{{ $dita->nama }}</td>
//                     <td>
//                         @if($dita->jenisKelamin == 1)
//                             Laki-Laki
//                         @elseif($dita->jenisKelamin == 2)
//                             Perempuan
//                         @else
//                             -
//                         @endif
//                     </td>
//                     <td>{{ $dita->tanggalLahir }}</td>
//                     <td>
//                         @php($tglLahir = date_create($dita->tanggalLahir))
//                         @php($tglToday = date_create($dita->tanggalPencairan))
//                         @php($diff  = date_diff($tglLahir, $tglToday))
//                         @php($usiaPeminjam = $diff->y)
//                         {{ $usiaPeminjam }}
//                     </td>
//                     <td>'{{ $dita->ktpBorrower == "" ? $dita->ktp_peminjam : $dita->ktpBorrower }}</td>
//                     <td>{{ $dita->email }}</td>
//                     <td>{{ $dita->tlpMobile == "" ? $dita->hp_peminjam : $dita->tlpMobile }}</td>
//                     <td>{{ $dita->namaPekerjaan }}</td>
//                     <td>{{ $dita->penghasilanPerBulan }}</td>
//                     <td>{{ $dita->biayaHidupPerBulan }}</td>
//                     <td>{{ $dita->namaSumberDana }}</td>
//                     <td>{{ $dita->sectorNama }}</td>
//                     <td>{{ $dita->userName }}</td>
//                     <td>{{ $dita->userId }}</td>
//                     <td>'{{ $dita->ktp }}</td>
//                     <td>{{ $dita->hp }}</td>
//                     <td>{{ $dita->namaProduk }}</td>
//                     <td>{{ $dita->tujuanPinjaman == "" ? $dita->tujuanMitra : $dita->tujuanPinjaman }}</td>
//                     <td>{{ $dita->namaCompany }}</td>
//                     <td>{{ $dita->kodeBranch }}</td>
//                     <td>{{ $dita->namaBranch }}</td>
//                     <td>{{ $dita->tangalJamPengajuan == "" ? $dita->created_at : $dita->tangalJamPengajuan }}</td>
//                     <td>{{ $dita->created_at }}</td>
//                     <td>{{ $dita->tglJt }}</td>
//                     <td>{{ $dita->namaBarang }}</td>
//                     <td>{{ $dita->jumlah }}</td>
//                     <td>{{ $dita->karat }}</td>
//                     <td>{{ $dita->avgKarat }}</td>
//                     <td>{{ $dita->gram }}</td>
//                     <td>{{ $dita->sumGram }}</td>
//                     <td>{{ $dita->nilaiStle }}</td>
//                     <td>{{ $dita->nilaiLtv }}%</td>
//                     <td>{{ $dita->rateJasaTaksir }}%</td>
//                     <td>{{ $dita->ratePendana }}%</td>
//                     <td>{{ $dita->rateFeeDanain }}%</td>
//                     <td>{{ $dita->ratePencairan }}%</td>
//                     <td>{{ $dita->nilaiTaksiran }}</td>
//                     <td>{{ $dita->nilaiMax }}</td>
//                     <td>{{ $dita->pinjaman }}</td>
//                     <td>{{ $dita->jasaTaksir }}</td>
//                     <td>{{ $dita->bungaPinjaman }}</td>
//                     <td>{{ $dita->adminPencairan }}</td>
//                     <td>{{ $dita->nilaiPencairan == 0 ? $dita->pinjaman : $dita->nilaiPencairan }}</td>
//                     <td>
//                         @php($estimasiPelunasan = $dita->pinjaman + $dita->jasaTaksir + $dita->bungaPinjaman)
//                         {{ $estimasiPelunasan }}
//                     </td>
//                     <td>
//                         @php($ekuivalen = round((($estimasiPelunasan - $dita->pinjaman) / $dita->tenor / $dita->pinjaman * 360) * 100, 2))
//                         {{ $ekuivalen }}
//                     </td>
//                     <td>{{ $dita->statusAplikasi }}</td>
//                     <td>{{ $dita->statusAplikasiMeta }}</td>
//                     <td>{{ $dita->statusAplikasiKonsol }}</td>
//                     <td>{{ $dita->referalSo != '' ? $dita->referalSales : '' }}</td>
//                     <td>{{ $dita->referalSo != '' ? $dita->namaSales : '' }}</td>
//                     <td>{{ $dita->referalSo != '' ? $dita->referalSo : $dita->referalSales }}</td>
//                     <td>{{ $dita->referalSo != '' ? $dita->namaSo : $dita->namaSales }}</td>
//                 </tr>

// namespace App\Http\Exports;

// use Illuminate\Contracts\View\View;
// use Maatwebsite\Excel\Concerns\FromView;
// use Maatwebsite\Excel\Concerns\ShouldAutoSize;
// //use Maatwebsite\Excel\ExcelServiceProvider;
// use Auth;
// use DB;
// use Exception;
// use Session;
// use app\CustomClass\helpers;

// class PencairanPinjamanExcel implements FromView, ShouldAutoSize
// {
//     protected $jenisLaporan;
//     protected $nama_company;
//     protected $nama_branch;
//     protected $tglAwal;
//     protected $tglSampai;
//     protected $borrower;
//     protected $lender;

//     function __construct($jenisLaporan, $nama_company, $nama_branch, $tglAwal, $tglSampai, $borrower, $lender) {
//         //dd($jenis);
//         $this->jenisLaporan = $jenisLaporan;
//         $this->nama_company = $nama_company;
//         $this->nama_branch = $nama_branch;
//         $this->tglAwal = $tglAwal;
//         $this->tglSampai = $tglSampai;
//         $this->borrower = $borrower;
//         $this->lender = $lender;
//     }
    
//     public function view(): View
//     {
//         if ($this->jenisLaporan == "Detail") {
//             $query = DB::Connection('mysql3')->table('view_pencairan AS view')
//                 ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
//                 ->join('db_mid_peminjam.register_jaminan_detail AS detail', 'detail.idJaminan', '=', 'jaminan.idJaminan')
//                 ->join('db_mid_peminjam.register_peminjam AS peminjam', 'peminjam.idBorrower', '=', 'jaminan.idBorrower')
//                 ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
//                 ->leftJoin('db_mid_peminjam.member_company AS company', 'company.idCompany', '=', 'branch.idCompany')
//                 ->selectRaw('view.idAgreement, view.idJaminan, view.idBorrower, view.nama, view.ktpBorrower, view.ktp_peminjam, view.email, view.tlpMobile, view.hp_peminjam, view.namaPekerjaan, view.penghasilanPerBulan, view.namaSumberDana, view.sectorNama, view.userId, view.userName, view.ktp, view.hp, view.noSbg, view.namaProduk, view.tenor, view.tujuanPinjaman, view.tujuanMitra, view.namaBranch, view.tanggalPengajuan, view.tanggalPencairan, view.tglJt, view.tangalJamPengajuan, view.created_at, view.namaBarang, view.jumlah, view.gram, view.karat, view.nilaiStle, view.nilaiLtv, view.rateJasaTaksir, view.ratePendana, view.rateFeeDanain, view.ratePencairan, view.nilaiTaksiran, view.nilaiMax, view.pinjaman, view.jasaTaksir, view.bungaPinjaman, view.adminPencairan, view.nilaiPencairan, peminjam.jenisKelamin, peminjam.tanggalLahir, peminjam.biayaHidupPerBulan, jaminan.referalSales, jaminan.namaSales, jaminan.referalSo, jaminan.namaSo, branch.kodeBranch, branch.namaBranch, company.namaCompany, SUM(detail.gram) AS sumGram, AVG(detail.karat) AS avgKarat, jaminan.statusAplikasi, jaminan.statusAplikasiMeta, jaminan.statusAplikasiKonsol')
//                 ->where('view.tanggalPencairan', '>=', $this->tglAwal)
//                 ->where('view.tanggalPencairan', '<=', $this->tglSampai)
//                 ->orderBy('view.tanggalPencairan', 'ASC')
//                 ->groupBy('view.idJaminan');
//             if ($this->nama_company) {
//                 $query = $query->where('branch.idCompany', $this->nama_company);
//             }
//             if ($this->nama_branch) {
//                 $query = $query->where('branch.idBranch', $this->nama_branch);
//             }
//             if ($this->borrower) {
//                 $query = $query->where('view.email', $this->borrower);
//             }
//             if ($this->lender) {
//                 $query = $query->where('view.userId', $this->lender);
//             }
//             $data = $query->get();
//         } else if ($this->jenisLaporan == "Tanggal") {
//             $query = DB::Connection('mysql3')->table('view_pencairan AS view')
//                 ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
//                 ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
//                 ->selectRaw('view.tanggalPencairan, COUNT(view.idDebPinjaman) AS jumlah, SUM(view.pinjaman) AS total')
//                 ->where('view.tanggalPencairan', '>=', $this->tglAwal)
//                 ->where('view.tanggalPencairan', '<=', $this->tglSampai)
//                 ->orderBy('view.tanggalPencairan', 'ASC')
//                 ->groupBy('view.tanggalPencairan');
//             if ($request->nama_company) {
//                 $query = $query->where('branch.idCompany', $request->nama_company);
//             }
//             if ($request->nama_branch) {
//                 $query = $query->where('branch.idBranch', $request->nama_branch);
//             }
//             if ($this->borrower) {
//                 $query = $query->where('view.email', $this->borrower);
//             }
//             if ($this->lender) {
//                 $query = $query->where('view.userId', $this->lender);
//             }
//             $data = $query->get();
//         } else {
//             $query = DB::Connection('mysql3')->table('view_pencairan AS view')
//                 ->join('db_mid_peminjam.register_jaminan AS jaminan', 'jaminan.idJaminan', '=', 'view.idJaminan')
//                 ->join('db_mid_peminjam.member_branch AS branch', 'branch.idBranch', '=', 'jaminan.idBranch')
//                 ->selectRaw('YEAR(view.tanggalPencairan) tahun, MONTH(view.tanggalPencairan) bulan, COUNT(view.idDebPinjaman) AS jumlah, SUM(view.pinjaman) AS total')
//                 ->where('view.tanggalPencairan', '>=', $this->tglAwal)
//                 ->where('view.tanggalPencairan', '<=', $this->tglSampai)
//                 ->orderBy('tahun', 'ASC')
//                 ->orderBy('bulan', 'ASC')
//                 ->groupBy('tahun')
//                 ->groupBy('bulan');
//             if ($request->nama_company) {
//                 $query = $query->where('branch.idCompany', $request->nama_company);
//             }
//             if ($request->nama_branch) {
//                 $query = $query->where('branch.idBranch', $request->nama_branch);
//             }
//             if ($this->borrower) {
//                 $query = $query->where('view.email', $this->borrower);
//             }
//             if ($this->lender) {
//                 $query = $query->where('view.userId', $this->lender);
//             }
//             $data = $query->get();
//         }
//         return view('login.laporanborrower.pencairanpinjaman.excel',[
//             "jenisLaporan"  => $this->jenisLaporan, 
//             "nama_company"  => $this->nama_company, 
//             "nama_branch"   => $this->nama_branch, 
//             "tglAwal"       => $this->tglAwal,
//             "tglSampai"     => $this->tglSampai,
//             "borrower"      => $this->borrower,
//             "lender"        => $this->lender,
//             "data"          => $data, 
//         ]);
//     }
// }
