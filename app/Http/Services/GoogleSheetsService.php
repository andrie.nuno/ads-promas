<?php

namespace App\Http\Services;

use Google\Client;
use Google\Service\Sheets;
use Google\Service\Sheets\ValueRange;
use Google\Service\Drive;
use Google\Service\Drive\Permission;
use Google\Service\Sheets\SpreadSheet;

class GoogleSheetsService
{
    public $client, $service, $documentId, $range;

    public function __construct()
    {
        $this->client = $this->getClient();
        $this->service = new Sheets($this->client);
        $this->documentId = '1ZaEvqOBSvLeQkSsy9K5JOkTQp2f6cTRi_OevoDyz8tI';
        $this->range = 'Sheet1';

    }

    public function getClient()
    {
        $client = new Client();
        $client->setApplicationName('Google Sheets Demo');
        $client->setScopes(Sheets::SPREADSHEETS);
        $client->setAuthConfig('credentials.json');
        $client->setAccessType('offline');

        return $client;
    }

    function create($title)
    {   
        /* Load pre-authorized user credentials from the environment.
           TODO(developer) - See https://developers.google.com/identity for
            guides on implementing OAuth2 for your application. */
        $this->client->useApplicationDefaultCredentials();
        $this->client->addScope(Sheets::DRIVE);
        try{

            $spreadsheet = new SpreadSheet([
                'properties' => [
                    'title' => $title
                    ]
                ]);
                $spreadsheet = $this->service->spreadsheets->create($spreadsheet, [
                    'fields' => 'spreadsheetId'
                ]);
                // Create a Drive service
                $drive = new Drive($this->client);

                // Define the permission for public access
                $permission = new Permission();
                $permission->setType('anyone');
                $permission->setRole('reader');

                // Request to set the sharing permissions
                $drive->permissions->create($spreadsheet->spreadsheetId, $permission);
                            printf("Spreadsheet ID: %s\n", $spreadsheet->spreadsheetId);
                            return $spreadsheet->spreadsheetId;
                    }
        catch(Exception $e) {
            // TODO(developer) - handle error appropriately
            echo 'Message: ' .$e->getMessage();
          }
    }

    public function readSheet()
    {
        $doc = $this->service->spreadsheets_values->get($this->documentId, $this->range);
        return $doc;
    }

    public function writeSheet($values)
    {
        // $values = [
        //     []
        // ];
            $body = new ValueRange([
                'values' => $values
            ]);
            $params = [
                'valueInputOption' => 'RAW'
            ];
            $result = $this->service->spreadsheets_values->update($this->documentId, $this->range, $body, $params);
    }

    public function appendSheet($values)
    {
        // $values = [
        //     []
        // ];
            $body = new ValueRange([
                'values' => $values
            ]);
            $params = [
                'valueInputOption' => 'USER_ENTERED'
            ];
            $result = $this->service->spreadsheets_values->append($this->documentId, $this->range, $body, $params);
    }
}