<?php

namespace App\Http\Controllers;

use Google\Service\Sheets;
use Illuminate\Http\Request;
use App\Http\Services\GoogleSheetsService;

class GoogleSheetsController extends Controller
{
    public function sheetOperation(Request $request)
    {
        (new GoogleSheetsService ())->writeSheet([
            [
                'M Razan Alfatin',
                'razan@gmail.com'
            ]
        ]);

        $data = (new GoogleSheetsService ())->readSheet();

        return response()->json($data);
    }
}