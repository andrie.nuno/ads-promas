<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\model\TblKaryawanInternal;
use App\model\ads_user_level;

class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }
}
