<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Model\tblcabang;
use Illuminate\Http\Request;

class CabangController extends Controller
{
    public function index()
    {
        return response()->json([
            'message'   => 'success',
            'data'      => tblcabang::all()
        ], 200);
    }

    public function show(Request $request, $id)
    {
        return response()->json([
            'message'   => 'success',
            'request' => $request,
            'data'      => tblcabang::where('idCabang',$id)
            ->first()
        ], 200);
    }

     public function store(Request $request)
    {
        $cabang = tblcabang::create($request->all());
        return response()->json($cabang, 201);
    }

    public function update(Request $request, $id)
    {
        $cabang = tblcabang::findOrFail($id);
        $cabang->update($request->all());
        return response()->json($cabang, 200);
    }

    public function destroy($id)
    {
        $cabang = tblcabang::findOrFail($id);
        $cabang->delete();
        return response()->json(null, 204);
    }
}