<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Google\Service\Sheets;
use App\Model\tblcabang;
use Illuminate\Http\Request;
use App\Http\Services\GoogleSheetsService;
use App\model\trans_payctrl;
use DB;

class GoogleSheetsApi extends Controller
{
    private $rownum = 1;

    public function index(Request $request)
    {

        // $tanggal = date('Y-m-d');
        // $limit = 10;

        // $query = trans_payctrl::
        // join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
        // ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
        // ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
        // ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
        // ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_payctrl.idCustomer')
        // ->leftjoin('tblkelurahan', 'tblkelurahan.idKelurahan', '=', 'tblcustomer.idKelurahanKtp')
        // ->leftjoin('tblkelurahan as domisili', 'domisili.idKelurahan', '=', 'tblcustomer.idKelurahanDomisili')
        // ->leftjoin('tblpekerjaan', 'tblpekerjaan.idPekerjaan', '=', 'tblcustomer.idPekerjaan')
        // ->leftjoin('tblsektorekonomi', 'tblsektorekonomi.idSektorEkonomi', '=', 'tblcustomer.idSektorEkonomi')
        // ->join('tran_taksiran', 'tran_taksiran.idTaksiran', '=', 'trans_gadai.idTaksiran')
        // ->join('trans_payment_schedule', function ($join) use ($tanggal) {
        //     $join->on('trans_payment_schedule.idGadai', '=', 'trans_payctrl.idGadai')
        //     ->where(function ($query) use ($tanggal) {
        //         $query->whereNull('trans_payment_schedule.tglLunas')
        //         // $dd2 = dd($tanggal);
        //             ->orWhere('trans_payment_schedule.tglLunas', '>', $tanggal);
        //     });
        //     })
        // ->leftjoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai')
        // ->leftJoin("tblsettingjf", function($join){
        //     $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
        //     $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            
        // })
        // ->leftjoin('tbltujuantransaksi', 'tbltujuantransaksi.idTujuanTransaksi', '=', 'trans_gadai.idTujuanTransaksi')
        // ->leftJoin("trans_fundingdetail", function($join){
        //     $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
        //     $join->whereNull('trans_fundingdetail.tglUnpledging');
            
        // })
        // ->leftjoin('trans_funding', 'trans_funding.idFunding', '=', 'trans_fundingdetail.idFunding')
        // ->leftjoin('tblpartner', 'tblpartner.idPartner', '=', 'trans_funding.idPartner')
        // ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.kd_wilayah AS kodeWilayah, tblwilayah.namaWilayah, 
        // trans_gadai.noSbg, tblcustomer.cif AS noCif, tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
        // tblcustomer.jenisKelamin, tblcustomer.statusPerkawinan AS statusPernikahan, tblpekerjaan.namaPekerjaan AS pekerjaan, 
        // REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
        // tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
        // tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
        // IF(tblcustomer.alamatDomisili != '', 
        //     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
        //     REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
        // ) AS alamatDomisili, 
        // IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
        // IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
        // IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
        // tblcustomer.hp AS noHp, tblsektorekonomi.namaSektorEkonomi, trans_payctrl.tanggalPencairan AS tanggalCair, trans_gadai.tglJatuhTempo, 
        // IF(tblproduk.idJenisProduk = 2, trans_payctrl.tanggalJtCicilan, trans_gadai.tglJatuhTempo) AS tglJatuhTempoCicilan, 
        // IF(tblproduk.idJenisProduk = 2, trans_payctrl.angsuranKe, 1) AS angsuranKe, 
        // IF(tblproduk.idJenisProduk = 2, DATEDIFF($tanggal, trans_payctrl.tanggalJtCicilan), DATEDIFF($tanggal, trans_gadai.tglJatuhTempo)) AS ovd,
        // trans_gadai.lamaPinjaman AS tenor, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, 
        // trans_gadai.nilaiPinjaman AS pokokAwal, 
        // IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.pokokAngsuran), trans_gadai.nilaiPinjaman) AS saldoPokok, 
        // trans_payctrl.bungaFull AS saldoSimpan, 
        // IF(tblproduk.idJenisProduk = 2, SUM(trans_payment_schedule.bungaAngsuran), trans_payctrl.bungaFull) AS saldoSimpanOutstanding, 
        // trans_gadai.biayaAdmin, tblpartner.namaPartner AS bankFunding, trans_gadaijf.idJf, 
        // tblsettingjf.namaPendana, trans_gadaijf.rateJf, trans_gadaijf.porsiJf, trans_payctrl.statusPinjaman, tran_taksiran.totalTaksiran, 
        // tran_taksiran.sumBeratBersih, tran_taksiran.avgKarat, tbltujuantransaksi.namaTujuanTransaksi, trans_gadai.statusAplikasi, 
        // trans_gadai.idAsalJaminan")
        // ->where('trans_payctrl.statusPinjaman', '!=', 'WO')
        // ->where('trans_payctrl.tanggalPencairan', '<=', $tanggal)
        // ->where('trans_payctrl.tanggalpelunasan', '>', $tanggal)
        // ->orWhereNull('trans_payctrl.tanggalpelunasan')
        // ->groupBy('trans_payctrl.idGadai')
        // ->paginate($limit);

        // $maps = [];
        // foreach ($query as $row) {
        //     $maps[] = (array)$row;
        // }
        
        // $maps = $query->map(function ($item) {
        //     return [
        //         $item->produk, 
        //         $item->namaCabang, 
        //         $item->namaWilayah, 
        //         $item->noSbg, 
        //         $item->noCif, 
        //         $item->namaCustomer, 
        //         "'".$item->noKtp, 
        //         $item->tempatLahir, 
        //         $item->tanggalLahir, 
        //         $item->jenisKelamin, 
        //         $item->statusPernikahan, 
        //         $item->pekerjaan, 
        //         $item->rtKtp, 
        //         $item->rwKtp, 
        //         $item->namakelurahan, 
        //         $item->namaKecamatan, 
        //         $item->namaKabupaten, 
        //         $item->namaProvinsi, 
        //         $item->kodepos, 
        //         $item->alamatDomisili, 
        //         $item->rtDomisili, 
        //         $item->rwDomisili, 
        //         $item->kelurahanDomisili, 
        //         $item->kecamatanDomisili, 
        //         $item->kabupatenDomisili, 
        //         $item->provinsiDomisili, 
        //         $item->kodePosDomisili, 
        //         $item->noHp, 
        //         $item->namaSektorEkonomi, 
        //         $item->tanggalCair,
        //         $item->tglJatuhTempo,
        //         $item->tglJatuhTempoCicilan,
        //         $item->angsuranKe,
        //         $item->ovd,
        //         $item->tenor, 
        //         $item->rateFlat, 
        //         $item->ltv, 
        //         $item->pokokAwal, 
        //         $item->saldoPokok, 
        //         $item->saldoSimpan, 
        //         $item->saldoSimpanOutstanding, 
        //         $item->biayaAdmin, 
        //         $item->bankFunding, 
        //         // $item->status == 'Proses Jual' ? $item->jmlHari : $item->jumlahHari, 
        //         // $item->status == 'Proses Jual' ? $item->ovd : $item->jumlahOvd, 
        //         // $item->jumlahHari <= $item->jmlHari ? $item->jumlahHari : $item->jmlHari, 
        //         // $item->jumlahOvd <= $item->ovd ? $item->jumlahOvd : $item->ovd, 
        //         $item->idJf, 
        //         $item->namaPendana, 
        //         // $item->tglJt < date('Y-m-d') ? $item->bungaTerhutang : $item->bungaAcrue, 
        //         $item->rateJf, 
        //         // $item->status == 'Gagal Bayar' ? $item->jasaMitraTerhutang : $item->mitraAcrue, 
        //         // $item->status == 'Proses Jual' ? $item->jasaMitraTerhutang : $item->mitraAcrue, 
        //         $item->porsiJf, 
        //         $item->statusPinjaman, 
        //         // $item->jumlahHari > $item->tenor ? $item->dendaAcrue : 0, 
        //         // $item->status == 'Proses Jual' ? $item->dendaTerhutang : ($item->jumlahHari > $item->tenor ? $item->dendaAcrue : 0), 
        //         // '=AI'.$this->current_row_al++.' + AR'.$this->current_row_ap++.' + AT'.$this->current_row_ar++.' + AW'.$this->current_row_au++, 
        //         $item->totalTaksiran, 
        //         $item->sumBeratBersih, 
        //         $item->avgKarat, 
        //         $item->namaTujuanTransaksi, 
        //         $item->statusAplikasi, 
        //         $item->idAsalJaminan, 
        //     ];
        // })->toArray();
        
        
        // for ($i=1; $i < 2000; $i++) { 
            // (new GoogleSheetsService ())->appendSheet($maps);
        // }

        // for ($i=1; $i < 2000; $i++) { 
            (new GoogleSheetsService ())->appendSheet(
                [
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101082202523",
                      "101.0.000430",
                      "NANANG RISDANTO",
                      "'3512132302780002",
                      "SITUBONDO",
                      "1978-02-23",
                      "Laki-Laki",
                      "Menikah",
                      "WIRASWASTA / PEDAGANG",
                      0,
                      0,
                      "Perante",
                      "Asembagus",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68373",
                      "KP. UTARA 001_001",
                      0,
                      0,
                      "Perante",
                      "Asembagus",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68373",
                      "089697404581",
                      null,
                    ],
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101112203391",
                      "101.0.001228",
                      "DIAZ HAFIDAH FEBRIYANTI",
                      "'3512055402990002",
                      "SITUBONDO",
                      "1999-02-14",
                      "Perempuan",
                      "Belum Menikah",
                      "PROFESIONAL (INSINYUR, ARSITEK DSB)",
                      0,
                      0,
                      "Klatakan",
                      "Kendit",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68352",
                      "DUSUN SEMEKAN UTARA 002_001",
                      0,
                      0,
                      "Klatakan",
                      "Kendit",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68352",
                      "082146028746",
                      null,
                    ],
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101092202837",
                      "101.0.000105",
                      "EVA JOHANNA DEWI S",
                      "'3512087004860001",
                      "SITUBONDO",
                      "1986-04-30",
                      "Perempuan",
                      "Menikah",
                      "PROFESIONAL (INSINYUR, ARSITEK DSB)",
                      0,
                      0,
                      "Mimbaan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68322",
                      "JL IRIAN JAYA IV / 17 001_001",
                      0,
                      0,
                      "Mimbaan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68322",
                      "08987491569",
                      null,
                    ],
                    [
                      "01-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "001101102203120",
                      "101.0.001005",
                      "NANANG HENDRA VIANA. SH",
                      "'3512082001850001",
                      "SITUBONDO",
                      "1905-01-20",
                      "Laki-Laki",
                      "Menikah",
                      "WIRASWASTA / PEDAGANG",
                      0,
                      0,
                      "Tokelan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "PERUM GRAHA KENCANURATA BLOK E-1B 002_005",
                      0,
                      0,
                      "Tokelan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "085259694920",
                      null,
                    ],
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101092202688",
                      "101.0.000016",
                      "INDARWATI",
                      "'3512085012660001",
                      "SITUBONDO",
                      "1966-12-10",
                      "Perempuan",
                      "Menikah",
                      "PROFESIONAL (INSINYUR, ARSITEK DSB)",
                      0,
                      0,
                      "Panji Kidul",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "KP. AENG SONOK 001_002",
                      0,
                      0,
                      "Panji Kidul",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "085220901611",
                      null,
                    ],
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101102203018",
                      "101.0.000563",
                      "SANHAJI",
                      "'3512081302680002",
                      "SITUBONDO",
                      "1968-02-13",
                      "Laki-Laki",
                      "Menikah",
                      "WIRASWASTA / PEDAGANG",
                      0,
                      0,
                      "Curah Jeru",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "CURAH JERU 001_005",
                      0,
                      0,
                      "Curah Jeru",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "082301574049",
                      null,
                    ],
                    [
                      "04-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "004101102203206",
                      "101.0.000145",
                      "NUR WASIH",
                      "'3512084711880006",
                      "SITUBONDO",
                      "1983-05-02",
                      "Perempuan",
                      "Menikah",
                      "PROFESIONAL (INSINYUR, ARSITEK DSB)",
                      0,
                      0,
                      "Panji Lor",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "KP. KRAJAN BARAT 002_001",
                      0,
                      0,
                      "Panji Lor",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "081703640315",
                      null,
                    ],
                    [
                      "01-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "001101082202403",
                      "101.0.001306",
                      "SAHRAWI",
                      "'3512111110640001",
                      "SITUBONDO",
                      "1964-10-11",
                      "Laki-Laki",
                      "Menikah",
                      "PETANI",
                      0,
                      0,
                      "Jatisari",
                      "Arjasa",
                      "Kab. Jember-Kabupaten",
                      "Jawa Timur",
                      "68371",
                      "KP. KRAJAN 002_001",
                      0,
                      0,
                      "Jatisari",
                      "Arjasa",
                      "Kab. Jember-Kabupaten",
                      "Jawa Timur",
                      "68371",
                      "085232864752",
                      null,
                    ],
                    [
                      "01-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "001101082202492",
                      "101.0.000541",
                      "AHMAD BASUKI",
                      "'3512081103820002",
                      "SITUBONDO",
                      "1932-03-11",
                      "Laki-Laki",
                      "Menikah",
                      "PEGAWAI NEGERI (PNS)",
                      0,
                      0,
                      "Tokelan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "TOKELAN SELATAN 003_004",
                      0,
                      0,
                      "Tokelan",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "085234114022",
                      null,
                    ],
                    [
                      "01-01",
                      "MAS SITUBONDO",
                      "PT.Gadai Mas Jatim",
                      "001101082202524",
                      "101.0.001329",
                      "ROSIA",
                      "'3512084810720004",
                      "SITUBONDO",
                      "1992-10-08",
                      "Perempuan",
                      "Menikah",
                      "PROFESIONAL (INSINYUR, ARSITEK DSB)",
                      0,
                      0,
                      "Curah Jeru",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "KAMPUNG BARAT 002_011",
                      0,
                      0,
                      "Curah Jeru",
                      "Panji",
                      "Kab. Situbondo-Kabupaten",
                      "Jawa Timur",
                      "68323",
                      "08000000000",
                      null,
                    ]
                  ]
            );
        // }

        // $data = (new GoogleSheetsService ())->readSheet();
        // $data = (new GoogleSheetsService ())->create('Spreadsheet dummy');

        // return response()->json($data);
    }

    
}