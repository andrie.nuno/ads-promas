<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPayctrl;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class CronPencairanGadai extends Controller
{
    public function calculate() {
        DB::beginTransaction();
        $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_gadai.noSbg, trans_gadai.pengajuanPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, tblcabang.kodeCabang, tblcabang.namaCabang')
            ->orderBy('trans_payctrl.tanggalPencairan', 'ASC')
            ->orderBy('trans_payctrl.created_at', 'ASC')
            ->where('tblproduk.statusFunding', '=', 0)
            ->where('trans_payctrl.tanggalPencairan', '>=', '2023-01-04')
            ->where('trans_payctrl.isJurnal', '=', 0)
            ->offset(0)->limit(100)->get();

        if (!$data->isEmpty()) {
            foreach ($data as $ditagadai) {
                $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
                    ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
                    ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.idCabang, trans_gadai.nilaiPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, trans_gadai.biayaPenyimpanan, trans_gadai.pembulatan, tblcabang.kodeCabang, tblcabang.namaCabang')
                    ->where('trans_payctrl.idGadai', '=', $ditagadai->idGadai)
                    ->first();
                if ($data) {
                    // yyyy-mm-dd
                    $v_tahunJurnal = substr($data->tanggalPencairan, 0, 4);
                    $v_bulanJurnal = substr($data->tanggalPencairan, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $data->tanggalPencairan)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($data->tanggalPencairan, 2, 2).substr($data->tanggalPencairan, 5, 2).substr($data->tanggalPencairan, 8, 2).'000001';
                    }
                    $dataSummary = [
                        'idJenisJurnal'         => 4, // Pencairan Gadai Reguler - Tunai
                        'idGadai'               => $data->idGadai, 
                        'idProduk'              => $data->idProduk, 
                        'idCabang'              => $data->idCabang, 
                        'idBankKeluar'          => 25, // Buffer
                        'tanggal'               => $data->tanggalPencairan, 
                        'tahun'                 => $v_tahunJurnal, 
                        'bulan'                 => $v_bulanJurnal, 
                        'batch'                 => $v_batchJurnal, 
                        'kodeTransaksi'         => $data->noSbg, 
                        'pokok'                 => $data->nilaiPinjaman, 
                        'pencairan'             => $data->nilaiApCustomer, 
                        'bunga'                 => $data->biayaPenyimpanan, 
                        'admin'                 => $data->biayaAdmin, 
                        'pembulatan'            => $data->pembulatan, 
                        'keterangan'            => 'Pencairan '.$data->noSbg, 
                        'keteranganTransaksi'   => 'Pencairan Gadai Reguler '.$data->noSbg, 
                    ];
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;
                    
                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.pokok, akunting_summary.pencairan, akunting_summary.bunga, akunting_summary.admin, akunting_summary.pembulatan')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        $sumDebet = 0;
                        $sumKredit = 0;
                        foreach($dataJurnal as $dita) {
                            if ($dita->nilai == 'pokok') {
                                $amount = $dita->pokok;
                            }
                            if ($dita->nilai == 'pencairan') {
                                $amount = $dita->pencairan;
                            }
                            if ($dita->nilai == 'bunga') {
                                $amount = $dita->bunga;
                            }
                            if ($dita->nilai == 'admin') {
                                $amount = $dita->admin;
                            }
                            if ($dita->nilai == 'pembulatan') {
                                $amount = $dita->pembulatan;
                            }
                            if ($amount > 0) {
                                if ($dita->idCoa == 207) {
                                    if ($sumDebet != $sumKredit) {
                                        $dataDetail = [
                                            'idSummary'     => $lastId, 
                                            'idCabang'      => $dita->idCabang, 
                                            'urut'          => $dita->urut, 
                                            'idCoa'         => $dita->idCoa, 
                                            'tanggal'       => $dita->tanggal, 
                                            'tahun'         => $v_tahunJurnal, 
                                            'bulan'         => $v_bulanJurnal, 
                                            'batch'         => $v_batchJurnal, 
                                            'coa'           => $dita->coa, 
                                            'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa, 
                                            'keterangan'    => $dita->description, 
                                            'dk'            => $dita->dk, 
                                            'amount'        => $amount
                                        ];
                                        $detail = AkuntingDetail::create($dataDetail);
                                    }
                                } else {
                                    if ($dita->dk == 'D') {
                                        $sumDebet += $amount;
                                    } else {
                                        $sumKredit += $amount;
                                    }
                                    $dataDetail = [
                                        'idSummary'     => $lastId, 
                                        'idCabang'      => $dita->idCabang, 
                                        'urut'          => $dita->urut, 
                                        'idCoa'         => $dita->idCoa, 
                                        'tanggal'       => $dita->tanggal, 
                                        'tahun'         => $v_tahunJurnal, 
                                        'bulan'         => $v_bulanJurnal, 
                                        'batch'         => $v_batchJurnal, 
                                        'coa'           => $dita->coa, 
                                        'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa, 
                                        'keterangan'    => $dita->description, 
                                        'dk'            => $dita->dk, 
                                        'amount'        => $amount
                                    ];
                                    $detail = AkuntingDetail::create($dataDetail);
                                }
                            }
                        }
                    }
                    $dataUpdate = [
                        'isJurnal'      => 1,  
                        'dateJurnal'    => date('Y-m-d H:i:s'),
                    ];
                    TransPayctrl::where('idGadai', '=', $ditagadai->idGadai)
                        ->update($dataUpdate);
                }
            }
        }
        DB::commit();
    }
}
