<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\FaJenisTransaksi;
use App\model\FaTransaksiInternal;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class CronMutasiInternalUnit extends Controller
{
    public function calculate() {
        DB::beginTransaction();
        $data = FaTransaksiInternal::join('fa_jenistransaksi', 'fa_jenistransaksi.idJenisTransaksi', '=', 'fa_transaksiinternal.idJenisTransaksi') 
            ->selectRaw('fa_transaksiinternal.idTransaksiInternal, fa_transaksiinternal.idCabang, fa_transaksiinternal.idJenisTransaksi, fa_transaksiinternal.idBankKeluar, fa_transaksiinternal.idBankMasuk, fa_transaksiinternal.tanggal, fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi, fa_transaksiinternal.keteranganTransaksi, fa_transaksiinternal.statusApproval, fa_jenistransaksi.namaJenisTransaksi, fa_jenistransaksi.idJenisJurnal')
            ->where('fa_jenistransaksi.typeTransaksi', '=', 1)
            // ->where('fa_transaksiinternal.statusApproval', '=', 1)
            ->where(function ($query2) {
                $query2->where('fa_transaksiinternal.statusApproval', '=', 1)
                    ->orWhere('fa_transaksiinternal.statusApproval', '=', 999);
            })
            ->where('fa_transaksiinternal.isJurnal', '=', 0)
            ->where('fa_transaksiinternal.nominalTransaksi', '>', 0)
            ->orderBy('fa_transaksiinternal.idTransaksiInternal', 'ASC')
            ->offset(0)->limit(10)->get();

        if (!$data->isEmpty()) {
            foreach ($data as $dita) {
                if ($dita->nominalTransaksi > 0) {
                    $v_tahunJurnal = substr($dita->tanggal, 0, 4);
                    $v_bulanJurnal = substr($dita->tanggal, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $dita->tanggal)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($dita->tanggal, 2, 2).substr($dita->tanggal, 5, 2).substr($dita->tanggal, 8, 2).'000001';
                    }
                    $dataSummary = [
                        'idJenisJurnal'         => $dita->idJenisJurnal, 
                        'idTransaksiInternal'   => $dita->idTransaksiInternal, 
                        'idCabang'              => $dita->idCabang, 
                        'idBankMasuk'           => $dita->idBankMasuk, 
                        'idBankKeluar'          => $dita->idBankKeluar, 
                        'tanggal'               => $dita->tanggal, 
                        'tahun'                 => $v_tahunJurnal, 
                        'bulan'                 => $v_bulanJurnal, 
                        'batch'                 => $v_batchJurnal, 
                        'kodeTransaksi'         => $dita->noVoucher, 
                        'amount'                => $dita->nominalTransaksi, 
                        'keterangan'            => $dita->namaJenisTransaksi, 
                        'keteranganTransaksi'   => $dita->keteranganTransaksi, 
                    ];
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;

                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.idBankMasuk, akunting_summary.idBankKeluar, akunting_summary.amount, acc_skemajurnal.jenisCoa')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        foreach($dataJurnal as $ditaJurnal) {
                            $dataDetail = [
                                'idSummary'     => $lastId, 
                                'idCabang'      => $ditaJurnal->idCabang, 
                                'urut'          => $ditaJurnal->urut, 
                                'idCoa'         => $ditaJurnal->idCoa, 
                                'tanggal'       => $ditaJurnal->tanggal, 
                                'tahun'         => $v_tahunJurnal, 
                                'bulan'         => $v_bulanJurnal, 
                                'batch'         => $v_batchJurnal, 
                                'coa'           => $ditaJurnal->coa, 
                                'coaCabang'     => $ditaJurnal->kodeCabang.'.'.$ditaJurnal->coa, 
                                'keterangan'    => $ditaJurnal->description, 
                                'dk'            => $ditaJurnal->dk, 
                                'amount'        => $ditaJurnal->amount
                            ];
                            $detail = AkuntingDetail::create($dataDetail);
                        }
                    }
                    $dataUpdate = [
                        'isJurnal'      => 1,  
                        'dateJurnal'    => date('Y-m-d H:i:s'),
                    ];
                    FaTransaksiInternal::where('idTransaksiInternal', '=', $dita->idTransaksiInternal)
                        ->update($dataUpdate);
                }
            }
        }
        DB::commit();
    }
}
