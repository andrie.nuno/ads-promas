<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\trans_gadai;
use App\model\RegisterJaminan;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\OutstandingPinjamanExcel;
use App\Http\Exports\WaktuTransaksiKonvensionalExcel;
use App\model\tblwilayah;

class WaktuTransaksiKonvensional extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::where('isActive', '=', 1)->orderBy('idWilayah')->get();
        return view('login.reporttransaksi.waktutransaksikonvensional.index',[
            "wilayahs"      => $wilayah,
            "nama_wilayah"  => '', 
            "tgl_awal"      => date('Y-m-d'), 
            "tgl_sampai"    => date('Y-m-d'), 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = trans_gadai::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
        ->join('tblcabang AS cabang', 'cabang.idCabang', '=', 'tblcabang.headCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
        ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai.idCustomer')
        ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'trans_gadai.idGadai')
        ->join('tran_fapg', 'tran_fapg.idFAPG', '=', 'trans_gadai.idFAPG')
        ->join('tran_taksiran AS taksirawal', function($join){
            $join->on('taksirawal.idFAPG', '=', 'trans_gadai.idFAPG');
            $join->where(DB::raw('taksirawal.isFinal'), '=',  0);
        })
        ->join('tran_taksiran AS taksirfinal', function($join){
            $join->on('taksirfinal.idFAPG', '=', 'trans_gadai.idFAPG');
            $join->where(DB::raw('taksirfinal.isFinal'), '=', 1);
        })
        ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, CONCAT(cabang.kodeCabang, '-', cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, tran_fapg.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, trans_gadai.created_at AS timeInputGadai, trans_gadai.tglApprovalKaunit, trans_gadai.tglApprovalKacab, trans_gadai.tglApprovalKawil, trans_gadai.tglApprovalDirektur, trans_gadai.tglApprovalDirut, trans_gadai.tglCetakKwitansi AS timeCetakKwitansi, trans_gadai.nilaiPinjaman, ROUND((trans_gadai.nilaiPinjaman / taksirfinal.totalTaksiran) * 100, 2) AS ltv, trans_gadai.rateFlat, trans_gadai.totalObligor, trans_gadai.statusAplikasi, trans_gadai.approvalFinal, trans_gadai.approvalLtv, trans_gadai.approvalUangPinjaman, trans_gadai.approvalRateFlat, trans_gadai.approvalOneObligor, trans_gadai.timeFotoNasabah, trans_gadai.tglApprovalKadeptCpa, trans_gadai.tglApprovalKadivCpa")
        ->where('trans_payctrl.tanggalPencairan', '>=', $request->tgl_awal)
        ->where('trans_payctrl.tanggalPencairan', '<=', $request->tgl_sampai)
        ->groupBy('trans_gadai.noSbg')
        ->orderBy('trans_payctrl.tanggalPencairan');
        if ($request->nama_wilayah) {
            $data = $query->where('tblcabang.idWilayah', $request->nama_wilayah);
        }
        $data = $query->paginate($limit);
        return view('login.reporttransaksi.waktutransaksikonvensional.populate',[
            "nama_wilayah"  => $request->nama_wilayah, 
            "tgl_awal"      => $request->tgl_awal, 
            "tgl_sampai"    => $request->tgl_sampai, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function export(Request $request){
        Storage::disk('publicupload')->deleteDirectory('/uploads/TimeTransaksi');

        $data = trans_gadai::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->join('tblcabang AS cabang','cabang.idCabang', '=', 'tblcabang.headCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai.idCustomer')
            ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'trans_gadai.idGadai')
            ->join('tran_fapg', 'tran_fapg.idFAPG', '=', 'trans_gadai.idFAPG')
            ->join('tran_taksiran AS taksirawal', function($join){
                $join->on('taksirawal.idFAPG', '=', 'trans_gadai.idFAPG');
                $join->where(DB::raw('taksirawal.isFinal'), '=', 0);
            })
            ->join('tran_taksiran as taksirfinal', function($join){
                $join->on('taksirfinal.idFAPG', '=', 'trans_gadai.idFAPG');
                $join->where(DB::raw('taksirfinal.isFinal'), '=', 1);
            })
            ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, CONCAT(cabang.kodeCabang, '-', cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, tran_fapg.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, trans_gadai.created_at AS timeInputGadai, trans_gadai.tglApprovalKaunit, trans_gadai.tglApprovalKacab, trans_gadai.tglApprovalKawil, trans_gadai.tglApprovalDirektur, trans_gadai.tglApprovalDirut, trans_gadai.tglCetakKwitansi AS timeCetakKwitansi, trans_gadai.nilaiPinjaman, ROUND((trans_gadai.nilaiPinjaman / taksirfinal.totalTaksiran) * 100, 2) AS ltv, trans_gadai.rateFlat, trans_gadai.totalObligor, trans_gadai.statusAplikasi, trans_gadai.approvalFinal, trans_gadai.approvalLtv, trans_gadai.approvalUangPinjaman, trans_gadai.approvalRateFlat, trans_gadai.approvalOneObligor, trans_gadai.timeFotoNasabah, trans_gadai.tglApprovalKadeptCpa, trans_gadai.tglApprovalKadivCpa")
            ->groupBy('trans_gadai.noSbg')
            ->orderBy('trans_payctrl.tanggalPencairan');
            if ($request->nama_wilayah) {
                $data = $data->where('tblcabang.idWilayah', $request->nama_wilayah);
            }
            if ($request->tgl_awal) {
                $data = $data->where('trans_payctrl.tanggalPencairan', '>=', $request->tgl_awal);
            }
            // if ($request->tgl_sampai) {
            //     $data = $data->where('trans_payctrl.tanggalPencairan', '<=', $request->tgl_sampai);
            // }
            $data = $data->get();

        $fileName = 'WaktuTransaksiKonvensional_'.str_replace("-", "", $request->tgl_awal).'_'.str_replace("-", "", $request->tgl_sampai).'.csv';
        // dd($fileName);

        $baris = 'No|Kode Outlet|Nama Outlet|Nama Cabang|Nama Wilayah|Tgl Pencairan|No SBG|Jenis Pembayaran Final|CIF|Nama|No Ktp|Nilai Pinjaman|Rate|LTV|One Obligor|Status Aplikasi|Time Input FAPG|Time Taksiran I|Time Taksiran II|Time Input Gadai|Time KaUnit|Time Kacab|Time Kawil|Time KaDept CPA|Time KaDiv CPA|Time Direktur|Time Dirut|Time Kwitansi|Time Foto Nasabah|Final Approval|Approval UP|Approval Rate|Approval LTV|Approval Obligor';
        Storage::disk('publicupload')->put('/uploads/TimeTransaksi/'.$fileName, $baris);
        
        if ($data) {
            $lineNo = 0;
            foreach($data as $dita) {
                $lineNo++;
                $baris = $lineNo.'|'.$dita->kodeOutlet.'|'.$dita->namaOutlet.'|'.$dita->namaCabang.'|'.$dita->namaWilayah.'|'.$dita->tanggalPencairan.'|'.$dita->noSbg.'|'.$dita->jenisPembayaranFinal.'|'.$dita->cif.'|'.$dita->namaCustomer.'|'.$dita->noKtp.'|'.$dita->nilaiPinjaman.'|'.$dita->rateFlat.'|'.$dita->ltv.'|'.$dita->totalObligor.'|'.$dita->statusAplikasi.'|'.$dita->timeInputFAPG.'|'.$dita->timeTaksirPenaksir.'|'.$dita->timeTaksirKaUnit.'|'.$dita->timeInputGadai.'|'.$dita->tglApprovalKaunit.'|'.$dita->tglApprovalKacab.'|'.$dita->tglApprovalKawil.'|'.$dita->tglApprovalKadeptCpa.'|'.$dita->tglApprovalKadivCpa.'|'.$dita->tglApprovalDirektur.'|'.$dita->tglApprovalDirut.'|'.$dita->timeCetakKwitansi.'|'.$dita->timeFotoNasabah.'|'.$dita->approvalFinal.'|'.$dita->approvalUangPinjaman.'|'.$dita->approvalRateFlat.'|'.$dita->approvalLtv.'|'.$dita->approvalOneObligor;
                Storage::disk('publicupload')->append('/uploads/TimeTransaksi/'.$fileName, $baris);
            }
        }

        $tanggalNext = date('Y-m-d', strtotime('+1 days', strtotime($request->tgl_awal)));
        if ($request->tgl_sampai == $tanggalNext) {
            return Storage::disk('publicupload')->download('/uploads/TimeTransaksi/'.$fileName);
        } else {
            return WaktuTransaksiKonvensional::exportextends($request->nama_wilayah, $request->tgl_awal, $tanggalNext, $request->tgl_sampai, $lineNo);
        }        
    }

    public function exportextends($nama_wilayah, $tgl_awal, $tanggalNext, $tgl_sampai, $lineNo){
        $data = trans_gadai::join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->join('tblcabang AS cabang','cabang.idCabang', '=', 'tblcabang.headCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->join('tblcustomer', 'tblcustomer.idCustomer', '=', 'trans_gadai.idCustomer')
            ->join('trans_payctrl', 'trans_payctrl.idGadai', '=', 'trans_gadai.idGadai')
            ->join('tran_fapg', 'tran_fapg.idFAPG', '=', 'trans_gadai.idFAPG')
            ->join('tran_taksiran AS taksirawal', function($join){
                $join->on('taksirawal.idFAPG', '=', 'trans_gadai.idFAPG');
                $join->where(DB::raw('taksirawal.isFinal'), '=', 0);
            })
            ->join('tran_taksiran as taksirfinal', function($join){
                $join->on('taksirfinal.idFAPG', '=', 'trans_gadai.idFAPG');
                $join->where(DB::raw('taksirfinal.isFinal'), '=', 1);
            })
            ->selectRaw("tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, CONCAT(cabang.kodeCabang, '-', cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.jenisPembayaranFinal, tblcustomer.cif, tblcustomer.namaCustomer, tblcustomer.noKtp, tran_fapg.created_at AS timeInputFAPG, taksirawal.created_at AS timeTaksirPenaksir, taksirfinal.created_at AS timeTaksirKaUnit, trans_gadai.created_at AS timeInputGadai, trans_gadai.tglApprovalKaunit, trans_gadai.tglApprovalKacab, trans_gadai.tglApprovalKawil, trans_gadai.tglApprovalDirektur, trans_gadai.tglApprovalDirut, trans_gadai.tglCetakKwitansi AS timeCetakKwitansi, trans_gadai.nilaiPinjaman, ROUND((trans_gadai.nilaiPinjaman / taksirfinal.totalTaksiran) * 100, 2) AS ltv, trans_gadai.rateFlat, trans_gadai.totalObligor, trans_gadai.statusAplikasi, trans_gadai.approvalFinal, trans_gadai.approvalLtv, trans_gadai.approvalUangPinjaman, trans_gadai.approvalRateFlat, trans_gadai.approvalOneObligor, trans_gadai.timeFotoNasabah, trans_gadai.tglApprovalKadeptCpa, trans_gadai.tglApprovalKadivCpa")
            ->groupBy('trans_gadai.noSbg')
            ->orderBy('trans_payctrl.tanggalPencairan');
            if ($nama_wilayah) {
                $data = $data->where('tblcabang.idWilayah', $nama_wilayah);
            }
            if ($tanggalNext) {
                $data = $data->where('trans_payctrl.tanggalPencairan', '>=', $tanggalNext);
            }
            $data = $data->get();

        $fileName = 'WaktuTransaksiKonvensional_'.str_replace("-", "", $tgl_awal).'_'.str_replace("-", "", $tgl_sampai).'.csv';
        // dd($fileName);

        if ($data) {
            $lineNoExtend = $lineNo;
            foreach($data as $dita) {
                $lineNoExtend++;
                $baris = $lineNo.'|'.$dita->kodeOutlet.'|'.$dita->namaOutlet.'|'.$dita->namaCabang.'|'.$dita->namaWilayah.'|'.$dita->tanggalPencairan.'|'.$dita->noSbg.'|'.$dita->jenisPembayaranFinal.'|'.$dita->cif.'|'.$dita->namaCustomer.'|'.$dita->noKtp.'|'.$dita->nilaiPinjaman.'|'.$dita->rateFlat.'|'.$dita->ltv.'|'.$dita->totalObligor.'|'.$dita->statusAplikasi.'|'.$dita->timeInputFAPG.'|'.$dita->timeTaksirPenaksir.'|'.$dita->timeTaksirKaUnit.'|'.$dita->timeInputGadai.'|'.$dita->tglApprovalKaunit.'|'.$dita->tglApprovalKacab.'|'.$dita->tglApprovalKawil.'|'.$dita->tglApprovalKadeptCpa.'|'.$dita->tglApprovalKadivCpa.'|'.$dita->tglApprovalDirektur.'|'.$dita->tglApprovalDirut.'|'.$dita->timeCetakKwitansi.'|'.$dita->timeFotoNasabah.'|'.$dita->approvalFinal.'|'.$dita->approvalUangPinjaman.'|'.$dita->approvalRateFlat.'|'.$dita->approvalLtv.'|'.$dita->approvalOneObligor;
                Storage::disk('publicupload')->append('/uploads/TimeTransaksi/'.$fileName, $baris);
            }
        }

        $tanggalNextExtend = date('Y-m-d', strtotime('+1 days', strtotime($tanggalNext)));
        if ($tgl_sampai == $tanggalNextExtend) {
            return Storage::disk('publicupload')->download('/uploads/TimeTransaksi/'.$fileName);
        } else {
            return WaktuTransaksiKonvensional::exportextends($nama_wilayah, $tgl_awal, $tanggalNextExtend, $tgl_sampai, $lineNoExtend);
        }        
    }

    public function excel(Request $request){
        return Excel::download(new WaktuTransaksiKonvensionalExcel($request->nama_wilayah, $request->tgl_awal, $request->tgl_sampai), 'WaktuTransaksiKonvensional.xlsx');
    }
}
