<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\tblcoatemplate;
use App\model\AkuntingGlSaldo;
use App\model\AkuntingDetail;
use Maatwebsite\Excel\Facades\Excel;
// use App\Http\Exports\OutstandingPinjamanExcel;

class GL extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::orderBy('kd_Wilayah')->where('isActive', '=', 1)->get();
        $coa = tblcoatemplate::orderBy('coaOracle')->groupBy('coaOracle')->where('coaOracle', '!=', '')->get();
        return view('login.akunting.gl.index',[
            "wilayahs"  => $wilayah, 
            "coas"      => $coa, 
        ]);
    }

    public function populate(Request $request) {
        $wilayah = tblwilayah::where('idWilayah', '=', $request->idWilayah)->first();
        $outlet1 = tblcabang::where('kodeCabang', '=', $request->idOutlet1)->first();
        $outlet2 = tblcabang::where('kodeCabang', '=', $request->idOutlet2)->first();
        $coa1 = tblcoatemplate::where('coaOracle', '=', $request->idCoa1)->first();
        $coa2 = tblcoatemplate::where('coaOracle', '=', $request->idCoa2)->first();
        return view('login.akunting.gl.populate',[
            "wilayah"       => $wilayah, 
            "outlet1"       => $outlet1, 
            "outlet2"       => $outlet2, 
            "coa1"          => $coa1, 
            "coa2"          => $coa2, 
            "periode"       => $request->tanggal, 
        ]);
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'outlet1':
                $idWilayah = $request->idWilayah;
                $cabangs = tblcabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    // ->where('idJenisCabang', '=', 4)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->kodeCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
            case 'outlet2':
                $idWilayah = $request->idWilayah;
                $idOutlet1 = $request->idOutlet1;
                $cabangs = tblcabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->where('kodeCabang', '>=', $idOutlet1)
                    ->orderBy('kodeCabang')
                    ->get();
                $result = "";
                if ($cabangs) {
                    foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->kodeCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
                    }
                }
                echo $result;
                break;
            case 'unit':
                $idCabang = $request->idCabang;
                $cabangs = tblcabang::where('isActive', '=', 1)
                    ->where('headCabang', '=', $idCabang)
                    ->orderBy('kodeCabang')
                    ->get();
                $result = "<option value=''>== Nama Unit ==</option>";
                if ($cabangs) {
                    foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
                    }
                }
                echo $result;
                break;
            case 'coa':
                $idCoa1 = $request->idCoa1;
                $coas = tblcoatemplate::where('coaOracle', '!=', '')
                    ->where('coaOracle', '>=', $idCoa1)
                    ->orderBy('coaOracle')
                    ->groupBy('coaOracle')
                    ->get();
                $result = "";
                if ($coas) {
                    foreach ($coas as $coa) {
                        $result .= "<option value='".$coa->coaOracle."'>".$coa->coaOracle."</option>";
                    }
                }
                echo $result;
                break;
		}
	}

    public function export(Request $request){
        Storage::disk('publicupload')->deleteDirectory('/uploads/TB');

        $wilayah = tblwilayah::where('idWilayah', '=', $request->idWilayah)->first();
        $outlet1 = tblcabang::where('kodeCabang', '=', $request->outlet1)->first();
        $outlet2 = tblcabang::where('kodeCabang', '=', $request->outlet2)->first();
        $coa1 = tblcoatemplate::where('coaOracle', '=', $request->coa1)->first();
        $coa2 = tblcoatemplate::where('coaOracle', '=', $request->coa2)->first();
        // dd($outlet1);
        $fileName = 'TB_'.date('ymdhis').'.csv';
        $tanggal = explode(' - ', $request->periode);
        $tanggalAwalBulan = substr($tanggal[0], 0, 7).'-01';
        $tanggalBulanLalu = date('Y-m-d', strtotime('-1 days', strtotime($tanggalAwalBulan)));
        // dd($tanggalBulanLalu);

        $baris = 'MAS GL Ledger Report';
        Storage::disk('publicupload')->put('/uploads/TB/TB_'.$fileName, $baris);
        $baris = '';
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        $baris = 'Parameter :';
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        $baris = 'Start Date|'.$tanggal[0];
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        $baris = 'End Date|'.$tanggal[1];
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        if (!$wilayah) $baris = 'Company|-';
        else $baris = 'Company|'.$wilayah->namaWilayah;
        // dd($baris);
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        if (!$outlet1) $baris = 'Outlet From|-';
        else $baris = 'Outlet From|'.$outlet1->kodeCabang.' - '.$outlet1->namaCabang;
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        if (!$outlet2) $baris = 'Outlet To|-';
        else $baris = 'Outlet To|'.$outlet2->kodeCabang.' - '.$outlet2->namaCabang;
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        if (!$coa1) $baris = 'Natural Account From|-';
        else $baris = 'Natural Account From|'.$coa1->coa.'|'.$coa1->coaOracle.'|'.$coa1->description;
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        if (!$coa2) $baris = 'Natural Account To|-';
        else $baris = 'Natural Account To|'.$coa2->coa.'|'.$coa2->coaOracle.'|'.$coa2->description;
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        $baris = '';
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
        $baris = 'Line_No|Tanggal|Account_Promas|Account_Oracle|Account_Description|Batch|Journal_Name|Source|Description|No_Voucher|Debit|Credit|Saldo';
        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);

        $dataOutlet = tblcabang::where('idWilayah', '=', $request->idWilayah)
            ->where('kodeCabang', '>=', $request->outlet1)
            ->where('kodeCabang', '<=', $request->outlet2)
            ->selectRaw('kodeCabang')
            ->orderBy('kodeCabang', 'ASC')
            ->get();
        $dataCOA = tblcoatemplate::where('coaOracle', '>=', $request->coa1)
            ->where('coaOracle', '<=', $request->coa2)
            ->where('coaOracle', '!=', '')
            ->selectRaw('coaOracle')
            ->groupBy('coaOracle')
            ->orderBy('coaOracle')
            ->get();
        if ($dataOutlet) {
            foreach($dataOutlet as $ditaOutlet) {
                if ($dataCOA) {
                    foreach ($dataCOA as $ditaCOA) {
                        $sumSaldo = 0;
                        $saldo = AkuntingGlSaldo::where('kodeCabang', '=', $ditaOutlet->kodeCabang)
                            ->where('coaOracle', '=', $ditaCOA->coaOracle)
                            ->where('periode', '=', $tanggalBulanLalu)
                            ->first();
                        if (!$saldo) $sumSaldo += 0;
                        else $sumSaldo += $saldo->amount;
                        $baris = '||'.$ditaOutlet->kodeCabang.' - '.$ditaCOA->coaOracle.' : '.$sumSaldo;
                        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);

                        $data = AkuntingDetail::Join('akunting_summary', 'akunting_summary.idSummary', '=', 'akunting_detail.idSummary')
                            ->Join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                            ->Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_detail.idCabang')
                            ->Join('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'akunting_detail.idCoa')
                            ->where('tblcabang.kodeCabang', '=', $ditaOutlet->kodeCabang)
                            ->where('tblcoatemplate.coaOracle', '=', $ditaCOA->coaOracle)
                            ->where('akunting_detail.tanggal', '>=', $tanggalAwalBulan)
                            ->where('akunting_detail.tanggal', '<=', $tanggal[1])
                            ->where('akunting_summary.isPost', '=', 1)
                            ->selectRaw('akunting_detail.tanggal, akunting_detail.coaCabang, tblcoatemplate.coaOracle, tblcoatemplate.description, akunting_detail.batch, acc_jenisjurnal.namaJenisJurnalOracle, akunting_summary.keteranganTransaksi, akunting_summary.kodeTransaksi, akunting_detail.dk, akunting_detail.amount')
                            ->orderBy('akunting_detail.tanggal')
                            ->get();
                        // dd($data);
                        $sumDebit = 0;
                        $sumCredit = 0;
                        if ($data) {
                            $lineNo = 0;
                            foreach ($data as $dita) {
                                $lineNo++;
                                if ($dita->dk == 'D') {
                                    $sumSaldo += $dita->amount;
                                    $sumDebit += $dita->amount;
                                    $baris = $lineNo.'|'.$dita->tanggal.'|'.$dita->coaCabang.'|'.$dita->coaOracle.'|'.$dita->description.'|'.$dita->batch.'|'.$dita->namaJenisJurnalOracle.'|Promas|'.$dita->keteranganTransaksi.'|'.$dita->kodeTransaksi.'|'.$dita->amount.'||'.$sumSaldo;
                                } else {
                                    $sumSaldo -= $dita->amount;
                                    $sumCredit += $dita->amount;
                                    $baris = $lineNo.'|'.$dita->tanggal.'|'.$dita->coaCabang.'|'.$dita->coaOracle.'|'.$dita->description.'|'.$dita->batch.'|'.$dita->namaJenisJurnalOracle.'|Promas|'.$dita->keteranganTransaksi.'|'.$dita->kodeTransaksi.'||'.$dita->amount.'|'.$sumSaldo;
                                }
                                Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
                            }
                            
                        }
                        $baris = '|||||||||Sub Total|'.$sumDebit.'|'.$sumCredit.'|';
                        Storage::disk('publicupload')->append('/uploads/TB/TB_'.$fileName, $baris);
                    }
                }
            }
        }
        return Storage::disk('publicupload')->download('/uploads/TB/TB_'.$fileName);
    }


    public function excel(Request $request){
        return Excel::download(new WaktuTransaksiSyariahExcel($request->nama_wilayah, $request->tgl_awal, $request->tgl_sampai), 'WaktuTransaksiSyariah.xlsx');
    }
}
