<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPelunasanGadai;
use App\model\TransJfLunas;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class PelunasanGadaiJF extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Pelunasan Gadai JF');
        return view('login.pelunasangadaijf.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = TransPelunasanGadai::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_pelunasangadai.idGadai')
            ->join('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_pelunasangadai.idCabang')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadai.idJf')
            ->leftJoin('trans_jf_lunas', 'trans_jf_lunas.idGadai', '=', 'trans_pelunasangadai.idGadai')
            ->selectRaw('trans_pelunasangadai.idPelunasanGadai, trans_pelunasangadai.idGadai, trans_pelunasangadai.tglPelunasan, trans_pelunasangadai.nilaiPokok, trans_pelunasangadai.nilaiTitipan, trans_pelunasangadai.isJurnal, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.tanggalArt, tblcabang.kodeCabang, tblcabang.namaCabang, tblproduk.isElektronik, tblsettingjf.namaJf, trans_jf_lunas.idTransJf, trans_jf_lunas.isJurnal AS jurnalBayar')
            ->orderBy('trans_pelunasangadai.tglPelunasan', 'ASC')
            ->orderBy('trans_pelunasangadai.idPelunasanGadai', 'ASC')
            // ->where('trans_pelunasangadai.isJf', '=', 1)
            ->where('trans_pelunasangadai.isStatus', '=', 1)
            ->where('trans_gadaijf.isApprove', '=', 1)
            ->where(function($query2){
                $query2->where('trans_gadaijf.idJf', '=', 22)
                    ->orWhere('trans_gadaijf.idJf', '=', 23);
            });
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('trans_pelunasangadai.tglPelunasan', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('trans_pelunasangadai.tglPelunasan', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('trans_payctrl.isJurnal', '=', $request->statusJurnal);
        }
        // dd($query->toSql());
        $data = $query->paginate($limit);
        return view('login.pelunasangadaijf.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'   => 1,
                    'dateJurnal' => date('Y-m-d H:i:s'),
                ];
                TransPelunasanGadai::where('idPelunasanGadai', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.pelunasangadaijf.createjurnal',[]);
				break;
            case 'jurnalbayar':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'   => 1,
                    'dateJurnal' => date('Y-m-d H:i:s'),
                ];
                TransJfLunas::where('idTransJf', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.pelunasangadaijf.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                    // dd($data->toSql());
                return view('login.pelunasangadaijf.jurnal',[
                    "idGadai"               => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
