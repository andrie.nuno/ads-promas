<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPayctrl;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use App\model\TblSettingJf;
use App\model\TransGadaiJf;
use App\model\TransJf;
use app\CustomClass\helpers;

class PencairanGadaiJf extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Pencairan Gadai Reguler');
        return view('login.pencairangadaijf.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->leftJoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->leftjoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
            ->leftJoin('trans_jf', 'trans_jf.idGadai', '=', 'trans_payctrl.idGadai')
            ->selectRaw("trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_payctrl.isJurnalJfGantung, trans_gadai.noSbg, trans_gadai.pengajuanPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, tblcabang.kodeCabang, tblcabang.namaCabang, tblsettingjf.namaJf, trans_gadaijf.idGadai AS idGadaiJf, trans_gadaijf.isApprove, trans_gadaijf.isJurnal AS jurnalApproval, trans_jf.idTransJf, trans_jf.isJurnal AS jurnalVerifikasi")
            ->orderBy('trans_payctrl.tanggalPencairan', 'ASC')
            ->orderBy('trans_payctrl.created_at', 'ASC')
            ->where('tblproduk.statusFunding', '=', 1);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('trans_payctrl.tanggalPencairan', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('trans_payctrl.isJurnal', '=', $request->statusJurnal);
        }
        // dd($query->toSql());
        $data = $query->paginate($limit);
        return view('login.pencairangadaijf.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
                    ->leftJoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
                    ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
                    ->leftJoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
                    ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_payctrl.isJurnal, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.idCabang, trans_gadai.nilaiPinjaman, trans_gadai.biayaAdmin, trans_gadai.nilaiApCustomer, trans_gadai.biayaPenyimpanan, trans_gadai.idJf, trans_gadai.pembulatan, tblcabang.kodeCabang, tblcabang.namaCabang, tblsettingjf.namaJf, trans_gadaijf.pinjamanGadai, trans_gadaijf.pinjamanJf')
                    ->where('trans_payctrl.idGadai', '=', $request->id)
                    ->first();
                if ($data) {
                    // yyyy-mm-dd
                    $v_tahunJurnal = substr($data->tanggalPencairan, 0, 4);
                    $v_bulanJurnal = substr($data->tanggalPencairan, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $data->tanggalPencairan)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($data->tanggalPencairan, 2, 2).substr($data->tanggalPencairan, 5, 2).substr($data->tanggalPencairan, 8, 2).'000001';
                    }
                    if($data->pinjamanGadai != '') {
                        $pinjamanGadai = $data->pinjamanGadai;
                        $pinjamanJf = $data->pinjamanJf;
                    } else {
                        if ($data->idJf != 0) {
                            $bankJf = TblSettingJf::find($data->idJf);
                            $pinjamanJf = round(($data->nilaiPinjaman * $bankJf->porsi) / 100);
                            $pinjamanGadai = $data->nilaiPinjaman - $pinjamanJf;
                        } else {
                            $bankJf = TblSettingJf::find(22);
                            $pinjamanJf = round(($data->nilaiPinjaman * $bankJf->porsi) / 100);
                            $pinjamanGadai = $data->nilaiPinjaman - $pinjamanJf;
                        }

                    }
                    $dataSummary = [
                        'idJenisJurnal'         => 5, // Pencairan Gadai JF - Tunai
                        'idGadai'               => $data->idGadai,
                        'idProduk'              => $data->idProduk,
                        'idCabang'              => $data->idCabang,
                        'idBankKeluar'          => 25, // Buffer
                        'tanggal'               => $data->tanggalPencairan,
                        'tahun'                 => $v_tahunJurnal,
                        'bulan'                 => $v_bulanJurnal,
                        'batch'                 => $v_batchJurnal,
                        'kodeTransaksi'         => $data->noSbg,
                        'pokok'                 => $data->nilaiPinjaman,
                        // 'pokokgadai'            => $data->pinjamanGadai,
                        // 'pokokbank'             => $data->pinjamanJf,
                        'pokokgadai'            => $pinjamanGadai,
                        'pokokbank'             => $pinjamanJf,
                        'pencairan'             => $data->nilaiApCustomer,
                        'bunga'                 => $data->biayaPenyimpanan,
                        'admin'                 => $data->biayaAdmin,
                        'pembulatan'            => $data->pembulatan,
                        'keterangan'            => 'Pencairan JF '.$data->noSbg,
                        'keteranganTransaksi'   => 'Pencairan Gadai JF '.$data->noSbg,
                    ];
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;

                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.pokok, akunting_summary.pokokgadai, akunting_summary.pokokbank, akunting_summary.pencairan, akunting_summary.bunga, akunting_summary.admin, akunting_summary.pembulatan')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        $sumDebet = 0;
                        $sumKredit = 0;
                        foreach($dataJurnal as $dita) {
                            if ($dita->nilai == 'pokok') {
                                $amount = $dita->pokok;
                            }
                            if ($dita->nilai == 'pokokgadai') {
                                $amount = $dita->pokokgadai;
                            }
                            if ($dita->nilai == 'pokokbank') {
                                $amount = $dita->pokokbank;
                            }
                            if ($dita->nilai == 'pencairan') {
                                $amount = $dita->pencairan;
                            }
                            if ($dita->nilai == 'bunga') {
                                $amount = $dita->bunga;
                            }
                            if ($dita->nilai == 'admin') {
                                $amount = $dita->admin;
                            }
                            if ($dita->nilai == 'pembulatan') {
                                $amount = $dita->pembulatan;
                            }
                            if ($amount > 0) {
                                if ($dita->idCoa == 207) {
                                    if ($sumDebet != $sumKredit) {
                                        $dataDetail = [
                                            'idSummary'     => $lastId,
                                            'idCabang'      => $dita->idCabang,
                                            'urut'          => $dita->urut,
                                            'idCoa'         => $dita->idCoa,
                                            'tanggal'       => $dita->tanggal,
                                            'tahun'         => $v_tahunJurnal,
                                            'bulan'         => $v_bulanJurnal,
                                            'batch'         => $v_batchJurnal,
                                            'coa'           => $dita->coa,
                                            'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa,
                                            'keterangan'    => $dita->description,
                                            'dk'            => $dita->dk,
                                            'amount'        => $amount
                                        ];
                                        $detail = AkuntingDetail::create($dataDetail);
                                    }
                                } else {
                                    if ($dita->dk == 'D') {
                                        $sumDebet += $amount;
                                    } else {
                                        $sumKredit += $amount;
                                    }
                                    $dataDetail = [
                                        'idSummary'     => $lastId,
                                        'idCabang'      => $dita->idCabang,
                                        'urut'          => $dita->urut,
                                        'idCoa'         => $dita->idCoa,
                                        'tanggal'       => $dita->tanggal,
                                        'tahun'         => $v_tahunJurnal,
                                        'bulan'         => $v_bulanJurnal,
                                        'batch'         => $v_batchJurnal,
                                        'coa'           => $dita->coa,
                                        'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa,
                                        'keterangan'    => $dita->description,
                                        'dk'            => $dita->dk,
                                        'amount'        => $amount
                                    ];
                                    $detail = AkuntingDetail::create($dataDetail);
                                }
                            }
                        }
                    }
                    $dataUpdate = [
                        'isJurnal'      => 1,
                        'dateJurnal'    => date('Y-m-d H:i:s'),
                    ];
                    TransPayctrl::where('idGadai', '=', $request->id)
                        ->update($dataUpdate);
                }
                DB::commit();
                return view('login.pencairangadai.createjurnal',[]);
				break;
            case 'jurnalapprove':
                DB::beginTransaction();
                $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
                    ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.idCabang, trans_gadai.nilaiPinjaman, trans_gadai.biayaPenyimpanan, tblcabang.kodeCabang, tblcabang.namaCabang, trans_gadaijf.idJf, trans_gadaijf.pinjamanGadai, trans_gadaijf.pinjamanJf, trans_gadaijf.bungaJfHarian, trans_gadaijf.isApprove, tblcabang.idWilayah')
                    ->where('trans_payctrl.idGadai', '=', $request->id)
                    ->first();
                if ($data) {
                    // yyyy-mm-dd
                    $v_tahunJurnal = substr($data->tanggalPencairan, 0, 4);
                    $v_bulanJurnal = substr($data->tanggalPencairan, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $data->tanggalPencairan)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($data->tanggalPencairan, 2, 2).substr($data->tanggalPencairan, 5, 2).substr($data->tanggalPencairan, 8, 2).'000001';
                    }
                    if ($data->isApprove == 1) {
                        $ho = TblCabang::where('idWilayah', '=', $data->idWilayah)
                            ->where('idJenisCabang', '=', 2)
                            ->where('isActive', '=', 1)
                            ->selectRaw('idCabang, kodeCabang')
                            ->first();
                        $bungajfmas = $data->biayaPenyimpanan - ROUND($data->bungaJfHarian * 120);
                        $dataSummary = [
                            'idJenisJurnal'         => 22, // OBL Pendanaan JF
                            'idGadai'               => $data->idGadai,
                            'idProduk'              => $data->idProduk,
                            'idCabang'              => $data->idCabang,
                            'idJf'                  => $data->idJf,
                            'idRPKP'                => $ho->idCabang,
                            'idRPKC'                => $data->idCabang,
                            'tanggal'               => $data->tanggalPencairan,
                            'tahun'                 => $v_tahunJurnal,
                            'bulan'                 => $v_bulanJurnal,
                            'batch'                 => $v_batchJurnal,
                            'kodeTransaksi'         => $data->noSbg,
                            'pokok'                 => $data->nilaiPinjaman,
                            'pokokgadai'            => $data->pinjamanGadai,
                            'pokokbank'             => $data->pinjamanJf,
                            'bunga'                 => $data->biayaPenyimpanan,
                            'bungajfmas'            => $bungajfmas,
                            'keterangan'            => 'OBL JF '.$data->noSbg,
                            'keteranganTransaksi'   => 'OBL Gadai JF '.$data->noSbg,
                        ];
                    } else {
                        if ($data->idJf != 0) {
                            $pinjamanJf = $data->pinjamanJf;
                            $pinjamanGadai = $data->pinjamanGadai;
                        } else {
                            $bankJf = TblSettingJf::find(22);
                            $pinjamanJf = round(($data->nilaiPinjaman * $bankJf->porsi) / 100);
                            $pinjamanGadai = $data->nilaiPinjaman - $pinjamanJf;
                        }
                        $dataSummary = [
                            'idJenisJurnal'         => 24, // Batal JF
                            'idGadai'               => $data->idGadai,
                            'idProduk'              => $data->idProduk,
                            'idCabang'              => $data->idCabang,
                            'idJf'                  => $data->idJf,
                            'tanggal'               => $data->tanggalPencairan,
                            'tahun'                 => $v_tahunJurnal,
                            'bulan'                 => $v_bulanJurnal,
                            'batch'                 => $v_batchJurnal,
                            'kodeTransaksi'         => $data->noSbg,
                            'pokok'                 => $data->nilaiPinjaman,
                            // 'pokokgadai'            => $data->pinjamanGadai,
                            // 'pokokbank'             => $data->pinjamanJf,
                            'pokokgadai'            => $pinjamanGadai,
                            'pokokbank'             => $pinjamanJf,
                            'keterangan'            => 'Batal JF '.$data->noSbg,
                            'keteranganTransaksi'   => 'Batal Gadai JF '.$data->noSbg,
                        ];
                    }
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;

                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.pokok, akunting_summary.pokokgadai, akunting_summary.pokokbank, akunting_summary.bunga, akunting_summary.bungajfmas')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        foreach($dataJurnal as $dita) {
                            if ($dita->nilai == 'pokok') {
                                $amount = $dita->pokok;
                            }
                            if ($dita->nilai == 'pokokgadai') {
                                $amount = $dita->pokokgadai;
                            }
                            if ($dita->nilai == 'pokokbank') {
                                $amount = $dita->pokokbank;
                            }
                            if ($dita->nilai == 'bunga') {
                                $amount = $dita->bunga;
                            }
                            if ($dita->nilai == 'bungajfmas') {
                                $amount = $dita->bungajfmas;
                            }
                            if ($amount > 0) {
                                $dataDetail = [
                                    'idSummary'     => $lastId,
                                    'idCabang'      => $dita->idCabang,
                                    'urut'          => $dita->urut,
                                    'idCoa'         => $dita->idCoa,
                                    'tanggal'       => $dita->tanggal,
                                    'tahun'         => $v_tahunJurnal,
                                    'bulan'         => $v_bulanJurnal,
                                    'batch'         => $v_batchJurnal,
                                    'coa'           => $dita->coa,
                                    'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa,
                                    'keterangan'    => $dita->description,
                                    'dk'            => $dita->dk,
                                    'amount'        => $amount
                                ];
                                $detail = AkuntingDetail::create($dataDetail);
                            }
                            $urutCoa = $dita->urut;
                        }
                        if ($data->isApprove == 1) {
                            // Piutang Bank JF
                            $urutCoa = $urutCoa + 1;
                            $coa = TblSettingJf::join('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'tblsettingjf.idCoa')
                                ->where('TblSettingJf.idJf', '=', $data->idJf)
                                ->selectRaw('tblcoatemplate.idCoa, tblcoatemplate.coa, tblcoatemplate.description')
                                ->first();
                            $kodeCabang = $ho->kodeCabang;
                            $dataDetail = [
                                'idSummary'     => $lastId,
                                'idCabang'      => $ho->idCabang,
                                'urut'          => $urutCoa++,
                                'idCoa'         => $coa->idCoa,
                                'tanggal'       => $data->tanggalPencairan,
                                'tahun'         => $v_tahunJurnal,
                                'bulan'         => $v_bulanJurnal,
                                'batch'         => $v_batchJurnal,
                                'coa'           => $coa->coa,
                                'coaCabang'     => $kodeCabang.'.'.$coa->coa,
                                'keterangan'    => $coa->description,
                                'dk'            => 'D',
                                'amount'        => $data->pinjamanJf
                            ];
                            $detail = AkuntingDetail::create($dataDetail);
                            // RPKC
                            $urutCoa = $urutCoa + 1;
                            $coa = TblCabang::join('tblcoatemplate', 'tblcoatemplate.coa', '=', 'tblcabang.coa')
                                ->where('tblcabang.idCabang', '=', $data->idCabang)
                                ->selectRaw('tblcabang.kodeCabang, tblcoatemplate.idCoa, tblcoatemplate.coa, tblcoatemplate.description')
                                ->first();
                            $dataDetail = [
                                'idSummary'     => $lastId,
                                'idCabang'      => $ho->idCabang,
                                'urut'          => $urutCoa++,
                                'idCoa'         => $coa->idCoa,
                                'tanggal'       => $data->tanggalPencairan,
                                'tahun'         => $v_tahunJurnal,
                                'bulan'         => $v_bulanJurnal,
                                'batch'         => $v_batchJurnal,
                                'coa'           => $coa->coa,
                                'coaCabang'     => $kodeCabang.'.'.$coa->coa,
                                'keterangan'    => $coa->description,
                                'dk'            => 'K',
                                'amount'        => $data->pinjamanJf
                            ];
                            $detail = AkuntingDetail::create($dataDetail);
                        }
                    }
                    $dataUpdate = [
                        'isJurnal'      => 1,
                        'dateJurnal'    => date('Y-m-d H:i:s'),
                    ];
                    TransGadaiJf::where('idGadai', '=', $request->id)
                        ->update($dataUpdate);
                }
                DB::commit();
                return view('login.pencairangadai.createjurnal',[]);
				break;
            case 'jurnalrejectgantung':
                DB::beginTransaction();
                $data = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
                    ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
                    ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPencairan, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.idCabang, trans_gadai.nilaiPinjaman, trans_gadai.biayaPenyimpanan, trans_gadai.idJf, tblcabang.kodeCabang, tblcabang.namaCabang, tblcabang.idWilayah')
                    ->where('trans_payctrl.idGadai', '=', $request->id)
                    ->first();
                if ($data) {
                    // yyyy-mm-dd
                    $v_tahunJurnal = substr($data->tanggalPencairan, 0, 4);
                    $v_bulanJurnal = substr($data->tanggalPencairan, 5, 2);
                    $last = AkuntingSummary::where('tanggal', '=', $data->tanggalPencairan)
                        ->selectRaw('batch')
                        ->orderBy('batch', 'DESC')
                        ->first();
                    if ($last) {
                        $v_batchJurnal = $last->batch + 1;
                    } else {
                        $v_batchJurnal = substr($data->tanggalPencairan, 2, 2).substr($data->tanggalPencairan, 5, 2).substr($data->tanggalPencairan, 8, 2).'000001';
                    }
                    if ($data->idJf != 0) {
                        $bankJf = TblSettingJf::find($data->idJf);
                        $pinjamanJf = round(($data->nilaiPinjaman * $bankJf->porsi) / 100);
                        $pinjamanGadai = $data->nilaiPinjaman - $pinjamanJf;
                    } else {
                        $bankJf = TblSettingJf::find(22);
                        $pinjamanJf = round(($data->nilaiPinjaman * $bankJf->porsi) / 100);
                        $pinjamanGadai = $data->nilaiPinjaman - $pinjamanJf;
                    }

                    $dataSummary = [
                        'idJenisJurnal'         => 24, // Batal JF
                        'idGadai'               => $data->idGadai,
                        'idProduk'              => $data->idProduk,
                        'idCabang'              => $data->idCabang,
                        'idJf'                  => $data->idJf,
                        'tanggal'               => $data->tanggalPencairan,
                        'tahun'                 => $v_tahunJurnal,
                        'bulan'                 => $v_bulanJurnal,
                        'batch'                 => $v_batchJurnal,
                        'kodeTransaksi'         => $data->noSbg,
                        'pokok'                 => $data->nilaiPinjaman,
                        // 'pokokgadai'            => $data->pinjamanGadai,
                        // 'pokokbank'             => $data->pinjamanJf,
                        'pokokgadai'            => $pinjamanGadai,
                        'pokokbank'             => $pinjamanJf,
                        'keterangan'            => 'Batal JF '.$data->noSbg,
                        'keteranganTransaksi'   => 'Batal Gadai JF '.$data->noSbg,
                    ];
                    $summary = AkuntingSummary::create($dataSummary);
                    $lastId = $summary->idSummary;

                    $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                        ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                        ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                        ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.pokok, akunting_summary.pokokgadai, akunting_summary.pokokbank, akunting_summary.bunga, akunting_summary.bungajfmas')
                        ->where('akunting_summary.idSummary', '=', $lastId)
                        ->orderBy('acc_skemajurnal.urut', 'ASC')
                        ->get();
                    if ($dataJurnal) {
                        foreach($dataJurnal as $dita) {
                            if ($dita->nilai == 'pokok') {
                                $amount = $dita->pokok;
                            }
                            if ($dita->nilai == 'pokokgadai') {
                                $amount = $dita->pokokgadai;
                            }
                            if ($dita->nilai == 'pokokbank') {
                                $amount = $dita->pokokbank;
                            }
                            if ($dita->nilai == 'bunga') {
                                $amount = $dita->bunga;
                            }
                            if ($dita->nilai == 'bungajfmas') {
                                $amount = $dita->bungajfmas;
                            }
                            if ($amount > 0) {
                                $dataDetail = [
                                    'idSummary'     => $lastId,
                                    'idCabang'      => $dita->idCabang,
                                    'urut'          => $dita->urut,
                                    'idCoa'         => $dita->idCoa,
                                    'tanggal'       => $dita->tanggal,
                                    'tahun'         => $v_tahunJurnal,
                                    'bulan'         => $v_bulanJurnal,
                                    'batch'         => $v_batchJurnal,
                                    'coa'           => $dita->coa,
                                    'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa,
                                    'keterangan'    => $dita->description,
                                    'dk'            => $dita->dk,
                                    'amount'        => $amount
                                ];
                                $detail = AkuntingDetail::create($dataDetail);
                            }
                            $urutCoa = $dita->urut;
                        }
                    }
                    $dataUpdate = [
                        'isJurnalJfGantung'      => 1,
                        'dateJurnalJfGantung'    => date('Y-m-d H:i:s'),
                    ];
                    TransPayctrl::where('idGadai', '=', $request->id)
                        ->update($dataUpdate);
                }
                DB::commit();
                return view('login.pencairangadai.createjurnal',[]);
				break;
            case 'jurnalverifikasi':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'      => 1,
                    'dateJurnal'    => date('Y-m-d H:i:s'),
                ];
                TransJf::where('idTransJf', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.pencairangadai.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.pencairangadai.jurnal',[
                    "idGadai"   => $request->id,
                    "data"      => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
