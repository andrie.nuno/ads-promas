<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblbatchfullfill;
use App\model\tblproduk;
use App\model\member_jamMulai;
use App\model\member_branch;
use App\model\tbllogit;

class MasterTimeFullFill extends Controller
{
    public function index()
    {
        return view('login.parameterfinance.mastertimefullfill.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $data = tblbatchfullfill::paginate($limit);
        // $data = tblbatchfullfill::all();
        return view('login.parameterfinance.mastertimefullfill.populate',[
            "data"          => $data, 
            // "date"          => "test", 
            // "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.parameterfinance.mastertimefullfill.create', [
                ]);				
				break;
            case 'edit':
                $data = tblbatchfullfill::where('idBatch', '=', $request->id)
                    ->first();
                return view('login.parameterfinance.mastertimefullfill.edit', [
                    "data"      => $data
                ]);
                break;
            // case 'detail':
            //     $data = tblbatchfullfill::Join('tblproduk', 'tblproduk.namaBatch', '=', 'tblbatchfullfill.namaBatch')
            //     ->Join('member_jamMulai', 'member_jamMulai.idjamMulai', '=', 'tblbatchfullfill.idjamMulai')
            //     ->Join('member_branch', 'member_branch.idBranch', '=', 'tblbatchfullfill.idBranch')
            //     ->selectRaw('tblbatchfullfill.*, tblproduk.keterangan, member_jamMulai.namajamMulai, member_branch.namaBranch')
            //     ->where('tblproduk.isActive', '=', 1)
            //     ->orderBY('tblbatchfullfill.namaBatch', 'ASC')
            //     ->orderBY('tblbatchfullfill.idjamMulai', 'ASC')
            //         ->where('tblbatchfullfill.idBatch', '=', $request->id)
            //         ->first();
            //     return view('login.parameterfinance.mastertimefullfill.detail', [
            //         "data"  => $data
            //     ]);
            //     break;
		}
    }

    public function proses(Request $request) {
        $rules = [
            'namaBatch'          => 'required',
            'jamMulai'           => 'required',
            'jamSelesai'             => 'required',
        ]; 
        $messages = [
            'namaBatch.required'         => 'Nama Batch wajib diisi.',
            'jamMulai.required'          => 'jamMulai wajib diisi.',
            'jamSelesai.required'            => 'jamSelesai wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = tblbatchfullfill::firstOrNew(['idBatch' => $request->idBatch]);
                $data->namaBatch = $request->namaBatch;
                $data->jamMulai = $request->jamMulai;
                $data->jamSelesai = $request->jamSelesai;
                $data->isActive = $request->isActive;
                $data->idUser = Auth::id();
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
