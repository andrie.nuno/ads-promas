<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\Http\Exports\PelunasanKonvensionalExcel;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\trans_gadai;
// use App\model\RegisterJaminan;
// use App\model\tblsuperlender;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\exportOutstandingJob;
use App\Http\Exports\OutstandingPinjamanExcel;
use Illuminate\Support\Facades\Storage;
use App\model\report_harian;

class PelunasanKonvensional extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::orderBy('idWilayah')->where('isActive', 1)->get();
        return view('login.reportpromas.pelunasankonvensional.index',[
            "wilayahs"      => $wilayah,
            // "idCompany"     => Auth::user()->idCompany, 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $tanggal = $request->tanggal;
        $query = report_harian::where('tanggal', '>=', $request->tanggalAwal)
        ->where('tanggal', '<=', $request->tanggalAkhir)
        ->where('jenisReport', 'Pelunasan');
        $data = $query->paginate($limit);
        // $tanggalAwal = substr($tanggal,0,7)."-01";
        // $query = trans_gadai::
        //     join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        //     ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        //     ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        //     ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        //     ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        //     ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        //     ->leftJoin("trans_pelunasangadai", function($join){
        //         $join->on('trans_pelunasangadai.idGadai', '=', 'trans_gadai.idGadai');
        //         $join->where(DB::raw("trans_pelunasangadai.isStatus"), "=", 1);
        //     })
        //     ->leftJoin("trans_pelunasanjual", function($join){
        //         $join->on('trans_pelunasanjual.idGadai', '=', 'trans_gadai.idGadai');
        //         $join->where(DB::raw("trans_pelunasanjual.isStatus"), "=", 1);
        //     })
        //     ->leftJoin("trans_pembayarancicilan", function($join){
        //         $join->on('trans_pembayarancicilan.idGadai', '=', 'trans_gadai.idGadai');
        //         $join->where(DB::raw("trans_pembayarancicilan.isStatus"), "=", 1);
        //     })
        //     ->leftJoin("trans_payment_schedule", function($join){
        //         $join->on('trans_payment_schedule.idGadai', '=', 'trans_payment_schedule.idGadai');
        //         $join->on('trans_payment_schedule.angsuranKe', '=', 'trans_pembayarancicilan.angsuranKe');
        //     })
        //     ->leftJoin("trans_gadaijf", function($join){
        //         $join->on('trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai');
        //         $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
        //     })
        //     ->leftjoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
        //     ->leftJoin(\DB::raw("(SELECT outs.idCustomer FROM trans_payctrl AS outs WHERE outs.tanggalPencairan <= '$tanggal' AND (outs.tanggalPelunasan IS NULL OR outs.tanggalPelunasan > '$tanggal') GROUP BY outs.idCustomer) AS pinjamanAktif"), 'pinjamanAktif.idCustomer', '=', 'trans_gadai.idCustomer')
        //     ->selectRaw("tblproduk.kodeProduk AS produk, trans_gadai.noSbg, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, 
        //     CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, tblcustomer.namaCustomer, 
        //     tblcustomer.cif, tblcustomer.noKtp, tblcustomer.hp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, trans_gadai.lamaPinjaman AS tenor, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.ovd, DATEDIFF(trans_pembayarancicilan.tglPembayaran, trans_payment_schedule.tglJatuhTempo)) AS ovd, DATE(trans_gadai.tglCair) AS tglCair, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.tanggalPelunasan, trans_pembayarancicilan.tglPembayaran) AS tanggalPelunasan, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.tglJatuhTempo, trans_payment_schedule.tglJatuhTempo) AS tglJatuhTempo, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.nilaiPinjaman, IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', trans_payment_schedule.pokokAngsuran, trans_pembayarancicilan.nilaiPokok)) AS pokokAwal, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan), (trans_payment_schedule.pokokAngsuran + trans_payment_schedule.bungaAngsuran)) AS pinjamAwal, 
        //     ROUND(trans_gadai.rateFlat * 12, 2) AS rate, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.totalPelunasan, trans_pelunasanjual.totalKewajiban), (trans_pembayarancicilan.totalPembayaran)) AS nilaiLunas, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(tblsettingjf.kodeJf IS NULL, trans_payctrl.acrueBunga, (trans_payctrl.acrueBungaMurni + trans_payctrl.acrueBungaSelisih)), (trans_payment_schedule.acrualAkhirBulan + trans_payment_schedule.acrualJatuhTempo)) AS acrueBunga, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiBunga, trans_pelunasanjual.nilaiBunga), trans_pembayarancicilan.nilaiBunga) AS pendapatanBunga, 
        //     IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiDenda, 0) AS biayaPemeliharaan, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, trans_pelunasanjual.nilaiAdminJual), 0) AS adminPenjualan, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_gadai.biayaPenyimpanan - trans_pelunasangadai.nilaiBunga), (trans_gadai.biayaPenyimpanan - trans_pelunasanjual.nilaiBunga)), 0) AS upsite, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_pelunasangadai.totalKewajiban - trans_pelunasangadai.totalPelunasan), 0), 0) AS diskonPelunasan, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, (trans_pelunasanjual.totalKewajiban - trans_pelunasanjual.totalPelunasan)), 0) AS diskonPenjualan, 
        //     tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.namaJf AS bankPendana, tblsettingjf.rate AS RateJf, tblsettingjf.porsi AS porsiBank, 
        //     IF(trans_pelunasangadai.pelunasanBankJfReal != 0, trans_pelunasangadai.pelunasanBankJfReal, trans_pelunasangadai.pelunasanBankJf) AS pelunasanJf, 
        //     IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.statusLunas, trans_payctrl.statusPinjaman), IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', 'Pembayaran Cicilan', 'Pelunasan Cicilan Dipercepat')) AS statusPelunasan, 
        //     IF(pinjamanAktif.idCustomer IS NULL, 'EXIT', '') AS statusExit")
        //     ->whereRaw("trans_payctrl.tanggalPelunasan >= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggalAwal') 
        //     AND trans_payctrl.tanggalPelunasan <= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggal') OR 
        //     (trans_pembayarancicilan.tglPembayaran >= '$tanggalAwal' AND trans_pembayarancicilan.tglPembayaran <= '$tanggal')")
        //     // ->where(DB::raw("(trans_payctrl.tanggalpelunasan > $tanggal or trans_payctrl.tanggalpelunasan is null)"))
        //     ;
        // if ($request->nama_wilayah) {
        //     $data = $query->where('tblcabang.idWilayah', $request->nama_wilayah);
        // }
        // if ($request->nama_cabang) {
        //     $data = $query->where('tblcabang.idCabang', $request->nama_cabang);
        // }
        // if ($request->nama_unit) {
        //     $data = $query->where('tblcabang.headCabang', $request->nama_unit);
        // }
        // $data = $query->paginate($limit);

        // $dd = dd($query->toSql());
        // $dd2 = dd($request);
        return view('login.reportpromas.pelunasankonvensional.populate',[
            "tanggalAwal"   => $request->tanggalAwal, 
            "tanggalAkhir"  => $request->tanggalAkhir, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('trans_payctrl', 'trans_payctrl.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, trans_payctrl.jmlHari, trans_payctrl.bungaHarian, trans_payctrl.jasaMitraHarian, trans_payctrl.bungaTerhutang, trans_payctrl.jasaMitraTerhutang, trans_payctrl.dendaTerhutang, trans_payctrl.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.reportpromas.pelunasankonvensional.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
                // if ($request->idWilayah != 0){
                    $cabangs = tblcabang::where('idWilayah', '=', $idWilayah)->where('idJenisCabang', '=', 4)->orderBy('namaCabang')->get();
                // }
                $result = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
						$result .= "<option value='".$cabang->idCabang."'>".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
            case 'unit':
                $idHead = $request->idHead;
                // if ($request->idHead != 0){
                    $units = tblcabang::where('headCabang', '=', $idHead)->get();
                // }
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($units) {
                    foreach ($units as $unit) {
                        $result2 .= "<option value='".$unit->idCabang."'>".$unit->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
		}
	}
    
    public function download(Request $request) {
        $data = report_harian::find($request->id);
        $url = $data->urlFile;
        $fileName = "Pelunasan_".str_replace('-', '', $data->tanggal).".txt";
        // Fetch the content from the URL
        $fileContent = file_get_contents($url);
        // Set the headers for force download
        $headers = [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="'.$fileName.'"',
        ];
        // Return the response with the file content and headers
        return response($fileContent, 200, $headers);
    }

    public function excel(Request $request) {
        return Excel::download(new PelunasanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'PelunasanKonvensional.xlsx');

        // (new PelunasanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->queue('PelunasanKonvensional.xlsx');
        // return back()->withSuccess('Export started!');

        // (new PelunasanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->store('PelunasanKonvensional.xlsx');

        // exportOutstandingJob::dispatch($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit);
        // return Storage::download('public/AzKonvensional.xlsx');
    }
}
