<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\model\menuads;
use DB;
use Exception;
use Session;
use Validator;
use Illuminate\Support\Facades\Redirect;
use App\CustomClass\helpers;

class Menu extends Controller {
    
    public function index() {
        return view('login.menu.index');
    }

    public function populate() {
        $data = menuads::header();
        return view('login.menu.populate',[
            "data"  => $data
        ]);
    }
    
    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.menu.create');				
				break;
            case 'createchild':
                $header = menuads::find($request->id);
                return view('login.menu.createchild', [
                    "header"    => $header
                ]);
                break;
            case 'edit':
                $menu = menuads::where('isActive', 1)
                        ->where('menuNav', 'Header')
                        ->get();
				$data = menuads::find($request->id);
                return view('login.menu.edit', [
                    "menus" => $menu, 
                    "data"  => $data
                ]);
                break;
            
		}
    }
    
    public function proses(Request $request) {
        $rules = [
            'menuNama'      => 'required',
            'menuSegmen'    => 'required|unique:ads_menu,menuId',
            'menuUrl'       => 'required|unique:ads_menu,menuId',
            'menuNav'       => 'required',
            'menuUrut'      => 'required',
            'headerId'      => 'required',
            'isActive'      => 'required',
        ]; 
        $messages = [
            'menuNama.required'     => 'Nama Menu wajib diisi.',
            'menuSegmen.required'   => 'Nama Segmen wajib diisi.',
            'menuSegmen.unique'     => 'Nama Segmen sudah digunakan pada menu lain.',
            'menuUrl.required'      => 'Nama URL wajib diisi.',
            'menuUrl.unique'        => 'Nama URL sudah digunakan pada menu lain.',
            'menuNav.required'      => 'Nama Navigasi wajib diisi.',
            'menuUrut.required'     => 'Nomor Urut Menu wajib diisi.',
            'headerId.required'     => 'Nama Header wajib diisi.',
            'isActive.required'     => 'Status wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = menuads::firstOrNew(array('menuId' => $request->menuId));
                $data->menuNama = $request->menuNama;
                $data->menuSegmen = $request->menuSegmen;
                $data->menuUrl = $request->menuUrl;
                $data->menuNav = $request->menuNav;
                $data->menuUrut = $request->menuUrut;
                $data->headerId = $request->headerId;
                $data->isActive = $request->isActive;
                $data->addUser = Auth::id();
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Menu");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Menu");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
