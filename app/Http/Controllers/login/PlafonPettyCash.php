<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
// use App\model\mitra_membercompany;
// use App\model\mitra_memberbranch;
// use App\model\member_branch;
// use App\model\member_company;
// use App\model\member_mitra;
// use App\model\mitra_level;
use App\model\tblcabang;
use App\model\tblgradecabang;
use App\model\tblwilayah;
use Illuminate\Support\Facades\Hash;

class PlafonPettyCash extends Controller
{
    public function index()
    {
        $wilayah = tblwilayah::orderBy('idWilayah')->where('isActive', 1)->get();
        return view('login.settingfinance.plafonpettycash.index',[
            "wilayahs"  => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        // if ($request->limit) {
        //     $limit = $request->limit;
        // } else {
        //     $limit = 10;
        // }
        // if ($request->page) {
        //     $page = $request->page;
        // } else {
        //     $page = 1;
        // }
        $rules = [
            'idWilayah' => 'required',
        ]; 
        $messages = [
            'idWilayah.required'            => 'Wilayah wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            $query = tblcabang::leftJoin('tblgradecabang','tblgradecabang.idGradeCabang','=','tblcabang.idGradeCabang')
            ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
            ->where('tblcabang.isActive','=', 1)
            ->orderBy('tblwilayah.idWilayah','asc')
            ->orderBy('tblcabang.kodeCabang','asc')
            ->selectRaw('tblcabang.idCabang, tblcabang.kodeCabang, tblcabang.namaCabang, tblcabang.plaffonPettyCash as plafon, tblcabang.idGradeCabang, tblwilayah.namaWilayah');
            // dd($query->toSql());
            $data = $query->where('tblcabang.idWilayah', $request->idWilayah);
            $data = $query->get();
            return view('login.settingfinance.plafonpettycash.populate',[
                "data"  => $data,
                "idWilayah" => $request->idWilayah, 
                // "limit" => $limit, 
                // "page"  => $page, 
            ]);
        }
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'edit':
                $data = tblcabang::leftJoin('tblgradecabang','tblgradecabang.idGradeCabang','=','tblcabang.idGradeCabang')
                ->join('tbljeniscabang','tbljeniscabang.idJenisCabang','=','tblcabang.idJenisCabang')
                ->selectRaw('tblcabang.idCabang, tblcabang.namaCabang, tblcabang.plaffonPettyCash as plafon')
                ->where('tblcabang.idWilayah', $request->id)
                ->where('tblcabang.isActive','=', 1)
                ->orderBy('tblcabang.kodeCabang','asc')
                ->get();
                return view('login.settingfinance.plafonpettycash.edit', [
                    "data"      => $data,
                ]);
                break;
		}
    }

    public function proses(Request $request) {
            try {
                DB::beginTransaction();
                foreach ($request->data as $itemData) {
                    $item = tblcabang::findOrFail($itemData['idCabang']);
                    $item->update([
                        'plaffonPettyCash' => str_replace(".","",$itemData['plafon']),
                    ]);
                }
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        
	}
}
