<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\FaTransaksiInternal;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class MutasiBank extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Mutasi Bank');
        return view('login.mutasibank.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = FaTransaksiInternal::join('tblcabang AS cabangtujuan', 'cabangtujuan.idCabang', '=', 'fa_transaksiinternal.idCabang')
            ->join('tblcabang AS cabangasal', 'cabangasal.idCabang', '=', 'fa_transaksiinternal.idCabangKeluar')
            ->join('fa_jenistransaksi', 'fa_jenistransaksi.idJenisTransaksi', '=', 'fa_transaksiinternal.idJenisTransaksi')
            ->join('tblbank AS bankmasuk', 'bankmasuk.idbank', '=', 'fa_transaksiinternal.idBankMasuk')
            ->join('tblbank AS bankkeluar', 'bankkeluar.idbank', '=', 'fa_transaksiinternal.idBankKeluar')
            ->orderBy('fa_transaksiinternal.tanggal', 'ASC')
            ->orderBy('fa_transaksiinternal.idTransaksiInternal', 'ASC')
            ->selectRaw('fa_transaksiinternal.idTransaksiInternal, fa_transaksiinternal.idCabang, fa_transaksiinternal.idBankKeluar, fa_transaksiinternal.idBankMasuk, fa_transaksiinternal.tanggal, fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi, fa_transaksiinternal.isJurnal, cabangtujuan.kodeCabang AS kodeCabangTujuan, cabangtujuan.namaCabang AS namaCabangTujuan, cabangasal.kodeCabang AS kodeCabangAsal, cabangasal.namaCabang AS namaCabangAsal, fa_jenistransaksi.namaJenisTransaksi, bankmasuk.namaBank AS namaBankMasuk, bankkeluar.namaBank AS namaBankKeluar')
            ->where('fa_jenistransaksi.typeTransaksi', '=', 3)
            ->where('fa_transaksiinternal.statusApproval', '=', 1);
        if ($request->idWilayah) {
            $data = $query->where('cabangasal.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('cabangasal.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('fa_transaksiinternal.tanggal', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('fa_transaksiinternal.tanggal', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('fa_transaksiinternal.isJurnal', '=', $request->statusJurnal);
        }
        if ($request->idJenisTransaksi) {
            $query = $query->where('fa_transaksiinternal.idJenisTransaksi', '=', $request->idJenisTransaksi);
        }
        $data = $query->paginate($limit);
        return view('login.mutasibank.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "idJenisTransaksi"  => $request->idJenisTransaksi,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'      => 1,
                    'dateJurnal'    => date('Y-m-d H:i:s'),
                ];
                FaTransaksiInternal::where('idTransaksiInternal', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.mutasibank.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idTransaksiInternal', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.mutasibank.jurnal',[
                    "idTransaksiInternal"   => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
