<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\fa_transaksiinternal;
use App\model\RegisterJaminan;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\OutstandingPinjamanExcel;
use App\Http\Exports\MutasiAntarUnitExcel;
use App\model\tblwilayah;

class MutasiAntarUnit extends Controller
{
    public function index()
    {
        return view('login.report-akunting.mutasiantarunit.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = fa_transaksiinternal::join('tblcabang AS pengirim', 'pengirim.idCabang', '=', 'fa_transaksiinternal.idCabangKeluar')
        ->join('tblcabang AS penerima', 'penerima.idCabang', '=', 'fa_transaksiinternal.idCabang')
        ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'pengirim.idWilayah')
        ->join('tblbank AS bankpengirim', 'bankpengirim.idbank', '=', 'fa_transaksiinternal.idBankKeluar')
        ->join('tblbank AS bankpenerima', 'bankpenerima.idbank', '=', 'fa_transaksiinternal.idBankMasuk')
        ->join('tbluser AS userpengirim', 'userpengirim.idUser', '=', 'fa_transaksiinternal.idCreated')
        ->join('tblkaryawaninternal AS karpengirim', 'karpengirim.idKaryawan', '=', 'userpengirim.idKaryawan')
        ->leftJoin('tbluser AS userpenerima', 'userpenerima.idUser', '=', 'fa_transaksiinternal.idApproval')
        ->leftJoin('tblkaryawaninternal AS karpenerima', 'karpenerima.idKaryawan', '=', 'userpenerima.idKaryawan')
        ->selectRaw("fa_transaksiinternal.tanggal, 'Mutasi Antar Unit' AS jenisTransaksi, tblwilayah.namaWilayah,
        pengirim.kodeCabang AS kodeCabangPengirim, pengirim.namaCabang AS namaCabangPengirim, bankpengirim.namaBank AS bankKeluar,
        penerima.kodeCabang AS kodeCabangPenerima, penerima.namaCabang AS namaCabangPenerima, bankpenerima.namaBank AS bankMasuk,
        fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(fa_transaksiinternal.keteranganTransaksi, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS keteranganTransaksi,
        karpengirim.npk AS npkPengirim, karpengirim.namaKaryawan AS namaPengirim,
        IF(fa_transaksiinternal.statusApproval = 1, 'Sudah di Approve', 'Belum di Approve / Reject') AS statusApproval, karpenerima.npk AS npkPenerima,
        karpenerima.namaKaryawan AS namaPenerima, fa_transaksiinternal.dateApproval,
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(fa_transaksiinternal.keteranganApproval, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS keteranganApproval")
        ->where('fa_transaksiinternal.idJenisTransaksi', 30)
        ->where('fa_transaksiinternal.tanggal', '>=', $request->tgl_awal)
        ->where('fa_transaksiinternal.tanggal', '<=', $request->tgl_sampai)
        ->orderBy('fa_transaksiinternal.tanggal');
        $data = $query->paginate($limit);
        return view('login.report-akunting.mutasiantarunit.populate',[
            "tgl_awal"      => $request->tgl_awal,
            "tgl_sampai"    => $request->tgl_sampai,
            "data"          => $data,
            "limit"         => $limit,
            "page"          => $page,
        ]);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('payctrl', 'payctrl.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, payctrl.jmlHari, payctrl.bungaHarian, payctrl.jasaMitraHarian, payctrl.bungaTerhutang, payctrl.jasaMitraTerhutang, payctrl.dendaTerhutang, payctrl.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.laporanborrower.outstandingpinjaman.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    // public function ajax(Request $request) {
	// 	switch ($request->type) {
	// 		case 'branch':
	// 			$idCompany = $request->idCompany;
	// 			$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
	// 			$result = "<option value=''>== Pilih Branch ==</option>";
	// 			if ($branchs) {
	// 				foreach ($branchs as $branch) {
	// 					$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
	// 				}
	// 			}
	// 			echo $result;
	// 			break;
	// 	}
	// }

    public function excel(Request $request){
        return Excel::download(new MutasiAntarUnitExcel($request->tgl_awal, $request->tgl_sampai), "Mutasi Antar Unit Tgl $request->tgl_awal - $request->tgl_sampai .xlsx");
    }
}
