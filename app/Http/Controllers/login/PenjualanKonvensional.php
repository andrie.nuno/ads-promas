<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\Http\Exports\JaminanKonvensionalExcel;
use App\model\tblwilayah;
use App\model\tblcabang;
use App\model\trans_payctrl;
// use App\model\RegisterJaminan;
// use App\model\tblsuperlender;
use Maatwebsite\Excel\Facades\Excel;
use App\Jobs\exportOutstandingJob;
use App\Http\Exports\OutstandingPinjamanExcel;
use Illuminate\Support\Facades\Storage;
use App\model\report_harian;

class PenjualanKonvensional extends Controller
{
    public function index()
    {
        // $wilayah = tblwilayah::orderBy('idWilayah')->where('isActive', 1)->get();
        return view('login.reportpromas.penjualankonvensional.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $tanggal = $request->tanggal;
        $query = report_harian::where('tanggal', '>=', $request->tanggalAwal)
        ->where('tanggal', '<=', $request->tanggalAkhir)
        ->where('jenisReport', 'Penjualan');
        $data = $query->paginate($limit);
        // $query = trans_payctrl::
        //     join('trans_gadai','trans_gadai.idGadai','=','trans_payctrl.idGadai')
        //     ->join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        //     ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        //     ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        //     ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        //     ->join('tran_taksiran','tran_taksiran.idTaksiran','=','trans_gadai.idTaksiran')
        //     ->join('tran_taksirandetail','tran_taksirandetail.idTaksiran','=','trans_gadai.idTaksiran')
        //     ->join('tblbarangemas','tblbarangemas.idBarangEmas','=','tran_taksirandetail.idBarangEmas')
        //     ->selectRaw("tblproduk.kodeProduk, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, 
        //     tblcustomer.cif, tblcustomer.noKtp, tblcustomer.namaCustomer, trans_payctrl.tanggalPencairan, trans_gadai.lamaPinjaman, 
        //     tran_taksirandetail.ltv, tran_taksirandetail.hargaStle, trans_gadai.transKe, trans_gadai.tglJatuhTempo, trans_payctrl.ovd, 
        //     trans_payctrl.totalHutang, trans_gadai.rateFlat, trans_payctrl.pokokAwal, trans_payctrl.pokokSaldo, trans_gadai.biayaAdmin, '' AS kodeMitra, '' AS namaMitra, tran_taksirandetail.idTaksiran AS idTaksiran, 
        //     REPLACE(REPLACE(REPLACE(tran_taksirandetail.keterangan, CHAR(13), ' '), CHAR(10), ' '), '|', ' ') AS model, tran_taksirandetail.taksiran, 
        //     tran_taksirandetail.karat, tran_taksirandetail.beratBersih, tran_taksirandetail.jumlah, tblbarangemas.kodeBarangEmas, 
        //     tblbarangemas.namaBarangEmas")
        //     ->where('trans_payctrl.statusPinjaman', '!=', 'WO')
        //     ->where('trans_payctrl.tanggalPencairan', '<=', $tanggal)
        //     ->where('trans_payctrl.tanggalpelunasan', '>', $tanggal)
        //     ->orWhereNull('trans_payctrl.tanggalpelunasan')
        //     ->orderBy('tran_taksirandetail.idTaksiran')
        //     ->orderBy('tran_taksirandetail.idTaksiranDetail')
        //     // ->where(DB::raw("(trans_payctrl.tanggalpelunasan > $tanggal or trans_payctrl.tanggalpelunasan is null)"))
        //     ;
        // if ($request->nama_wilayah) {
        //     $data = $query->where('tblcabang.idWilayah', $request->nama_wilayah);
        // }
        // if ($request->nama_cabang) {
        //     $data = $query->where('tblcabang.idCabang', $request->nama_cabang);
        // }
        // if ($request->nama_unit) {
        //     $data = $query->where('tblcabang.headCabang', $request->nama_unit);
        // }
        // $data = $query->paginate($limit);

        // $dd = dd($query->toSql());
        // $dd2 = dd($request);
        return view('login.reportpromas.penjualankonvensional.populate',[
            "tanggalAwal"   => $request->tanggalAwal, 
            "tanggalAkhir"  => $request->tanggalAkhir, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    // public function modal(Request $request) {
    //     switch ($request->type) {
    //         case 'detail':
    //             $data = RegisterJaminan::join('register_peminjam', 'register_peminjam.idBorrower', '=', 'register_jaminan.idBorrower')
    //                 ->leftJoin('member_branch', 'member_branch.idBranch', '=', 'register_jaminan.idBranch')
    //                 ->leftJoin('master_provinsi', 'master_provinsi.idProvinsi', '=', 'register_peminjam.provinsi')
    //                 ->leftJoin('master_jeniskelamin', 'master_jeniskelamin.idJenisKelamin', '=', 'register_peminjam.jenisKelamin')
    //                 ->leftJoin('master_kabupaten', 'master_kabupaten.idKabupaten', '=', 'register_peminjam.kota')
    //                 ->leftJoin('master_sumberdana', 'master_sumberdana.idSumberDana', '=', 'register_peminjam.sumberDanaUtama')
    //                 ->leftJoin('master_pekerjaan', 'master_pekerjaan.idPekerjaan', '=', 'register_peminjam.pekerjaan')
    //                 ->join('agreement_data', 'agreement_data.idJaminan', '=', 'register_jaminan.idJaminan')
    //                 ->join('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('trans_payctrl', 'trans_payctrl.idAgreement', '=', 'agreement_data.idAgreement')
    //                 ->join('tblproduk', 'tblproduk.idProduk', '=', 'agreement_peminjam.idProduk')
    //                 ->selectRaw('register_peminjam.*, master_jeniskelamin.namaJenisKelamin, master_provinsi.namaProvinsi, master_kabupaten.namaKabupaten, master_pekerjaan.namaPekerjaan, master_sumberdana.namaSumberDana, register_jaminan.fotoJaminan, register_jaminan.jumlahJaminan, register_jaminan.namaJaminan, register_jaminan.gram, register_jaminan.karat, register_jaminan.nilaiTaksiran, register_jaminan.nilaiMax, register_jaminan.nilaiPinjaman, register_jaminan.hargaPasar, register_jaminan.hargaBeliPasar, member_branch.namaBranch, agreement_data.idAgreement, agreement_data.idJaminan, agreement_data.noPerjanjian, agreement_data.tanggal, agreement_data.tglJt, agreement_data.pokokHutang, agreement_data.jasaTaksir, agreement_data.bungaPinjaman, agreement_data.adminPencairan, agreement_data.nilaiPencairan, agreement_peminjam.idProduk, agreement_peminjam.nilaiStle, agreement_peminjam.nilaiLtv, agreement_peminjam.rateJasaTaksir, agreement_peminjam.rateFeeDanain, agreement_peminjam.ratePendana, agreement_peminjam.ratePencairan, trans_payctrl.jmlHari, trans_payctrl.bungaHarian, trans_payctrl.jasaMitraHarian, trans_payctrl.bungaTerhutang, trans_payctrl.jasaMitraTerhutang, trans_payctrl.dendaTerhutang, trans_payctrl.totalHutang, tblproduk.keterangan AS namaProduk')
    //                 ->where('register_jaminan.idJaminan', '=', $request->id)
    //                 ->first();
    //             return view('login.reportpromas.penjualankonvensional.detail', [
    //                 "data"  => $data
    //             ]);
    //             break;
	// 	}
    // }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
                // if ($request->idWilayah != 0){
                    $cabangs = tblcabang::where('idWilayah', '=', $idWilayah)->where('idJenisCabang', '=', 4)->orderBy('namaCabang')->get();
                // }
                $result = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
						$result .= "<option value='".$cabang->idCabang."'>".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
            case 'unit':
                $idHead = $request->idHead;
                // if ($request->idHead != 0){
                    $units = tblcabang::where('headCabang', '=', $idHead)->get();
                // }
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($units) {
                    foreach ($units as $unit) {
                        $result2 .= "<option value='".$unit->idCabang."'>".$unit->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
		}
	}
    
    public function download(Request $request)
    {
        $url = $request->url;
        // dd($url);
        // You can perform any additional validation or checks here

        // Fetch the content from the URL
        $fileContent = file_get_contents($url);

        // Set the headers for force download
        $headers = [
            'Content-Type' => 'text/plain',
            'Content-Disposition' => 'attachment; filename="downloaded_file.txt"',
        ];

        // Return the response with the file content and headers
        return response($fileContent, 200, $headers);
    }

    public function excel(Request $request) {
        return Excel::download(new JaminanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'JaminanKonvensional.xlsx');

        // (new JaminanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->queue('JaminanKonvensional.xlsx');
        // return back()->withSuccess('Export started!');

        // (new JaminanKonvensionalExcel($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit))->store('JaminanKonvensional.xlsx');

        // exportOutstandingJob::dispatch($request->tanggal, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit);
        // return Storage::download('public/AzKonvensional.xlsx');
    }
}
