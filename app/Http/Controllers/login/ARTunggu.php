<?php
 
namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPayctrl;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class ARTunggu extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'AR Tunggu');        
        return view('login.artunggu.index',[
            "wilayah"       => $wilayah, 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $tanggalMulai = '2023-01-01';
        $query = TransPayctrl::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_payctrl.idGadai')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_gadai.idCabang')
            ->leftJoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai')
            ->selectRaw('trans_payctrl.idGadai, trans_payctrl.tanggalPelunasan, trans_payctrl.isJurnalArt, trans_gadai.noSbg, trans_gadai.tglPendanaan, trans_gadai.tanggalArt, trans_gadai.isJf, tblcabang.kodeCabang, tblcabang.namaCabang, trans_gadaijf.isApprove')
            ->where('trans_gadai.tanggalArt', '>=', $tanggalMulai)
            ->where(function ($query2) {
                $query2->WhereNull('trans_payctrl.tanggalPelunasan')
                ->orWhere('trans_gadai.tanggalArt', '<=', 'trans_payctrl.tanggalPelunasan');
            });
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalArt) {
            $query = $query->where('trans_gadai.tanggalArt', '=', $request->tanggalArt);
        }
        if ($request->statusJurnal) {
            $query = $query->where('trans_payctrl.isJurnalArt', '=', $request->statusJurnal);
        }
        $query = $query->orderBy('trans_gadai.tanggalArt', 'ASC');
        $data = $query->paginate($limit);
        return view('login.artunggu.populate',[
            "idWilayah"         => $request->idWilayah, 
            "idCabang"          => $request->idCabang, 
            "tanggalArt"       => $request->tanggalArt, 
            "statusJurnal"      => $request->statusJurnal, 
            "data"              => $data, 
            "limit"             => $limit, 
            "page"              => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnalArt'   => 1,  
                    'dateJurnalArt' => date('Y-m-d H:i:s'),
                ];
                TransPayctrl::where('idGadai', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.artunggu.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.artunggu.jurnal',[
                    "idGadai"               => $request->id, 
                    "data"                  => $data, 
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
