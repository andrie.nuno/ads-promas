<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\TransPelunasanJual;
use App\model\TransJfLunas;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class Penjualan extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Penjualan');
        return view('login.penjualan.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = TransPelunasanJual::join('trans_gadai', 'trans_gadai.idGadai', '=', 'trans_pelunasanjual.idGadai')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'trans_pelunasanjual.idCabang')
            ->join('tblproduk', 'tblproduk.idProduk', '=', 'trans_gadai.idProduk')
            ->leftJoin('trans_gadaijf', 'trans_gadaijf.idGadai', '=', 'trans_pelunasanjual.idGadai')
            ->leftJoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
            ->selectRaw('trans_pelunasanjual.idPelunasanJual, trans_pelunasanjual.idGadai, trans_pelunasanjual.tglPelunasan, trans_pelunasanjual.nilaiPokok, trans_pelunasanjual.titipan, trans_pelunasanjual.totalKewajiban, trans_pelunasanjual.totalPelunasan, trans_pelunasanjual.isJurnal, trans_gadai.noSbg, trans_gadai.idProduk, trans_gadai.tanggalArt, tblcabang.kodeCabang, tblcabang.namaCabang, tblproduk.isElektronik, tblsettingjf.namaJf')
            ->orderBy('trans_pelunasanjual.tglPelunasan', 'ASC')
            ->orderBy('trans_pelunasanjual.idPelunasanJual', 'ASC')
            ->where('trans_pelunasanjual.isStatus', '=', 1);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('trans_pelunasanjual.tglPelunasan', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('trans_pelunasanjual.tglPelunasan', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('trans_payctrl.isJurnal', '=', $request->statusJurnal);
        }
        // dd($query->toSql());
        $data = $query->paginate($limit);
        return view('login.penjualan.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'   => 1,
                    'dateJurnal' => date('Y-m-d H:i:s'),
                ];
                TransPelunasanJual::where('idPelunasanJual', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.penjualan.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idGadai', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.penjualan.jurnal',[
                    "idGadai"               => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
