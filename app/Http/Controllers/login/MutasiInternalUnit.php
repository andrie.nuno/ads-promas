<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\FaJenisTransaksi;
use App\model\FaTransaksiInternal;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class MutasiInternalUnit extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        $jenistransaksi = FaJenisTransaksi::where('typeTransaksi', '=', 1)->get(); // 1 : Mutasi Internal
        Session::put('breadcrumb', 'Mutasi Internal Unit');
        return view('login.mutasiinternalunit.index',[
            "wilayah"       => $wilayah,
            "jenistransaksi"=> $jenistransaksi,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = FaTransaksiInternal::join('tblcabang', 'tblcabang.idCabang', '=', 'fa_transaksiinternal.idCabang')
            ->join('fa_jenistransaksi', 'fa_jenistransaksi.idJenisTransaksi', '=', 'fa_transaksiinternal.idJenisTransaksi')
            ->join('tblbank AS bankmasuk', 'bankmasuk.idbank', '=', 'fa_transaksiinternal.idBankMasuk')
            ->join('tblbank AS bankkeluar', 'bankkeluar.idbank', '=', 'fa_transaksiinternal.idBankKeluar')
            ->orderBy('fa_transaksiinternal.tanggal', 'ASC')
            ->orderBy('fa_transaksiinternal.idTransaksiInternal', 'ASC')
            ->selectRaw('fa_transaksiinternal.idTransaksiInternal, fa_transaksiinternal.idCabang, fa_transaksiinternal.idBankKeluar, fa_transaksiinternal.idBankMasuk, fa_transaksiinternal.tanggal, fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi, fa_transaksiinternal.isJurnal, tblcabang.kodeCabang, tblcabang.namaCabang, fa_jenistransaksi.namaJenisTransaksi, bankmasuk.namaBank AS namaBankMasuk, bankkeluar.namaBank AS namaBankKeluar')
            ->where('fa_jenistransaksi.typeTransaksi', '=', 1)
            ->where('fa_transaksiinternal.statusApproval', '=', 1);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('fa_transaksiinternal.tanggal', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('fa_transaksiinternal.tanggal', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('fa_transaksiinternal.isJurnal', '=', $request->statusJurnal);
        }
        if ($request->idJenisTransaksi) {
            $query = $query->where('fa_transaksiinternal.idJenisTransaksi', '=', $request->idJenisTransaksi);
        }
        $data = $query->paginate($limit);
        return view('login.mutasiinternalunit.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "idJenisTransaksi"  => $request->idJenisTransaksi,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $data = FaTransaksiInternal::join('fa_jenistransaksi', 'fa_jenistransaksi.idJenisTransaksi', '=', 'fa_transaksiinternal.idJenisTransaksi')
                    ->selectRaw('fa_transaksiinternal.idTransaksiInternal, fa_transaksiinternal.idCabang, fa_transaksiinternal.idJenisTransaksi, fa_transaksiinternal.idBankKeluar, fa_transaksiinternal.idBankMasuk, fa_transaksiinternal.tanggal, fa_transaksiinternal.noVoucher, fa_transaksiinternal.nominalTransaksi, fa_transaksiinternal.keteranganTransaksi, fa_transaksiinternal.statusApproval, fa_jenistransaksi.namaJenisTransaksi, fa_jenistransaksi.idJenisJurnal')
                    ->where('fa_transaksiinternal.idTransaksiInternal', '=', $request->id)
                    ->first();
                if ($data) {
                    if ($data->statusApproval == 1 && $data->nominalTransaksi > 0) {
                        // yyyy-mm-dd
                        $v_tahunJurnal = substr($data->tanggal, 0, 4);
                        $v_bulanJurnal = substr($data->tanggal, 5, 2);
                        $last = AkuntingSummary::where('tanggal', '=', $data->tanggal)
                            ->selectRaw('batch')
                            ->orderBy('batch', 'DESC')
                            ->first();
                        if ($last) {
                            $v_batchJurnal = $last->batch + 1;
                        } else {
                            $v_batchJurnal = substr($data->tanggal, 2, 2).substr($data->tanggal, 5, 2).substr($data->tanggal, 8, 2).'000001';
                        }
                        $dataSummary = [
                            'idJenisJurnal'         => $data->idJenisJurnal,
                            'idTransaksiInternal'   => $data->idTransaksiInternal,
                            'idCabang'              => $data->idCabang,
                            'idBankMasuk'           => $data->idBankMasuk,
                            'idBankKeluar'          => $data->idBankKeluar,
                            'tanggal'               => $data->tanggal,
                            'tahun'                 => $v_tahunJurnal,
                            'bulan'                 => $v_bulanJurnal,
                            'batch'                 => $v_batchJurnal,
                            'kodeTransaksi'         => $data->noVoucher,
                            'amount'                => $data->nominalTransaksi,
                            'keterangan'            => $data->namaJenisTransaksi,
                            'keteranganTransaksi'   => $data->keteranganTransaksi,
                        ];
                        $summary = AkuntingSummary::create($dataSummary);
                        $lastId = $summary->idSummary;

                        $dataJurnal = AkuntingSummary::Join('tblcabang', 'tblcabang.idCabang', '=', 'akunting_summary.idCabang')
                            ->Join('acc_skemajurnal', 'acc_skemajurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                            ->leftJoin('tblcoatemplate', 'tblcoatemplate.idCoa', '=', 'acc_skemajurnal.idCoa')
                            ->selectRaw('akunting_summary.idCabang, akunting_summary.tanggal, tblcabang.kodeCabang, acc_skemajurnal.urut, acc_skemajurnal.idCoa, acc_skemajurnal.dk, acc_skemajurnal.nilai, tblcoatemplate.coa, tblcoatemplate.description, akunting_summary.idBankMasuk, akunting_summary.idBankKeluar, akunting_summary.amount, acc_skemajurnal.jenisCoa')
                            ->where('akunting_summary.idSummary', '=', $lastId)
                            ->orderBy('acc_skemajurnal.urut', 'ASC')
                            ->get();
                        if ($dataJurnal) {
                            foreach($dataJurnal as $dita) {
                                $dataDetail = [
                                    'idSummary'     => $lastId,
                                    'idCabang'      => $dita->idCabang,
                                    'urut'          => $dita->urut,
                                    'idCoa'         => $dita->idCoa,
                                    'tanggal'       => $dita->tanggal,
                                    'tahun'         => $v_tahunJurnal,
                                    'bulan'         => $v_bulanJurnal,
                                    'batch'         => $v_batchJurnal,
                                    'coa'           => $dita->coa,
                                    'coaCabang'     => $dita->kodeCabang.'.'.$dita->coa,
                                    'keterangan'    => $dita->description,
                                    'dk'            => $dita->dk,
                                    'amount'        => $dita->amount
                                ];
                                $detail = AkuntingDetail::create($dataDetail);
                            }
                        }
                        $dataUpdate = [
                            'isJurnal'      => 1,
                            'dateJurnal'    => date('Y-m-d H:i:s'),
                        ];
                        FaTransaksiInternal::where('idTransaksiInternal', '=', $request->id)
                            ->update($dataUpdate);
                    }
                }
                DB::commit();
                return view('login.mutasiinternalunit.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idTransaksiInternal', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                return view('login.mutasiinternalunit.jurnal',[
                    "idTransaksiInternal"   => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
