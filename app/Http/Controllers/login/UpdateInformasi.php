<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\tblcabang;
use App\model\tbluserkonfirmasi;
use App\model\tblwilayah;
use App\model\updateinfomodel;
use Illuminate\Support\HtmlString;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\ReportKonfirmasiExcel;

class UpdateInformasi extends Controller
{
    public function index()
    {
        return view('login.masterinformasi.updateinformasi.index');
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $data = updateinfomodel::orderBy('idUpdate', 'DESC')->paginate($limit);
        return view('login.masterinformasi.updateinformasi.populate',[
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function populatereport(Request $request) {
        $query = tbluserkonfirmasi::join('update_informasi', 'update_informasi.idUpdate', '=', 'tbluserkonfirmasi.idUpdate')
            ->join('tbluser', 'tbluser.idUser', '=', 'tbluserkonfirmasi.idUser')
            ->join('tblkaryawaninternal', 'tblkaryawaninternal.idKaryawan', '=', 'tbluser.idKaryawan')
            ->join('tblcabang', 'tblcabang.idCabang', '=', 'tbluser.idCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->selectRaw('tbluser.username, tblkaryawaninternal.namaKaryawan, tbluserkonfirmasi.timeKonfirmasi, tblcabang.kodeCabang, tblcabang.namaCabang, tblwilayah.namaWilayah');
        if ($request->idUpdate) {
            $query->where('update_informasi.idUpdate', $request->idUpdate);
        }
        if ($request->nama_wilayah) {
            $query->where('tblwilayah.idWilayah', $request->nama_wilayah);
        }
        if ($request->nama_cabang) {
            $query->where('tblcabang.headCabang', $request->nama_cabang);
        }
        if ($request->nama_unit) {
            $query->where('tblcabang.idCabang', $request->nama_unit);
        }
        $data = $query->orderBy('tbluserkonfirmasi.timeKonfirmasi')->get();
        // $sql = $data->toSql();
        // dd($sql);
        return view('login.masterinformasi.updateinformasi.populatereport',[
            "idUpdate"      => $request->idUpdate, 
            "nama_wilayah"  => $request->nama_wilayah, 
            "nama_cabang"   => $request->nama_cabang,
            "nama_unit"     => $request->nama_unit,             
            "data"          => $data, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                return view('login.masterinformasi.updateinformasi.create');				
				break;
            case 'edit':
                $data = updateinfomodel::find($request->id);
                return view('login.masterinformasi.updateinformasi.edit', [
                    "data"  => $data
                ]);
                break;
            case 'detail':
                $data = updateinfomodel::find($request->id);
                return view('login.masterinformasi.updateinformasi.detail', [
                    "data"  => $data
                ]);
                break;
            case 'report':
                $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
                return view('login.masterinformasi.updateinformasi.report',[
                    "wilayahs"          => $wilayah,
                    "idUpdate"          => $request->id,
                ]);
                break;
            case 'delete':
                $data = updateinfomodel::firstOrNew(array('id' => $request->id));
                $data->isActive = 0;
                $data->save();
                echo '<script>closeMultiModal(1);doSearch("Populate");swal("Sukses!", "Proses Berhasil", "success")</script>';
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
			case 'cabang':
				$idWilayah = $request->idWilayah;
				$cabang = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('idWilayah', $idWilayah)
                    ->where('idJenisCabang', 4)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('kodeCabang')
                    ->get();
				$result1 = "<option value=''>== Pilih Cabang ==</option>";
				if ($cabang) {
					foreach ($cabang as $branch) {
						$result1 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
					}
				}
				echo $result1;
				break;
            case 'unit':
                $idCabang = $request->idCabang;
                $branchs = tblcabang::selectRaw('idCabang, kodeCabang, namaCabang')
                    ->where('headCabang', $idCabang)
                    ->where('isActive', 1)
                    ->whereNull('tglNonAktif')
                    ->orderBy('kodeCabang')->get();
                $result2 = "<option value=''>== Pilih Unit ==</option>";
                if ($branchs) {
                    foreach ($branchs as $branch) {
                        $result2 .= "<option value='".$branch->idCabang."'>".$branch->namaCabang."</option>";
                    }
                }
                echo $result2;
                break;
		}
	}

    public function proses(Request $request) {
        $rules = [
            'division'  => 'required',
            'judul'     => 'required',
            'tglMulai'  => 'required|date|after_or_equal:tglMin',
            'tglSampai' => 'required|date|after_or_equal:tglMulai',
            'profilPdf' => 'mimes:jpg,jpeg,png',
        ]; 
        $messages = [
            'division.required'     => 'Divisi wajib diisi.',
            'judul.required'        => 'Judul wajib diisi.',
            'tglMulai.required'     => 'Tanggal Mulai wajib diisi.',
            'tglSampai.required'    => 'Tanggal Sampai wajib diisi.',
        ];
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                // \dd($request);
                DB::beginTransaction();
                if ($request->profilPdf != "") {
                    $image = "data:image/".$request->profilPdf->getClientOriginalExtension().";base64,".base64_encode(file_get_contents($request->profilPdf));
                    $urlImage_S3 = helpers::saveimgS3($image,'/uploads/','/image-asset/',$request->profilPdf->getClientOriginalExtension());
                }
                $data = updateinfomodel::firstOrNew(array('idUpdate' => $request->idUpdate));
                $data->division = $request->division;
                $data->judul = $request->judul;
                // how can i pars the ckeditor data
                $data->message = $request->message;
                $data->jenisInformasi = $request->jenisInformasi;
                $data->tglMulai = $request->tglMulai;
                $data->tglSampai = $request->tglSampai;
                $data->isActive = $request->isActive;
                $data->idUser = Auth::id();
                if ($request->profilPdf != "") {
                    $data->linkUrl = $urlImage_S3;
                }
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}

    public function excel(Request $request){
        return Excel::download(new ReportKonfirmasiExcel($request->idUpdate, $request->nama_wilayah, $request->nama_cabang, $request->nama_unit), 'ReportKonfirmasi.xlsx');
    }
}
