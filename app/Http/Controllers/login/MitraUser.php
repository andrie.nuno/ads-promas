<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use DB;
use Exception;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use App\CustomClass\helpers;
use App\model\mitra_membercompany;
use App\model\mitra_memberbranch;
use App\model\member_branch;
use App\model\member_company;
use App\model\member_mitra;
use App\model\mitra_level;
use App\model\mitra_user;
use App\model\ads_user_level;
use Illuminate\Support\Facades\Hash;

class MitraUser extends Controller
{
    public function index()
    {
        return view('login.mastermitra.mitra_user.index',[
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = ads_user_level::orderBy('ads_user_level.idLevel');
        $data = $query->join('ads_mitra_level', 'ads_mitra_level.idLevel', '=', 'ads_user_level.idLevel')
        ->join('tbluser', 'tbluser.idUser', '=', 'ads_user_level.idUser')
        ->join('tblkaryawaninternal', 'tblkaryawaninternal.idKaryawan', '=', 'tbluser.idKaryawan');
        $data = $query->selectRaw('ads_user_level.*, ads_mitra_level.namaLevel, tbluser.username, tblkaryawaninternal.namaKaryawan');
        $data = $query->paginate($limit);
        return view('login.mastermitra.mitra_user.populate',[
            "data"  => $data, 
            "limit" => $limit, 
            "page"  => $page, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'create':
                $user = mitra_user::
                join('tblkaryawaninternal', 'tblkaryawaninternal.idKaryawan', '=', 'tbluser.idKaryawan')
                ->selectRaw('tbluser.idUser, tbluser.username, tblkaryawaninternal.namaKaryawan')
                ->get();
                $level = mitra_level::All();
                return view('login.mastermitra.mitra_user.create',[
                    "users"    => $user,
                    "levels"    => $level,
                ]);				
				break;	
            case 'edit':
                $user = mitra_user::
                join('tblkaryawaninternal', 'tblkaryawaninternal.idKaryawan', '=', 'tbluser.idKaryawan')
                ->selectRaw('tbluser.idUser, tbluser.username, tblkaryawaninternal.namaKaryawan')
                ->where('tbluser.idUser', '=', $request->id)
                ->first();
                $level = mitra_level::All();
                $data = ads_user_level::find($request->id);
                return view('login.mastermitra.mitra_user.edit', [
                    "users"    => $user,
                    "levels"    => $level,
                    "data"      => $data,
                ]);
                break;
            case 'delete':
                $data = ads_user_level::firstOrNew(array('idUser' => $request->id));
                $data->isActive = 0;
                $data->isTampil = 0;
                $data->save();
                echo '<script>closeMultiModal(1);doSearch("Populate");swal("Sukses!", "Proses Berhasil", "success")</script>';
                break;
		}
    }

    // public function ajax(Request $request) {
	// 	switch ($request->type) {
	// 		case 'branch':
	// 			$idCompany = $request->idCompany;
	// 			$branchs = mitra_memberbranch::where('idCompany',$idCompany)->orderBy('namaBranch')->get();
	// 			$result = "<option value=''>== Pilih Branch ==</option>";
	// 			if ($branchs) {
	// 				foreach ($branchs as $branch) {
	// 					$result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
	// 				}
	// 			}
	// 			echo $result;
	// 			break;
	// 	}
	// }

    public function proses(Request $request) {
        if ($request->_action == "Add") {
            $rules = [
                'idUser'         => 'required',
                'idLevel'       => 'required',
            ];
            $messages = [
                'idUser.required'            => 'User wajib diisi.',
                'idUser.unique'              => 'User sudah digunakan.',
                'idLevel.required'          => 'Kelompok Akses wajib diisi.',
            ];
        } else {
            $rules = [
                'idUser'         => 'required',
                'idLevel'       => 'required',
            ];
            $messages = [
                'idUser.required'            => 'User wajib diisi.',
                'idUser.unique'              => 'User sudah digunakan.',
                'idLevel.required'          => 'Kelompok Akses wajib diisi.',
            ];
        }
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $data = ads_user_level::firstOrNew(array('idUser' => $request->idUser));
                $data->idUser = $request->idUser;
                $data->idLevel = $request->idLevel;
                $data->save();
            } catch (\Exception  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                DB::rollback();
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            DB::commit();
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}
}
