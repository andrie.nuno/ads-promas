<?php
 
namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblJenisCabang;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\FaSaldoBank;
use App\model\TblBank;
use App\model\ReportBankTransaksi;
use App\model\AccSmTrans;
use app\CustomClass\helpers;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Exports\SaldoBankExcel;

class SaldoBank extends Controller
{
    public function index()
    {
        $jeniscabang = TblJenisCabang::where('isActive', '=', 1)->orderBy('idJenisCabang')->get();
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Saldo Bank Core MAS');        
        return view('login.saldobank.index',[
            "jeniscabang"   => $jeniscabang, 
            "wilayah"       => $wilayah, 
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = TblCabang::join('tbljeniscabang', 'tbljeniscabang.idJenisCabang', '=', 'tblcabang.idJenisCabang')
            ->join('tblwilayah', 'tblwilayah.idWilayah', '=', 'tblcabang.idWilayah')
            ->orderBy('tblcabang.kodeCabang', 'ASC')
            ->selectRaw('tblcabang.idCabang, tblcabang.kodeCabang, tblcabang.namaCabang, tbljeniscabang.namaJenisCabang, tblwilayah.namaWilayah')
            ->where('tblcabang.isActive', '=', 1);
        if ($request->idJenisCabang) {
            $data = $query->where('tblcabang.idJenisCabang', '=', $request->idJenisCabang);
        }
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->search) {
            $query = $query->where('tblcabang.namaCabang', 'like', '%'.$request->search.'%');
        }
        $data = $query->paginate($limit);
        return view('login.saldobank.populate',[
            "idJenisCabang" => $request->idJenisCabang, 
            "idWilayah"     => $request->idWilayah, 
            "search"        => $request->search, 
            "data"          => $data, 
            "limit"         => $limit, 
            "page"          => $page, 
        ]);
    }

    public function populatemutasi(Request $request) {
        $bulanAwal = substr($request->tanggalAwal, 0, 7);
        $bulanAkhir = substr($request->tanggalAkhir, 0, 7);
        if ($bulanAwal == $bulanAkhir) {
            if ($request->tanggalAwal <= $request->tanggalAkhir) {
                $data = ReportBankTransaksi::where('idCabang', '=', $request->idCabang)
                    ->where('idBank', '=', $request->idBank)
                    ->where('tanggalSistem', '>=', $request->tanggalAwal)
                    ->where('tanggalSistem', '<=', $request->tanggalAkhir)
                    ->orderBy('tanggalSistem', 'ASC')
                    ->orderBy('idBankTransaksi', 'ASC')
                    ->get();
                $error = "";
            } else {
                $data = "";
                $error = "Tanggal Awal tidak boleh lebih besar dari Tanggal Akhir";
            }
        } else {
            $data = "";
            $error = "Tanggal Awal dan Tanggal Akhir harus dalam bulan yang sama";
        }
        return view('login.saldobank.populatemutasi',[
            "tanggalAwal"   => $request->tanggalAwal, 
            "tanggalAkhir"  => $request->tanggalAkhir, 
            "idCabang"      => $request->idCabang, 
            "idBank"        => $request->idBank, 
            "error"         => $error, 
            "data"          => $data, 
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'bank':
                $cabang = TblCabang::find($request->id);
                $bank = FaSaldoBank::Join('tblbank', 'tblbank.idBank', '=', 'fa_saldobank.idBank')
                    ->selectRaw('fa_saldobank.idBank, fa_saldobank.idCabang, fa_saldobank.tanggal, fa_saldobank.saldoAwal, fa_saldobank.saldoMasuk, fa_saldobank.saldoKeluar, fa_saldobank.saldoAkhir, tblbank.kd_bank, tblbank.namaBank')
                    ->where('fa_saldobank.idCabang', '=', $request->id)
                    ->get();
                return view('login.saldobank.saldobank',[
                    "idCabang"          => $request->id, 
                    "cabang"            => $cabang, 
                    "bank"              => $bank, 
                ]);
				break;
            case 'editsaldo':
                $post = explode('-', $request->id);
                $idCabang = $post[0];
                $idBank = $post[1];
                $cabang = TblCabang::find($idCabang);
                $bank = TblBank::find($idBank);
                $data = FaSaldoBank::where('idCabang', '=', $idCabang)
                    ->where('idBank', '=', $idBank)
                    ->first();
                return view('login.saldobank.saldobankedit',[
                    "idCabang"          => $idCabang, 
                    "idBank"            => $idBank, 
                    "cabang"            => $cabang, 
                    "bank"              => $bank, 
                    "data"              => $data, 
                ]);
                break;
            case 'mutasi':
                $post = explode('|', $request->id);
                $idCabang = $post[0];
                $idBank = $post[1];
                $tanggal = $post[2];
                $cabang = TblCabang::find($idCabang);
                $bank = TblBank::find($idBank);
                $data = FaSaldoBank::where('idCabang', '=', $idCabang)
                    ->where('idBank', '=', $idBank)
                    ->first();
                return view('login.saldobank.mutasibank',[
                    "idCabang"          => $idCabang, 
                    "idBank"            => $idBank, 
                    "tanggal"           => $tanggal, 
                    "cabang"            => $cabang, 
                    "bank"              => $bank, 
                ]);
                break;
            case 'jurnal':
                $data = AccSmTrans::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'acc_sm_trans.idJenisJurnal')
                    ->join('acc_dt_trans', 'acc_dt_trans.idSmTrans', '=', 'acc_sm_trans.idSmTrans')
                    ->selectRaw('acc_sm_trans.idSmTrans, acc_sm_trans.batch, acc_sm_trans.kodeTransaksi, acc_jenisjurnal.namaJenisJurnal, acc_dt_trans.tanggal, acc_dt_trans.coa, acc_dt_trans.coaCabang, acc_dt_trans.keterangan, acc_dt_trans.dk, acc_dt_trans.amount')
                    ->where('acc_sm_trans.kodeTransaksi', '=', $request->id)
                    ->orderBy('acc_sm_trans.idSmTrans', 'ASC')
                    ->orderBy('acc_dt_trans.urut', 'ASC')
                    ->get();
                return view('login.saldobank.jurnal',[
                    "noReferensi"       => $request->id, 
                    "data"              => $data, 
                ]);
                break;
		}
    }

    public function proses(Request $request) {
        $rules = [
            'saldoAwal'     => 'required',
            'saldoMasuk'    => 'required',
            'saldoKeluar'   => 'required',
            'saldoAkhir'    => 'required',
        ];
        $messages = [
            'saldoAwal.required'    => 'saldoAwal wajib diisi.',
            'saldoMasuk.required'   => 'saldoMasuk wajib diisi.',
            'saldoKeluar.required'  => 'saldoKeluar wajib diisi.',
            'saldoAkhir.required'   => 'saldoAkhir wajib diisi.',
        ];        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()) {
            $errMsg = "<ul>";
            foreach ($validator->errors()->all() as $error) {
                $errMsg .= "<li>".$error."</li>";
            }
            $errMsg .= "</ul>";
            echo '<div class="alert alert-danger">'.$errMsg.'</div>';
            return FALSE;
        } else {
            try {
                DB::beginTransaction();
                $dataPost = [
                    "saldoAwal"     => str_replace('.','',$request->saldoAwal),
                    "saldoMasuk"    => str_replace('.','',$request->saldoMasuk),
                    "saldoKeluar"   => str_replace('.','',$request->saldoKeluar),
                    "saldoAkhir"    => str_replace('.','',$request->saldoAkhir),
                ];
                FaSaldoBank::where('idCabang', '=', $request->idCabang)
                    ->where('idBank', '=', $request->idBank)
                    ->update($dataPost);
                DB::commit();
            } catch (\Exception  $e) {
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            } catch (\Throwable  $e) {
                echo '<div class="alert alert-danger text-center mb-0"><strong>Gagal Menyimpan Data '.$e->getMessage().'</strong></div>';				
				return FALSE;
            }
            echo '<div class="alert alert-success text-center mb-0">Berhasil Menyimpan Data</div>';
            if ($request->_action == "Add") {
                echo '<script>doSearch("Populate");$("#form-Add").slideUp();$("#btn-Add").hide()</script>';
            } else {
                echo '<script>doSearch("Populate");$("#form-Edit").slideUp();$("#btn-Edit").hide()</script>';
            }
        }
	}

    public function excel(Request $request){
        $dataCabang = TblCabang::find($request->idCabang);
        $dataBank = TblBank::find($request->idBank);
        return Excel::download(new SaldoBankExcel($request->idCabang, $request->idBank, $request->tanggalAwal, $request->tanggalAkhir), 'Saldo_'.$dataCabang->kodeCabang.'_Bank_'.$dataBank->kd_bank.'_'.str_replace('-','',$request->tanggalAwal).'_'.str_replace('-','',$request->tanggalAkhir).'.xlsx');
    }

    // public function ajax(Request $request) {
	// 	switch ($request->type) {
    //         case 'branch':
    //             $idCompany = $request->idCompany;
    //             $branchs = helpers::api("GET","api/memberbranch",[
    //                 "idCompany" => $idCompany, 
    //                 "limit"     => 500, 
    //                 "page"      => 1, 
    //             ])->data;
	// 			$result = "<option value=''>== Pilih Branch ==</option>";
	// 			if ($branchs->data) {
	// 				foreach ($branchs->data as $branch) {
    //                     if (Session::get('idBranch') != 0) {
    //                         if (Session::get('idAkses') == 9 || Session::get('idAkses') == 14 || Session::get('idAkses') == 15) { // Akses Kepala Cabang / Operasional Manager / Operasional Supervisor
    //                             if (Session::get('idBranch') == $branch->idCabang) {
    //                                 if ($branch->isRo == 1) {
    //                                     $result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
    //                                 }
    //                             }
    //                         } else {
    //                             if (Session::get('idBranch') == $branch->idBranch) {
    //                                 // if ($branch->isRo == 1) {
    //                                     $result = "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
    //                                 // }
    //                             }
    //                         }
    //                     } else {
    //                         if ($branch->isRo == 1) {
    //                             $result .= "<option value='".$branch->idBranch."'>".$branch->namaBranch."</option>";
    //                         }
    //                     }
	// 				}
	// 			}
	// 			echo $result;
	// 			break;
    //         case 'respon':
    //             $media = $request->media;
    //             $data = helpers::api("GET","api/nasabahfucreate",[
    //                 "media"     => $media, 
    //             ])->dataRespon;
    //             $result = "<option value=''>Pilih Satatus Follow Up</option>";
    //             if ($data) {
    //                 for ($x=0; $x < count($data); $x++) {
    //                     $result .= "<option value='".$data[$x]."'>".$data[$x]."</option>";
    //                 }
    //             }
    //             echo $result;
    //             break;
	// 	}
	// }
}
