<?php

namespace App\Http\Controllers\login;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Session;
use DB;
use App\model\TblWilayah;
use App\model\TblCabang;
use App\model\RealisasiPettycash;

use App\model\FaTransaksiInternal;
use App\model\AkuntingSummary;
use App\model\AkuntingDetail;
use app\CustomClass\helpers;

class RealisasiPC extends Controller
{
    public function index()
    {
        $wilayah = TblWilayah::where('isActive', '=', 1)->orderBy('kd_wilayah')->get();
        Session::put('breadcrumb', 'Mutasi Bank');
        return view('login.realisasipettycash.index',[
            "wilayah"       => $wilayah,
        ]);
    }

    public function populate(Request $request) {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 10;
        }
        if ($request->page) {
            $page = $request->page;
        } else {
            $page = 1;
        }
        $query = RealisasiPettycash::join('tblcabang', 'tblcabang.idCabang', '=', 'realisasi_pettycash.idCabang')
            ->orderBy('realisasi_pettycash.tglRealisasi', 'ASC')
            ->orderBy('realisasi_pettycash.idRealisasiPettyCash', 'ASC')
            ->selectRaw('realisasi_pettycash.idRealisasiPettyCash, realisasi_pettycash.idCabang, realisasi_pettycash.kodeVoucher, realisasi_pettycash.statusApproval, realisasi_pettycash.tglRealisasi, realisasi_pettycash.totalRealisasi, realisasi_pettycash.isJurnal, tblcabang.kodeCabang, tblcabang.namaCabang')
            ->where('realisasi_pettycash.statusApproval', '=', 1);
        if ($request->idWilayah) {
            $data = $query->where('tblcabang.idWilayah', '=', $request->idWilayah);
        }
        if ($request->idCabang) {
            $data = $query->where('tblcabang.idCabang', '=', $request->idCabang);
        }
        if ($request->tanggalAwal) {
            $query = $query->where('realisasi_pettycash.tglRealisasi', '>=', $request->tanggalAwal);
        }
        if ($request->tanggalAkhir) {
            $query = $query->where('realisasi_pettycash.tglRealisasi', '<=', $request->tanggalAkhir);
        }
        if ($request->statusJurnal) {
            $query = $query->where('realisasi_pettycash.isJurnal', '=', $request->statusJurnal);
        }
        $data = $query->paginate($limit);
        return view('login.realisasipettycash.populate',[
            "idWilayah"         => $request->idWilayah,
            "idCabang"          => $request->idCabang,
            "tanggalAwal"       => $request->tanggalAwal,
            "tanggalAkhir"      => $request->tanggalAkhir,
            "statusJurnal"      => $request->statusJurnal,
            "data"              => $data,
            "limit"             => $limit,
            "page"              => $page,
        ]);
    }

    public function modal(Request $request) {
        switch ($request->type) {
            case 'jurnal':
                DB::beginTransaction();
                $dataUpdate = [
                    'isJurnal'      => 1,
                    'dateJurnal'    => date('Y-m-d H:i:s'),
                ];
                RealisasiPettycash::where('idRealisasiPettyCash', '=', $request->id)
                    ->update($dataUpdate);
                DB::commit();
                return view('login.realisasipettycash.createjurnal',[]);
				break;
            case 'detail':
                $data = AkuntingSummary::join('acc_jenisjurnal', 'acc_jenisjurnal.idJenisJurnal', '=', 'akunting_summary.idJenisJurnal')
                    ->join('akunting_detail', 'akunting_detail.idSummary', '=', 'akunting_summary.idSummary')
                    ->selectRaw('akunting_summary.idSummary, akunting_summary.batch, akunting_summary.kodeTransaksi, akunting_summary.referenceTrans, acc_jenisjurnal.namaJenisJurnal, akunting_detail.tanggal, akunting_detail.coa, akunting_detail.coaCabang, akunting_detail.keterangan, akunting_detail.dk, akunting_detail.amount')
                    ->where('akunting_summary.idRealisasiPettyCash', '=', $request->id)
                    ->orderBy('akunting_summary.idSummary', 'ASC')
                    ->orderBy('akunting_detail.urut', 'ASC')
                    ->get();
                    // dd($data->toSql());
                return view('login.realisasipettycash.jurnal',[
                    "idRealisasiPettyCash"  => $request->id,
                    "data"                  => $data,
                ]);
                break;
		}
    }

    public function ajax(Request $request) {
		switch ($request->type) {
            case 'cabang':
                $idWilayah = $request->idWilayah;
                $cabangs = TblCabang::where('isActive', '=', 1)
                    ->where('idWilayah', '=', $idWilayah)
                    ->orderBy('kodeCabang')
                    ->get();
				$result = "<option value=''>== Nama Ho/Cabang/Unit ==</option>";
				if ($cabangs) {
					foreach ($cabangs as $cabang) {
                        $result .= "<option value='".$cabang->idCabang."'>".$cabang->kodeCabang." - ".$cabang->namaCabang."</option>";
					}
				}
				echo $result;
				break;
		}
	}
}
