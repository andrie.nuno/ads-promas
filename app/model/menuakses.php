<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class menuakses extends Model
{
    protected $table = "ads_menu_akses";
    protected $primaryKey = 'idLevel';
    protected $fillable = [
        'idLevel', 'menuId'
    ];
    protected $hidden = [
        'create_at', 'update_at'
    ];
}
