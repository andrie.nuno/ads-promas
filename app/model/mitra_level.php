<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class mitra_level extends Model
{
    protected $table = "ads_mitra_level";
    protected $primaryKey = 'idLevel';
    protected $fillable = [
        'namaLevel', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=mitra_level::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=mitra_level::where('idLevel',$id)
        ->first();
        return $data;
    }
}