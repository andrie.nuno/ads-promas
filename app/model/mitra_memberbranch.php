<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class mitra_memberbranch extends Model
{
    protected $connection = 'mysql';
    protected $table = "ads_member_branch";
    protected $primaryKey = 'idBranch';
    protected $keyType = 'string';
    protected $fillable = [
        'idCompany', 'kodeBranch', 'namaBranch', 'alamatBranch', 'idKota', 'idProvinsi', 'kodeposBranch', 'telepon', 'kepalaCabang', 'isActive', 'isJF', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=mitra_memberbranch::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=mitra_memberbranch::where('idBranch',$id)
        ->first();
        return $data;
    }

    public function updateData($id,$update){
        $data = mitra_memberbranch::where('idBranch',$id)->first();

        $data->idCompany= $update['idCompany'];
        $data->kodeBranch= $update['kodeBranch'];
        $data->namaBranch= $update['namaBranch'];
        $data->alamatBranch= $update['alamatBranch'];
        $data->idKota= $update['idKota'];
        $data->idProvinsi= $update['idProvinsi'];
        $data->kodeposBranch= $update['kodeposBranch'];
        $data->telepon= $update['telepon'];
        $data->kepalaCabang= $update['kepalaCabang'];
        $data->isActive=$update['isActive'];
        $data->isJF= $update['isJF'];
        $data->addUser=$update['addUser'];
        
        $data->save();
    }

    public function insertData($query){
        $data= mitra_memberbranch::newInstance($query);
        $data->save();
        return $data;
    }
}