<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
class updateinformasimodel extends Model
{
    protected $table = "update_informasi";
    protected $primaryKey = 'idUpdate';
    protected $fillable = [
        'idUpdate', 'division', 'message', 'linkUrl', 'tglMulai', 'tglSampai', 'isActive', 'idUser'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getAllData(){
    //     $data=updateinformasimodel::
    //     // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
    //     // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
    //     get();
    //     return $data;
    // }

    // public function getData($id){
    //     $data=updateinformasimodel::where('idUpdate',$id)
    //     ->first();
    //     return $data;
    // }
}