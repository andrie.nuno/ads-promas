<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblbank extends Model
{
    protected $table = "tblbank";
    protected $primaryKey = 'idBank';
    protected $fillable = [
        'idBank', 
        'namaBank',
        'isActive',
    ];
}
