<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AkuntingDetail extends Model
{
    protected $table = "akunting_detail";
	protected $primaryKey = 'idDetail';
    protected $fillable = [
        'idDetail', 
        'idSummary', 
        'idCabang', 
        'urut', 
        'idCoa', 
        'tanggal', 
        'tahun', 
        'bulan', 
	    'batch', 
        'coa', 
        'coaCabang', 
        'keterangan', 
        'referenceTrans', 
        'dk', 
        'amount', 
	    'lineId', 
        'idBatch', 
        'statusOracle'
    ];
}
