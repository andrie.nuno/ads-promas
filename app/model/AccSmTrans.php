<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AccSmTrans extends Model
{
    protected $table = "acc_sm_trans";
	protected $primaryKey = 'idSmTrans';
    protected $fillable = [
        'idSmTrans', 
        'idJenisJurnal', 
        'idCabang', 
        'idBankMasuk', 
        'idBankKeluar', 
        'idRPKP', 
        'idRPKC', 
        'tanggal', 
        'tahun', 
        'bulan', 
        'batch', 
        'kodeTransaksi', 
        'amount', 
	    'pokok', 
        'pencairan', 
        'bunga', 
        'admin', 
        'accrual', 
        'pokoktunggu', 
        'titipan', 
        'bungareal', 
        'diskonbunga', 
        'denda', 
        'diskondenda', 
        'adminjual', 
        'diskonadminjual', 
        'diskonpokok', 
        'kelebihanlelang', 
        'pembulatan', 
        'keterangan', 
        'referenceTrans', 
    ];
}