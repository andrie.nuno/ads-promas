<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class member_company extends Model
{
    protected $connection = 'mysql';
    protected $table = "ads_member_company";
    protected $primaryKey = 'idCompany';
    protected $fillable = [
        //'kodeMitra', 'namaMitra', 'alamatMitra', 'idKota', 'idProvinsi', 'kodeposMitra', 'jenisMitra', 'isActive'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=member_company::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=member_company::where('idCompany',$id)
        ->first();
        return $data;
    }
}