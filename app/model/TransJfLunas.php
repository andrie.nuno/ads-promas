<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransJfLunas extends Model
{
    protected $table = "trans_jf_lunas";
	protected $primaryKey = 'idTransJf';
    protected $fillable = [
        'idTransJf', 
        'noVoucher', 
        'jenis', 
        'idPartner', 
        'idGadai', 
        'tanggalPelunasan', 
        'tanggalBayarJf', 
        'noBatch', 
        'nominalJf', 
        'bungaNasabah', 
        'bungaJfBank', 
        'bungaJfMas', 
        'bungaSimpanMurni', 
        'bungaSelishSpread', 
        'dendaJf', 
        'jumlahHariJf', 
        'isPembayaran', 
        'datePembayaran', 
        'isJurnal', 
        'dateJurnal', 
    ];
}
