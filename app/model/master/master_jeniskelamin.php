<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_jeniskelamin extends Model
{
    protected $table = "master_jeniskelamin";
    protected $primaryKey = 'idJenisKelamin';
    protected $fillable = [
        'namaJenisKelamin', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_jeniskelamin::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_jeniskelamin::where('idJenisKelamin',$id)
        ->first();
        return $data;
    }
}