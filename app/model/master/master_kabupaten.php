<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_kabupaten extends Model
{
    protected $table = "master_kabupaten";
    protected $primaryKey = 'idKabupaten';
    protected $fillable = [
        'idProvinsi', 'namaKabupaten', 'lokasi', 'kodeDanainLama', 'kodeBI', 'kodePusdafil', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_kabupaten::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_kabupaten::where('idKabupaten',$id)
        ->first();
        return $data;
    }
}