<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_statusperkawinan extends Model
{
    protected $table = "master_statusperkawinan";
    protected $primaryKey = 'idStatusPerkawinan';
    protected $fillable = [
        'namaStatusPerkawinan', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_statusperkawinan::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_statusperkawinan::where('idStatusPerkawinan',$id)
        ->first();
        return $data;
    }
}