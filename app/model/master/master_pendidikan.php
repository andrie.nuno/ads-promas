<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_pendidikan extends Model
{
    protected $table = "master_pendidikan";
    protected $primaryKey = 'idPendidikan';
    protected $fillable = [
        'namaPendidikan', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_pendidikan::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_pendidikan::where('idPendidikan',$id)
        ->first();
        return $data;
    }
}