<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_bank extends Model
{
    protected $connection = 'mysql3';
    protected $table = "tblbank";
    protected $primaryKey = 'idBank';
    protected $fillable = [
        'keterangan', 'isActive', 'tgl_proses', 'userId', 'nourut'
    ];

    protected $hidden = [
        //'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_bank::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_bank::where('idBank',$id)
        ->first();
        return $data;
    }
}