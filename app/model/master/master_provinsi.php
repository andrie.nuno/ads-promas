<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_provinsi extends Model
{
    protected $table = "master_provinsi";
    protected $primaryKey = 'idProvinsi';
    protected $fillable = [
        'namaProvinsi', 'kodeDanainLama', 'kodeBI', 'kodePusdafil', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_provinsi::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_provinsi::where('idProvinsi',$id)
        ->first();
        return $data;
    }
}