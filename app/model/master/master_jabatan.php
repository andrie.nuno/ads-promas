<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_jabatan extends Model
{
    protected $table = "master_jabatan";
    protected $primaryKey = 'idJabatan';
    protected $fillable = [
        'namaJabatan', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_jabatan::get();
        return $data;
    }
    
    public function getData($id){
        $data=master_jabatan::where('idJabatan',$id)
        ->first();
        return $data;
    }
}