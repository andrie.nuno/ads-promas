<?php

namespace App\model\master;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class master_ovd extends Model
{
    protected $table = "master_ovd";
    protected $primaryKey = 'idOvd';
    protected $fillable = [
        'hariOvd', 'keteranganOvd'
    ];

    protected $hidden = [
        //'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=master_ovd::orderBy('hariOvd')->get();
        return $data;
    }
    
    public function getData($id){
        $data=master_ovd::where('idOvd',$id)
        ->first();
        return $data;
    }
}