<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RealisasiPettycash extends Model
{
    protected $table = "realisasi_pettycash";
	protected $primaryKey = 'idRealisasiPettyCash';
    protected $fillable = [
        'idRealisasiPettyCash',
        'idCabang',
        'tahun',
	    'kodeTrans',
	    'urutVoucher',
	    'kodeVoucher',
	    'keterangan',
        'fileBukti',
        'tglRealisasi',
        'totalRealisasi',
        'idUser',
        'statusApproval',
        'tglApproval',
        'keteranganApproval',
        'idApproval',
        'idBank',
        'saldoAwal',
        'saldoAkhir',
        'isActive',
        'idCorrection',
        'isJurnal',
        'dateJurnal',
    ];
}
