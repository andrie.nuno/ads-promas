<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransPelunasanGadai extends Model
{
    protected $table = "trans_pelunasangadai";
	protected $primaryKey = 'idPelunasanGadai';
    protected $fillable = [
        'idPelunasanGadai',
        'idGadai',
        'idCabang',
        'idBank',
        'tglPelunasan',
        'kodePelunasan',
        'jumlahHari',
        'nilaiPokok',
        'nilaiBunga',
        'nilaiDenda',
        'nilaiTitipan',
        'totalKewajiban',
        'diskonPokok',
        'diskonBunga',
        'diskonDenda',
        'finalDiskonPokok',
        'finalDiskonBunga',
        'finalDiskonDenda',
        'pembulatan',
        'totalPelunasan',
        'isStatus',
        'isJf',
        'bungaJfFull',
        'dendaJfFull',
        'pelunasanBankJf',
        'bungaJfReal',
        'dendaJfReal',
        'pelunasanBankJfReal',
        'statusLunas',
        'statusExit',
        'isJurnal',
        'dateJurnal',
    ];
}
