<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblcashflow extends Model
{
    protected $table = "tblcashflow";
    protected $primaryKey = 'idCashflow';
    // protected $fillable = [
    //     'namaLevel', 'isActive', 'addUser'
    // ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblcashflow::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblcashflow::where('idCashflow',$id)
        ->first();
        return $data;
    }
}