<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AkuntingSummary extends Model
{
    protected $table = "akunting_summary";
	protected $primaryKey = 'idSummary';
    protected $fillable = [
        'idSummary', 
        'idJenisJurnal', 
        'idTransaksiInternal', 
        'idRealisasiPettyCash', 
        'idSuspend', 
        'idRekonBank', 
        'idGadai', 
        'idProduk', 
        'idCabang', 
        'idBankMasuk', 
        'idBankKeluar', 
        'idRPKP', 
        'idRPKC', 
        'idJf', 
	    'idJfBank', 
        'pemilikCoa', 
        'tanggal', 
        'tahun', 
        'bulan', 
        'batch', 
        'kodeTransaksi', 
        'amount', 
	    'pokok', 
        'pokokgadai', 
        'pokokbank', 
        'pencairan', 
        'bunga', 
        'bungajfmas', 
        'bungamurni', 
        'bungaselisih', 
        'admin', 
        'accrual', 
        'accrualbungamurni', 
        'accrualbungaselisih', 
        'pokoktunggu', 
        'pokokgadaitunggu', 
        'pokokbanktunggu', 
        'titipan', 
        'bungareal', 
        'diskonbunga', 
        'denda', 
        'diskondenda', 
        'adminjual', 
        'diskonadminjual', 
        'diskonpokok', 
        'kelebihanlelang', 
        'pembulatan', 
        'selisih', 
        'hitungcustom', 
        'keterangan', 
        'keteranganTransaksi', 
        'referenceTrans', 
        'isPost', 
        'tanggalPost'
    ];
}
