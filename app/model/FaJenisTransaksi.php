<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FaJenisTransaksi extends Model
{
    protected $table = "fa_jenistransaksi";
	protected $primaryKey = 'idJenisTransaksi';
    protected $fillable = [
        'idJenisTransaksi', 
        'kodeJenisTransaksi', 
        'namaJenisTransaksi', 
        'namaJenisTransaksiOracle', 
        'idBankKeluar', 
        'idBankMasuk', 
        'isActive', 
        'idUser', 
        'idJenisJurnal', 
        'typeTransaksi', 
    ];
}
