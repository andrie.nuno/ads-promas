<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblcoatemplate extends Model
{
    protected $table = "tblcoatemplate";
    protected $primaryKey = 'idCoa';
    protected $fillable = [
        'idCoa',
        'idUsedFor',
        'coa',
        'headAccount',
        'description',
        'additionalDesc',
        'transactionType',
        'currency',
        'pettyCash',
        'rekonBank',
        'paymentRequest',
        'coaOracle',
        'accountType',
        'qualifiers',
        'beginBalance',
        'isActive',
        'isActiveSyariah',
        'isDefault',
        'idBank',
        'idUser',
        'isBuffer',
        'isKasBesar',
        'idCashflow',
        'kasMarketing',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblcoatemplate::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblcoatemplate::where('idCoa',$id)
        ->first();
        return $data;
    }
}