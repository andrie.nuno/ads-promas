<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class trans_gadai_syariah extends Model
{
    protected $table = "trans_gadai_syariah";
    protected $primaryKey = 'idGadai';
    // protected $fillable = [
    //     'idGadai', 
    //     'idCabang',
    //     'idCustomer', 
    //     'idFAPG', 
    //     'idTaksiran', 
    //     'idProduk', 
    //     'transKe', 
    //     'tglPengajuan', 
    //     'noSbg', 
    //     'noSbgCopy1', 
    //     'noSbgCopy2', 
    //     'noSbgCopy3', 
    //     'pengajuanPinjaman', 
    //     'pembulatan', 
    //     'nilaiPinjaman', 
    //     'biayaAdminAwal', 
    //     'diskonAdmin', 
    //     'biayaAdmin', 
    //     'nilaiApCustomer', 
    //     'nilaiCicilan', 
    //     'lamaPinjaman', 
    //     'tglJatuhTempo', 
    //     'minRate', 
    //     'rateFlat', 
    //     'rateEffective', 
    //     'biayaPenyimpanan', 
    //     'akumulasiOsPokok', 
    //     'totalObligor', 
    //     'idAsalJaminan', 
    //     'idTujuanTransaksi', 
    //     'idJenisReferensi', 
    //     'referensiNpk', 
    //     'referensiNama', 
    //     'referensiCif', 
    //     'jenisPembayaran', 
    //     'idBankPencairan', 
    //     'statusAplikasi', 
    //     'idProgram', 
    //     'idSektorEkonomi', 
    //     'approvalLtv', 
    //     'approvalUangPinjaman', 
    //     'approvalRateFlat', 
    //     'approvalOneObligor', 
    //     'approvalFinal', 
    //     'keterangan', 
    //     'isStatus', 
    //     'isStatusGadai', 
    //     'idApprovalKapos', 
    //     'idApprovalKaunit', 
    //     'idApprovalKacab', 
    //     'idApprovalKaarea', 
    //     'idApprovalKawil', 
    //     'idApprovalDirektur', 
    //     'idApprovalDirut',
    //     'ketApproveKapos',
    //     'ketApproveKaunit',
    //     'ketApproveKacab',
    //     'ketApproveKaarea',
    //     'ketApproveKawil',
    //     'ketApproveDirektur',
    //     'ketApproveDirut',
    //     'isApprovalKapos',
    //     'isApprovalKaunit',
    //     'isApprovalKacab',
    //     'isApprovalKaarea',
    //     'isApprovalKawil',
    //     'isApprovalDirektur',
    //     'isApprovalDirut',
    //     'tglApprovalKapos',
    //     'tglApprovalKaunit',
    //     'tglApprovalKacab',
    //     'tglApprovalKaarea',
    //     'tglApprovalKawil',
    //     'tglApprovalDirektur',
    //     'tglApprovalDirut',
    //     'selfieApprovalKapos',
    //     'selfieApprovalKaunit',
    //     'idApprovalFinal',
    // ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}