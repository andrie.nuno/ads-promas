<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class mitra_user extends Model
{
    protected $connection = 'mysql';
    protected $table = "tbluser";
    protected $primaryKey = 'id';
    protected $fillable = [
        'email', 'password', 'namaUser', 'hp', 'idMitra', 'idCompany', 'idBranch', 'idLevel', 'isActive', 'addUser'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=mitra_user::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=mitra_user::where('id',$id)
        ->first();
        return $data;
    }

    public function updateData($id,$update){
        $data = mitra_user::where('id',$id)->first();
        if($update['password']!=null){
            $data->password= Hash::make($update['password']);
        }
        $data->email= $update['email'];
        $data->namaUser=$update['namaUser'];
        $data->hp=$update['hp'];
        $data->idMitra=$update['idMitra'];
        $data->idCompany=$update['idCompany'];
        $data->idBranch=$update['idBranch'];
        $data->idLevel=$update['idLevel'];
        $data->isActive=$update['isActive'];
        $data->addUser=$update['addUser'];
        $data->save();
    }

    public function insertData($query){
        $data= mitra_user::newInstance($query);
        $data->save();
        return $data;
    }
}