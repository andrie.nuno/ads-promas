<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransPayctrl extends Model
{
    protected $table = "trans_payctrl";
	protected $primaryKey = 'idGadai';
    protected $fillable = [
        'idGadai',
        'idCustomer',
        'tanggalPencairan',
        'pokokAwal',
        'bungaFull',
        'pokokSaldo',
        'bungaSaldo',
        'bungaHarian',
        'jumlahHari',
        'ovd',
        'pokokHutang',
        'bungaHutang',
        'dendaHutang',
        'adminPenjualanHutang',
        'titipan',
        'totalHutang',
        'rateDenda',
        'ratePenjualan',
        'tanggalPelunasan',
        'statusPinjaman',
        'created_at',
        'updated_at',
        'acrueBunga',
        'hutangBungaJfBank',
        'hutangBungaJfMas',
        'hutangBungaMurni',
        'hutangBungaSelisih',
        'acrueBungaJfBank',
        'acrueBungaJfMas',
        'acrueBungaMurni',
        'acrueBungaSelisih',
        'tglCalculate',
        'statusInventory',
        'tambahanPokok',
        'penguranganPokok',
        'prosesCalculate',
        'isJurnal',
        'dateJurnal',
        'isJurnalArt',
        'dateJurnalArt',
        'isJurnalJfGantung',
        'dateJurnalJfGantung',
    ];
}
