<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransTitipan extends Model
{
    protected $table = "trans_titipan";
	protected $primaryKey = 'idTitipan';
    protected $fillable = [
        'idTitipan',
        'idTransaksiInternal',
        'idGadai',
        'idCabang',
        'noSbg',
        'jumlahHari',
        'jumlahOvd',
        'nominalTitipan',
        'keterangan',
        'tanggal',
        'isActive',
        'jenisTitipan',
        'isJurnal',
        'dateJurnal',
    ];
}
