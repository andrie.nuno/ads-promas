<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransJf extends Model
{
    protected $table = "trans_jf";
	protected $primaryKey = 'idTransJf';
    protected $fillable = [
        'idTransJf',
        'tahun',
        'kodeTrans',
        'urutVoucher',
        'noVoucher',
        'idPartner',
        'idGadai',
        'tanggalPendanaan',
        'tanggalAr',
        'tanggalVerifikasi',
        'noBatch',
        'nominalJf',
        'bungaJf',
        'idUser',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}
