<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblwilayah extends Model
{
    protected $table = "tblwilayah";
    protected $primaryKey = 'idWilayah';
    // protected $fillable = [
    //     'namaLevel', 'isActive', 'addUser'
    // ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblwilayah::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblwilayah::where('idWilayah',$id)
        ->first();
        return $data;
    }
}