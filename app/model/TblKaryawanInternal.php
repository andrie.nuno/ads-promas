<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class TblKaryawanInternal extends Model
{
    protected $table = 'tblkaryawaninternal';
    protected $primaryKey = 'idKaryawan';
    protected $fillable = [
        'idKaryawan',
        'idJabatanKaryawan',
        'idCabang',
        'npk',
        'namaKaryawan',
        'alamat',
        'idKelurahan',
        'tempatLahir',
        'tanggalLahir',
        'jenisKelamin',
        'agama',
        'telp',
        'email',
        'tglMasuk',
        'tglKeluar',
        'alasanKeluar',
        'isActive',
        'idUser',
        'idDivisi',
        'idAgama',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}