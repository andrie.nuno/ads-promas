<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TblJenisCabang extends Model
{
    protected $table = "tbljeniscabang";
	protected $primaryKey = 'idJenisCabang';
    protected $fillable = [
        'idJenisCabang', 
        'namaJenisCabang', 
        'isActive',
        'idUser',
    ];
}