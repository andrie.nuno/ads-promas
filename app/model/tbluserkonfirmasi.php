<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tbluserkonfirmasi extends Model
{
    protected $table = "tbluserkonfirmasi";
    protected $primaryKey = 'idKonfirmasi';
    protected $fillable = [
        'idKonfirmasi', 'idUser', 'idUpdate', 'isSyariah', 'timeKonfirmasi',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getAllData(){
    //     $data=tbluserkonfirmasi::
    //     get();
    //     return $data;
    // }
}