<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblcoausedfor extends Model
{
    protected $table = "tblcoausedfor";
    protected $primaryKey = 'idUsedfor';
    // protected $fillable = [
    //     'namaLevel', 'isActive', 'addUser'
    // ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblcoausedfor::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblcoausedfor::where('idUsedfor',$id)
        ->first();
        return $data;
    }
}