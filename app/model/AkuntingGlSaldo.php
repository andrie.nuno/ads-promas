<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class AkuntingGlSaldo extends Model
{
    protected $table = "akunting_gl_saldo";
	protected $primaryKey = 'idGlSaldo';
    protected $fillable = [
        'idGlSaldo', 
        'periode', 
        'coaOracle', 
        'kodeCabang', 
        'amount', 
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}