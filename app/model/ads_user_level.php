<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ads_user_level extends Model
{
    protected $table = "ads_user_level";
    protected $primaryKey = 'idUser';
    protected $fillable = [
        'idUser', 'idLevel'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=ads_user_level::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=ads_user_level::where('idUser',$id)
        ->first();
        return $data;
    }
}