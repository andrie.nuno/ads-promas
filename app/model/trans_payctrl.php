<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class trans_payctrl extends Model
{
    protected $table = "trans_payctrl";
    protected $primaryKey = 'idGadai';
    protected $fillable = [
        'idGadai', 
        'idCustomer', 
        'tanggalPencairan', 
        'pokokAwal', 
        'bungaFull', 
        'pokokSaldo', 
        'bungaSaldo', 
        'bungaHarian', 
        'angsuranKe', 
        'jumlahHari', 
        'bungaDanain', 
        'ovd', 
        'pokokHutang', 
        'bungaHutang', 
        'dendaHutang', 
        'adminPenjualanHutang', 
        'titipan', 
        'totalHutang', 
        'rateDenda', 
        'ratePenjualan', 
        'tanggalPelunasan', 
        'statusPinjaman', 
        'acrueBunga', 
        'hutangBungaJfBank', 
        'hutangBungaJfMas', 
        'hutangBungaMurni', 
        'hutangBungaSelisih', 
        'acrueBungaJfBank', 
        'acrueBungaJfMas', 
        'acrueBungaMurni', 
        'acrueBungaSelisih', 
        'tglCalculate', 
        'statusInventory', 
        'tambahanPokok', 
        'penguranganPokok', 
        'prosesCalculate', 
        'isJurnal', 
        'dateJurnal', 
        'isJurnalArt', 
        'dateJurnalArt', 
        'isJurnalJfGantung', 
        'dateJurnalJfGantung', 
        'tanggalJtCicilan', 
        'nilaiAcrueBunga', 
        'nilaiAcrueBungaJfBank', 
        'nilaiAcrueBungaJfMas', 
        'nilaiAcrueBungaMurni', 
        'nilaiAcrueBungaSelisih', 
        'osAcrueBunga', 
        'osAcrueBungaJfBank', 
        'osAcrueBungaJfMas', 
        'osAcrueBungaMurni', 
        'osAcrueBungaSelisih',
        'jumlahAcrue',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    // public function getAllData(){
    //     $data=payctrl::
    //     // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
    //     // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
    //     get();
    //     return $data;
    // }

    // public function getData($id){
    //     $data=payctrl::where('idAgreement',$id)
    //     ->first();
    //     return $data;
    // }

    // public function updateData($id,$update){
    //     $data = payctrl::where('idAgreement',$id)->first();
        
    //     $data->tglPencairan=$update['tglPencairan'];
    //     $data->tglJt=$update['tglJt'];
    //     $data->pokokHutang=$update['pokokHutang'];
    //     $data->saldoPokok=$update['saldoPokok'];
    //     $data->pokokPendana=$update['pokokPendana'];

    //     $data->bungaPendana=$update['bungaPendana'];
    //     $data->adminPendana=$update['adminPendana'];
    //     $data->feePendana=$update['feePendana'];
    //     $data->dendaPendana=$update['dendaPendana'];
    //     $data->bungaDanain=$update['bungaDanain'];

    //     $data->adminDanain=$update['adminDanain'];
    //     $data->feeDanain=$update['feeDanain'];
    //     $data->dendaDanain=$update['dendaDanain'];
    //     $data->jasaMitra=$update['jasaMitra'];
    //     $data->status=$update['status'];

    //     $data->ovd=$update['ovd'];
    //     $data->update_date=$update['update_date'];
    //     $data->save();
    // }

    // public function insertData($query){
    //     $data= payctrl::newInstance($query);
    //     $data->save();
    //     return $data;
    // }

    // public function getborrowerpayctrl($id){
    //     $data=payctrl::where('payctrl.idAgreement',$id)
    //     ->leftJoin('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'payctrl.idAgreement')
    //     ->leftJoin('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_peminjam.idBorrower')
    //     ->first();
    //     return $data;
    // }

    // public function getAllDatasp(){
    //     $data=payctrl::
    //     leftjoin('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'payctrl.idAgreement')
    //     ->leftjoin('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_peminjam.idBorrower')
    //     ->get();
    //     return $data;
    // }

    // public function getDatasp($id){
    //     $data=payctrl::where('payctrl.idAgreement', $id)
    //     ->leftjoin('agreement_peminjam', 'agreement_peminjam.idAgreement', '=', 'payctrl.idAgreement')
    //     ->leftjoin('register_peminjam', 'register_peminjam.idBorrower', '=', 'agreement_peminjam.idBorrower')
    //     ->leftjoin('agreement_data', 'agreement_data.idAgreement', '=', 'payctrl.idAgreement')
    //     ->leftjoin('register_jaminan_detail', 'register_jaminan_detail.idJaminan', '=', 'agreement_data.idJaminan')
    //     ->first();
    //     return $data;
    // }

}