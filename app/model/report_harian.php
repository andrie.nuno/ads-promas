<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class report_harian extends Model
{
    protected $table = "report_harian";
    protected $primaryKey = 'idReportHarian';
    protected $fillable = [
        'idReportHarian', 
        'tanggal', 
        'jenisReport', 
        'urlFile',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];
}