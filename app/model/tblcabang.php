<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblcabang extends Model
{
    protected $table = "tblcabang";
    protected $primaryKey = 'idCabang';
    protected $fillable = [
        'idCabang', 'kodeCabang', 'namaCabang', 'plaffonPettyCash', 'plaffonKasMarketing', 'idGradeCabang', 'idGradeCabangSyariah', 'plaffonPettyCashSyariah', 'plaffonKasMarketingSyariah',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblcabang::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblcabang::where('idCabang',$id)
        ->first();
        return $data;
    }
}