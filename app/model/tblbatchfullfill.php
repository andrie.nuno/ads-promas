<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblbatchfullfill extends Model
{
    protected $table = "tblbatchfullfill";
    protected $primaryKey = 'idBatch';
    protected $fillable = [
        'idBatch',
        'namaBatch',
        'isActive',
        'idUser',
        'jamMulai',
        'jamSelesai',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblbatchfullfill::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblbatchfullfill::where('idBatch',$id)
        ->first();
        return $data;
    }
}