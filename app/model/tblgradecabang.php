<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class tblgradecabang extends Model
{
    protected $table = "tblgradecabang";
    protected $primaryKey = 'idGradeCabang';
    protected $fillable = [
        'idGradeCabang', 'maxBuffer', 'maxPettyCash', 'maxKasMarketing',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    public function getAllData(){
        $data=tblgradecabang::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=tblgradecabang::where('idGradeCabang',$id)
        ->first();
        return $data;
    }
}