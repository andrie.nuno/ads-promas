<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Auth;

class tblcoa extends Model
{
    protected $table = "tblcoa";
    protected $primaryKey = ['idCoa', 'idCabang'];
    public $incrementing = false;
    protected $fillable = [
        'idCoa',
        'idCabang',
        'idBank',
        'noRekening',
        'idUser',
        'isActive',
        'coa',
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected function setKeysForSaveQuery(Builder $query)
    {
        return $query->where('idCoa', $this->getAttribute('idCoa'))
            ->where('idCabang', $this->getAttribute('idCabang'));
    }

    public function getAllData(){
        $data=tblcoa::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    // public function getData($id){
    //     $data=tblcoa::where('idCoa',$id)
    //     ->first();
    //     return $data;
    // }
}