<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TblSettingJf extends Model
{
    protected $table = "tblsettingjf";
	protected $primaryKey = 'idJf';
    protected $fillable = [
        'idJf', 
        'kodeJf', 
        'namaJf', 
        'namaPendana', 
        'tglKerjaSama', 
        'rate', 
        'porsi', 
        'idPartner', 
        'idCoa', 
        'idBank', 
        'hariBuyback', 
        'perhitunganBunga', 
        'porsiDenda', 
        'porsiPenjualan', 
        'isInternal', 
        'isActive', 
        'idUser', 
    ];
}
