<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransPelunasanJual extends Model
{
    protected $table = "trans_pelunasanjual";
	protected $primaryKey = 'idPelunasanJual';
    protected $fillable = [
        'idPelunasanJual', 
        'idPenjualan', 
        'idGadai', 
        'idCabang', 
        'idBank', 
        'tglPelunasan', 
        'idJenisTransaksi', 
        'tahun', 
        'kodeTrans', 
        'urutVoucher', 
        'kodePelunasan', 
        'jumlahHari', 
        'nilaiPokok', 
        'nilaiBunga', 
        'nilaiAdminJual', 
        'titipan', 
        'totalKewajiban', 
        'diskonPokok', 
        'diskonBunga', 
        'diskonAdminJual', 
        'pembulatan', 
        'totalPelunasan', 
        'uangDiterima', 
        'uangKembalian', 
        'jenisPelunasan', 
        'isStatus', 
        'fileKwitansi', 
        'idUser', 
        'acrueBunga', 
        'isJf', 
        'postPelunasanJf', 
        'hariPelunasanJf', 
        'tanggalPelunasanJf', 
        'bungaJfFull', 
        'dendaJfFull', 
        'pelunasanBankJf', 
        'bungaJfReal', 
        'dendaJfReal', 
        'pelunasanBankJfReal', 
        'isJurnal', 
        'dateJurnal', 
    ];
}
