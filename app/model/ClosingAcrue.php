<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ClosingAcrue extends Model
{
    protected $table = "closing_acrue";
	protected $primaryKey = 'idGadai';
    protected $fillable = [
        'idGadai', 
        'tanggalClosing', 
        'tanggalPencairan', 
        'pokokAwal', 
	    'jumlahHari', 
        'ovd', 
	    'bungaHutang', 
	    'acrueBunga', 
        'selisihAcrue', 
        'hutangBungaJfBank', 
        'acrueBungaJfBank', 
        'selisihAcrueJfBank', 
        'hutangBungaJfMas', 
        'acrueBungaJfMas', 
        'selisihAcrueJfMas', 
        'hutangBungaMurni', 
        'acrueBungaMurni', 
        'selisihAcrueBungaMurni', 
        'hutangBungaSelisih', 
        'acrueBungaSelisih', 
        'selisihAcrueBungaSelisih', 
        'isJurnal', 
        'dateJurnal', 
    ];
}