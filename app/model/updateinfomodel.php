<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class updateinfomodel extends Model
{
    protected $table = "update_informasi";
    protected $primaryKey = 'idUpdate';
    protected $fillable = [
        'idUpdate', 'division', 'judul', 'message', 'linkUrl', 'tglMulai', 'tglSampai', 'isActive', 'idUser'
    ];

    protected $hidden = [
        'created_at', 'updated_at'
    ];

}