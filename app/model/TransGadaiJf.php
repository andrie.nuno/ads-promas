<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TransGadaiJf extends Model
{
    protected $table = "trans_gadaijf";
	protected $primaryKey = 'idGadai';
    protected $fillable = [
        'idGadai', 
        'idJf', 
        'tanggal', 
        'rateJf', 
        'porsiJf', 
        'porsiGadai', 
        'pinjamanFull', 
        'pinjamanJf', 
        'pinjamanGadai', 
        'bungaJfHarian', 
        'timeApprove', 
        'isApprove', 
        'keteranganApprove', 
        'noPerjanjian', 
        'isVerifikasi', 
        'isJurnal', 
        'dateJurnal', 
    ];
}
