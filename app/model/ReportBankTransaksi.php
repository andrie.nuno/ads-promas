<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class ReportBankTransaksi extends Model
{
    protected $table = "report_banktransaksi";
	protected $primaryKey = 'idBankTransaksi';
    protected $fillable = [
        'idBankTransaksi', 
        'idBank', 
        'idCabang', 
        'idJenisTransaksi', 
        'tanggalSistem', 
        'tanggalProses', 
        'kodeTrans', 
        'keterangan', 
        'saldoAwal', 
        'debet', 
        'kredit', 
        'saldoAkhir', 
    ];
}