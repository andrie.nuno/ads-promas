<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class FaSuspend extends Model
{
    protected $table = "fa_suspend";
	protected $primaryKey = 'idSuspend';
    protected $fillable = [
        'idSuspend', 
        'idCabang', 
        'idBank', 
        'idCoa', 
        'tanggal', 
	    'noVoucher', 
        'tglCreated', 
	    'keterangan', 
	    'total', 
	    'tglRK', 
        'isActive', 
        'isReversal', 
        'idReversal', 
        'dateReversal', 
        'keteranganReversal', 
        'idApproval', 
        'statusApproval', 
        'isJurnal', 
        'dateJurnal', 
        'isJurnalReversal', 
        'dateJurnalReversal', 
    ];
}