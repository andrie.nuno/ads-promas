<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class mitra_membercompany extends Model
{
    protected $connection = 'mysql';
    protected $table = "ads_member_company";
    protected $primaryKey = 'idCompany';
    protected $keyType = 'string';
    protected $fillable = [
        'idMitra', 'kodeCompany', 'namaCompany', 'alamatCompany', 'idKota', 'idProvinsi', 'kodeposCompany', 'isActive', 'isJF', 'addUser'
    ];

    protected $hidden = [
        'create_at', 'update_at'
    ];

    public function getAllData(){
        $data=mitra_membercompany::
        // leftjoin('db_mid_peminjam.master_bank', 'db_mid_peminjam.master_bank.idBank', '=', 'bni_bankcode.idBank')
        // ->select('bni_bankcode.*', 'db_mid_peminjam.master_bank.namaBank as namaBank2')
        get();
        return $data;
    }

    public function getData($id){
        $data=mitra_membercompany::where('idCompany',$id)
        ->first();
        return $data;
    }

    public function updateData($id,$update){
        $data = mitra_membercompany::where('idCompany',$id)->first();

        $data->idMitra= $update['idMitra'];
        $data->kodeCompany= $update['kodeCompany'];
        $data->namaCompany= $update['namaCompany'];
        $data->alamatCompany= $update['alamatCompany'];
        $data->idKota= $update['idKota'];
        $data->idProvinsi= $update['idProvinsi'];
        $data->kodeposCompany= $update['kodeposCompany'];
        $data->isActive=$update['isActive'];
        $data->isJF= $update['isJF'];
        $data->addUser=$update['addUser'];
        
        $data->save();
    }

    public function insertData($query){
        $data= mitra_membercompany::newInstance($query);
        $data->save();
        return $data;
    }
}