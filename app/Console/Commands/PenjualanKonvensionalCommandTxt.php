<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\model\trans_gadai;
use DB;

class PenjualanKonvensionalCommandTxt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportpenjualan:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export PenjualanKonvensional data to txt, upload to S3, and log file URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $tanggal = date("Y-m-d", strtotime("yesterday"));
            
            $data = $this->getData($tanggal);

            $exportedData = $this->formatData($data);

            $filename = "export-penjualankonvensional/exported_data_penjualan_$tanggal.txt";
            Storage::put($filename, $exportedData);
            $contentType = 'application/json';

            $s3Url = Storage::disk('s3')->put($filename, $exportedData, 'public', ['Content-Type' => $contentType]);
            $url = "https://gadaimas.nos.wjv-1.neo.id/$filename";

            $this->logFileUrl($s3Url, $url, $tanggal);
        } catch (\Exception $e) {
            $this->error("An error occurred: {$e->getMessage()}");
        }
    }

    protected function getData($tanggal)
    {
        $tanggalAwal = substr($tanggal,0,7)."-01";
        $query = trans_gadai::
        join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        ->join("trans_penjualangadai", function($join){
            $join->on('trans_penjualangadai.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_penjualangadai.isStatus"), "=", 1);
        })
        ->join('trans_pelunasanjual','trans_pelunasanjual.idPenjualan','=','trans_penjualangadai.idPenjualan')
        ->leftJoin("trans_gadaijf", function($join){
            $join->on('trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
        })
        ->leftjoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
        ->leftjoin('trans_fundingdetail', 'trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai')
        ->selectRaw("tblproduk.kodeProduk AS produk, trans_gadai.noSbg, tblcustomer.cif, tblcabang.kodeCabang AS kodeOutlet, 
        tblcabang.namaCabang AS namaOutlet, tblcustomer.namaCustomer, trans_payctrl.tanggalPencairan AS tanggalCair, 
        trans_gadai.nilaiPinjaman AS pokokPinjaman, trans_pelunasanjual.nilaiBunga AS jasaSimpan, trans_pelunasanjual.nilaiAdminJual AS adminPenjualan, 
        trans_payctrl.tanggalPelunasan AS tanggalPenjualan, (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) AS angkaPelunasan, 
        trans_pelunasanjual.totalPelunasan AS angkaPenjualan, 
        trans_pelunasanjual.totalPelunasan - (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) AS selisihPenjualan, 
        IF(trans_pelunasanjual.totalPelunasan - (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) < 0, 
            IF((trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan > trans_pelunasanjual.nilaiAdminJual, 
                trans_pelunasanjual.nilaiAdminJual, (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan), 
            0
        ) AS diskonAdminPenjualan, 
        IF(trans_pelunasanjual.totalPelunasan - (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) < 0, 
            IF((trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan > trans_pelunasanjual.nilaiAdminJual, 
                IF((trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan > (trans_pelunasanjual.nilaiAdminJual + trans_pelunasanjual.nilaiBunga), 
                    trans_pelunasanjual.nilaiBunga, 
                    IF((trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan - trans_pelunasanjual.nilaiAdminJual > 0, 
                        (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) - trans_pelunasanjual.totalPelunasan - trans_pelunasanjual.nilaiAdminJual, 
                        0)
                ), 
                0
            ), 
            0
        ) AS diskonJasaSimpan, 
        trans_pelunasanjual.diskonPokok AS kerugian, trans_penjualangadai.stleAntamPenjualan AS stle, 
        IF(trans_pelunasanjual.totalPelunasan - (trans_pelunasanjual.totalKewajiban + trans_pelunasanjual.titipan) > 0, 'Aktif', 'Tidak Ada') AS statusPengembalian, 
        trans_penjualangadai.namaPembeli, 
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(trans_penjualangadai.teleponPembeli, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS teleponPembeli, 
        trans_penjualangadai.ktpPembeli, 
        IF(tblsettingjf.kodeJf IS NULL, '', 'Fintech') AS statusFintech, 
        IF(trans_fundingdetail.idFundingDetail IS NULL, '', 'Funding') AS statusFunding, 
        IF(trans_gadai.tanggalArt <= trans_payctrl.tanggalPelunasan, 'BJDP', '') AS statusFlag")
        ->where('trans_payctrl.tanggalPelunasan', '>=', $tanggalAwal)
        ->where('trans_payctrl.tanggalPelunasan', '<=', $tanggal)
        ->get()
        ->toArray();
        return $query;
    }

    protected function formatData($data)
    {
        $exportedData = '';

        // Check if there is any data
        if (empty($data)) {
            $this->info('No data found for export.');
            return $exportedData;
        }

        // Get the headers
        $headers = array_keys($data[0]);

        // Set the delimiter
        $delimiter = '|';

        // Add headers to the exported data
        $exportedData .= implode($delimiter, $headers) . PHP_EOL;

        foreach ($data as $row) {
            $rowArray = [];
            foreach ($row as $column) {
                // Convert objects and arrays to JSON
                $value = is_array($column) || is_object($column) ? json_encode($column) : $column;
                $rowArray[] = str_replace(["\r", "\n", '|', "\t"], ' ', $value);
            }
            $exportedData .= implode($delimiter, $rowArray) . PHP_EOL;
        }

        return $exportedData;
    }

    protected function logFileUrl($s3Url, $url, $tanggal)
    {
        $insertedId = DB::table('report_harian')->insert([
            'urlFile' => $url,
            'tanggal' => $tanggal,
            'jenisReport' => 'Penjualan'
        ]);

        if ($insertedId) {
            $this->info("File uploaded to S3. URL: {$url}. Log ID: {$insertedId}");
        } else {
            $this->error("Failed to insert log record for URL: {$url}");
        }
    }

}
