<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\PelunasanKonvensionalExcel;
use Maatwebsite\Excel\Facades\Excel;

class PelunasanKonvensionalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pelunasankonvensionalcron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Pelunasan Konvensional Export daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tanggal = date('Y-m-d');
        $fileName = "PelunasanKonvensional";
        $extension = "xlsx";

        $fileNameWithExtension = "excel/$fileName-$tanggal.$extension";
        return Excel::store(new PelunasanKonvensionalExcel($tanggal, '', '', ''), $fileNameWithExtension, 'local');
    }
}
