<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;

class AzKonvensionalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'azkonvensional:cron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'AZ Konvensional Export daily, Process and export large data in chunks';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        AzKonvensionalAppendJob::dispatch();
        $this->info('Large data export job dispatched.');
    }
}
