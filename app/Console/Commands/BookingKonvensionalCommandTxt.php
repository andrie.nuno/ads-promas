<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\model\trans_gadai;
use DB;

class BookingKonvensionalCommandTxt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportbooking:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export BookingKonvensional data to txt, upload to S3, and log file URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $tanggal = date("Y-m-d", strtotime("yesterday"));
            
            $data = $this->getData($tanggal);

            $exportedData = $this->formatData($data);

            $filename = "export-bookingkonvensional/exported_data_booking_$tanggal.txt";
            Storage::put($filename, $exportedData);
            $contentType = 'application/json';

            $s3Url = Storage::disk('s3')->put($filename, $exportedData, 'public', ['Content-Type' => $contentType]);
            $url = "https://gadaimas.nos.wjv-1.neo.id/$filename";

            $this->logFileUrl($s3Url, $url, $tanggal);
        } catch (\Exception $e) {
            $this->error("An error occurred: {$e->getMessage()}");
        }
    }

    protected function getData($tanggal)
    {
        $query = trans_gadai::
        join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        ->leftJoin('tblkelurahan','tblkelurahan.idKelurahan','=','tblcustomer.idKelurahanKtp')
        ->leftJoin('tblkelurahan AS domisili','domisili.idKelurahan','=','tblcustomer.idKelurahanDomisili')
        ->leftJoin('tblpekerjaan','tblpekerjaan.idPekerjaan','=','tblcustomer.idPekerjaan')
        ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        ->join('tran_taksiran','tran_taksiran.idTaksiran','=','trans_gadai.idTaksiran')
        ->join('tran_fapg','tran_fapg.idFAPG','=','trans_gadai.idFAPG')
        ->leftJoin("tran_taksiran as taksir", function($join){
            $join->on('taksir.idFAPG','=','trans_gadai.idFAPG');
            $join->where(DB::raw("taksir.isFinal"), "=", 0);
            
        })
        ->leftJoin('tbltujuantransaksi','tbltujuantransaksi.idTujuanTransaksi','=','trans_gadai.idTujuanTransaksi')
        ->leftJoin('tblsektorekonomi','tblsektorekonomi.idSektorEkonomi','=','trans_gadai.idSektorEkonomi')
        ->leftJoin('tbljenisreferensi','tbljenisreferensi.idJenisReferensi','=','trans_gadai.idJenisReferensi')
        ->leftJoin('tblcustomer AS cgc','cgc.cif','=','trans_gadai.referensiCif')
        ->leftJoin('tblasaljaminan','tblasaljaminan.idAsalJaminan','=','trans_gadai.idAsalJaminan')
        ->leftJoin('tbljenisreferensi AS refCust','refCust.idJenisReferensi','=','tblcustomer.idJenisReferensi')
        ->leftJoin('tblcustomer AS cgcNasabah','cgcNasabah.cif','=','tblcustomer.referensiCif')
        ->leftJoin('trans_gadai AS sbgLama','sbgLama.idGadai','=','tran_fapg.idGadai')
        ->leftJoin('trans_payctrl AS payctrlLama','payctrlLama.idGadai','=','tran_fapg.idGadai')
        ->leftJoin('trans_gadaijf','trans_gadaijf.idGadai','=','trans_gadai.idGadai')
        ->leftJoin('tbluser','tbluser.idUser','=','tran_taksiran.idUser')
        ->leftJoin('tblkaryawaninternal','tblkaryawaninternal.idKaryawan','=','tbluser.idKaryawan')
        ->leftJoin("tblkaryawaninternal as refInternal", function($join){
            $join->on('refInternal.npk', '=', 'trans_gadai.referensiNpk');
            $join->where(DB::raw("refInternal.isActive"), "=", 1);
            
        })
        ->leftJoin("tblkaryawaneksternal as refEksternal", function($join){
            $join->on('refEksternal.kodeAgent', '=', 'trans_gadai.referensiNpk');
            $join->where(DB::raw("refEksternal.isActive"), "=", 1);
            
        })
        ->leftJoin('tbljabatankaryawan as jabInternal','jabInternal.idJabatanKaryawan','=','refInternal.idJabatanKaryawan')
        ->leftJoin('tbljabatankaryawan as jabeksternal','jabeksternal.idJabatanKaryawan','=','refEksternal.idJabatanKaryawan')
        ->leftJoin('tblprogram','tblprogram.idProgram','=','trans_gadai.idProgram')
        ->leftJoin("trans_fundingdetail", function($join){
            $join->on('trans_fundingdetail.idGadai', '=', 'trans_gadai.idGadai');
            $join->whereNull('trans_fundingdetail.tglUnpledging');
            
        })
        ->leftJoin("tblsettingjf", function($join){
            $join->on('tblsettingjf.idJf', '=', 'trans_gadaijf.idJf');
            $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
            
        })
        ->leftJoin('trans_funding','trans_funding.idFunding','=','trans_fundingdetail.idFunding')
        ->leftJoin('tblpartner','tblpartner.idPartner','=','trans_funding.idPartner')
        ->selectRaw("tblproduk.kodeProduk AS produk, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, 
        CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, trans_gadai.noSbg, tblcustomer.cif, 
        tblcustomer.namaCustomer, tblcustomer.noKtp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
        tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
        tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
        IF(tblcustomer.alamatDomisili != '', 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
        ) AS alamatDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
        tblcustomer.hp AS noHp, tblpekerjaan.namaPekerjaan AS pekerjaan, 
        tblcustomer.tglRegister, DATE(trans_gadai.tglCair) AS tglCair, trans_payctrl.tanggalPelunasan, trans_gadai.lamaPinjaman AS tenor, trans_payctrl.ovd, 
        trans_gadai.transKe AS ke, trans_gadai.rateFlat, ROUND((trans_gadai.nilaiPinjaman / tran_taksiran.totalTaksiran) * 100, 2) AS ltv, 
        trans_gadai.nilaiPinjaman AS pokokAwal, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS pinjamAwal, 
        trans_gadai.nilaiPinjaman AS saldoPokok, trans_gadai.biayaPenyimpanan AS saldoBunga, 
        (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan) AS saldoPinjam, 
        trans_gadai.biayaAdmin, trans_gadai.biayaAdminAwal, trans_gadai.diskonAdmin, taksir.totalTaksiran AS taksir1, 
        taksir.avgKarat AS karat1, taksir.sumBeratBersih AS berat1, tran_taksiran.totalTaksiran AS taksir, tran_taksiran.avgKarat AS karat, 
        tran_taksiran.sumBeratBersih AS berat, tran_fapg.stleAntam AS stle, tbltujuantransaksi.namaTujuanTransaksi, 
        tblsektorekonomi.namaSektorEkonomi, tbljenisreferensi.namaJenisReferensi AS asalAplikasi, trans_gadai.referensiNpk AS kodeReferensiAplikasi, 
        trans_gadai.referensiNama AS namaReferensiAplikasi, 
        IF(refInternal.idKaryawan != '', jabInternal.namaJabatanKaryawan, jabeksternal.namaJabatanKaryawan) AS jabatan, 
        CONCAT(cgc.cif, '-', cgc.namaCustomer) AS namaCgcAplikasi, tblprogram.namaProgram, trans_gadai.statusAplikasi, tblasaljaminan.namaAsalJaminan, 
        refCust.namaJenisReferensi AS referensiNasabah, tblcustomer.referensiNpk AS kodeReferensiNasabah, tblcustomer.referensiNama AS namaReferensiNasabah, 
        tblcustomer.referensiCif AS cifReferensiNasabah, cgcNasabah.namaCustomer AS namaCgcReferensiNasabah, sbgLama.noSbg AS noSbgLama, 
        payctrlLama.ovd AS ovdLama, (trans_gadai.transKe - 1) AS perpanjanganKe, trans_gadai.jenisPembayaran, tblcustomer.statusOcrKtp, 
        tblpartner.namaPartner AS bankFunding, tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.rate AS RatePendana, 
        tblsettingjf.namaJf AS bankPendana, tblsettingjf.porsi AS porsiBank, tblkaryawaninternal.namaKaryawan AS namaPenaksir, 
        trans_payctrl.created_at AS tanggalJam, tran_fapg.imgJaminan AS foto, trans_gadai.approvalFinal, 
        IF(trans_gadai.statusAplikasi = 'NEW ORDER', tblcustomer.kodeVoucher, '') AS kodeVoucher, trans_gadai.jenisPembayaranFinal, 
        tran_fapg.created_at AS timeFAPG, trans_gadai.tglCetakKwitansi AS timeCetakKwitansi")
            ->where('trans_payctrl.tanggalPencairan', '>=', substr($tanggal,0,7)."-01")
            ->where('trans_payctrl.tanggalPencairan', '<', $tanggal)
        ->get()
        ->toArray();
        return $query;
    }

    protected function formatData($data)
    {
        $exportedData = '';

        // Check if there is any data
        if (empty($data)) {
            $this->info('No data found for export.');
            return $exportedData;
        }

        // Get the headers
        $headers = array_keys($data[0]);

        // Set the delimiter
        $delimiter = '|';

        // Add headers to the exported data
        $exportedData .= implode($delimiter, $headers) . PHP_EOL;

        foreach ($data as $row) {
            $rowArray = [];
            foreach ($row as $column) {
                // Convert objects and arrays to JSON
                $value = is_array($column) || is_object($column) ? json_encode($column) : $column;
                $rowArray[] = str_replace(["\r", "\n", '|', "\t"], ' ', $value);
            }
            $exportedData .= implode($delimiter, $rowArray) . PHP_EOL;
        }

        return $exportedData;
    }

    protected function logFileUrl($s3Url, $url, $tanggal)
    {
        $insertedId = DB::table('report_harian')->insert([
            'urlFile' => $url,
            'tanggal' => $tanggal,
            'jenisReport' => 'Booking'
        ]);

        if ($insertedId) {
            $this->info("File uploaded to S3. URL: {$url}. Log ID: {$insertedId}");
        } else {
            $this->error("Failed to insert log record for URL: {$url}");
        }
    }

}
