<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class GoogleSheetApiCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'googlesheets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description ayyy';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $response = Http::get('http://ads-promas.test/api/googlesheets');

        if ($response->successful()) {
            // Process the API response
            $data = $response->json();
            return $data;
        } else {
            $this->error('API request failed');
        }
    }
}
