<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\JaminanKonvensionalExcel;
use Maatwebsite\Excel\Facades\Excel;

class JaminanKonvensionalCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'jaminankonvensionalcron';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Jaminan Konvensional Export daily';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $tanggal = date('Y-m-d');
        $fileName = "JaminanKonvensional";
        $extension = "xlsx";

        $fileNameWithExtension = "excel/$fileName-$tanggal.$extension";
        return Excel::store(new JaminanKonvensionalExcel($tanggal, '', '', ''), $fileNameWithExtension, 'local');
    }
}
