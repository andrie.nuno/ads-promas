<?php

namespace App\Console\Commands;

use App\Http\Controllers\login\AzKonvensional;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Http\Exports\AzKonvensionalExcel;
use App\Jobs\AzKonvensionalAppendJob;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Storage;
use App\model\trans_gadai;
use DB;

class PelunasanKonvensionalCommandTxt extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'exportpelunasan:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export PelunasanKonvensional data to txt, upload to S3, and log file URL';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $tanggal = date("Y-m-d", strtotime("yesterday"));
            
            $data = $this->getData($tanggal);

            $exportedData = $this->formatData($data);

            $filename = "export-pelunasankonvensional/exported_data_pelunasan_$tanggal.txt";
            Storage::put($filename, $exportedData);
            $contentType = 'application/json';

            $s3Url = Storage::disk('s3')->put($filename, $exportedData, 'public', ['Content-Type' => $contentType]);
            $url = "https://gadaimas.nos.wjv-1.neo.id/$filename";

            $this->logFileUrl($s3Url, $url, $tanggal);
        } catch (\Exception $e) {
            $this->error("An error occurred: {$e->getMessage()}");
        }
    }

    protected function getData($tanggal)
    {
        $tanggalAwal = substr($tanggal,0,7)."-01";
        $query = trans_gadai::
        join('tblproduk','tblproduk.idProduk','=','trans_gadai.idProduk')
        ->join('tblcabang','tblcabang.idCabang','=','trans_gadai.idCabang')
        ->join('tblcabang AS cabang','cabang.idCabang','=','tblcabang.headCabang')
        ->join('tblwilayah','tblwilayah.idWilayah','=','tblcabang.idWilayah')
        ->join('tblcustomer','tblcustomer.idCustomer','=','trans_gadai.idCustomer')
        ->join('trans_payctrl','trans_payctrl.idGadai','=','trans_gadai.idGadai')
        ->leftJoin('tblkelurahan','tblkelurahan.idKelurahan','=','tblcustomer.idKelurahanKtp')
        ->leftJoin('tblkelurahan AS domisili','domisili.idKelurahan','=','tblcustomer.idKelurahanDomisili')
        ->leftJoin('tblpekerjaan','tblpekerjaan.idPekerjaan','=','tblcustomer.idPekerjaan')
        ->leftJoin("trans_pelunasangadai", function($join){
            $join->on('trans_pelunasangadai.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_pelunasangadai.isStatus"), "=", 1);
        })
        ->leftJoin("trans_pelunasanjual", function($join){
            $join->on('trans_pelunasanjual.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_pelunasanjual.isStatus"), "=", 1);
        })
        ->leftJoin("trans_pembayarancicilan", function($join){
            $join->on('trans_pembayarancicilan.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_pembayarancicilan.isStatus"), "=", 1);
        })
        ->leftJoin("trans_payment_schedule", function($join){
            $join->on('trans_payment_schedule.idGadai', '=', 'trans_payment_schedule.idGadai');
            $join->on('trans_payment_schedule.angsuranKe', '=', 'trans_pembayarancicilan.angsuranKe');
        })
        ->leftJoin("trans_gadaijf", function($join){
            $join->on('trans_gadaijf.idGadai', '=', 'trans_gadai.idGadai');
            $join->where(DB::raw("trans_gadaijf.isApprove"), "=", 1);
        })
        ->leftjoin('tblsettingjf', 'tblsettingjf.idJf', '=', 'trans_gadaijf.idJf')
        ->leftJoin(\DB::raw("(SELECT outs.idCustomer FROM trans_payctrl AS outs WHERE outs.tanggalPencairan <= '$tanggal' AND (outs.tanggalPelunasan IS NULL OR outs.tanggalPelunasan > '$tanggal') GROUP BY outs.idCustomer) AS pinjamanAktif"), 'pinjamanAktif.idCustomer', '=', 'trans_gadai.idCustomer')
        ->selectRaw("tblproduk.kodeProduk AS produk, trans_gadai.noSbg, tblcabang.kodeCabang AS kodeOutlet, tblcabang.namaCabang AS namaOutlet, 
        CONCAT(cabang.kodeCabang,'-',cabang.namaCabang) AS namaCabang, tblwilayah.namaWilayah, tblcustomer.namaCustomer, 
        tblcustomer.cif, tblcustomer.noKtp, tblcustomer.hp, tblcustomer.tempatLahir, tblcustomer.tanggalLahir, 
        REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '') AS alamatKtp, 
        tblcustomer.rtKtp, tblcustomer.rwKtp, tblkelurahan.namakelurahan, tblkelurahan.namaKecamatan, tblkelurahan.namaKabupaten, 
        tblkelurahan.namaProvinsi, tblkelurahan.kodepos, 
        IF(tblcustomer.alamatDomisili != '', 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatDomisili, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', ''), 
            REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(tblcustomer.alamatKtp, ';', ''), CHAR(13), ' '), CHAR(10), ' '), '|', ' '), '\t', '')
        ) AS alamatDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rtDomisili, tblcustomer.rtKtp) AS rtDomisili, 
        IF(tblcustomer.alamatDomisili != '', tblcustomer.rwDomisili, tblcustomer.rwKtp) AS rwDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namakelurahan, tblkelurahan.namakelurahan) AS kelurahanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKecamatan, tblkelurahan.namaKecamatan) AS kecamatanDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaKabupaten, tblkelurahan.namaKabupaten) AS kabupatenDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.namaProvinsi, tblkelurahan.namaProvinsi) AS provinsiDomisili, 
        IF(tblcustomer.alamatDomisili != '', domisili.kodepos, tblkelurahan.kodepos) AS kodePosDomisili, 
        tblpekerjaan.namaPekerjaan AS pekerjaan, trans_gadai.lamaPinjaman AS tenor, 
        IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.ovd, DATEDIFF(trans_pembayarancicilan.tglPembayaran, trans_payment_schedule.tglJatuhTempo)) AS ovd, DATE(trans_gadai.tglCair) AS tglCair, 
        IF(trans_pembayarancicilan.idGadai IS NULL, trans_payctrl.tanggalPelunasan, trans_pembayarancicilan.tglPembayaran) AS tanggalPelunasan, 
        IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.tglJatuhTempo, trans_payment_schedule.tglJatuhTempo) AS tglJatuhTempo, 
        IF(trans_pembayarancicilan.idGadai IS NULL, trans_gadai.nilaiPinjaman, IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', trans_payment_schedule.pokokAngsuran, trans_pembayarancicilan.nilaiPokok)) AS pokokAwal, 
        IF(trans_pembayarancicilan.idGadai IS NULL, (trans_gadai.nilaiPinjaman + trans_gadai.biayaPenyimpanan), (trans_payment_schedule.pokokAngsuran + trans_payment_schedule.bungaAngsuran)) AS pinjamAwal, 
        ROUND(trans_gadai.rateFlat * 12, 2) AS rate, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.totalPelunasan, trans_pelunasanjual.totalKewajiban), (trans_pembayarancicilan.totalPembayaran)) AS nilaiLunas, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(tblsettingjf.kodeJf IS NULL, trans_payctrl.acrueBunga, (trans_payctrl.acrueBungaMurni + trans_payctrl.acrueBungaSelisih)), (trans_payment_schedule.acrualAkhirBulan + trans_payment_schedule.acrualJatuhTempo)) AS acrueBunga, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiBunga, trans_pelunasanjual.nilaiBunga), trans_pembayarancicilan.nilaiBunga) AS pendapatanBunga, 
        IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.nilaiDenda, 0) AS biayaPemeliharaan, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, trans_pelunasanjual.nilaiAdminJual), 0) AS adminPenjualan, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_gadai.biayaPenyimpanan - trans_pelunasangadai.nilaiBunga), (trans_gadai.biayaPenyimpanan - trans_pelunasanjual.nilaiBunga)), 0) AS upsite, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', (trans_pelunasangadai.totalKewajiban - trans_pelunasangadai.totalPelunasan), 0), 0) AS diskonPelunasan, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', 0, (trans_pelunasanjual.totalKewajiban - trans_pelunasanjual.totalPelunasan)), 0) AS diskonPenjualan, 
        tblsettingjf.kodeJf AS kodeMitra, tblsettingjf.namaJf AS bankPendana, tblsettingjf.rate AS RateJf, tblsettingjf.porsi AS porsiBank, 
        IF(trans_pelunasangadai.pelunasanBankJfReal != 0, trans_pelunasangadai.pelunasanBankJfReal, trans_pelunasangadai.pelunasanBankJf) AS pelunasanJf, 
        IF(trans_pembayarancicilan.idGadai IS NULL, IF(trans_pelunasangadai.statusLunas != '', trans_pelunasangadai.statusLunas, trans_payctrl.statusPinjaman), IF(trans_pembayarancicilan.jenisPelunasan = 'Cicilan', 'Pembayaran Cicilan', 'Pelunasan Cicilan Dipercepat')) AS statusPelunasan, 
        IF(pinjamanAktif.idCustomer IS NULL, 'EXIT', '') AS statusExit")
        ->whereRaw("trans_payctrl.tanggalPelunasan >= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggalAwal') 
        AND trans_payctrl.tanggalPelunasan <= IF(tblproduk.idJenisProduk = 2, '9999-99-99', '$tanggal') OR 
        (trans_pembayarancicilan.tglPembayaran >= '$tanggalAwal' AND trans_pembayarancicilan.tglPembayaran <= '$tanggal')")
        ->get()
        ->toArray();
        return $query;
    }

    protected function formatData($data)
    {
        $exportedData = '';

        // Check if there is any data
        if (empty($data)) {
            $this->info('No data found for export.');
            return $exportedData;
        }

        // Get the headers
        $headers = array_keys($data[0]);

        // Set the delimiter
        $delimiter = '|';

        // Add headers to the exported data
        $exportedData .= implode($delimiter, $headers) . PHP_EOL;

        foreach ($data as $row) {
            $rowArray = [];
            foreach ($row as $column) {
                // Convert objects and arrays to JSON
                $value = is_array($column) || is_object($column) ? json_encode($column) : $column;
                $rowArray[] = str_replace(["\r", "\n", '|', "\t"], ' ', $value);
            }
            $exportedData .= implode($delimiter, $rowArray) . PHP_EOL;
        }

        return $exportedData;
    }

    protected function logFileUrl($s3Url, $url, $tanggal)
    {
        $insertedId = DB::table('report_harian')->insert([
            'urlFile' => $url,
            'tanggal' => $tanggal,
            'jenisReport' => 'Pelunasan'
        ]);

        if ($insertedId) {
            $this->info("File uploaded to S3. URL: {$url}. Log ID: {$insertedId}");
        } else {
            $this->error("Failed to insert log record for URL: {$url}");
        }
    }

}
