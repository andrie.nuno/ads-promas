<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use Illuminate\Support\Facades\DB;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\GoogleSheetApiCommand::class,
        // Commands\AzKonvensionalCommandTxt::class,
        // Commands\JaminanKonvensionalCommandTxt::class,
        // Commands\BookingKonvensionalCommandTxt::class,
        // Commands\PelunasanKonvensionalCommandTxt::class,
        // Commands\PenjualanKonvensionalCommandTxt::class,
        'App\Console\Commands\AzKonvensionalCommandTxt',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        // $schedule->command('googlesheets')->everyMinute();
        $schedule->command('exportaz:data')->dailyAt('01:00');
        // $schedule->command('exportbooking:data')->dailyAt('01:00');
        // $schedule->command('exportjaminan:data')->dailyAt('01:00');
        // $schedule->command('exportpelunasan:data')->dailyAt('01:00');
        // $schedule->command('exportpenjualan:data')->dailyAt('01:00');

        // $schedule->command('exportaz:data')->everyFiveMinutes();

        // $schedule->call(function () {
        //     // Run your export controller method
        //     Artisan::call('azkonvensionalcron'); // Replace with your command to trigger the export
        // })->daily();

        // $schedule->call(function () {
        //     DB::table('dummy_data')->insert(
        //         ['data' => 'dummy']
        //     );
        // })->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
