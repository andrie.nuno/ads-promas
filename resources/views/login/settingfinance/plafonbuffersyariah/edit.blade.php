<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Plafon Buffer</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.plafonbuffersyariahpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
            <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
            <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama Cabang</th>
                        <th>Plafon</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            @foreach ($data as $dita)
                    @php 
                    $nomor=1 
                    @endphp
                    <tr>
                        <td>{{ $nomor++ }}</td>
                        <td>{{ $dita->namaCabang }}</td>
                        <td>
                            <input type="hidden" name="data[{{ $dita->idCabang }}][idCabang]" value="{{ $dita->idCabang }}">
                            @if ($listGrade)
                            <select class="form-control" name="data[{{ $dita->idCabang }}][idGradeCabangSyariah]" required>
                                <option value="0">== Pilih Plafon ==</option>                                    
                                @foreach ($listGrade as $list)
                                    @if ($list->idJenisCabang == $dita->idJenisCabang)
                                    <option value="{{ $list->idGradeCabang }}" {{ $list->idGradeCabang == $dita->idGradeCabangSyariah ? 'selected="selected"' : '' }}>Rp. {{ number_format($list->maxBuffer) }}</option>
                                    @endif
                                @endforeach
                            </select>
                            @endif
                            
                        </td>
                    </tr>
                </div>
            @endforeach
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});

function inputAngka(objek) {
    a = objek.value;
    b = a.replace(/[^\d]/g,"");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i-1,1) + "." + c;
        } else {
            c = b.substr(i-1,1) + c;
        }
    }
    objek.value = c;
}
</script>
