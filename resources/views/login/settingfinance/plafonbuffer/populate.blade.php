<div class="table-responsive">
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Cabang</th>
                <th>Nama Cabang</th>
                <th>Jenis Cabang</th>
                <th>Plafon</th>
                <th>Nama Wilayah</th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            <div>
                <center><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', {{ $idWilayah }}, '{{ route('login.plafonbuffermodal') }}')"><span class="fa fa-fw fa-edit" ></span>Edit</a></center>
            </div>
            @php 
            $nomor=1 
            @endphp
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->kodeCabang }}</td>
                <td>{{ $dita->namaCabang }}</td>
                <td>{{ $dita->namaJenisCabang }}</td>
                <td>Rp. {{ number_format($dita->plafon) }}</td>
                <td>{{ $dita->namaWilayah }}</td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
