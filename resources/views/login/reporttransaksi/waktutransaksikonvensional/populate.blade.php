<div class="col-md-12">
    <span data-href="{{ route('waktutransaksikonvensionalexcel') }}?nama_wilayah={{ $nama_wilayah }}&tgl_awal={{ $tgl_awal }}&tgl_sampai={{ $tgl_sampai }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span> 
    
    <a href="{{ route('waktutransaksikonvensionalexport') }}?nama_wilayah={{ $nama_wilayah }}&tgl_awal={{ $tgl_awal }}&tgl_sampai={{ $tgl_sampai }}" id="export" class="btn btn-primary btn-sm theme-button-colour" target="_blank"> Export Data</a>
</div>
<div class="table-responsive">
    <p>&nbsp;</p>
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Kode Outlet</th>
                <th>Nama Outlet</th>
                <th>Nama Cabang</th>
                <th>Nama Wilayah</th>
                <th>Tgl Pencairan</th>
                <th>No SBG</th>
                <th>Jenis Pembayaran Final</th>
                <th>CIF</th>
                <th>Nama</th>
                <th>No KTP</th>
                <th>Nilai Pinjaman</th>
                <th>Rate</th>
                <th>LTV</th>
                <th>One Obligor</th>
                <th>Status Aplikasi</th>
                <th>Time Input FAPG</th>
                <th>Time Taksiran I</th>
                <th>Time Taksiran II</th>
                <th>Time Input Gadai</th>
                <th>Time KaUnit</th>
                <th>Time Kacab</th>
                <th>Time Kawil</th>
                <th>Time KaDept CPA</th>
                <th>Time KaDiv CPA</th>
                <th>Time Direktur</th>
                <th>Time Dirut</th>
                <th>Time Kwitansi</th>
                <th>Time Foto Nasabah</th>
                <th>Final Approval</th>
                <th>Approval UP</th>
                <th>Approval Rate</th>
                <th>Approval LTV</th>
                <th>Approval Obligor</th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->kodeOutlet }}</td>
                <td>{{ $dita->namaOutlet }}</td>
                <td>{{ $dita->namaCabang }}</td>
                <td>{{ $dita->namaWilayah }}</td>
                <td>{{ $dita->tanggalPencairan }}</td>
                <td>{{ $dita->noSbg }}</td>
                <td>{{ $dita->jenisPembayaranFinal }}</td>
                <td>{{ $dita->cif }}</td>
                <td>{{ $dita->namaCustomer }}</td>
                <td>{{ $dita->noKtp }}</td>
                <td align="right">{{ number_format($dita->nilaiPinjaman) }}</td>
                <td align="right">{{ number_format($dita->rateFlat) }}%</td>
                <td align="right">{{ number_format($dita->ltv) }}%</td>
                <td align="right">{{ number_format($dita->totalObligor) }}</td>
                <td>{{ $dita->statusAplikasi }}</td>
                <td>{{ $dita->timeInputFAPG }}</td>
                <td>{{ $dita->timeTaksirPenaksir }}</td>
                <td>{{ $dita->timeTaksirKaUnit }}</td>
                <td>{{ $dita->timeInputGadai }}</td>
                <td>{{ $dita->tglApprovalKaunit }}</td>
                <td>{{ $dita->tglApprovalKacab }}</td>
                <td>{{ $dita->tglApprovalKawil }}</td>
                <td>{{ $dita->tglApprovalKadeptCpa }}</td>
                <td>{{ $dita->tglApprovalKadivCpa }}</td>
                <td>{{ $dita->tglApprovalDirektur }}</td>
                <td>{{ $dita->tglApprovalDirut }}</td>
                <td>{{ $dita->timeCetakKwitansi }}</td>
                <td>{{ $dita->timeFotoNasabah }}</td>
                <td>{{ $dita->approvalFinal }}</td>
                <td>{{ $dita->approvalUangPinjaman }}</td>
                <td>{{ $dita->approvalRateFlat }}</td>
                <td>{{ $dita->approvalLtv }}</td>
                <td>{{ $dita->approvalOneObligor }}</td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
