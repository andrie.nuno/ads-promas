<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Divisi</th>
                <th>Message</th>
                <th>Image</th>
                <th><center>Status</center></th>
                <th><center>Action</center></th>
                <th><center>Report</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
        {{-- @dd($data) --}}
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->division }}</td>
                <td>{{ substr($dita->message, 0, 200) }}</td>
                <td>
                    @if($dita->linkUrl != "")
                        <a href="{{ $dita->linkUrl }}" target="_blank">Image</a>
                    @endif
                </td>
                <td><center>{{ $dita->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                <td><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idUpdate }}', '{{ route('login.updateinformasimodal') }}')"><span class="fa fa-fw fa-edit"></span></a></td>
                <td><a onclick="showMultiModal(2, 'report', '{{ csrf_token() }}', '{{ $dita->idUpdate }}', '{{ route('login.updateinformasimodal') }}')"><span class="fa fa-fw fa-file"></span></a></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
