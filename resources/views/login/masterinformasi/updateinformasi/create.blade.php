<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Tambah Update Informasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.updateinformasipost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post" accept-charset="UTF-8">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="division">Divisi *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="division" name="division" value="" autocomplete="off" required>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                    <input type="hidden" name="tglMin" id="tglMin" value="{{ date('Y-m-d', strtotime('+1 day')) }}" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="judul">Judul *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="judul" name="judul" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="tglMulai">Tanggal Mulai</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="tglMulai" name="tglMulai" min="{{ date('Y-m-d', strtotime('+1 day')) }}" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="tglSampai">Tanggal Sampai</label>
                <div class="col-sm-9">
                    <input type="date" class="form-control" id="tglSampai" name="tglSampai" min="{{ date('Y-m-d', strtotime('+1 day')) }}" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="jenisInformasi">Jenis Informasi *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="jenisInformasi" name="jenisInformasi">
                        <option value="1">KONVEN</option>
                        <option value="2">SYARIAH</option>
                        <option value="3">KONVEN & SYARIAH</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="profilPdf">IMG</label>
                <div class="col-sm-9">
                    <input type="file" class="form-control-file" id="profilPdf" name="profilPdf" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="message">Message *</label>
                <div class="col-sm-9">
                    <textarea class="ckeditor form-control" id="message" name="message" autocomplete="off" required rows="3"></textarea>
                </div>

            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1">AKTIF</option>
                        <option value="0">TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="button" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script src="{{ asset('ckeditor/ckeditor.js') }}"></script>
<script type="text/javascript">
$(document).ready(function(e){
	// sizeMultiModal(1, "lg"); // large
    sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);

    // Initialize CKEditor
    CKEDITOR.replace('message', {
            customConfig: '{{ asset("ckeditor/config.js") }}' // Optional: Custom configuration file
    });
});

function doSubmit(id) {
    swal({
        title: "Are you sure?",
        text: "You will submit this form.",
        type: "warning",
        cancelButtonText: "Cancel",
        confirmButtonText: "Submit",
        showCancelButton: true
    }).then(function(result) {
        if (result.value) {
            setTimeout(function() {
                var form = $("#form-" + id);

                // Get CKEditor content
                var editorContent = CKEDITOR.instances['message'].getData();

                // Append CKEditor content to FormData
                var formData = new FormData(form[0]);
                formData.append('message', editorContent);

                $.ajax({
                    type: "POST",
                    data: formData,
                    url: form.attr("action"),
                    async: false,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(response) {
                        swal.close();
                        setTimeout(function() {
                            if (!$("#result-" + id).length) {
                                $('<div id="result-' + id + '"></div>').insertBefore("#form-" + id);
                            }
                            $("#result-" + id).html(response);
                        }, 250);
                    },
                    beforeSend: function() {
                        swalLoader();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        swal("Form", ajaxOptions.toUpperCase() + ': ' + thrownError, "error");
                    }
                });
            }, 250);
        }
    });
}
</script>