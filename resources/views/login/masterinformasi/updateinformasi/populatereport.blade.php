@if($data)
    <div class="col-md-12">
        <span data-href="{{ route('login.updateinformasiexcel') }}?idUpdate={{ $idUpdate }}&nama_wilayah={{ $nama_wilayah }}&nama_cabang={{ $nama_cabang }}&nama_unit={{ $nama_unit }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span>
    </div>
@endif
<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Nik</th>
                <th>Nama</th>
                <th>Time Konfirmasi</th>
                <th>Kode Unit</th>
                <th>Nama Unit</th>
                <th>Nama Wilayah</th>
            </tr>
        </thead>
        <tbody>
            @if($data)
                @php($nomor=1)
                @foreach($data as $dita)
                    <tr>
                        <td>{{ $nomor++ }}</td>
                        <td>{{ $dita->username }}</td>
                        <td>{{ $dita->namaKaryawan }}</td>
                        <td>{{ $dita->timeKonfirmasi }}</td>
                        <td>{{ $dita->kodeCabang }}</td>
                        <td>{{ $dita->namaCabang }}</td>
                        <td>{{ $dita->namaWilayah }}</td>
                    </tr>
                @endforeach
            @else
                <tr>
                    <td colspan="7">Data Tidak Ada</td>
                </tr>
            @endif
        </tbody>
    </table>
</div>
<script type="text/javascript">
    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>