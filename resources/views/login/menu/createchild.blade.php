<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Tambah Menu Child</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.menupost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="headerId">Menu Header *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="headerId" name="headerId">
                        <option value="{{ $header->menuId }}">{{ $header->menuNama }}</option>
                    </select>
                    <input type="hidden" id="menuNav" name="menuNav" value="Child" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="menuNama">Nama Menu *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="menuNama" name="menuNama" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="menuSegmen">Nama Segmen *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="menuSegmen" name="menuSegmen" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="menuUrl">Nama URL *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="menuUrl" name="menuUrl" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="menuUrut">Nomor Urut Menu *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="menuUrut" name="menuUrut" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1">AKTIF</option>
                        <option value="0">TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg");
	centerMultiModal(1);
});
</script>
