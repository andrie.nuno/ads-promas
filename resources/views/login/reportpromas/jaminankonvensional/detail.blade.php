<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">{{ $data->namaBorrower }}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body" style="font-size: small">
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item"><b>Nama Borrower :</b> {{ $data->namaBorrower }}</li>
                <li class="list-group-item"><b>CIF :</b> {{ \helpers::sembilanAngka($data->idBorrower) }}</li>
                <li class="list-group-item"><b>No KTP :</b> {{ $data->ktp }}</li>
                <li class="list-group-item"><b>Email :</b> {{ $data->email }}</li>
                <li class="list-group-item"><b>No Telepon :</b> {{ $data->tlpMobile }}</li>
                <li class="list-group-item"><b>Pekerjaan :</b> {{ $data->namaPekerjaan }}</li>
                <li class="list-group-item"><b>Pengahasilan :</b> {{ number_format($data->penghasilanPerBulan) }}</li>
                <li class="list-group-item"><b>Sumber Penghasilan :</b> {{ $data->namaSumberDana }}</li>
            </ul>
        </div>
        <div class="card">
            <ul class="list-group">
                <li class="list-group-item"><b>Foto Jaminan :</b> <a href="{{ $data->fotoJaminan }}" target="_blank" class="btn btn-primary btn-xs">Lihat Foto Jaminan</a></li>
                <li class="list-group-item"><b>Nama Produk :</b> {{ $data->namaProduk }}</li>
                <li class="list-group-item"><b>Cabang Penaksir :</b> {{ $data->namaBranch }}</li>
                <li class="list-group-item"><b>Tgl Pencairan / Tgl JT :</b> {{ $data->tanggal }} / {{ $data->tglJt }}</li>
                <li class="list-group-item"><b>Nama Jaminan / Jumlah :</b> {{ $data->namaJaminan }} / {{ $data->jumlahJaminan }}</li>
                <li class="list-group-item"><b>Gram / Karat :</b> {{ $data->gram }} / {{ $data->karat }}</li>
                <li class="list-group-item"><b>STLE / LTV :</b> {{ number_format($data->nilaiStle) }} / {{ $data->nilaiLtv }}%</li>
                <li class="list-group-item"><b>Rate Jasa Taksir :</b> {{ $data->rateJasaTaksir }}%</li>
                <li class="list-group-item"><b>Rate Pendana :</b> {{ $data->ratePendana }}%</li>
                <li class="list-group-item"><b>Rate Danain :</b> {{ $data->rateFeeDanain }}%</li>
                <li class="list-group-item"><b>Rate Admin :</b> {{ $data->ratePencairan }}%</li>
                <li class="list-group-item"><b>Nilai Taksiran / Max Pinjaman :</b> {{ number_format($data->nilaiTaksiran) }} / {{ number_format($data->nilaiMax) }}</li>
                <li class="list-group-item"><b>Harga Antam / Harga Jual Emas :</b> {{ number_format($data->hargaPasar) }} / {{ number_format($data->hargaBeliPasar) }}</li>
                <li class="list-group-item list-group-item-primary"><b>Nilai Pinjaman :</b> {{ number_format($data->nilaiPinjaman) }}</li>
                @if($data->idProduk == 4)
                    <li class="list-group-item"><b>Jasa Taksir Mitra :</b> {{ number_format($data->jasaTaksir) }}</li>
                    <li class="list-group-item"><b>Bunga Pinjaman :</b> {{ number_format($data->bungaPinjaman) }}</li>
                @endif
                <li class="list-group-item"><b>Nilai Admin :</b> {{ number_format($data->adminPencairan) }}</li>
                <li class="list-group-item list-group-item-success"><b>Nilai Pencairan :</b> {{ number_format($data->nilaiPencairan) }}</li>
                <li class="list-group-item"><b>Jumlah Hari Berjalan :</b> {{ $data->jmlHari }} Hari</li>
                <li class="list-group-item"><b>Bunga Harian/Terhutang :</b> {{ $data->bungaHarian == "" ? 0 : number_format($data->bungaHarian, 4) }} / {{ $data->bungaTerhutang == "" ? 0 : number_format($data->bungaTerhutang) }}</li>
                <li class="list-group-item"><b>Jasa Mitra Harian/Terhutang :</b> {{ $data->jasaMitraHarian == "" ? 0 : number_format($data->jasaMitraHarian, 4) }} / {{ $data->jasaMitraTerhutang == "" ? 0 : number_format($data->jasaMitraTerhutang) }}</li>
                <li class="list-group-item"><b>Denda Terhutang :</b> {{ $data->dendaTerhutang == "" ? 0 : number_format($data->dendaTerhutang) }}</li>
                <li class="list-group-item list-group-item-warning"><b>Total Hutang :</b> {{ number_format($data->totalHutang) }}</li>
            </ul>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
