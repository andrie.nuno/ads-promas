<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
    <tbody class="tbody">
        <tr role="row">
            <td colspan="4">Tanggal Outstanding Pinjaman : {{ $tanggal }}</td>
        </tr>
        <tr role="row">
            <td>No</td>
            <td>No Perjanjian Pinjaman</td>
            <td>Email</td>
            <td>CIF</td>
            <td>No KTP</td>
            <td>Nama Peminjam</td>
            <td>Jenis Kelamin</td>
            <td>Tanggal Lahir</td>
            <td>Usia</td>
            <td>Pekerjaan</td>
            <td>Penghasilan/Bulan</td>
            <td>Biaya Hidup/Bulan</td>
            <td>Tujuan Pinjaman</td>
            <td>Tanggal Cair</td>
            <td>Tanggal JT</td>
            <td>Wilayah</td>
            <td>Kode Outlet</td>
            <td>Outlet Penaksir</td>
            <td>Nama Produk</td>
            <td>Nama Jaminan</td>
            <td>Jumlah</td>
            <td>Karat</td>
            <td>Rata-Rata Karatase</td>
            <td>Gram</td>
            <td>Jumlah Gram</td>
            <td>STLE</td>
            <td>LTV</td>
            <td>Jasa Taksir</td>
            <td>Bunga Pendana</td>
            <td>Bunga Danain</td>
            <td>Admin</td>
            <td>Nilai Taksiran</td>
            <td>Max Pinjaman</td>
            <td><b>Nilai Pinjaman</b></td>
            <td>Jasa Taksir Mitra</td>
            <td>Bunga Pinjaman</td>
            <td>Nilai Admin</td>
            <td><b>Nilai Pencairan</b></td>
            <td>Jumlah Hari</td>
            <td>OVD</td>
            <td>Bunga Harian</td>
            <td>Bunga Terhutang</td>
            <td>Jasa Mitra Harian</td>
            <td>Jasa Mitra Terhutang</td>
            <td>Admin Pelunasan</td>
            <td>Denda Harian</td>
            <td>Denda Terhutang</td>
            <td><b>Total Hutang</b></td>
            <th>Status Aplikasi Danain</th>
            <th>Status Aplikasi Gadai MAS</th>
            <th>Status Aplikasi Konsol</th>
        </tr>
        @if($data)
            @php($nomor=1)
            @php($totalPinjaman=0)
            @php($totalPencairan=0)
            @php($totalHutang=0)
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>'{{ $dita->noPerjanjian }}</td>
                <td>{{ $dita->email }}</td>
                <td>{{ \helpers::sembilanAngka($dita->idBorrower) }}</td>
                <td>'{{ $dita->ktp }}</td>
                <td>{{ $dita->namaBorrower }}</td>
                <td>
                    @if($dita->jenisKelamin == 1)
                        Laki-Laki
                    @elseif($dita->jenisKelamin == 2)
                        Perempuan
                    @else
                        -
                    @endif
                </td>
                <td>{{ $dita->tanggalLahir }}</td>
                <td>
                    @php($tglLahir = date_create($dita->tanggalLahir))
                    @php($tglToday = date_create($tanggal))
                    @php($diff  = date_diff($tglLahir, $tglToday))
                    @php($usiaPeminjam = $diff->y)
                    {{ $usiaPeminjam }}
                </td>
                <td>{{ $dita->namaPekerjaan }}</td>
                <td>{{ $dita->penghasilanPerBulan }}</td>
                <td>{{ $dita->biayaHidupPerBulan }}</td>
                <td>{{ $dita->tujuanPinjaman }}</td>
                <td>{{ $dita->tanggal }}</td>
                <td>{{ $dita->tglJt }}</td>
                <td>{{ $dita->namaCompany }}</td>
                <td>{{ $dita->kodeBranch }}</td>
                <td>{{ $dita->namaBranch }}</td>
                <td>{{ $dita->namaProduk }}</td>
                <td>{{ $dita->namaJaminan }}</td>
                <td>{{ $dita->jumlahJaminan }}</td>
                <td>{{ $dita->karat }}</td>
                <td>{{ $dita->avgKarat }}</td>
                <td>{{ $dita->gram }}</td>
                <td>{{ $dita->sumGram }}</td>
                <td>{{ $dita->nilaiStle }}</td>
                <td>{{ $dita->nilaiLtv }}%</td>
                <td>{{ $dita->rateJasaTaksir }}%</td>
                <td>{{ $dita->ratePendana }}%</td>
                <td>{{ $dita->rateFeeDanain }}%</td>
                <td>{{ $dita->ratePencairan }}%</td>
                <td>{{ $dita->nilaiTaksiran }}</td>
                <td>{{ $dita->nilaiMax }}</td>
                <td><b>{{ $dita->nilaiPinjaman }}</b></td>
                <td>{{ $dita->jasaTaksir }}</td>
                <td>{{ $dita->bungaPinjaman }}</td>
                <td>{{ $dita->adminPencairan }}</td>
                <td><b>{{ $dita->nilaiPencairan }}</b></td>
                <td>
                    @php($tglPencairan = date_create($dita->tanggal))
                    @php($tglToday = date_create($tanggal))
                    @php($diffJumlahHari  = date_diff($tglPencairan, $tglToday))
                    @php($jumlahHari = $diffJumlahHari->days + 1)
                    @php($bungaTerhutang = ceil($dita->bungaHarian * $jumlahHari))
                    @php($jasaMitraTerhutang = ceil($dita->jasaMitraHarian * $jumlahHari))
                    @if($jumlahHari > $dita->tenor)
                        @php($dendaTerhutang = ceil($dita->dendaHarian * ($jumlahHari - $dita->tenor)))
                    @else
                        @php($dendaTerhutang = 0)
                    @endif
                    @php($totalHutang = $dita->nilaiPencairan + $bungaTerhutang + $jasaMitraTerhutang + $dendaTerhutang)
                    {{ $jumlahHari }}
                </td>
                <td>
                    @php($tglJatuhTempo = date_create($dita->tglJt))
                    @php($tglToday = date_create($tanggal))
                    @php($diffJumlahOvd  = date_diff($tglJatuhTempo, $tglToday))
                    @php($jumlahOvd = $diffJumlahOvd->days)
                    {{ $dita->tglJt >= $tanggal ? $jumlahOvd*-1 : $jumlahOvd }}
                </td>
                <td>{{ $dita->bungaHarian }}</td>
                <!-- <td>{{-- $dita->bungaTerhutang --}}</td> -->
                <td>{{ $bungaTerhutang }}</td>
                <td>{{ $dita->jasaMitraHarian }}</td>
                <!-- <td>{{-- $dita->jasaMitraTerhutang --}}</td> -->
                <td>{{ $jasaMitraTerhutang }}</td>
                <td>{{ $dita->adminPelunasan }}</td>
                <td>{{ $dita->dendaHarian }}</td>
                <!-- <td>{{-- $dita->dendaTerhutang --}}</td> -->
                <td>{{ $dendaTerhutang }}</td>
                <!-- <td><b>{{-- $dita->totalHutang --}}</b></td> -->
                <td><b>{{ $totalHutang }}</b></td>
                <td>{{ $dita->statusAplikasi }}</td>
                <td>{{ $dita->statusAplikasiMeta }}</td>
                <td>{{ $dita->statusAplikasiKonsol }}</td>
            </tr>
            @php($totalPinjaman+=$dita->nilaiPinjaman)
            @php($totalPencairan+=$dita->nilaiPencairan)
            @php($totalHutang+=$dita->totalHutang)
            @endforeach
            <tr role="row">
                <td colspan="33">Total Outstanding Per Tanggal {{ $tanggal }}</td>
                <td><b>{{ $totalPinjaman }}</b></td>
                <td colspan="3">&nbsp;</td>
                <td><b>{{ $totalPencairan }}</b></td>
                <td colspan="9">&nbsp;</td>
                <td><b>{{ $totalHutang }}</b></td>
                <td>&nbsp;</td>
            </tr>
        @endif
    </tbody>
</table>
