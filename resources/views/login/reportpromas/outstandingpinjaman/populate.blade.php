<div class="table-responsive">
    <div class="col-md-12">
        <span data-href="{{ route('outstandingpinjamanexcel') }}?tanggal={{ $tanggal }}&nama_company={{ $nama_company }}&nama_branch={{ $nama_branch }}&rate_pendana={{ $rate_pendana }}&lender={{ $lender }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span>
    </div>
    <p>&nbsp;</p>
    <h5>Total Outstanding : {{ number_format($total) }}</h5>
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>No PP</th>
                <th>Email</th>
                <th>CIF</th>
                <th>Nama Peminjam</th>
                <th>Produk</th>
                <th>Pinjaman</th>
                <th>Diterima</th>
                <th>Tanggal Cair</th>
                <th>Tanggal JT</th>
                <th>Status</th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->noPerjanjian }}</td>
                <td>{{ $dita->email }}</td>
                <td>{{ \helpers::sembilanAngka($dita->idBorrower) }}</td>
                <td>{{ $dita->namaBorrower }}</td>
                <td>{{ $dita->namaProduk }}</td>
                <td align='right'>{{ number_format($dita->pokokHutang) }}</td>
                <td align='right'>{{ number_format($dita->nilaiPencairan) }}</td>
                <td>{{ $dita->tanggal }}</td>
                <td>{{ $dita->tglJt }}</td>
                <td>{{ $dita->statusAplikasi }}</td>
                <td align='center'><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idJaminan }}', '{{ route('login.outstandingpinjamanmodal') }}')"><span class="fa fa-fw fa-info-circle"></span></a></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
