<div class="table-responsive">
    <div class="col-md-12">
        {{-- {{ dd($data)->toSql() }} --}}
        {{-- <span data-href="{{ route('jaminankonvensionalexcel') }}?tanggal={{ $tanggal }}&nama_wilayah={{ $nama_wilayah }}&nama_cabang={{ $nama_cabang }}&nama_unit={{ $nama_unit }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span> --}}
    </div>
    <p>&nbsp;</p>
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>File</th>
                {{-- <th>No Jaminan</th> --}}
                {{-- <th>No SBG</th> --}}
                {{-- <th><center>Action</center></th> --}}
            </tr>
        </thead>
        <tbody>
            
        @if($data)
        {{-- {{ dd($data) }} --}}
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
                    <tr>
                        <td>{{ $nomor++ }}</td>
                        <td>
                            <a href="{{ route('pelunasankonvensionaltxt', ['id' => $dita->idReportHarian]) }}" class="btn btn-primary btn-sm">Download File</a>
                        </td>
                        {{-- <td>{{ $incrementValue }}</td> --}}
                        {{-- <td align='right'>{{ number_format($dita->pokokHutang) }}</td>
                        <td align='right'>{{ number_format($dita->nilaiPencairan) }}</td>
                        <td>{{ $dita->tanggal }}</td>
                        <td>{{ $dita->tglJt }}</td>
                        <td>{{ $dita->statusAplikasi }}</td>
                        <td align='center'><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idJaminan }}', '{{ route('login.jaminankonvensionalmodal') }}')"><span class="fa fa-fw fa-info-circle"></span></a></td> --}}
                    </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    // function exportTasks(_this) {
    //     let _url = $(_this).data('href');
    //     window.location.href = _url;
    // }
</script>
