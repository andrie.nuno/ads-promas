<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>File</th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
        {{-- {{ dd($data) }} --}}
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->tanggal }}</td>
                <td>
                    <a href="{{ route('azkonvensionaltxt', ['id' => $dita->idReportHarian]) }}" class="btn btn-primary btn-sm">Download File</a>
                </td>
                {{-- <td>{{ $dita->noSbg }}</td>
                <td align='right'>{{ number_format($dita->pokokHutang) }}</td>
                <td align='right'>{{ number_format($dita->nilaiPencairan) }}</td>
                <td>{{ $dita->tglJt }}</td>
                <td>{{ $dita->statusAplikasi }}</td>
                <td align='center'><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idJaminan }}', '{{ route('login.azkonvensionalmodal') }}')"><span class="fa fa-fw fa-info-circle"></span></a></td> --}}
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
