@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">Mutasi Antar Unit</h1>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.mutasiantarunitpopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggal">Tanggal</label>
                                                    <div class="col-sm-5 input-group">
                                                        <input type="date" class="form-control" id="tgl_awal" name="tgl_awal" value="{{ date('Y-m-d') }}" autocomplete="off">
                                                        <label class="col-form-label"> S/D </label>
                                                        <input type="date" class="form-control" id="tgl_sampai" name="tgl_sampai" value="{{ date('Y-m-d') }}" autocomplete="off">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
   $(document).ready(function(e) {
    // Set default values for the date inputs
    var today = new Date();
    var currentMonth = today.getMonth() + 1; // January is 0, so add 1
    var firstDayOfCurrentMonth = new Date(today.getFullYear(), currentMonth - 1, 1);
    var lastDayOfCurrentMonth = new Date(today.getFullYear(), currentMonth, 0);

    $('#tgl_awal').val(firstDayOfCurrentMonth.toLocaleDateString('en-CA'));
    $('#tgl_sampai').val(lastDayOfCurrentMonth.toLocaleDateString('en-CA'));
    $('#tgl_sampai').attr('min', firstDayOfCurrentMonth.toLocaleDateString('en-CA'));

    // Update the max attribute of the second date input based on the selected date from the first input
    $('#tgl_awal').change(function() {
        var selectedDate = new Date($(this).val());
        var lastDayOfSelectedMonth = new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 0);

        $('#tgl_sampai').attr('min', selectedDate.toLocaleDateString('en-CA'));
        $('#tgl_sampai').attr('max', lastDayOfSelectedMonth.toLocaleDateString('en-CA'));
        $('#tgl_sampai').val(lastDayOfSelectedMonth.toLocaleDateString('en-CA'));
    });

    setTimeout(function() {
        doSearch('Populate');
    }, 500);
});

 </script>
@endsection
