<div class="table-responsive">

    <div class="col-md-12">
        <span data-href="{{ route('mutasiantarunitexcel') }}?tgl_awal={{ $tgl_awal }}&tgl_sampai={{ $tgl_sampai }}" id="export" class="btn btn-primary btn-sm theme-button-colour float-right" onclick="exportTasks(event.target);"><i class="fa fa-file-excel"></i> Export Excel</span>
    </div>

    <p>&nbsp;</p>
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Jenis Transaksi</th>
                <th>Nama Wilayah</th>
                <th>Kode Cabang Pengirim</th>
                <th>Nama Cabang Pengirim</th>
                <th>Bank Keluar</th>
                <th>Kode Cabang Penerima</th>
                <th>Nama Cabang Penerima</th>
                <th>Bank Masuk</th>
                <th>No Voucher</th>
                <th>Nominal Transaksi</th>
                <th>Keterangan Transaksi</th>
                <th>NPK Pengirim</th>
                <th>Nama Pengirim</th>
                <th>Status Approval</th>
                <th>NPK Penerima</th>
                <th>Nama Penerima</th>
                <th>Date Approval</th>
                <th>Keterangan Approval</th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->tanggal }}</td>
                <td>{{ $dita->jenisTransaksi }}</td>
                <td>{{ $dita->namaWilayah }}</td>
                <td>{{ $dita->kodeCabangPengirim }}</td>
                <td>{{ $dita->namaCabangPengirim }}</td>
                <td>{{ $dita->bankKeluar }}</td>
                <td>{{ $dita->kodeCabangPenerima }}</td>
                <td>{{ $dita->namaCabangPenerima }}</td>
                <td>{{ $dita->bankMasuk }}</td>
                <td>{{ $dita->noVoucher }}</td>
                <td>{{ $dita->nominalTransaksi }}</td>
                <td>{{ $dita->keteranganTransaksi }}</td>
                <td>{{ $dita->npkPengirim }}</td>
                <td>{{ $dita->namaPengirim }}</td>
                <td>{{ $dita->statusApproval }}</td>
                <td>{{ $dita->npkPenerima }}</td>
                <td>{{ $dita->namaPenerima }}</td>
                <td>{{ $dita->dateApproval }}</td>
                <td>{{ $dita->keteranganApproval }}</td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
