<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Master Coa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.mastercoapost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coa">COA *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coa" name="coa" value="{{ $data->coa }}" autocomplete="off" required>
                    <input type="hidden" name="idCoa" id="idCoa" value="{{ $data->idCoa }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idUsedFor">ID Used For *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="idUsedFor" name="idUsedFor" value="{{ $data->idUsedFor }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="headAccount">Head Account *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="headAccount" name="headAccount" value="{{ $data->headAccount }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="description">Deskripsi *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="description" name="description" value="{{ $data->description }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="additionalDesc">Deskripsi Tambahan *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="additionalDesc" name="additionalDesc" value="{{ $data->additionalDesc }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="transactionType">Transaction Type *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="transactionType" name="transactionType">
                        <option value="D" {{ $data->transactionType == "D" ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="C" {{ $data->transactionType == "C" ? 'selected="selected"' : '' }}>TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="pettyCash">Petty Cash *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="pettyCash" name="pettyCash" value="{{ $data->pettyCash }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="rekonBank">Rekon Bank *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="rekonBank" name="rekonBank" value="{{ $data->rekonBank }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="paymentRequest">Payment Request *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="paymentRequest" name="paymentRequest" value="{{ $data->paymentRequest }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coaOracle">COA Oracle *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coaOracle" name="coaOracle" value="{{ $data->coaOracle }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="accountType">Account Type *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="accountType" name="accountType" value="{{ $data->accountType }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="qualifiers">Qualifiers *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="qualifiers" name="qualifiers" value="{{ $data->qualifiers }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="beginBalance">Begin Balance *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="beginBalance" name="beginBalance" value="{{ $data->beginBalance }}" autocomplete="off" required>
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="deskripsiImage">Image Deskripsi</label>
                <div class="col-sm-9">
                    <input type="file" class="form-control-file" id="deskripsiImage" name="deskripsiImage" value="" autocomplete="off" required>
                </div>
            </div> --}}
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActiveSyariah">Status Syariah*</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActiveSyariah" name="isActiveSyariah">
                        <option value="1" {{ $data->isActiveSyariah == 1 ? 'selected="selected"' : '' }}>YA</option>
                        <option value="0" {{ $data->isActiveSyariah == 0 ? 'selected="selected"' : '' }}>TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isDefault">Status Default*</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isDefault" name="isDefault">
                        <option value="1" {{ $data->isDefault == 1 ? 'selected="selected"' : '' }}>YA</option>
                        <option value="0" {{ $data->isDefault == 0 ? 'selected="selected"' : '' }}>TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idBank">Bank *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idBank" name="idBank">
                        <option value="">== Pilih Bank ==</option>
                        @foreach($banks as $bank)
                        @if($bank->idbank != 0)
                        <option value="{{ $bank->idbank }}" {{ $data->idBank == $bank->idbank ? "selected":"" }}>{{ $bank->namaBank }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isBuffer">Status Buffer*</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isBuffer" name="isBuffer">
                        <option value="1" {{ $data->isBuffer == 1 ? 'selected="selected"' : '' }}>YA</option>
                        <option value="0" {{ $data->isBuffer == 0 ? 'selected="selected"' : '' }}>TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isKasBesar">Status Kas Besar*</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isKasBesar" name="isKasBesar">
                        <option value="1" {{ $data->isKasBesar == 1 ? 'selected="selected"' : '' }}>YA</option>
                        <option value="0" {{ $data->isKasBesar == 0 ? 'selected="selected"' : '' }}>TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idCashflow">ID Cashflow *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="idCashflow" name="idCashflow" value="{{ $data->idCashflow }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="kasMarketing">Kas Marketing *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="kasMarketing" name="kasMarketing" value="{{ $data->kasMarketing }}" autocomplete="off" required>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
