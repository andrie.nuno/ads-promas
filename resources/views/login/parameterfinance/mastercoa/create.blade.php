<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Tambah Master Coa</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.mastercoapost') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coa">Coa *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coa" name="coa" value="" autocomplete="off" required>
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idUsedFor">Id Used For *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="idUsedFor" name="idUsedFor" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="headAccount">Head Account *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="headAccount" name="headAccount" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="description">Deskripsi *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="description" name="description" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="additionalDesc">Deskripsi Tambahan *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="additionalDesc" name="additionalDesc" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="transactionType">Jenis Transaksi *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="transactionType" name="transactionType">
                        <option value="D">D</option>
                        <option value="C">C</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="pettyCash">Petty Cash *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="pettyCash" name="pettyCash" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="rekonBank">Rekon Bank *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="rekonBank" name="rekonBank" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="paymentRequest">Payment Request *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="paymentRequest" name="paymentRequest" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="coaOracle">Coa Oracle *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="coaOracle" name="coaOracle" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="accountType">Account Type *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="accountType" name="accountType" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="qualifiers">Qualifiers *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="qualifiers" name="qualifiers" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="beginBalance">Begin Balance *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="beginBalance" name="beginBalance" value="" autocomplete="off" required>
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="deskripsiImage">Image Deskripsi</label>
                <div class="col-sm-9">
                    <input type="file" class="form-control-file" id="deskripsiImage" name="deskripsiImage" value="" autocomplete="off" required>
                </div>
            </div> --}}
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1">AKTIF</option>
                        <option value="0">TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActiveSyariah">Status Syariah *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActiveSyariah" name="isActiveSyariah">
                        <option value="1">AKTIF</option>
                        <option value="0">TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isDefault">Default *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isDefault" name="isDefault">
                        <option value="1">YA</option>
                        <option value="0">TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idBank">Bank *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idBank" name="idBank" required>
                        <option value="">== Bank ==</option>
                        @if($banks)
                            @foreach($banks as $bank)
                                @if($bank->idbank != 0)
                                    <option value="{{ $bank->idbank }}">{{ $bank->namaBank }}</option>
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isBuffer">Buffer *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isBuffer" name="isBuffer">
                        <option value="1">YA</option>
                        <option value="0">TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isKasBesar">Kas Besar *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isKasBesar" name="isKasBesar">
                        <option value="1">YA</option>
                        <option value="0">TIDAK</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idCashflow">id Cash Flow *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="idCashflow" name="idCashflow" value="" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="kasMarketing">Kas Marketing *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="kasMarketing" name="kasMarketing" value="" autocomplete="off" required>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Add" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
