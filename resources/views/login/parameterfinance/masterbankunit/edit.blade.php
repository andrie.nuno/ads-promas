<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Master Bank Unit</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.masterbankunitpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idCoa">COA *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idCoa" name="idCoa">
                        <option value="">== Pilih COA ==</option>
                        @foreach($coas as $coa)
                            <option value="{{ $coa->idCoa }}" {{ $data->idCoa == $coa->idCoa ? "selected":"" }}>{{ $coa->coa }}</option>
                        @endforeach
                    </select>
                    <input type="hidden" name="idCoa" id="idCoa" value="{{ $data->idCoa }}" readonly="readonly">
                    <input type="hidden" name="idCabang" id="idCabang" value="{{ $data->idCabang }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idCabang">Cabang *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idCabang" name="idCabang">
                        <option value="">== Pilih Cabang ==</option>
                        @foreach($cabangs as $cabang)
                            <option value="{{ $cabang->idCabang }}" {{ $data->idCabang == $cabang->idCabang ? "selected":"" }}>{{ $cabang->namaCabang }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="idBank">Bank *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="idBank" name="idBank">
                        <option value="">== Pilih Bank ==</option>
                        @foreach($banks as $bank)
                        @if($bank->idbank != 0)
                        <option value="{{ $bank->idbank }}" {{ $data->idBank == $bank->idbank ? "selected":"" }}>{{ $bank->namaBank }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="noRekening">No Rekening *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="noRekening" name="noRekening" value="{{ $data->noRekening }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
