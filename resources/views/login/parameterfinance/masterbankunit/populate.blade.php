<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Jenis Cabang</th>
                <th>Wilayah</th>
                <th>Kode Cabang</th>
                <th>Nama Cabang</th>
                <th><center>Status</center></th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->namaJenisCabang }}</td>
                <td>{{ $dita->namaWilayah }}</td>
                <td>{{ $dita->kodeCabang }}</td>
                <td>{{ $dita->namaCabang }}</td>
                <td><center>{{ $dita->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                <td align='center'><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idCabang }}', '{{ route('login.masterbankunitmodal') }}')"><span class="fa fa-fw fa-info-circle"></span></a></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
