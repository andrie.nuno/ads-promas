<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Edit Master Time Full Fill Harian</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.mastertimefullfillpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaBatch">Nama Produk *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="namaBatch" name="namaBatch" value="{{ $data->namaBatch }}" autocomplete="off" required>
                    <input type="hidden" name="idBatch" id="idBatch" value="{{ $data->idBatch }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="jamMulai">Jam Mulai *</label>
                <div class="col-sm-9">
                    <input maxlength="5" type="text" class="form-control" id="jamMulai" name="jamMulai" value="{{ $data->jamMulai }}" autocomplete="off" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="jamSelesai">Jam Selesai *</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="jamSelesai" name="jamSelesai" value="{{ $data->jamSelesai }}" autocomplete="off" required>
                </div>
            </div>
            {{-- <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="deskripsiImage">Image Deskripsi</label>
                <div class="col-sm-9">
                    <input type="file" class="form-control-file" id="deskripsiImage" name="deskripsiImage" value="" autocomplete="off" required>
                </div>
            </div> --}}
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="isActive">Status *</label>
                <div class="col-sm-9">
                    <select class="form-control" id="isActive" name="isActive">
                        <option value="1" {{ $data->isActive == 1 ? 'selected="selected"' : '' }}>AKTIF</option>
                        <option value="0" {{ $data->isActive == 0 ? 'selected="selected"' : '' }}>TIDAK AKTIF</option>
                    </select>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg"); // large
    // sizeMultiModal(1, "xl"); // extra large
    // sizeMultiModal(1, "sm"); // small
	centerMultiModal(1);
});
</script>
