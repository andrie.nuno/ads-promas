<div class="table-responsive">
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>User</th>
                <th>Level</th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->username }} - {{ $dita->namaKaryawan }}</td>
                <td>{{ $dita->namaLevel }}</td>
                <td>
                    <center>
                        <a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idUser }}', '{{ route('login.mitrausermodal') }}')"><span class="fa fa-fw fa-edit"></span></a> 
                        @if($dita->isActive == 0)
                            <a onclick="showMultiModal(1, 'delete', '{{ csrf_token() }}', '{{ $dita->idUser }}', '{{ route('login.mitrausermodal') }}')"><span class="fa fa-fw fa-trash"></span></a>
                        @endif
                    </center>
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
