<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>No SBG</th>
                <th>Nama Cabang</th>
                <th>Bank JF</th>
                <th>Status</th>
                <th>Pinjaman</th>
                <th>Nilai AP</th>
                @if(Auth::user()->idLevel == 1)
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td nowrap>{{ $nomor++ }}</td>
                <td nowrap>{{ $dita->tanggalPencairan }}</td>
                @if($dita->isJurnal == 0)
                    <td nowrap>{{ $dita->noSbg }}</td>
                @else
                    <td nowrap><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.pencairangadaijfmodal') }}')"><span style="color:blue">{{ $dita->noSbg }}</span></a></td>
                @endif
                <td nowrap>{{ $dita->kodeCabang }} - {{ $dita->namaCabang }}</td>
                <td nowrap>{{ $dita->namaJf }}</td>
                <td nowrap>{{ $dita->isApprove == 1 ? 'Approve' : ($dita->isApprove == 2 ? 'Reject' : '') }}
                    @if($dita->isApprove == 1)
                        @php('Approve')
                    @elseif($dita->isApprove == 2)
                        @php('Reject')
                    @else
                        @php('')
                    @endif
                </td>
                <td align="right" nowrap>{{ number_format($dita->pengajuanPinjaman) }}</td>
                <td align="right" nowrap>{{ number_format($dita->nilaiApCustomer) }}</td>
                <td>
                @if(Auth::user()->idLevel == 1)
                    @if($dita->isJurnal == 0)
                        <center><a onclick="showMultiModal(1, 'jurnal', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.pencairangadaijfmodal') }}')"><span style="color:blue">PROSES</span></a></center>
                    @else
                        @if($dita->isApprove != 0)
                            @if($dita->jurnalApproval == 0)
                                <center><a onclick="showMultiModal(1, 'jurnalapprove', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.pencairangadaijfmodal') }}')"><span style="color:blue">APPROVAL</span></a></center>
                            @else
                                @if($dita->idTransJf != '')
                                    @if($dita->jurnalVerifikasi == 0)
                                        <center><a onclick="showMultiModal(1, 'jurnalverifikasi', '{{ csrf_token() }}', '{{ $dita->idTransJf }}', '{{ route('login.pencairangadaijfmodal') }}')"><span style="color:blue">VERIFIKASI</span></a></center>
                                    @endif
                                @endif
                            @endif
                        @endif
                    @endif

                    @if($dita->idGadaiJf == '' && $dita->isJurnalJfGantung == 0)
                        <center><a onclick="showMultiModal(1, 'jurnalrejectgantung', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.pencairangadaijfmodal') }}')"><span style="color:blue">APPROVAL GANTUNG</span></a></center>
                    @endif
                @endif
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });

    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
