<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Coa Promas</th>
                <th>Coa Oracle</th>
                <th>Deskripsi</th>
                <th>Head Account</th>
                <th>Used For</th>
                <th>Cash Flow</th>
                <th>Account Type</th>
                <th>Qualifiers</th>
                <th>Jenis COA</th>
                <th>Rekon Bank</th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
        @if($data)
        {{-- @dd($data) --}}
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td>{{ $nomor++ }}</td>
                <td>{{ $dita->coa }}</td>
                <td>{{ $dita->coaOracle }}</td>
                <td>{{ $dita->description }}</td>
                <td>{{ $dita->headAccount }}</td>
                <td>{{ $dita->usedFor }}</td>
                <td>{{ $dita->cashFlow }}</td>
                <td>{{ $dita->accountType }}</td>
                <td>{{ $dita->qualifiers }}</td>
                <td>{{ $dita->jenisCoa }}</td>
                <td>{{ $dita->rekonBank }}</td>
                <td><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', '{{ $dita->idCoa }}', '{{ route('login.menucoamodal') }}')"><span class="fa fa-fw fa-edit"></span></a></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }
</script>
