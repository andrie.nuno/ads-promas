@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container">
            <div class="row mb-2">
                <div class="col-sm-6">
                <h1 class="m-0 text-dark">GL</h1>
            </div>
        </div>
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.glpopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="idWilayah">Wilayah</label>
                                                    <div class="col-sm-3">
                                                        <select class="form-control" id="idWilayah" name="idWilayah">
                                                            @foreach($wilayahs as $wilayah)
                                                                <option value="{{ $wilayah->idWilayah }}">{{ $wilayah->namaWilayah }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-1 col-form-label" for="idOutlet">Outlet</label>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" id="idOutlet1" name="idOutlet1">
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" id="idOutlet2" name="idOutlet2">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggal">Tanggal</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" name="tanggal" id="tanggal"/>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-1 col-form-label" for="idCoa">COA</label>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" id="idCoa1" name="idCoa1">
                                                            @foreach($coas as $coa)
                                                                <option value="{{ $coa->coaOracle }}">{{ $coa->coaOracle }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <select class="form-control" id="idCoa2" name="idCoa2">
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggal1">Tanggal 1</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggal1" name="tanggal1">
                                                    </div>
                                                    <label class="col-sm-1 col-form-label"></label>
                                                    <label class="col-sm-2 col-form-label" for="tanggal2">Tanggal 2</label>
                                                    <div class="col-sm-3">
                                                        <input type="date" class="form-control" id="tanggal2" name="tanggal2">
                                                    </div>
                                                </div>
                                            </div> -->
                                            <!-- <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="tanggal">Tanggal</label>
                                                    <div class="col-sm-3">
                                                        <input type="text" class="form-control" name="tanggal" id="tanggal"/>
                                                    </div>
                                                </div>
                                            </div> -->
                                            <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="limit" id="limit" value="">
                                            <input type="hidden" name="page" id="page" value="">
                                            <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/moment/moment.min.js')}}"></script>
<script src="{{ url('assets/plugins/daterangepicker/daterangepicker.js')}}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        setTimeout(function(){
            doSearch('Populate');
        }, 500);

        $('input[name="tanggal"]').daterangepicker({
            opens: 'left',
            locale: {
                format: 'YYYY-MM-DD'
            }
        }, function(start, end, label) {
            console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });

        var idWilayah = $('select[name="idWilayah"]').val();
        $("#idOutlet1").html('');
        $("#idOutlet2").html('');
        // alert(idWilayah);
        $.ajax({
            type: "POST",
            url: "glpopulateajax?type=outlet1&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah
            },
            success: function (result) {
                $return = result;
                $('#idOutlet1').append($return);
                $('#idOutlet2').append($return);
            }
        });

        var idCoa1 = $('select[name="idCoa1"]').val();
        $("#idCoa2").html('');
        // alert(idCoa1);
        $.ajax({
            type: "POST",
            url: "glpopulateajax?type=coa&_token={{ csrf_token() }}",
            data: {
                idCoa1: idCoa1
            },
            success: function (result) {
                $return = result;
                $('#idCoa2').append($return);
            }
        });
    });

    $("#idWilayah").change(function() {
        var idWilayah = $('select[name="idWilayah"]').val();
        $("#idOutlet1").html('');
        $("#idOutlet2").html('');
        // alert(idWilayah);
        $.ajax({
            type: "POST",
            url: "glpopulateajax?type=outlet1&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah
            },
            success: function (result) {
                $return = result;
                $('#idOutlet1').append($return);
                $('#idOutlet2').append($return);
            }
        });
    });

    $("#idOutlet1").change(function() {
        var idWilayah = $('select[name="idWilayah"]').val();
        var idOutlet1 = $('select[name="idOutlet1"]').val();
        $("#idOutlet2").html('');
        // alert(idOutlet1);
        $.ajax({
            type: "POST",
            url: "glpopulateajax?type=outlet2&_token={{ csrf_token() }}",
            data: {
                idWilayah: idWilayah,
                idOutlet1: idOutlet1
            },
            success: function (result) {
                $return = result;
                $('#idOutlet2').append($return);
            }
        });
    });

    $("#idCoa1").change(function() {
        var idCoa1 = $('select[name="idCoa1"]').val();
        $("#idCoa2").html('');
        // alert(idCoa1);
        $.ajax({
            type: "POST",
            url: "glpopulateajax?type=coa&_token={{ csrf_token() }}",
            data: {
                idCoa1: idCoa1
            },
            success: function (result) {
                $return = result;
                $('#idCoa2').append($return);
            }
        });
    });
</script>
@endsection