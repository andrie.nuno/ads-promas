<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>Company</th>
                <th>{{ !$wilayah ? "" : $wilayah->namaWilayah }}</th>
            </tr>
            <tr>
                <th>Outlet From</th>
                <th>{{ !$outlet1 ? "" : $outlet1->kodeCabang }} {{ !$outlet1 ? "" : $outlet1->namaCabang }}</th>
            </tr>
            <tr>
                <th>Outlet To</th>
                <th>{{ !$outlet2 ? "" : $outlet2->kodeCabang }} {{ !$outlet2 ? "" : $outlet2->namaCabang }}</th>
            </tr>
            <tr>
                <th>Natural Account From</th>
                <th>{{ !$coa1 ? "" : $coa1->coa }} {{ !$coa1 ? "" : "- ".$coa1->coaOracle }} {{ !$coa1 ? "" : "- ".$coa1->description }}</th>
            </tr>
            <tr>
                <th>Natural Account To</th>
                <th>{{ !$coa2 ? "" : $coa2->coa }} {{ !$coa2 ? "" : "- ".$coa2->coaOracle }} {{ !$coa2 ? "" : "- ".$coa2->description }}</th>
            </tr>
            <tr>
                <th>Period</th>
                <th>{{ $periode }}</th>
            </tr>
            <tr>
                <th>Export Data</th>
                <th><a href="{{ route('glpopulateexport') }}?idWilayah={{ !$wilayah ? 0 : $wilayah->idWilayah }}&outlet1={{ !$outlet1 ? 0 : $outlet1->kodeCabang }}&outlet2={{ !$outlet2 ? 0 : $outlet2->kodeCabang }}&coa1={{ !$coa1 ? 0 : $coa1->coaOracle }}&coa2={{ !$coa2 ? 0 : $coa2->coaOracle }}&periode={{ $periode }}" id="export" class="btn btn-primary btn-sm theme-button-colour" target="_blank"></i> Export Data</span></th>
            </tr>
        </thead>
    </table>
</div>
