<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>No SBG</th>
                <th>Tgl Cair</th>
                <th>Tgl Acrue</th>
                <th>Tgl Lunas</th>
                <th>Nama JF</th>
                <th>Jml Hari</th>
                <th>OVD</th>
                <th>Pokok</th>
                <th>Acrue</th>
                <th>Acrue 1</th>
                <th>Acrue 2</th>
                @if(Auth::user()->idLevel == 1)
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td nowrap>{{ $nomor++ }}</td>
                @if($dita->isJurnal == 1)
                    <td nowrap><a onclick="showMultiModal(1, 'detail', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.acruemodal') }}')"><span style="color:blue">{{ $dita->noSbg }}</span></a></td>
                @else
                    <td nowrap>{{ $dita->noSbg }}</td>
                @endif
                <td nowrap>{{ $dita->tanggalPencairan }}</td>
                <td nowrap>{{ $dita->tanggalClosing }}</td>
                <td nowrap>{{ $dita->tanggalPelunasan }}</td>
                <td nowrap>{{ $dita->namaJf == '' ? 'Reguler' : $dita->namaJf }}</td>
                <td align="right" nowrap>{{ $dita->jumlahHari }}</td>
                <td align="right" nowrap>{{ $dita->ovd }}</td>
                <td align="right" nowrap>{{ number_format($dita->pokokAwal) }}</td>
                <td align="right" nowrap>{{ number_format($dita->selisihAcrue) }}</td>
                <td align="right" nowrap>{{ number_format($dita->selisihAcrueBungaMurni) }}</td>
                <td align="right" nowrap>{{ number_format($dita->selisihAcrueBungaSelisih) }}</td>
                <td>
                @if(Auth::user()->idLevel == 1 && $dita->isJurnal == 0)
                    <center><a onclick="showMultiModal(1, 'jurnal', '{{ csrf_token() }}', '{{ $dita->idGadai }}', '{{ route('login.acruemodal') }}')"><span style="color:blue">PROSES</span></a></center>
                @endif
                </td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });

    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
