<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Modul</th>
                <th><center>Status</center></th>
                <th><center>Akses Menu</center></th>
                <th><center>Action</center></th>
            </tr>
        </thead>
        <tbody>
            @if($data)
                @php($nomor=1)
                @foreach($data as $dita)
                <tr>
                    <td>{{ $nomor++ }}</td>
                    <td>{{ $dita->namaLevel }}</td>
                    <td><center>{{ $dita->isActive == 1 ? 'Active' : 'Disabled' }}</center></td>
                    <td><center><a onclick="showMultiModal(1, 'hakakses', '{{ csrf_token() }}', {{ $dita->idLevel }}, '{{ route('login.modulmodal') }}')"><span class="fa fa-fw fa-list" ></span></a></center></td>
                    <td><center><a onclick="showMultiModal(1, 'edit', '{{ csrf_token() }}', {{ $dita->idLevel }}, '{{ route('login.modulmodal') }}')"><span class="fa fa-fw fa-edit" ></span></a></center></td>
                </tr>
                @endforeach
            @endif
        </tbody>
    </table>
</div>
