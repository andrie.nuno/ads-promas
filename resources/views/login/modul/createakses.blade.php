<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Hak Akses Menu</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.modulpostakses') }}" id="form-Add" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaLevel">Nama Modul</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="namaLevel" name="namaLevel" value="{{ $modul->namaLevel }}" readonly="readonly" required>
                    <input type="hidden" id="idLevel" name="idLevel" value="{{ $modul->idLevel }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Add" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="namaLevel">Hak Akses Menu</label>
                <div class="col-sm-9">
                    @if($menus)
                        @foreach($menus as $dita)
                            <input type="checkbox" name="menuId[]" value="{{ $dita->menuId }}" {{ $dita->menuId == $dita->akses ? 'checked="checked"' : '' }}> 
                            <b>{{ $dita->menuNama }}</b><br>
                            @if($dita->sub)
                                @foreach($dita->sub as $sub)
                                    &nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="menuId[]" value="{{ $sub->menuId }}" {{ $sub->menuId == $sub->akses ? 'checked="checked"' : '' }}> 
                                    {{ $sub->menuNama }}<br>
                                @endforeach
                            @endif                          
                        @endforeach
                    @endif
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Add')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(1)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(1, "lg");
	centerMultiModal(1);
});
</script>
