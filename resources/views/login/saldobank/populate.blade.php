<div class="table-responsive">
    <p>&nbsp;</p>
    <!-- <table class="table table-striped table-bordered table-hover"> -->
    <table class="table table-striped table-bordered table-hover" width="100%" cellspacing="0" style="font-size: small">
        <thead>
            <tr>
                <th>No</th>
                <th>Jenis Cabang</th>
                <th>Wilayah</th>
                <th>Kode Cabang</th>
                <th>Nama Cabang</th>
                @if(Auth::user()->idLevel == 1)
                    <th>Action</th>
                @endif
            </tr>
        </thead>
        <tbody>
        @if($data)
            @php($nomor=\helpers::get_no($limit, $page))
            @foreach($data as $dita)
            <tr>
                <td nowrap>{{ $nomor++ }}</td>
                <td nowrap>{{ $dita->namaJenisCabang }}</td>
                <td nowrap>{{ $dita->namaWilayah }}</td>
                <td nowrap>{{ $dita->kodeCabang }}</td>
                <td nowrap>{{ $dita->namaCabang }}</td>
                <td nowrap><a onclick="showMultiModal(1, 'bank', '{{ csrf_token() }}', '{{ $dita->idCabang }}', '{{ route('login.saldobankmodal') }}')"><span class="btn btn-primary btn-sm">Saldo Bank</span></a></td>
            </tr>
            @endforeach
        @endif
        </tbody>
    </table>
</div>
{{ \helpers::pagination($data->total(), $limit, $page) }}

<script type="text/javascript">
    var tooltipTriggerList = [].slice.call(document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (tooltipTriggerEl) {
        return new bootstrap.Tooltip(tooltipTriggerEl)
    });

    $("#setLimit").change(function() {
		var limit = $('select[name="setLimit"]').val();
        document.getElementById("limit").value = limit;
        doSearch('Populate');
	});

    function setPage(page) {
        document.getElementById("page").value = page;
        doSearch('Populate');
    }

    function exportTasks(_this) {
        let _url = $(_this).data('href');
        window.location.href = _url;
    }
</script>
