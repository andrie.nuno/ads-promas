@extends('layouts.app')

@section('content')
    <!-- Main content -->
    <div class="content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-search" id="panel-searchPopulate">
                                        <form action="{{ route('login.saldobankpopulate') }}" class="row" id="form-searchPopulate">
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label" for="idJenisCabang">Jenis Cabang</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="idJenisCabang" name="idJenisCabang">
                                                        <option value="">== Jenis Cabang ==</option>
                                                        @foreach($jeniscabang as $dita)
                                                            <option value="{{ $dita->idJenisCabang }}">{{ $dita->namaJenisCabang }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                    <label class="col-sm-1 col-form-label">&nbsp;</label>
                                                    <label class="col-sm-2 col-form-label" for="idWilayah">Nama Wilayah</label>
                                                    <div class="col-sm-3">
                                                    <select class="form-control" id="idWilayah" name="idWilayah">
                                                        <option value="">== Nama Wilayah ==</option>
                                                        @foreach($wilayah as $dita)
                                                            <option value="{{ $dita->idWilayah }}">{{ $dita->namaWilayah }}</option>
                                                        @endforeach
                                                    </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group row">
                                                    <label class="col-sm-3 col-form-label" for="search">Nama HO / Cabang / Unit</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" class="form-control" id="search" name="search" value="" autocomplete="off"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-10">
                                                <div class="form-group row">
                                                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}">
                                                    <input type="hidden" name="limit" id="limit" value="">
                                                    <input type="hidden" name="page" id="page" value="">
                                                    <a onclick="doSearch('Populate')" ><button type="button" class="btn btn-sm btn-primary theme-button-colour"><i class="fa fa-search"></i> Search</button></a>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div><!-- ./card-header -->
                        <div class="card-body">
                            <div class="panel-data" id="panel-dataPopulate"></div>
                        </div><!-- ./card-body -->
                    </div>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content -->
@endsection

@section('js')
<script src="{{ url('assets/jQuery-2.1.3.min.js') }}"></script>
<script src="{{ url('assets/plugins/bootstrap2/bootstrap.min.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function(e){
        var idCompany = $('select[name="nama_company"]').val();
        $("#nama_branch").html('');
        // alert(idCompany);
        $.ajax({
            type: "POST",
            url: "nasabahlunaspopulateajax?type=branch&_token={{ csrf_token() }}",
            data: {
                idCompany: idCompany
            },
            success: function (result) {
                $return = result;
                $('#nama_branch').append($return);
            }
        });

        setTimeout(function(){
            doSearch('Populate');
        }, 500);
    });

    $("#nama_company").change(function() {
        var idCompany = $('select[name="nama_company"]').val();
        $("#nama_branch").html('');
        // alert(idCompany);
        $.ajax({
            type: "POST",
            url: "nasabahlunaspopulateajax?type=branch&_token={{ csrf_token() }}",
            data: {
                idCompany: idCompany
            },
            success: function (result) {
                $return = result;
                $('#nama_branch').append($return);
            }
        });
    });
</script>
@endsection