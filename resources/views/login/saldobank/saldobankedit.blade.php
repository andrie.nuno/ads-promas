<div class="modal-content">
    <div class="modal-header">
        <h4 class="modal-title">Update Saldo Bank</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div id="modal_form" current="1">
    <div class="modal-body">
        <form action="{{ route('login.saldobankpost') }}" id="form-Edit" class="form-horizontal" enctype="multipart/form-data" method="post">
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Cabang</label>
                <div class="col-sm-9">
                    {{ $cabang->kodeCabang }} - {{ $cabang->namaCabang }}
                    <input type="hidden" name="idCabang" id="idCabang" value="{{ $data->idCabang }}" readonly="readonly">
                    <input type="hidden" name="idBank" id="idBank" value="{{ $data->idBank }}" readonly="readonly">
                    <input type="hidden" name="_token" id="_token" value="{{ csrf_token() }}" readonly="readonly">
                    <input type="hidden" name="_action" id="_action" value="Edit" readonly="readonly">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label">Nama Bank</label>
                <div class="col-sm-9">
                    {{ $bank->kd_bank }} - {{ $bank->namaBank }}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="saldoAwal">Saldo Awal *</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="saldoAwal" name="saldoAwal" value="{{ str_replace(',','.',number_format($data->saldoAwal)) }}" autocomplete="off" onkeyup="inputAngka(this);" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="saldoMasuk">Saldo Masuk *</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="saldoMasuk" name="saldoMasuk" value="{{ str_replace(',','.',number_format($data->saldoMasuk)) }}" autocomplete="off" onkeyup="inputAngka(this);" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="saldoKeluar">Saldo Keluar *</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="saldoKeluar" name="saldoKeluar" value="{{ str_replace(',','.',number_format($data->saldoKeluar)) }}" autocomplete="off" onkeyup="inputAngka(this);" required>
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-3 col-form-label" for="saldoAkhir">Saldo Akhir *</label>
                <div class="col-sm-4">
                    <input type="text" class="form-control" id="saldoAkhir" name="saldoAkhir" value="{{ str_replace(',','.',number_format($data->saldoAkhir)) }}" autocomplete="off" onkeyup="inputAngka(this);" required>
                </div>
            </div>
        </form>
    </div>
    </div>
    <div class="modal-footer">
        <button type="submit" id="btn-Edit" class="btn btn-primary" onclick="doSubmit('Edit')"><i class="fa fa-save"></i> Submit</button>
        <button type="button" class="btn btn-secondary" onclick="closeMultiModal(2)"><i class="fa fa-remove"></i> Close</button>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(e){
	sizeMultiModal(2, "lg"); // large
    // sizeMultiModal(2, "xl"); // extra large
    // sizeMultiModal(2, "sm"); // small
	centerMultiModal(2);
});

function inputAngka(objek) {
    a = objek.value;
    b = a.replace(/[^\d]/g,"");
    c = "";
    panjang = b.length;
    j = 0;
    for (i = panjang; i > 0; i--) {
        j = j + 1;
        if (((j % 3) == 1) && (j != 1)) {
            c = b.substr(i-1,1) + "." + c;
        } else {
            c = b.substr(i-1,1) + c;
        }
    }
    objek.value = c;
}
</script>
