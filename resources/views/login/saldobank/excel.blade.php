<table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
    <tbody class="tbody">
        <tr role="row">
            <td colspan="4">Cabang : {{ $dataCabang->kodeCabang }} - {{ $dataCabang->namaCabang }}</td>
        </tr>
        <tr role="row">
            <td colspan="4">Bank : {{ $dataBank->kd_bank }} - {{ $dataBank->namaBank }}</td>
        </tr>
        <tr role="row">
            <td colspan="4">Tanggal : {{ $tanggalAwal }} s/d {{ $tanggalAkhir }}</td>
        </tr>
        <tr role="row">
            <td>Tanggal</td>
            <td>Keterangan</td>
            <td>Referensi</td>
            <td>Debet</td>
            <td>Kredit</td>
            <td>Saldo</td>
        </tr>
        @if($data)
            @php($tanggal = '')
            @php($saldoAkhir = '')
            @foreach($data as $dita)
                @if($tanggal != $dita->tanggalSistem)
                    @if($tanggal != '')
                        <tr>
                            <td>{{ $dita->tanggalSistem }}</td>
                            <td>Saldo Akhir Tanggal {{ $tanggal }}</td>
                            <td></td>
                            <td align='right'></td>
                            <td align='right'></td>
                            <td align='right'>{{ $saldoAkhir }}</td>
                        </tr>
                    @endif
                    <tr>
                        <td>{{ $dita->tanggalSistem }}</td>
                        <td>Saldo Awal Tanggal {{ $dita->tanggalSistem }}</td>
                        <td></td>
                        <td align='right'></td>
                        <td align='right'></td>
                        <td align='right'>{{ $dita->saldoAwal }}</td>
                    </tr>
                @endif
                <tr>
                    <td>{{ $dita->tanggalSistem }}</td>
                    <td>{{ $dita->keterangan }}</td>
                    <td>{{ $dita->kodeTrans }}</td>
                    <td align='right'>{{ $dita->debet }}</td>
                    <td align='right'>{{ $dita->kredit }}</td>
                    <td align='right'>{{ $dita->saldoAkhir }}</td>
                </tr>
                @php($tanggal = $dita->tanggalSistem)
                @php($saldoAkhir = $dita->saldoAkhir)
            @endforeach
        @else
            <tr>
                <td colspan="6">Data Tidak Ada</td>
            </tr>
        @endif
    </tbody>
</table>
