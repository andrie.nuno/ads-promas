<span style="font-size: small">Total Data : {{ $totalData }}</span><br>
<span style="font-size: small">
    Per Halaman : 
    <select id="setLimit" name="setLimit">
        <option value="10" {{ $limit == 10 ? "selected":"" }}>10</option>
    </select>
</span><br>
<nav aria-label="Page navigation example">
@php($total=ceil($totalData/$limit))
@if($total > 1)
    <ul class="pagination justify-content-center">
        <li class="page-item {{ $page == 1 ? "disabled":"" }}">
            <a class="page-link" onclick="setPage(1);" aria-label="Previous">
                First
            </a>
        </li>
        <li class="page-item {{ $page == 1 ? "disabled":"" }}">
            <a class="page-link" onclick="setPage({{ $page-1 }});" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>

        @for($i=($page - 2); $i<$page; $i++)
        @if($i>0)
        <li class="page-item"><a class="page-link" onclick="setPage({{ $i }});">{{ $i }}</a></li>
        @endif
        @endfor
        <li class="page-item disabled"><a class="page-link" onclick="setPage({{ $page }});">{{ $page }}</a></li>
        @for($i=($page + 1); $i<($page + 3); $i++)
        @if($i<=$total)
        <li class="page-item"><a class="page-link" onclick="setPage({{ $i }});">{{ $i }}</a></li>
        @endif
        @endfor
        
        <li class="page-item {{ $page == $total ? "disabled":"" }}">
            <a class="page-link" onclick="setPage({{ $page+1 }});" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
        <li class="page-item {{ $page == $total ? "disabled":"" }}">
            <a class="page-link" onclick="setPage({{ $total }});" aria-label="Previous">
                Last
            </a>
        </li>
    </ul>
@endif
</nav>
