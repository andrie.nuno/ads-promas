<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <div class="sidebar">
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{ Auth::user()->karyawan->namaKaryawan }}</a>
            </div>
        </div>

        <!-- <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
            <li class="nav-item has-treeview menu-open">
                <a href="#" class="nav-link active">
                <i class="nav-icon fas fa-tachometer-alt"></i>
                <p>
                    Starter Pages
                    <i class="right fas fa-angle-left"></i>
                </p>
                </a>
                <ul class="nav nav-treeview">
                <li class="nav-item">
                    <a href="#" class="nav-link active">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Active Page</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                    <p>Inactive Page</p>
                    </a>
                </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="#" class="nav-link">
                <i class="nav-icon fas fa-th"></i>
                <p>
                    Simple Link
                    <span class="right badge badge-danger">New</span>
                </p>
                </a>
            </li>
            </ul>
        </nav> -->

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu">
                @if(helpers::leftMenu(Auth::user()->level->idLevel))
                    @foreach(helpers::leftMenu(Auth::user()->level->idLevel) as $menu)
                    <li class="nav-item has-treeview {{ Request::segment(1) == "$menu->menuSegmen" ? "menu-open":"" }}">
                        <a href="{{ $menu->menuUrl != "#" ? route($menu->menuUrl):$menu->menuUrl }}" class="nav-link {{ Request::segment(1) == "$menu->menuSegmen" ? "active":"" }}">
                        <i class="nav-icon fas fa-th"></i>
                        <span>{{ $menu->menuNama }}</span>
                        </a>
                        @if($menu->sub)
                            @foreach($menu->sub as $sub)
                            <ul class="nav nav-treeview">
                                <li class="nav-item">
                                    <a href="{{ route($sub->menuUrl) }}" class="nav-link {{ Request::segment(2) == "$sub->menuSegmen" ? "active":"" }}">
                                    <i class="far fa-circle nav-icon"></i>
                                    <span>{{ $sub->menuNama }}</span>
                                    </a>
                                </li>
                            </ul>
                            @endforeach
                        @endif
                    </li>
                    @endforeach
                @endif

                
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>